/*
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#define _ISOC9X_SOURCE 1
#define _ISOC99_SOURCE 1
#include <math.h>
#include <mpi.h>
#define N1 (1.0/N)
#define sigma_tube 6.5
#define Rtube 81
#define S_FACTOR 50.0
#define Nseq 100000
#define E_Bin 1.
double HOH_Burried_Threshold=20.0;
double Hydropathy_Scale=1.0;
unsigned long int both_const=0,fseq_accepted_total=0,fseq_rejected_total=0,fseq_accepted_constraint=0,fseq_rejected_constraint=0,pswap_tried=0,pswap_acc=0,pswap_rejected=0,fseq_tried=0,sigma_on=0,ave_on=0,matrix_on=0,perm_on=0,matrix_off=0;
int SIZE=1000, max_perm=1000,sigma_flag=0,perm_flag=0,start_conf=0;
long seed=-1;
long seed2=-1;
long seed3=-1;
long seed4=-1;
unsigned long long int nsamp;
unsigned long long int Equi1,Equi2,Equi3;
double tempHisto=0,ntempswap=1;
float *beta2=NULL,P_sigma=-1, ave=0.;
float beta, beta_sigma, beta_ave;
double LJpower=3.0,ECA_Range=7.0;
double minE_FE,minord_FE,PI=0;
int treshold=10,*minseq=NULL,*minFEseq=NULL,*initseq=NULL;
unsigned long int **minFEseqHisto=NULL;
double minE=1000000.0,umbrella,FE=0.0,minFE=1000000.0;
double 	*MinEner=NULL;
char dir[20];
float **M=NULL,**M2=NULL;
double sigma6,Rmin,sigma_tube6=0, sigma_tube12=0;
FILE *fTemp,*fTempControl,*fMyprot,*fHisto,*fHistoS,*fHistoM,*fHisto_Ord,*out,*fMinFEseq=NULL,*MC_control=NULL;
int N=1000,link=0,SubN=0,ProtN=1000;
int **hsamp=NULL,**Wsamp=NULL,S=3,size=1;
unsigned long long int icycl,ncycl,Niteration,deltacycle;
double HminE=1000000,HmaxE=-10000000,**histo=NULL,**S_histo=NULL,**M_histo=NULL,**Whisto=NULL,**Wpot=NULL,**unbias=NULL,ndataen=-1000000.0,ndata;
double Ref_FE;
double lamda=1.0;
char **Sequences_in_FEmin=NULL;
unsigned long int Nseq_reduced=1;
double *seq_list=NULL,seq_list_ndataen,**Sequences_in_FEmin_orders=NULL;
struct gene{    /// element structure
	int *promotor;
	double *LJ_repulsive,*LJ,*LJ_repulsive2,*LJ2;
	int identity;
	int old_identity;
	double Energy,Energy2; 
	double Water,Water_H,Water_P;
	int link;
};
struct tot{    /// chain structure
	struct gene *genome;
	double Energy,Energy2,NE,Etype,E2type;
	double order;
  double sigma,matrix_ave;
  ///int *p,*np,*types,*ntypes,Nsigma;
  int *p,*np;
};
struct tot *network;
int my_rank,p,err5;
double minW_nobias;
long lround (double);
double  factorial   	    (int Nfact, int iniz);
double 	ran3	(long *idum);
double 	ran35	(long *idum);
double 	ran36	(long *idum);
double 	ran37	(long *idum);
void 	fsamp	 (void);
void 	finit  	 (void);
void 	fseq  	 (void);
void 	Pswap	 (void);
void 	Tswap	 (void);
double energy (int resi, int i);
double energy_SP (int resi, int i);
double energy_SP_pswap(int resi, int i, int resj, int j);
void sampling (double E,double order,double espo);
void O_minFE_Sampling (double E,double order,double espo);
void N_minFE_Sampling (double E,double order,double espo);
void W (void);
void writeBinaryMin(FILE *outFile);
void Order_Conf_Sampling (double E,double order_P);
void fastadecoder2 (int *Dec, char *Enc,int Length);
void sigma_calculator (double *new_sigma, double *new_ave);
int process=1;


int JOBID;
int main(int argc, char** argv){
	
	int i,j,k;
	char message[50],line[50];
	int minSIZE=2000,maxSIZE=-12000,minsize=2000,maxsize=-12000;
	
	
	double *roottempHisto=NULL;
	double starttime=0,endtime=0;
	float PTFreq;
	double bin_x;
	double tot,mean;
	double En=0.0,E2n=0.0,Etest=0.0,E2test=0.0,Etype=0.0,E2type=0.0,TEtype=0.0,TE2type=0.0;
	FILE *fp;
	double counter_histo;
	char processor_name[1024];
	int namelen;
	PI=atan(1)*4;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	MPI_Get_processor_name(processor_name, &namelen);
	
	printf("Start Process %d of %d running on %s\n", my_rank, p,processor_name);fflush(NULL);
	printf("Start ok %d\n",my_rank);
	fflush(NULL);
	
	
	if(my_rank==0){
		starttime=MPI_Wtime(); ///mpi function which determine wall-clock time
	}
	
	#ifdef RUN_ON_SCRATCH	
		fp=fopen("simdir.dat","r");	///file containing the path of the run directory
		fgets(line,sizeof(line),fp);
		if(chdir(line)!=0){
			printf("%d %s does not exists error %d\n",my_rank,line,chdir(line));
			//MPI_Abort(MPI_COMM_WORLD,err5);
		}

		fclose(fp);	
	#endif	

	sprintf(message,"OUT-P-%d.dat",my_rank);
	out=fopen(message,"w");
	sprintf(message,"MC_control-%d.dat",my_rank);
	MC_control=fopen(message,"w");
	fprintf(MC_control,"#Temp icycl perm_on sigma_on ave_on matrix_on matrix_off | pswap_tried pswap_acc pswap_rejected | fseq_tried fseq_accepted_total  fseq_accepted_constraint fseq_rejected_total fseq_rejected_constraint both_const \n ");fflush(MC_control);
	fp=fopen("param.dat","r");
	if ( fp == NULL) {
		printf ("File param.dat not found!!!\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		while(feof(fp)==0){
			fgets(line,sizeof(line),fp);
			switch (line[0]){
				case 'A':
				sscanf (line,"A%lf\n",&Rmin); /// only one radius for particles
				fprintf(out,"Rmin=%lf\n",Rmin);
				break;
				case 'N':
				sscanf (line,"N%llu\n",&ncycl);
				break;
				case 'C':
				sscanf (line,"C%llu\n",&nsamp);
				break;
				case 'B':
				sscanf (line,"B%f\n",&beta);  
				break;
				case 'R':
				sscanf (line,"R%ld\n",&seed);
				process=(int)(seed);
				seed=-seed;
				seed2=seed;
				seed3=seed;
				seed4=seed;
				break;
				case 'I':
				sscanf (line,"I%llu\n",&Niteration);
				break;
				case 'U':
				sscanf (line,"U%lf\n",&umbrella);
				break;
				case 'E':
				switch (line[1]){
					case '1':
					sscanf (line,"E1%llu\n",&Equi1);
					break;
					case '2':
					sscanf (line,"E2%llu\n",&Equi2);
					break;
					case '3':
					sscanf (line,"E3%llu\n",&Equi3);
					break;
					case 'R':
					sscanf (line,"ER%lf\n",&ECA_Range);
					break;
				}
				break;
				case 'T':
				sscanf (line,"T%d\n",&treshold);
				break;
				case 'L':
				sscanf (line,"L%lf\n",&lamda);
				break;
				case 'P':
				sscanf (line,"P%f\n",&PTFreq);
				break;
				case 'S':
				  switch (line[1]){
				  case 'I':				    
				    sscanf (line,"SI%f\n",&P_sigma);
				    break;
				  case 'B':
				    sscanf (line,"SB%f\n",&beta_sigma);
				    break;
				  case 'A':
				    sscanf (line,"SA%f\n",&beta_ave);
				    break;
				  }
				break;
			        case 'M':
			          switch (line[1]){
				  case 'P':
				  sscanf (line,"MP%d\n",&max_perm);
				  break;  
				}
				break;
				case 'D':
				sscanf (line,"D%d\n",&start_conf);
				break;
				default:
				fprintf(out,"Usage:\n");
				fprintf(out,"      N number of cycles \n");
				fprintf(out,"      C samplings step \n");
				fprintf(out,"      B beta \n");
				fprintf(out,"      R ramdom seed positive [1] \n");
				fprintf(out,"      T treshold [0-N] \n");
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
		}
		fclose(fp);
	}
	ncycl=ncycl+Equi1+(Equi2+Equi3)*Niteration;
	fprintf(out,"prima finit ok\n");
	sigma6=pow(Rmin,6.0);
#ifdef SIGMA_ON
	fprintf(out,"SIGMA ON!\n");fflush(out);
#else
	beta_sigma=0.;
	beta_ave=0.;
#endif	
	fprintf(out,"beta_sigma=%f\n",beta_sigma);fflush(out);
	fprintf(out,"beta_ave=%f\n",beta_ave);fflush(out);
	finit();
	
	
	#ifdef TEST		
			for(k=1;k<N;k++){
				Etest+=energy(network->genome[k].identity,k);
				
			}
			if(fabs(Etest-(network->Energy))>1e-5){
				fprintf(out,"INIT %llu Etest failed Etest=%lf En=%lf \n",icycl,Etest,network->Energy);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			
			/*	TEtype=0.0;
			TE2type=0.0;
			for(i=0;i<S;i++){
				for(j=i;j<S;j++){
					TEtype+=network->p[i]*network->p[j]*M[network->types[i]][network->types[j]];
					TE2type+=network->p[i]*network->p[j]*M2[network->types[i]][network->types[j]];
				}
			}
			
			
			if(fabs(TEtype-(network->Etype))>1e-5){
				fprintf(out,"INIT %llu Etype failed TEtype=%lf network->Etype=%lf\n",icycl,TEtype,network->Etype);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			if(fabs(TE2type-(network->E2type))>1e-5){
				fprintf(out,"fseq %llu E2type failed TE2type=%lf network->E2type=%lf\n",icycl,TE2type,network->E2type);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				}*/
			
			#endif
	
	
	fprintf(out,"finit ok\n");
	fflush(out);
	roottempHisto=(double *)malloc(p*sizeof(double));
	if(roottempHisto==NULL){
		fprintf(out,"allocation of roottempHisto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	size=lround(N*log(N));
	fprintf(out,"SIZE=%d size=%d\n",SIZE,size);fflush(out);
	
	MinEner=(double *)calloc(size,sizeof(double));
	for(i=0;i<size;i++){
	  MinEner[i]=100000.0;		  
	}

	histo=(double **)malloc(SIZE*sizeof(double *));
	

	Whisto=(double **)malloc(SIZE*sizeof(double *));
	Wsamp=(int **)malloc(SIZE*sizeof(int *));
	hsamp=(int **)malloc(SIZE*sizeof(int *));
	Wpot=(double **)malloc(SIZE*sizeof(double *));
	unbias=(double **)malloc(SIZE*sizeof(double *));
	
	for(i=0;i<SIZE;i++){
		histo[i]=(double *)malloc(size*sizeof(double));
		Whisto[i]=(double *)malloc(size*sizeof(double));
		Wsamp[i]=(int *)malloc(size*sizeof(int));
		hsamp[i]=(int *)malloc(size*sizeof(int));
		Wpot[i]=(double *)malloc(size*sizeof(double));
		unbias[i]=(double *)malloc(size*sizeof(double));
	}
	
	sprintf(message,"MinFE-T-%2.3f.bin",beta2[my_rank]);
	fMinFEseq=fopen(message,"a");
	fwrite(&N,sizeof(int),1,fMinFEseq);
	
	sprintf(message,"Wpot-T-%2.3f.dat",beta2[my_rank]);
	fp=fopen(message,"r");
	if ( fp == NULL) {
		for(i=0;i<SIZE;i++){
			for(j=0;j<size;j++){		
				Wpot[i][j]=0.0;	
			}	 
		}
	}else{
		
		
		
		fread(&i,sizeof(int),1,fp);
		if(i!=SIZE){
			fprintf(out,"Init Wpot reading on file %s wrong SizeC %d %d\n",message,i,SIZE);
			
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		fread(&i,sizeof(int),1,fp);
		if(i!=size){
			fprintf(out,"Init Wpot reading on file %s wrong sizex %d %d\n",message,i,size);
			
			fflush(out);
			
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		for(i=0;i<SIZE;i++){
			for(j=0;j<size;j++){
				
				
				fread(&bin_x,sizeof(double),1,fp);
				Wpot[i][j]=bin_x;
				
			}
			
		}
		fclose(fp);
	}
	
	for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			Whisto[i][j]=0.0;
			Wsamp[i][j]=0;
			hsamp[i][j]=0;
			histo[i][j]=-1000000.0;
			
			//unbias[i][j]=-Wpot[i][j];   
		}
		
	}
	
	
	
	for(i=0;i<Nseq;i++){
					seq_list[i]=-1000000.0;
				}
				
				seq_list_ndataen=-1000000.0;
	ndata=1.0;
	ndataen=-1000000.0;	
	deltacycle=Equi1;
	
	/*****************/
	/* File Creation */
	/*****************/
	
	sprintf(message, "Temp-BB%2.1f-B%2.1f-Th%d-P%d.dat",beta2[my_rank],beta,treshold,process);
	fTemp=fopen(message,"w");
	
	sprintf(message,"Histogram_Ord-T-%2.1f-%d.dat",beta2[my_rank],process);
	fHisto_Ord=fopen(message,"w");
	
	if(my_rank==0){
	  sprintf(message,"TempControl-B%2.1f-Th%d-P%d.dat",beta,treshold,process);
	  fTempControl=fopen(message,"w");
	  
	  if(fTempControl==NULL){
	    fprintf(out,"fTempControl creation failed\n");fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	fsamp();
	/*fprintf(out,"files ok\n");*/
	for(icycl=1;icycl<=ncycl;icycl++){
		
		if((icycl>Equi1)&&(icycl<Niteration*(Equi2+Equi3)+Equi1)){
			if(icycl==deltacycle+Equi2){
				/*fprintf(out,"init Whisto\n");*/
				for(i=0;i<SIZE;i++){		   
					for(j=0;j<size;j++){	    	
						Whisto[i][j]=0.0;	
					}	   
				}
				ndata=1.0;
				/*fprintf(out,"init Whisto ok\n");*/
			}
			
			if(icycl==deltacycle+Equi3+Equi2){
				deltacycle=icycl;
				fprintf(out,"try W %llu \n",icycl);fflush(out);
				W();
				fprintf(out,"try W ok\n");fflush(out);
			}
		}
			if(icycl==Equi1){
				
				
				sprintf(message,"Histogram_-T-%2.3f-%d-init.dat",beta2[my_rank],process);
				fHisto=fopen(message,"w");
				for(i=0;i<SIZE;i++){
					for(j=0;j<size;j++){
						if(hsamp[i][j]==1){
							if(minSIZE>=i) minSIZE=i;
							if(maxSIZE<=i) maxSIZE=i;
							if(minsize>=j) minsize=j;
							if(maxsize<=j) maxsize=j;
						}
					}
				}
				fprintf(fHisto,"# Dy=%d\n",(maxsize-minsize));
				for(i=minSIZE;i<maxSIZE;i++){
					for(j=minsize;j<maxsize;j++){		    	
						fprintf(fHisto,"%15.10f %5d %15.10f\n",(double)(i-SIZE/2)/E_Bin,j,(-histo[i][j]+ndataen));
						
					}
					fprintf(fHisto,"\n");   
				}
				fclose(fHisto);
				
				sprintf(message,"Histogram_2D_Energy-T-%2.3f-%d-init.dat",beta2[my_rank],process);
				fHisto=fopen(message,"w");
				for(i=minSIZE;i<maxSIZE;i++){
				  counter_histo=0;
				  for(j=minsize;j<maxsize;j++){		    	
				    counter_histo+=(-histo[i][j]+ndataen);
				  }
				  fprintf(fHisto,"%15.10f\t%15.10f\n",(double)(i-SIZE/2)/E_Bin,counter_histo);
				}
				fclose(fHisto);
				
				for(i=0;i<SIZE;i++){		   
					for(j=0;j<size;j++){	    	
						histo[i][j]=-1000000.0;	
						hsamp[i][j]=0;
						
						Wsamp[i][j]=0;		   
					}	   
				}

				ndataen=-1000000.0;		
				for(i=0;i<Nseq;i++){
					seq_list[i]=-1000000.0;
				}
				
				seq_list_ndataen=-1000000.0;
				
				
			}
			if((icycl>Equi1)&&(icycl%nsamp==0)){
				fprintf(out,"try Histo %llu \n",icycl);fflush(out);

				fprintf(MC_control,"%lf %lu %lu %lu %lu %lu %lu | %lu %lu %lu | %lu %lu %lu %lu %lu %lu \n",beta2[my_rank],icycl,perm_on,sigma_on,ave_on,matrix_on,matrix_off,pswap_tried,pswap_acc,pswap_rejected,fseq_tried, fseq_accepted_total,  fseq_accepted_constraint, fseq_rejected_total, fseq_rejected_constraint, both_const);fflush(MC_control);

				
				sprintf(message,"Histogram_-T-%2.3f-%d.dat",beta2[my_rank],process);
				fHisto=fopen(message,"w");

				for(i=0;i<SIZE;i++){
					for(j=0;j<size;j++){
						if(hsamp[i][j]==1){
							if(minSIZE>=i) minSIZE=i;
							if(maxSIZE<=i) maxSIZE=i;
							if(minsize>=j) minsize=j;
							if(maxsize<=j) maxsize=j;
						}
					}
				}
				fprintf(fHisto,"# Dy=%d\n",(maxsize-minsize));
				for(i=minSIZE;i<maxSIZE;i++){
					for(j=minsize;j<maxsize;j++){		    	
					  fprintf(fHisto,"%15.10f %5d %15.10f\n",(double)(i-SIZE/2)/E_Bin,j,(-histo[i][j]+ndataen));
					}
					fprintf(fHisto,"\n"); 
					fflush(fHisto);  
				}
				fclose(fHisto);
				///Order_Conf_Sampling(network->Energy,network->order); // DEBUG
				sprintf(message,"Histogram_2D_Energy-T-%2.3f-%d.dat",beta2[my_rank],process);
				fHisto=fopen(message,"w");
				for(i=minSIZE;i<maxSIZE;i++){
				  counter_histo=0;
				  for(j=minsize;j<maxsize;j++){		    	
				    counter_histo+=(-histo[i][j]+ndataen);
				  }
				  fprintf(fHisto,"%15.10f\t%15.10f\n",(double)(i-SIZE/2)/E_Bin,counter_histo);
				}
				fclose(fHisto);
				
				
				fprintf(out,"try Histo %llu ok \n",icycl);fflush(out);    	
				
			     
			}
		
		
		if(icycl>=nsamp){	    	    		    	    
			if (minE>=network->Energy){    		    
				for(i=0;i<N;i++){
					minseq[i]=network->genome[i].identity;
				}
				minE=network->Energy;
				
				
			}
		}
		
		if (icycl%nsamp==0){
		  fsamp();
		  
		  MPI_Gather(&tempHisto,1,MPI_DOUBLE,roottempHisto,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
		  if(my_rank==0){
		    
		    ///sprintf(message,"TempControl-B%2.1f-Th%d-P%d.dat",beta,treshold,process);
		    ///fTempControl=fopen(message,"w");
		    for(i=0;i<p;i++){
		      fprintf(fTempControl,"%2.4f %2.4f\n",beta2[i],roottempHisto[i]/ntempswap);
		    }
		    fprintf(fTempControl,"#ntempswap=%5.0f\n",ntempswap);
		    ///fclose(fTempControl);
		  }
		  
		}
		/*if (icycl%2000==0) {
		  Order_Conf_Sampling(network->Energy,network->order);
		  }*/
		if(icycl%100==0){
			fclose(out);
			sprintf(message,"OUT-P-%d.dat",my_rank);
			out=fopen(message,"a");
		}
		
		if(ran36(&seed3)>PTFreq){
			
			
			if(ran3(&seed)<0.5){
				#ifdef PROGRESS
				if (icycl>0){ fprintf(out,"Pswap %llu \n",icycl);  fflush(out); } 
				#endif
				Pswap();
				pswap_tried++;
				#ifdef PROGRESS
				//if (icycl>0){fprintf(out,"Pswap OK %llu \n",icycl);fflush(out); }
				#endif
			}else{ 
				#ifdef PROGRESS
				if (icycl>0){fprintf(out,"fseq %llu \n",icycl);fflush(out); }
				#endif
				fseq();
				fseq_tried++;
				#ifdef PROGRESS
				if (icycl>0){fprintf(out,"fseq ok %llu \n",icycl);fflush(out);} 
				#endif
			}
			
			
		}else{	
			#ifdef PROGRESS
			if (icycl>0){fprintf(out,"Tswap %llu \n",icycl); fflush(out); }    
			#endif
			Tswap(); 
			#ifdef PROGRESS
			if (icycl>0){fprintf(out,"Tswap ok %llu \n",icycl); 	fflush(out); } 
			#endif   	
		}
	}
	
	
	/*Saving the seqeunce with lowest energy*/
	
	/*fprintf (fMyprot,"%8.6f ",minE);*/
	sprintf(message, "Myprot-BB%2.1f-B%2.1f-Th%d-P%d.dat",beta2[my_rank],beta,treshold,process);
	fMyprot=fopen(message,"w");
	fprintf (fMyprot,"%lf\n",P_sigma);
	fprintf (fMyprot,"%d\n",S);
	for(i=0;i<N;i++) {
		fprintf (fMyprot,"%d\n",minseq[i]);
	}
	fclose(fMyprot);
	
	sprintf(message, "Myprot-BB%2.1f-B%2.1f-Th%d-P%d-FE.dat",beta2[my_rank],beta,treshold,process);
	fMyprot=fopen(message,"w");
	fprintf (fMyprot,"%lf\n",P_sigma);
	fprintf (fMyprot,"%d\n",S);
	for(i=0;i<N;i++) {
		fprintf (fMyprot,"%d\n",minFEseq[i]);
	}
	fclose(fMyprot);
	
	sprintf(message, "HistoMyprot-BB%2.1f-B%2.1f-Th%d-P%d-FE.dat",beta2[my_rank],beta,treshold,process);
	fMyprot=fopen(message,"w");
	for(i=0;i<N;i++) {
		for(j=0;j<S;j++) {
			fprintf (fMyprot,"%d %d %lu\n",i,j,minFEseqHisto[i][j]);
		}
		fprintf (fMyprot,"\n");
	}
	fclose(fMyprot);
	
	
	sprintf(message, "Myprot-BB%2.1f-B%2.1f-Th%d-P%d-MeanFE.dat",beta2[my_rank],beta,treshold,process);
	fMyprot=fopen(message,"w");
	fprintf (fMyprot,"%lf\n",P_sigma);
	fprintf (fMyprot,"%d\n",S);
	
	
	
	for(i=1;i<N;i++) {
		tot=0.0;
		for(j=1;j<S;j++) {
			tot+=minFEseqHisto[i][j];
		}
		mean=0.0;
		for(j=1;j<S;j++) {
			mean+=j*minFEseqHisto[i][j]/tot;
		}
		fprintf (fMyprot,"%ld\n",lround(mean));
	}
	fclose(fMyprot);
	fclose(fTemp);
	
	/****************************************/
	/*Saving Parallel Tempering Performance */
	/****************************************/
	MPI_Gather(&tempHisto,1,MPI_DOUBLE,roottempHisto,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	if(my_rank==0){
		
	  ///sprintf(message,"TempControl-B%2.1f-Th%d-P%d.dat",beta,treshold,process);
	  ///fTempControl=fopen(message,"w");
		for(i=0;i<p;i++){
			fprintf(fTempControl,"%2.4f %2.4f\n",beta2[i],roottempHisto[i]/ntempswap);
		}
		fprintf(fTempControl,"#ntempswap=%5.0f\n",ntempswap);
		fclose(fTempControl);
	}
	
	fclose(fHisto_Ord);
	if(my_rank==0){
		endtime=MPI_Wtime();
		fp=fopen("SimPar","a");
		fprintf(fp,"Simulation Time %f (seconds)\n",endtime-starttime);
		fclose(fp);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return(0);
}

double energy_SP_pswap (int resi, int i, int resj, int j){
	
	
	/************************************************
	* energy -- calculate the energy interaction    *
	* 	    with the i residue and all the    	    *
	*	    neighbour residues and exclude  	    *
	*	    the backbone interactions	    	    *
	* 	    	    	    	    	    	    *
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	i -- the refernce residue   	    	    *
	*	res -- the type of the i residue    	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	energ -- the energy of all the contacts	    *
	*	    	 with particle i    	    	    *
	*************************************************/
	
	
	int k,l;
	double energ=0.0;
	double potential;
	
	
	
	
	for(k=0;k<network->genome[i].link;k++){
		if(network->genome[i].promotor[k]!=0){
			
			potential=(double)(M[resi][network->genome[network->genome[i].promotor[k]].identity]);
			if(network->genome[i].promotor[k]==j){
			
					energ+=potential*network->genome[i].LJ[k]/2.0;
				

			}else{
				
					energ+=potential*network->genome[i].LJ[k];
				

			}
		
			
			
		}
		
	}
	
	for(k=0;k<network->genome[j].link;k++){
		if(network->genome[j].promotor[k]!=0){
			
			potential=(double)(M[resj][network->genome[network->genome[j].promotor[k]].identity]);
			if(network->genome[j].promotor[k]==i){
				
					energ+=potential*network->genome[j].LJ[k]/2.0;
				
			}else{
				
					energ+=potential*network->genome[j].LJ[k];
				
			}
			
			
			
		}
		
	}
	
	
	
	
	if(M[resi][0]>0) energ+=network->genome[i].Water_H*M[resi][0];
	if(M[resj][0]>0) energ+=network->genome[j].Water_H*M[resj][0];
	
	if(M[resi][0]<0) energ+=network->genome[i].Water_P*M[resi][0];
	if(M[resj][0]<0) energ+=network->genome[j].Water_P*M[resj][0];
	return(energ);
	
}


double energy_SP (int resi, int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction    *
	* 	    with the i residue and all the    	    *
	*	    neighbour residues and exclude  	    *
	*	    the backbone interactions	    	    *
	* 	    	    	    	    	    	    *
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	i -- the refernce residue   	    	    *
	*	res -- the type of the i residue    	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	energ -- the energy of all the contacts	    *
	*	    	 with particle i    	    	    *
	*************************************************/
	
	
	int j,l;
	double energ=0.0;
	double potential;
	
	
	
	
	for(j=0;j<network->genome[i].link;j++){
		if(network->genome[i].promotor[j]!=0){
			
			potential=(double)(M[resi][network->genome[network->genome[i].promotor[j]].identity]);
			
			
				energ+=potential*network->genome[i].LJ[j];
			
			
			
			
		}
		
	}
	
	
	
	if(M[resi][0]>0)  energ+=network->genome[i].Water_H*M[resi][0];
	
	if(M[resi][0]<0)  energ+=network->genome[i].Water_P*M[resi][0];
	
	return(energ);
	
}
double energy (int resi, int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction    *
	* 	    with the i residue and all the    	    *
	*	    neighbour residues and exclude  	    *
	*	    the backbone interactions	    	    *
	* 	    	    	    	    	    	    *
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	i -- the refernce residue   	    	    *
	*	res -- the type of the i residue    	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	energ -- the energy of all the contacts	    *
	*	    	 with particle i    	    	    *
	*************************************************/
	
	
	int j,l;
	double energ=0.0;
	double potential;
	
	
	
	for(j=0;j<network->genome[i].link;j++){
		if(network->genome[i].promotor[j]!=0){
			//fprintf(out,"-Ener %d %d %d\n",j,resi,network->genome[network->genome[i].promotor[j]].identity);fflush(out);
			
			potential=(double)(M[resi][network->genome[network->genome[i].promotor[j]].identity]);
			//fprintf(out,"-Ener %d OK 1\n",j);fflush(out);
			
				energ+=potential*network->genome[i].LJ[j]/2.0;
			
			//fprintf(out,"-Ener %d OK 2\n",j);fflush(out);
			
			
			
		}
		
	}
	
	
	
	if(M[resi][0]>0)  energ+=network->genome[i].Water_H*M[resi][0];
	
	if(M[resi][0]<0)  energ+=network->genome[i].Water_P*M[resi][0];
	
	
	return(energ);
	
}
void fsamp (void){
	
	/************************************************
	* fsamp -- write the file with the result of    *
	* 	    the simulation every nsamp steps        *
	*	    and convert the squence from number     *
	* 	    to letters 	    	    	    	    *
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	fp -- the file where to write the Results of*
	*	      one process   	    	    	    *
	*	fp2-- the file where are stored all the     *
	*	      temperatures form all procs    	    *
	* Return:	    	    	    	    	    *
	*	void	    	    	        	    *
	*	    	    	    	    	    	    *
	*************************************************/
	
	int i,j;
	double tot,mean,Sigma=0;
	char message[200];
	///char codice[]="OACDEFGHIKLMNPQRSTVWY"; 
	unsigned long int index,index2;
	/*float Ntot;*/
	
	
	
	
	/*fprintf (fTemp,"%8.6f \n",network->Energy);*/
	fprintf (fTemp,"%8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f ",minE,network->Energy,Sigma,network->order,minFE,minE_FE,minord_FE);
	for(i=0;i<N-1;i++) {
		fprintf (fTemp,"%d-",network->genome[i].identity);
	}
	fprintf (fTemp,"%d",network->genome[N-1].identity);
	
	fprintf (fTemp," %f %d %d\n",beta2[my_rank],N,link);
	
	fflush(fTemp);
	if(icycl>Niteration*(Equi2+Equi3)+Equi1){
		sprintf(message, "Myprot-BB%2.1f-B%2.1f-Th%d-P%d.dat",beta2[my_rank],beta,treshold,process);
		fMyprot=fopen(message,"w");
		fprintf (fMyprot,"%lf\n",P_sigma);
		fprintf (fMyprot,"%d\n",S);
		for(i=0;i<N;i++) {
			fprintf (fMyprot,"%d\n",minseq[i]);
		}
		fprintf (fMyprot,"###%llu\n",icycl);
		fclose(fMyprot);
		
		sprintf(message, "Myprot-BB%2.1f-B%2.1f-Th%d-P%d-FE.dat",beta2[my_rank],beta,treshold,process);
		fMyprot=fopen(message,"w");
		fprintf (fMyprot,"%lf\n",P_sigma);
		fprintf (fMyprot,"%d\n",S);
		for(i=0;i<N;i++) {
			fprintf (fMyprot,"%d\n",minFEseq[i]);
		}
		fprintf (fMyprot,"###%llu\n",icycl);
		fclose(fMyprot);
		
		sprintf(message, "HistoMyprot-BB%2.1f-B%2.1f-Th%d-P%d-FE.dat",beta2[my_rank],beta,treshold,process);
		fMyprot=fopen(message,"w");
		for(i=0;i<N;i++) {
			for(j=0;j<S;j++) {
				fprintf (fMyprot,"%d %d %lu\n",i,j,minFEseqHisto[i][j]);
			}
			fprintf (fMyprot,"\n");
		}
		fclose(fMyprot);
		
		sprintf(message, "Myprot-BB%2.1f-B%2.1f-Th%d-P%d-MeanFE.dat",beta2[my_rank],beta,treshold,process);
		fMyprot=fopen(message,"w");
		fprintf (fMyprot,"%lf\n",P_sigma);
		fprintf (fMyprot,"%d\n",S);
		
		
		
		for(i=1;i<N;i++) {
			tot=0.0;
			for(j=1;j<S;j++) {
				tot+=minFEseqHisto[i][j];
			}
			mean=0.0;
			for(j=1;j<S;j++) {
				mean+=j*minFEseqHisto[i][j]/tot;
			}
			fprintf (fMyprot,"%ld\n",lround(mean));
		}
		fclose(fMyprot);
		sprintf(message, "MinFE_Seqes-BB%2.1f-B%2.1f-Th%d-P%d.dat",beta2[my_rank],beta,treshold,process);
		fMyprot=fopen(message,"w");
		fprintf (fMyprot,"%lu\n",Nseq_reduced);
		fprintf (fMyprot,"%d\n",N-1);
		fprintf (fMyprot,"%lf\n",seq_list_ndataen);
		for(i=1;i<Nseq_reduced;i++) {
			for(j=1;j<N-1;j++) {
				fprintf(fMyprot,"%d-",Sequences_in_FEmin[i][j]);
			}
			fprintf(fMyprot,"%d",Sequences_in_FEmin[i][N-1]);
			index=lround(Sequences_in_FEmin_orders[i][0]*E_Bin+SIZE/2);
			index2=lround(Sequences_in_FEmin_orders[i][1]);
		
			fprintf(fMyprot," %lf %lf %lf\n",-histo[index][index2],Sequences_in_FEmin_orders[i][0],Sequences_in_FEmin_orders[i][1]);
		}	
		fclose(fMyprot);
		/*sprintf(message, "MinFE_Seqes-BB%2.1f-B%2.1f-Th%d-P%d.bin",beta2[my_rank],beta,treshold,process);
		fMyprot=fopen(message,"w");
		writeBinaryMin(fMyprot);
		fclose(fMyprot);*/
	}
	/*Ntot=0; 
	for (i=0;i<S;i++){
		Ntot=Ntot+(*prot).np[i];
		printf ("frazione tipo %d = %f\n",
		i,(*prot).np[i]);
	} 
	fprintf(out,"Ntot=%f\n",Ntot); */
	
}

void finit (void){
	
	/************************************************
	* finit -- initialize the protein:	 	        *
	*	    take the interaction matrix M form the  *
	*	    file aapot.dat    	    	    	    *
	*	    take the protein struct from the file   *
	*	    prot  	    	    	    	        *
	*	    give a random sequence to the protein   *  	
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	void	    		    	    	        *
	*************************************************/
	
	
	FILE *fp,*ECAOUT=NULL;
	int i,j,jj,kk=0,kkk=0,n,p3,p2,l,count=0;
	int tmp_link,max_link=-100;
	int simil=0,l1=0,test=0,letter=0;
	double *k,*nn,tot,x;
	char line[200],message[200],*pch=NULL;
	float y;
	double r,LJ,LJ_repulsive;
	double LJ2_a,LJ2_c,LJ2_b;
	double Rint2;
	double En2;
	int pr,pr_i,prt,prt_j;
	int dumm;
	///char codice[]="OACDEFGHIKLMNPQRSTVWY"; 
	network=(struct tot *) malloc (sizeof(struct tot));
	beta2=(float *)malloc(p*sizeof(float));
	
	
	Rint2=(ECA_Range+5.0)*(ECA_Range+5.0);
	
	
	fp=fopen("aapot.dat","r");
	fscanf(fp,"%d",&S);
	S=S+1;/// we include into the alphabet also the solvent molecule
	fclose(fp);
		
			M=(float**)malloc(S*sizeof(float*));
		if (M == 0) {
			fprintf(out,"Allocation M OR prot OR prot Failed\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		for(i=0;i<S;i++){
			M[i]=(float*)malloc(S*sizeof(float));
			if (M[i] == 0) {
				fprintf(out,"Allocation M[%d] Failed\n",i);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}

#ifdef TEST


			M2=(float**)malloc(S*sizeof(float*));
		if (M2 == 0) {
			fprintf(out,"Allocation M2 OR prot OR prot Failed\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		for(i=0;i<S;i++){
			M2[i]=(float*)malloc(S*sizeof(float));
			if (M2[i] == 0) {
				fprintf(out,"Allocation M2[%d] Failed\n",i);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
#endif	
		fp=fopen("aapot.dat","r");
		if ( fp == NULL) {
			printf ("File aapot.dat not found!!!\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}else{
			p2=fscanf (fp,"%d",&dumm); /// skip S already red
			p2=fscanf (fp,"%f",&y);
			HOH_Burried_Threshold=y;
			p2=fscanf (fp,"%f",&y);
			Hydropathy_Scale=y;
			for(j=0;j<S;j++){
				for(i=j;i<S;i++){
					fscanf (fp,"%f",&M[i][j]);
					//M[i][j]*=4.0;
					M[j][i]=M[i][j];

				}
			}
			for(i=0;i<S;i++){
				M[0][i]*=Hydropathy_Scale;  /// CA_Scale???
				M[i][0]=M[0][i];
			}
			count=0;
			ave=0.;
			for(i=1;i<S;i++){
			  for(j=i;j<S;j++){
			    ave+=M[i][j];
			    count++;
			  }
			}
			ave=ave/count;
			count=0;
			fprintf(out,"matrix ave=%f\n", ave);
			if ( P_sigma < 0) {
			  P_sigma=0;
			  for(i=1;i<S;i++){
			    for(j=i;j<S;j++){
			      P_sigma+=(M[i][j]-ave)*(M[i][j]-ave);
			      count++;
			    }
			  }
			  P_sigma=sqrt(P_sigma/count);
			  ///fprintf(out,"P_sigma matrix=%f\n", P_sigma);
			  /*if (count!=(S-1)*(S-1)/2+(S-1)/2){
			    fprintf(out,"COUNT NO MERDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA count=%d altro=%d\n", count,(S-1)*(S-1)/2+(S-1)/2);
			    fflush(out);
			    MPI_Abort(MPI_COMM_WORLD,err5);
			    }*/
			  }
			fclose(fp);
		}
		
	
	
	fprintf(out,"aapot OK\n");fflush(out);
	
	fprintf(out,"prot\n");
	fflush(out);
	
	fp=fopen("prot2","r");
	if ( fp == NULL) {
		fprintf(out,"File prot2 not found\n");
		fflush(out);
		lamda=1.0;
		//MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		fgets(line,sizeof(line),fp);
		p2=sscanf (line,"%d\n",&n);
		fgets(line,sizeof(line),fp);
		p3=sscanf (line,"%d\n",&n);
		link=n;
		fclose(fp);
	}
	
	fp=fopen("prot","r");
	if ( fp == NULL) {
		fprintf(out,"File prot not found\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		fgets(line,sizeof(line),fp);
		p2=sscanf (line,"%d\n",&n);
		N=n;   /// N is the number of beads: length of the chain
		fgets(line,sizeof(line),fp);
		p3=sscanf (line,"%d\n",&n);
		
		link+=n;
		tmp_link=n;
		fgets(line,sizeof(line),fp);
		p3=sscanf (line,"%d\n",&n);
		SubN=n;
		ProtN=N-SubN;
		
		if((p2!=1)||(p3!=1)){
			fprintf(out,"Bad file Format prot!!!\n");
			fflush(out);
		}else{ 
			network->genome= (struct gene *)malloc(N*sizeof(struct gene)); /// it`s allocated in N total number of beads but it should be in S alphabet
			network->p= (int *)malloc(S*sizeof(int));
			network->np= (int *)malloc(S*sizeof(int));
			///network->types= (int *)malloc(S*sizeof(int));
			///network->ntypes= (int *)malloc(S*sizeof(int));
			
			initseq=(int *) malloc (N*sizeof(int));
			minseq=(int *) malloc (N*sizeof(int));
			minFEseq=(int *) malloc (N*sizeof(int));
			minFEseqHisto=(unsigned long int **) malloc (N*sizeof(unsigned long int*));
			
			nn=(double *) malloc (N*sizeof(double));
			k=(double *) malloc (N*sizeof(double));
			
			Sequences_in_FEmin=(char**)malloc(Nseq*sizeof(char*));
			Sequences_in_FEmin_orders=(double**)malloc(Nseq*sizeof(double*));
			for(i=0;i<Nseq;i++){
				Sequences_in_FEmin[i]=(char *)malloc(N*sizeof(char));
				Sequences_in_FEmin_orders[i]=(double*)malloc(2*sizeof(double));
			}
			
			
			seq_list=(double*)malloc(Nseq*sizeof(double));
			
			
			
			
			
			for(i=0;i<N;i++){	
			  network->genome[i].promotor=(int *) calloc (link,sizeof(int));
				network->genome[i].LJ=(double *) malloc (link*sizeof(double));
				network->genome[i].LJ_repulsive=(double *) malloc (link*sizeof(double));
				network->genome[i].LJ2=(double *) malloc (link*sizeof(double));
				network->genome[i].LJ_repulsive2=(double *) malloc (link*sizeof(double));
				minFEseqHisto[i]=(unsigned long int *) malloc (S*sizeof(unsigned long int));
				network->genome[i].link=0;
				network->genome[i].Water=0.0;
				
			}
			for(i=0;i<N;i++){	
				for(j=0;j<S;j++){
					minFEseqHisto[i][j]=0;	
				}
			}
			fprintf(out,"prot allocation OK, %d\n",N);
			fflush(out);
			sprintf(message,"ECAOUT-P-%d.dat",my_rank);
			ECAOUT=fopen(message,"w");
			for(i=0;i<N;i++){
				j=0;
				for(jj=0;jj<tmp_link;jj++){
					fgets(line,sizeof(line),fp);
					p2=sscanf (line,"%*d%d%d%d%d%d%lf\n",&n,&pr,&pr_i,&prt,&prt_j,&x);
					
					if(pr==prt){
						
						if((prt_j!=pr_i)&&(prt_j!=pr_i+5)&&(prt_j!=pr_i-5)&&(prt_j!=pr_i-10)&&(prt_j!=pr_i+10)){
							
							
							if(x<Rint2){
								network->genome[i].Water+=(lamda)*(-1.0/(1.0 + exp(2.5*(ECA_Range - sqrt(x)))) + 1.0);
								if(j>=link){
									fprintf(out,"Wrong Finit network creation j=%d link=%d\n",j,link);fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								network->genome[i].link++;
								network->genome[i].promotor[j]=n;
								
								#ifdef TUBE
								sigma_tube6=pow(sigma_tube,6);
								sigma_tube12=pow(sigma_tube,12);
								if (x<Rtube){
								  r6=pow(x,3);
								  r12=pow(x,6);
								  network->genome[i].LJ[j]=((sigma_tube12/r12)-(sigma_tube6/r6));
								}
								#else
								LJ=sigma6/pow(x,LJpower);
								LJ_repulsive=(LJ+LJ*LJ)/2.0;LJ-=(LJ*LJ)*4.0;
								LJ2_a=Rmin;
								LJ2_c=ECA_Range;
								LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));
								
								network->genome[i].LJ[j]=(lamda)*(-1.0/(1.0 + exp(2.5*(LJ2_c - sqrt(x)))) + 1.0);
																
								network->genome[i].LJ_repulsive[j]=network->genome[i].LJ[j];
								#endif
								
								
								fprintf(ECAOUT,"%d %d %lf %lf %lf\n",i,j,sqrt(x),network->genome[i].LJ[j],network->genome[i].LJ_repulsive[j]);
								fflush(ECAOUT);
								j++;
							}
							
						}
					}else{
						
						
						
						if(x<Rint2){
							network->genome[i].Water+=(lamda)*(-1.0/(1.0 + exp(2.5*(ECA_Range - sqrt(x)))) + 1.0);
							network->genome[i].link++;
							network->genome[i].promotor[j]=n;
							#ifdef TUBE
							sigma_tube6=pow(sigma_tube,6);
							sigma_tube12=pow(sigma_tube,12);
							if (x<Rtube){
							  r6=pow(x,3);
							  r12=pow(x,6);
							  network->genome[i].LJ[j]=((sigma_tube12/r12)-(sigma_tube6/r6));
							}
							#else
							LJ=sigma6/pow(x,LJpower);
							LJ_repulsive=(LJ+LJ*LJ)/2.0;LJ-=(LJ*LJ)*4.0;
							LJ2_a=Rmin;
							LJ2_c=ECA_Range;
							LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));
							
							network->genome[i].LJ[j]=(lamda)*(-1.0/(1.0 + exp(2.5*(LJ2_c - sqrt(x)))) + 1.0);
							
							//network->genome[i].LJ[j]=LJ;
							network->genome[i].LJ_repulsive[j]=network->genome[i].LJ[j];
							#endif
							
							fprintf(ECAOUT,"%d %d %lf %lf %lf\n",i,j,sqrt(x),network->genome[i].LJ[j],network->genome[i].LJ_repulsive[j]);
							fflush(ECAOUT);
							j++;
						}
						
					}
					
				}
				
				
			}
		}
		fclose(fp);
	}
	fclose(ECAOUT);
	fprintf(out,"ECAOUT OK\n");
	fflush(out);
	
	
	
	
	fp=fopen("prot2","r");
	if ( fp == NULL) {
	}else{
		fgets(line,sizeof(line),fp);
		p2=sscanf (line,"%d\n",&n);
		
		fgets(line,sizeof(line),fp);
		p3=sscanf (line,"%d\n",&n);
		
		tmp_link=n;
		
		fgets(line,sizeof(line),fp);
		p3=sscanf (line,"%d\n",&n);
		
		
		if((p2!=1)||(p3!=1)){
			fprintf(out,"Bad file Format prot!!!\n");
			fflush(out);
		}else{ 
			
			
			
			sprintf(message,"ECAOUT2-P-%d.dat",my_rank);
			ECAOUT=fopen(message,"w");
			for(i=0;i<N;i++){
				j=network->genome[i].link;
				for(jj=0;jj<tmp_link;jj++){
					fgets(line,sizeof(line),fp);
					p2=sscanf (line,"%*d%d%d%d%d%d%lf\n",&n,&pr,&pr_i,&prt,&prt_j,&x);
					
					if(pr==prt){
						
						if((prt_j!=pr_i)&&(prt_j!=pr_i+5)&&(prt_j!=pr_i-5)&&(prt_j!=pr_i-10)&&(prt_j!=pr_i+10)){
							
							
							if(x<Rint2){
								 network->genome[i].Water+=(1.0-lamda)*(-1.0/(1.0 + exp(2.5*(ECA_Range - sqrt(x)))) + 1.0);
								if(j>=link){
									fprintf(out,"Wrong Finit network creation j=%d link=%d\n",j,link);fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								test=-100;
								for(kk=0;kk<network->genome[i].link;kk++){
									if(n==network->genome[i].promotor[kk]){
										test=kk;
										kk=network->genome[i].link;
									}
								}
								if(test==-100){
									network->genome[i].link++;
									network->genome[i].promotor[j]=n;
									
                                                                        #ifdef TUBE
									sigma_tube6=pow(sigma_tube,6);
									sigma_tube12=pow(sigma_tube,12);
									if (x<Rtube){
								           r6=pow(x,3);
								           r12=pow(x,6);
								           network->genome[i].LJ[j]+=((sigma_tube12/r12)-(sigma_tube6/r6));
								        }
                                                                        #else
									LJ=sigma6/pow(x,LJpower);
									LJ_repulsive=(LJ+LJ*LJ)/2.0;LJ-=(LJ*LJ)*4.0;
									LJ2_a=Rmin;
									LJ2_c=ECA_Range;
									LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));
									network->genome[i].LJ[j]+=(1.0-lamda)*(-1.0/(1.0 + exp(2.5*(LJ2_c - sqrt(x)))) + 1.0);
									
									network->genome[i].LJ_repulsive[j]=network->genome[i].LJ[j];
									#endif


									fprintf(ECAOUT,"%d %d %lf %lf %lf\n",i,j,sqrt(x),network->genome[i].LJ[j],network->genome[i].LJ_repulsive[j]);
									fflush(ECAOUT);
									j++;
								}else{
                                                                        #ifdef TUBE
									sigma_tube6=pow(sigma_tube,6);
									sigma_tube12=pow(sigma_tube,12);
									if (x<Rtube){
								           r6=pow(x,3);
								           r12=pow(x,6);
								           network->genome[i].LJ[test]+=((sigma_tube12/r12)-(sigma_tube6/r6));
								        }
                                                                        #else
									LJ=sigma6/pow(x,LJpower);
									LJ_repulsive=(LJ+LJ*LJ)/2.0;LJ-=(LJ*LJ)*4.0;
									LJ2_a=Rmin;
									LJ2_c=ECA_Range;
									LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));

									network->genome[i].LJ[test]+=(1.0-lamda)*(-1.0/(1.0 + exp(2.5*(LJ2_c - sqrt(x)))) + 1.0);
									
									network->genome[i].LJ_repulsive[test]=network->genome[i].LJ[test];
									#endif


									fprintf(ECAOUT,"%d %d %lf %lf %lf\n",i,test,sqrt(x),network->genome[i].LJ[test],network->genome[i].LJ_repulsive[test]);
									fflush(ECAOUT);
								}
							}
							
						}
					}else{
						
						
						
						if(x<Rint2){
							network->genome[i].Water+=(1.0-lamda)*(-1.0/(1.0 + exp(2.5*(ECA_Range - sqrt(x)))) + 1.0);
							test=-100;
								for(kk=0;kk<network->genome[i].link;kk++){
									if(n==network->genome[i].promotor[kk]){
										test=kk;
										kk=network->genome[i].link;
									}
								}
								if(test==-100){
									network->genome[i].link++;
									network->genome[i].promotor[j]=n;
									#ifdef TUBE
									sigma_tube6=pow(sigma_tube,6);
									sigma_tube12=pow(sigma_tube,12);
									if (x<Rtube){
								           r6=pow(x,3);
								           r12=pow(x,6);
								           network->genome[i].LJ[j]+=((sigma_tube12/r12)-(sigma_tube6/r6));
								        }
                                                                        #else
									LJ=sigma6/pow(x,LJpower);
									LJ_repulsive=(LJ+LJ*LJ)/2.0;LJ-=(LJ*LJ)*4.0;
									LJ2_a=Rmin;
									LJ2_c=ECA_Range;
									LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));

									network->genome[i].LJ[j]+=(1.0-lamda)*(-1.0/(1.0 + exp(2.5*(LJ2_c - sqrt(x)))) + 1.0);
									
									network->genome[i].LJ_repulsive[j]=network->genome[i].LJ[j];

									#endif

									fprintf(ECAOUT,"%d %d %lf %lf %lf\n",i,j,sqrt(x),network->genome[i].LJ[j],network->genome[i].LJ_repulsive[j]);
									fflush(ECAOUT);
									j++;
								}else{
								  
								        #ifdef TUBE
									sigma_tube6=pow(sigma_tube,6);
									sigma_tube12=pow(sigma_tube,12);
									if (x<Rtube){
								           r6=pow(x,3);
								           r12=pow(x,6);
								           network->genome[i].LJ[test]+=((sigma_tube12/r12)-(sigma_tube6/r6));
								        }
                                                                        #else
									LJ=sigma6/pow(x,LJpower);
									LJ_repulsive=(LJ+LJ*LJ)/2.0;LJ-=(LJ*LJ)*4.0;
									LJ2_a=Rmin;
									LJ2_c=ECA_Range;
									LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));

									network->genome[i].LJ[test]+=(1.0-lamda)*(-1.0/(1.0 + exp(2.5*(LJ2_c - sqrt(x)))) + 1.0);
									
									network->genome[i].LJ_repulsive[test]=network->genome[i].LJ[test];

									#endif

									fprintf(ECAOUT,"%d %d %lf %lf %lf\n",i,test,sqrt(x),network->genome[i].LJ[test],network->genome[i].LJ_repulsive[test]);
									fflush(ECAOUT);
								}
						}
						
					}
					
				}
				
				
			}
		}
		fclose(fp);
	}
	
	
	for(i=0;i<N;i++){
		if(network->genome[i].Water>HOH_Burried_Threshold){
			network->genome[i].Water_H=0.0;
			network->genome[i].Water_P=(HOH_Burried_Threshold-network->genome[i].Water);
		}else{
			network->genome[i].Water_H=(HOH_Burried_Threshold-network->genome[i].Water);
			network->genome[i].Water_P=0.0;
		}
	}
	for(i=0;i<N;i++){
		
		for(j=0;j<link;j++){
			network->genome[i].LJ2[j]=network->genome[i].LJ[j]*network->genome[i].LJ[j];
			network->genome[i].LJ_repulsive2[j]=network->genome[i].LJ_repulsive[j]*network->genome[i].LJ_repulsive[j];
		}
	}
	for(j=0;j<link;j++){
		network->genome[0].promotor[j]=0;
		network->genome[0].LJ[j]=1.0;
		network->genome[0].LJ_repulsive[j]=1.0;
	}
	for(i=0;i<N;i++){
		fprintf(out,"%d ",i);
		for(j=0;j<link;j++){
			fprintf(out,"%d ",network->genome[i].promotor[j]);	
		}
		fprintf(out,"\n");
	}
	fprintf(out,"prot OK\n");
	fflush(out);
	
	fp=fopen("tempera.dat","r");
	if ( fp == NULL) {
		printf ("File tempera.dat not found!!!\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		for(j=0;j<p;j++){
			fgets(line,sizeof(line),fp);
			sscanf (line,"%f\n",&y);
			beta2[j]=y;
		}
		Ref_FE=2.0*beta2[my_rank];
		fclose(fp);
	}
	fprintf(out,"tempera OK\n");fflush(out);
	fp=fopen("prot3","r");
	if ( fp == NULL) {
	  ///homopolymer
	  if(start_conf==0){
	    for(i=0;i<N;i++){	
	      network->genome[i].old_identity=network->genome[i].identity=1;
	      initseq[i]=network->genome[i].identity;
	      if(i==0) initseq[i]=network->genome[i].identity=0;		
	    }
	  }
	  if(start_conf==1){ 
	  // 1-2-3-4... sequence:
	    for(i=0;i<N;i++){
	      if (letter==S) letter=1;
	      network->genome[i].old_identity=network->genome[i].identity=letter;
	      letter++;
	      initseq[i]=network->genome[i].identity;
	      if(i==0) initseq[i]=network->genome[i].identity=0;		
	    }
	  }
	  if(start_conf==2){
	    
	    // random generated sequence with sigma= fixed sigma:
	    
	      ///	      fprintf(out,"network->sigma\n",i,network->genome[i].identity);fflush(out);
	    
	      #ifdef SIGMA_ON
	      fprintf(out,"sequence with fixed P_sigma and ave\n");fflush(out);
	      while (  (fabs(network->sigma-P_sigma)>1e-2)||(fabs(network->matrix_ave>ave)>1e-3)  ){
		for(i=0;i<N;i++){
		network->genome[i].old_identity=network->genome[i].identity=(int)(ran3(&seed)*(S-1))+1;
		///fprintf(out,"network->genome[i].identity=%d\n",i,network->genome[i].identity);fflush(out);
		initseq[i]=network->genome[i].identity;
		if(i==0) initseq[i]=network->genome[i].identity=0;
              }

	      sigma_calculator(&network->sigma,&network->matrix_ave);
	      }
	    #else
	      for(i=0;i<N;i++){
	      network->genome[i].old_identity=network->genome[i].identity=(int)(ran3(&seed)*(S-1))+1;
	      initseq[i]=network->genome[i].identity;
	      if(i==0) initseq[i]=network->genome[i].identity=0;
	      }
	      #endif
	  }

	}else{
	  int *Res=NULL;
	  Res=(int *)calloc(N+1,sizeof(int));
	  fgets(line,sizeof(line),fp);
	  i=0;
	  printf("N=%d\n",N);fflush(NULL);
	  pch = strtok (line,"-");
	  while (pch != NULL){

	    if(i<N){
	    	Res[i]=atoi(pch);
		if(Res[i]>=S){
		  printf("Seqeunce requires a larger alphabet of interaction\n");fflush(NULL);
		  printf("Res[%d]=%d S=%d\n",i,Res[i],S);fflush(NULL);
		  MPI_Abort(MPI_COMM_WORLD,err5);
		}
	    	pch = strtok (NULL, "-");
	    	///printf("Res[%d]=%d\n",i,Res[i]);fflush(NULL);
	    	i++;
	    }
	  }
	  ///fprintf(out,"Init Seq  %s\n",Seq);
	  ///fastadecoder2(Res,Seq,N);
	  for(i=1;i<N;i++){
	    
	    network->genome[i].old_identity=network->genome[i].identity=Res[i-1];
	    ///printf("network Res[%d]=%d\n",i,Res[i]);
	    }
	  initseq[0]=network->genome[0].identity=0;
	  
	  fclose(fp);
	  /*fgets(line,sizeof(line),fp);
		p2=sscanf (line,"%f\n",&y);
		P_sigma=y;
		fgets(line,sizeof(line),fp);
		p2=sscanf (line,"%d\n",&n);
		S=n;
		
		for(i=0;i<N;i++){
			fgets(line,sizeof(line),fp);
			p2=sscanf(line,"%d\n",&n);
			network->genome[i].old_identity=network->genome[i].identity=n;
		}
		fclose(fp);*/
	  //free(Seq);
	  free(Res);
	}
	fprintf (out,"initial sequence   ");fflush(out);
	for(i=0;i<N-1;i++) {
		fprintf (out,"%d-",network->genome[i].identity);fflush(out);
	}
	fprintf (out,"%d\n",network->genome[N-1].identity);fflush(out);
	
	fprintf(out,"prot2 OK\n");fflush(out);
	
	

	/*for(i=0;i<N;i++){
		fprintf(out,"%d ",network->genome[i].identity);fflush(out);
		for(j=0;j<link;j++){
			fprintf(out,"%d ",network->genome[network->genome[i].promotor[j]].identity);fflush(out);	
		}
		fprintf(out,"\n");fflush(out);
	}*/
	
	
	for(i=0;i<S;i++){
		//network->genome[i].Energy=0;
		network->p[i]=network->np[i]=1;
		///network->ntypes[i]=network->types[i]=0;
	}
	
	fprintf(out,"Init OK\n");fflush(out);
	
	for(i=1;i<N;i++){
		
		for(j=0;j<network->genome[i].link;j++){
			test=0;
			if(network->genome[i].promotor[j]!=0){
				for(kk=0;kk<network->genome[network->genome[i].promotor[j]].link;kk++){
					if(i==network->genome[network->genome[i].promotor[j]].promotor[kk]) test++;
				}
				if((test!=1)){
					
					fprintf(out,"test=%d network->genome[%d].promotor[%d]=%d \n",test,i,j,network->genome[i].promotor[j]);
					for(kk=0;kk<network->genome[network->genome[i].promotor[j]].link;kk++){
						fprintf(out,"****** network->genome[%d].promotor[%d]=%d \n",network->genome[i].promotor[j],kk,network->genome[network->genome[i].promotor[j]].promotor[kk]);
					}
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
		
	}
	
	fprintf(out,"Test OK\n");fflush(out);
	
	
	
	
	
	
	
	network->Energy=0.0;
	En2=0.0;
	network->Energy2=0.0;
	network->NE=0.0;
	for(i=1;i<N;i++){
		//network->genome[i].Energy=energy(network->genome[i].identity,i);
		//network->Energy=network->Energy+network->genome[i].Energy;
		//fprintf(out,"Ener %d \n",i);fflush(out);
		network->Energy+=energy(network->genome[i].identity,i);
		//fprintf(out,"Ener %d OK \n",i);fflush(out);
	}
	
	
	fprintf(out,"Energy OK Energy=%lf Energy2=%lf NE=%lf\n",network->Energy,network->Energy2,network->NE);fflush(out);
	/*for(j=1;j<N;j++){
		simil=0;
		
		for(kk=0;kk<l1;kk++){
			
			if(network->genome[j].identity==network->types[kk]){
				simil++;
				network->p[kk]++;
			}		    
		}
		
		if(simil==0){		     
			network->types[l1]=network->genome[j].identity;
			network->p[l1]++;
			
			l1++;
		}
		
		
		}*/
	for(j=1;j<N;j++){
	  network->p[network->genome[j].identity]++;
	}
	
	fprintf(out,"Permutation OK l1=%d\n",l1);fflush(out);
	for(i=0;i<S;i++){
		network->np[i]=network->p[i];
		///network->ntypes[i]=network->types[i];
	}
	
	/*for(i=1;i<N;i++){
		simil=0;
		for(kk=0;kk<S;kk++){
			if(network->genome[i].identity==network->types[kk]){ 
				simil++;
				kkk=kk;
			}
			
			
		}
		if(simil==0){
			fprintf(out,"non e' possibile\n");
			fprintf(out,"network->genome[%d].identity=%d \n",i,network->genome[i].identity);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			
		}else{
			if(network->np[kkk]==1){
				fprintf(out,"non e' possibile\n");
				fprintf(out,"Finit %llu network->genome[%d].identity=%d network->np[%d]=%d\n",icycl,i,network->genome[i].identity,kkk,network->np[kkk]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
		}*/
	
	
	

	network->Etype=0.0;

	network->E2type=0.0;
	sigma_calculator(&network->sigma,&network->matrix_ave);
	fprintf(out,"aapot P_sigma=%f ave=%f\n",P_sigma,ave);
	fprintf(out,"initial network->sigma=%f network->matrix_ave=%f\n",network->sigma,network->matrix_ave);
	//	if(N<100){
	network->order=(factorial(N,1));
	///network->order=factorial(N,1);
		//	}else{
		//	  network->order=N*log(N)-N+0.5*log(2*PI*N);
		//	}
	
	for(j=0;j<S;j++){
	  //		if(network->p[j]<100){
			network->order=network->order-(factorial(network->p[j],1));	
			///printf("network order %lf \n",network->order ); fflush(NULL);
			//		}else{
			//	  network->order=network->order-(network->p[j]*log(network->p[j])-network->p[j]+0.5*log(2*PI*network->p[j]));
			//	}    
	}
	fprintf(out,"initial network->order=%f\n", network->order);fflush(out);
	if(lround(network->order)<0){ 
		fprintf(out,"Finit %llu cazzo order e' negativo %f\n",icycl,network->order);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	/*printf ("%d %f\n",N,network->Energy);*/
	MPI_Barrier(MPI_COMM_WORLD);
	if(my_rank==0){	
		fp=fopen("SimPar","a");
		for(i=1;i<N;i++){
			nn[i]=0;
			k[i]=0;
		}
		tot=0;
		for(i=1;i<N;i++){
			
			for(j=0;j<network->genome[i].link;j++){
				if(network->genome[i].promotor[j]!=0){
					k[i]++;
					for(l=0;l<network->genome[network->genome[i].promotor[j]].link;l++){
						for(jj=0;jj<network->genome[i].promotor[j];jj++){	
							if((network->genome[i].promotor[jj]==network->genome[network->genome[i].promotor[j]].promotor[l])&&
							(network->genome[i].promotor[jj]!=0)) nn[i]+=0.5;
						}
					} 
				}
			}
			
		}		
		for(i=1;i<N;i++){
			/*tot+=(2*nn[i]/(k[i]*(k[i]-1)));*/
			if (k[i]>1) tot+=2*nn[i]/(k[i]*(k[i]-1));
		}	
		for(i=1;i<N;i++){
			
			if (k[i]>1) fprintf(fp,"histo %d %f %f %f %f\n",i,(2*nn[i]/(k[i]*(k[i]-1)))/tot,nn[i],k[i],tot/(double)(N));
		}
		
		
		fprintf(fp,"Init energy %f\n",network->Energy);
		fprintf(fp,"ncycl %llu\n",ncycl);
		fprintf(fp,"nsamp %llu\n",nsamp);
		
		fclose(fp);
		
		
		
	}
	MPI_Barrier(MPI_COMM_WORLD);
	
}
void fseq (void){
	
	
	/************************************************
	* fexec -- make the montecarlo move: 	    *
	*	    randomly change the type of a random    *
	*	    particle    	    	    	    *
	* 	    	    	    	    	    	    *  	
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	void	    		    	    	    *
	*************************************************/
	
	int err=0,sim=0,simul=0;    
	int simul2=0,kkk=20000;
	int i=0,k,res=0,reso=0,j;
	int k1=20000,k2=20000,k3=20000;
	double acc,acc2,denominator;
	double En=0.0,E2n=0.0,Etest=0.0,Etype=0.0,E2type=0.0,TEtype=0.0,TE2type=0.0;
	static double *Eni=NULL;
	double deltaerg,dW=0.0,new_sigma=0.,new_ave=0.,DS=0.,DA=0.;
	double deltaP;
	long int index,indexO;
	double espo,espo1;
	
	
	if(Eni==NULL) Eni=(double *)malloc(ProtN*sizeof(double));
	/*fprintf(out,"%llu\n",icycl);*/
	/* for(i=1;i<N;i++){
		simul2=0;
		for(k=0;k<N;k++){
			if(network->genome[i].identity==network->types[k]){ 
				simul2++;
				kkk=k;
			}
			
			
		}
		if(simul2==0){
			fprintf(out,"non e' possibile\n");
			fprintf(out,"network->genome[%d].identity=%d \n",i,network->genome[i].identity);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			
		}else{
			if(network->np[kkk]==1){
				fprintf(out,"non e' possibile\n");
				fprintf(out,"%llu network->genome[%d].identity=%d network->np[%d]=%d\n",icycl,i,network->genome[i].identity,kkk,network->np[kkk]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
	}*/
	
	
	i=(int)(ran3(&seed)*(N-1))+1;
	res=(int)(ran3(&seed)*(S-1))+1;
      
	if(res!=network->genome[i].identity) {
		
		En-=energy_SP(network->genome[i].identity,i);
		network->genome[i].old_identity=reso=network->genome[i].identity;
		network->genome[i].identity=res;
	
		///fprintf(out,"network->tmp_sigma outside= %f, P_sigma= %f i= %d res= %d reso= %d\n",network->tmp_sigma,P_sigma,i,res,reso);
		/*fprintf(out,"nuova seq:  icycl=%llu\n", icycl);
		for(k=1;k<N-1;k++){
		  fprintf(out,"%d-",network->genome[k].identity);
		}
		fprintf(out,"%d\n",network->genome[N-1].identity);*/
		
		E2n=-E2n;
		
		/*for(k=0;k<S;k++){
			if(network->types[k]==0){
				k3=k;
				if(network->np[k]!=1){
					fprintf(out,"Fseq non e' possibile\n");
					fprintf(out,"%llu network->types[%d]=%d network->np[%d]=%d\n",icycl,k,network->types[k],k,network->np[k]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				
				k=S;
			}
		}
		
		for(k=0;k<S;k++){
			if(reso==network->types[k]){
				
				network->np[k]--;
				if(network->np[k]==1) network->ntypes[k]=0;
				
				k1=k;
				
			}
			
			if(res==network->types[k]){
				network->np[k]++;
				k2=k;
				simul++;
			}
			
			
				
			}*/
		network->np[res]++;
		network->np[reso]--;
		/*if(simul==0){
			network->ntypes[k3]=res;
			network->np[k3]++; 
			k2=k3;
			}*/
		for(k=0;k<N;k++){
		if(k!=i){
				err=initseq[k]-reso;
			}else{
				err=initseq[k]-res;
			}
			if(err!=0) sim++;
		}		
		
		if(sim<=treshold){
			
			
			En+=energy_SP(network->genome[i].identity,i);
			
			
			
			
			
			/*for(k=1;k<N;k++){
				Eni[k]=energy(network->genome[k].identity,k);
				En=En+Eni[k];				    				
			}*/
			
			#ifdef TEST		
			for(k=1;k<N;k++){
				Etest+=energy(network->genome[k].identity,k);
				
			}
			if(fabs(Etest-(network->Energy+En))>1e-5){
				fprintf(out,"fseq %llu Etest failed Etest=%lf En=%lf \n",icycl,Etest,network->Energy+En);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			
			
			#endif
			deltaerg=(En);
			index=lround((network->Energy+deltaerg)*E_Bin+SIZE/2);
			if(index<0) index=0;
			if(index>=SIZE) index=SIZE-1;
			indexO=lround(network->Energy*E_Bin+SIZE/2);
			if(indexO<0) indexO=0;
			if(indexO>=SIZE) indexO=SIZE-1;
			
			/*if(lround(network->Energy+deltaerg)>=SIZE/2.0){
				fprintf(out,"Fseq %llu cazzo Ener e' troppo grande %f %f %d beta2=%f\n",icycl,deltaerg,SIZE/2.0,lround(network->Energy+deltaerg),beta2[my_rank]);
				fsamp();
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			if(lround(network->Energy+deltaerg)<-SIZE/2.0){
				fprintf(out,"Fseq %llu cazzo Ener e' troppo piccola %f %f %d beta2=%f\n",icycl,deltaerg,-SIZE/2.0,lround(network->Energy+deltaerg),beta2[my_rank]);
				fsamp();
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}*/
			
			deltaP=((double)(network->p[reso])/(double)(network->np[res]));
			
			deltaP=log(deltaP);
			if(lround(network->order+deltaP)<0){ 
				fprintf(out,"Fseq %llu order e' negativo %f %f %ld beta2=%f\n",icycl,network->order,deltaP,lround(network->order+deltaP),beta2[my_rank]);
				fsamp();
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			} 
			
#ifdef SIGMA_ON
			sigma_calculator(&new_sigma,&new_ave);
			if(new_sigma<0){
			  fprintf(out,"New_sigma<0!!! icycl=%llu my_rank=%d new_sigma=%d\n",icycl,my_rank,new_sigma);
			  fflush(out);
			  MPI_Abort(MPI_COMM_WORLD,err5);
                        }

			//fprintf(out,"JUST AFTER FUNCTION new_sigma=%e new_ave=%e \n",new_sigma,new_ave);fflush(out);
			if ((fabs(new_sigma-P_sigma)<1e-2)&&(fabs(new_ave-ave)<1e-3)){
			  sigma_flag=1;
			  matrix_on++;
			  if (fabs(network->sigma-P_sigma)<1e-2) sigma_on++;
			  if (fabs(network->matrix_ave-ave)<1e-3) ave_on++;
			}else{
			  matrix_off++;
			  sigma_flag=0;
			}
			DS=(new_sigma-P_sigma)*(new_sigma-P_sigma)-(network->sigma-P_sigma)*(network->sigma-P_sigma);
			DA=(new_ave-ave)*(new_ave-ave)-(network->matrix_ave-ave)*(network->matrix_ave-ave);
#endif
			if(network->order+deltaP > max_perm){
			  perm_flag=0;
			}else{
			  /// fprintf(out,"perm raggiunta!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			  perm_flag=1;
			  perm_on++;
			  /// if ((perm_flag==1)&&(sigma_flag==1)) fprintf(out,"entrambe raggiunte!! perm=%lf network->tmp_sigma=%lf\n",network->order+deltaP,network->tmp_sigma );
			  
			}
			if ((perm_flag==1)&&(sigma_flag==1)) both_const++;
			dW=Wpot[index][lround(network->order+deltaP)]-Wpot[indexO][lround(network->order)];
			
			espo=-beta2[my_rank]*(deltaerg-beta*deltaP+beta_sigma*DS+beta_ave*DA)+dW;
			  
			
			///fprintf(out,"espo deltaerg=%e    -beta*deltaP=%lf deltaP=%lf  beta=%e   beta_sigma=%e beta_sigma*DS=%lf new_sigma=%e  network->sigma=%e  P_sigma=%e beta_ave=%e beta_ave*DA=%e new_ave=%e ave=%e network->matrix_ave=%e \n",deltaerg,-beta*deltaP,deltaP,beta,beta_sigma,beta_sigma*DS,new_sigma,network->sigma,P_sigma,beta_ave,beta_ave*DA,new_ave,ave,network->matrix_ave);fflush(out);
			if(espo<50.0){
				espo1=-log(1+exp(espo));
			}else{
				espo1=-espo;
			}
			
			if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
				fprintf(out,"Cavolo fseq 1  %llu my_rank=%d  espo=%lf\n",icycl,my_rank,espo);	
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			acc=exp(espo);
			
			//  	    acc2=exp(beta2[my_rank]*deltaP+dW);
			//acc2=acc2/(1+acc2);
			
			#ifdef SIGMA_ON
			if ((perm_flag==1)&&(sigma_flag==1)) {
			  ///fprintf(out,"max_perm=%d    perm_flag=%d      network->order+deltaP=%f\n",max_perm,perm_flag,network->order+deltaP);fflush(out);
			  sampling(network->Energy+deltaerg,network->order+deltaP,espo+espo1);
			  sampling(network->Energy,network->order,espo1);
			  Order_Conf_Sampling(network->Energy+deltaerg,network->order+deltaP);
			  
			  N_minFE_Sampling(network->Energy+deltaerg,network->order+deltaP,espo+espo1);
			  O_minFE_Sampling(network->Energy,network->order,espo1);
			}
			#else
			sampling(network->Energy+deltaerg,network->order+deltaP,espo+espo1);
			sampling(network->Energy,network->order,espo1);
			Order_Conf_Sampling(network->Energy+deltaerg,network->order+deltaP);
			
			N_minFE_Sampling(network->Energy+deltaerg,network->order+deltaP,espo+espo1);
			O_minFE_Sampling(network->Energy,network->order,espo1);
                        #endif
			
			if((ran3(&seed)<acc) ){
				
				network->Energy=network->Energy+deltaerg;
				/*for(k=0;k<N;k++){
					network->genome[k].Energy=Eni[k];
					
				}*/
				for(k=0;k<S;k++){    	    	    
				  ///network->types[k]=network->ntypes[k];
					network->p[k]=network->np[k];
				}
				network->order=network->order+deltaP;
				network->sigma=new_sigma;
				network->matrix_ave=new_ave;
				///if ((perm_flag==1)&&(sigma_flag==1)) fprintf(out,"giustaaaaaaa accettata, perm=%lf network->tmp_sigma=%lf\n",network->order,network->tmp_sigma );
				fseq_accepted_total++;
				if ((perm_flag==1)&&(sigma_flag==1)) fseq_accepted_constraint++;
			}else{
				network->genome[i].identity=reso;
				for(k=0;k<S;k++){
				  ///network->ntypes[k]=network->types[k];
				  network->np[k]=network->p[k];
				}
				if ((perm_flag==1)&&(sigma_flag==1)) fseq_rejected_constraint++;
				fseq_rejected_total++;
			}
			
		}
	}
	
	network->genome[i].old_identity=network->genome[i].identity;
	
}					
void Pswap (void){
	
	
	/************************************************
	* Pswap --  randomly swap the type of two 	    * 
	*	    	random particles    	    	    *
	*	        	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	void	    		    	    	    *
	*************************************************/
	
	
  int err=0,sim=0;
	int i=0,j=0,k,res[2];
	double acc;
	double En=0.0,E2n=0.0,Etest=0.0,E2test=0.0;
	static double *Eni=NULL;
	double deltaerg=0.0,dW=0;
	long int index,indexO;
	double espo,espo1;
	if(Eni==NULL) Eni=(double *)malloc(ProtN*sizeof(double));
	
	
	i=(int)(ran3(&seed)*(N-1))+1;
	j=(int)(ran3(&seed)*(N-1))+1;
	
	En-=energy_SP_pswap(network->genome[i].identity,i,network->genome[j].identity,j);
	E2n=-E2n;
	network->genome[i].old_identity=res[0]=network->genome[i].identity;
	network->genome[j].old_identity=res[1]=network->genome[j].identity;
	for(k=0;k<N;k++){
		if((k==i)||(k==j)){
			if(k==i){
				err=initseq[k]-res[1];
			}else{
				err=initseq[k]-res[0];
			}
		}else{
			err=initseq[k]-network->genome[k].identity;
		}
		if(err!=0) sim++;
	}
	//fprintf(out,"Pswap OK 1\n");fflush(out);
	if(sim<=treshold){
		
		
		network->genome[i].identity=res[1];
		network->genome[j].identity=res[0];
		
		En+=energy_SP_pswap(network->genome[i].identity,i,network->genome[j].identity,j);
		/*En[0]=energy(network->genome[i].identity,i);
		En[1]=energy(network->genome[j].identity,j);
		deltaerg=2*((En[0]+En[1])-(network->genome[i].Energy+network->genome[j].Energy));*/
		
		#ifdef TEST				
		for(k=1;k<N;k++){
			Etest+=energy(network->genome[k].identity,k);
			//En=En+Eni[k];				    				
		}
		if(fabs(Etest-(network->Energy+En))>1e-5){
			fprintf(out,"Pswap %llu Etest failed Etest=%lf En=%lf \n",icycl,Etest,network->Energy+En);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		#endif
		deltaerg=(En);
		index=lround((network->Energy+deltaerg)*E_Bin+SIZE/2);
			if(index<0) index=0;
			if(index>=SIZE) index=SIZE-1;
			indexO=lround(network->Energy*E_Bin+SIZE/2);
			if(indexO<0) indexO=0;
			if(indexO>=SIZE) indexO=SIZE-1;
		//fprintf(out,"Pswap OK 2 index=%lu indexO=%lu \n",index,indexO);fflush(out);
		/*if(lround(network->Energy+deltaerg)>=SIZE/2.0){
			fprintf(out,"Fseq %llu cazzo Ener e' troppo grande %f %f %d beta2=%f\n",icycl,deltaerg,SIZE/2.0,lround(network->Energy+deltaerg),beta2[my_rank]);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}

		if(lround(network->Energy+deltaerg)<-SIZE/2.0){
			fprintf(out,"Fseq %llu cazzo Ener e' troppo piccola %f %f %d beta2=%f\n",icycl,deltaerg,-SIZE/2.0,lround(network->Energy+deltaerg),beta2[my_rank]);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		if(lround(network->order)>size){
			fprintf(out,"Cavolo Pswap 2  %llu network->order=%lf  size=%d\n",icycl,network->order,size);	
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(lround(network->order)<0){
			fprintf(out,"Cavolo Pswap 2  %llu network->order=%lf  size=%d\n",icycl,network->order,size);	
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		dW=Wpot[index][lround(network->order)]-Wpot[indexO][lround(network->order)];
		//fprintf(out,"Pswap OK 3\n");fflush(out);
		espo=-beta2[my_rank]*(deltaerg)+dW;
		if(espo<50.0){
			espo1=-log(1+exp(espo));
		}else{
			espo1=-espo;
		}

		if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
			fprintf(out,"Cavolo Pswap 1  %llu my_rank=%d  espo=%lf\n",icycl,my_rank,espo);	
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}

		acc=exp(espo);
		
		//fprintf(out,"Pswap OK 4\n");fflush(out);
#ifdef SIGMA_ON
				
		if ((fabs(network->sigma-P_sigma)<1e-2)&&(fabs(network->matrix_ave-ave)<1e-3)){
		  sigma_flag=1;
		  matrix_on++;
		  if (fabs(network->sigma-P_sigma)<1e-2) sigma_on++;
		  if (fabs(network->matrix_ave-ave)<1e-3) ave_on++;
		  ///fprintf(out,"sigma raggiunta!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}else{
		  matrix_off++;
		  sigma_flag=0;
		}
#endif
		if(network->order > max_perm){
		  perm_flag=0;
		}else{
		  perm_flag=1;
		 		  
		}
#ifdef SIGMA_ON
			if ((perm_flag==1)&&(sigma_flag==1)) {
			  sampling(network->Energy+deltaerg,network->order,espo+espo1);
			  sampling(network->Energy,network->order,espo);
			  Order_Conf_Sampling(network->Energy+deltaerg,network->order);
			  
			  N_minFE_Sampling(network->Energy+deltaerg,network->order,espo+espo1);
			  O_minFE_Sampling(network->Energy,network->order,espo);
			}
#else
			sampling(network->Energy+deltaerg,network->order,espo+espo1);
			sampling(network->Energy,network->order,espo);
			Order_Conf_Sampling(network->Energy+deltaerg,network->order);
			
			N_minFE_Sampling(network->Energy+deltaerg,network->order,espo+espo1);
			O_minFE_Sampling(network->Energy,network->order,espo);
#endif
		//fprintf(out,"Pswap OK 5\n");fflush(out);
		
		if(ran3(&seed)<acc){
			
			
			network->Energy+=En;
			network->Energy2+=E2n;
			/*for(k=1;k<N;k++){
				network->genome[k].Energy=Eni[k];
				
			}*/
			pswap_acc++;
			
		}else{
			
			network->genome[i].identity=res[0];
			network->genome[j].identity=res[1];
			pswap_rejected++;
		}
		
		
	} 
	
	network->genome[i].old_identity=network->genome[i].identity;
	network->genome[j].old_identity=network->genome[j].identity;
	//fprintf(out,"Pswap OK 6\n");fflush(out);
}
void Tswap (void){
	
	/************************************************
	* Tswap --  Swap the states at different 	    *
	*	    	temperatures        	    	    *
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	void	    	    	        	    *
	*	    	    	    	    	    	    *
	*************************************************/
	
	int i,ii,a,j,k=0,kk;
	static int kkt=0;
	int tag = 0 ;
	int simil=0,l=0,kkk=2000,simul2;
	double acc=0.0;
	double DP=1,rand,DW,DE,DS,DA;
	double espo,espo1;
	static double *WW1,*WW2;
	int position=0;
	int num_arg=6;
	
	static int *P=NULL;
	int index;
	static double *sendord=NULL;
	static double *recvord=NULL;
	static char *buff=NULL;
	MPI_Status status;
	
	
	if(sendord==NULL) sendord=(double *)malloc((p*num_arg)*sizeof(double));
	if(recvord==NULL) recvord=(double *)malloc((p*num_arg)*sizeof(double));
	if(WW1==NULL) WW1=(double *)malloc(p*p*sizeof(double));
	if(WW2==NULL) WW2=(double *)malloc(p*p*sizeof(double));
	for (i=0;i<p*p;i++){
		WW1[i]=WW2[i]=0;
	}
	
	for (i=0;i<num_arg*p;i++){		
		sendord[i]=0.0;
		recvord[i]=0.0;
	}
	
	sendord[my_rank*num_arg]=network->Energy;
	sendord[my_rank*num_arg+1]=network->order;
	sendord[my_rank*num_arg+2]=network->matrix_ave;
	sendord[my_rank*num_arg+3]=network->Etype;
	sendord[my_rank*num_arg+4]=network->E2type;
	sendord[my_rank*num_arg+5]=network->sigma;
	MPI_Allreduce(sendord,recvord,num_arg*p,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	
	for (j=0;j<p;j++){
		index=lround(recvord[(j)*num_arg]*E_Bin+SIZE/2);
		if(index<0) index=0;
		if(index>=SIZE) index=SIZE-1;
		WW1[my_rank*p+j]=Wpot[index][lround(recvord[(j)*num_arg+1])];
	}
	
	MPI_Allreduce(WW1,WW2,p*p,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	
	for (j=0;j<p;j++){
		if(j!=my_rank){
			DW=WW2[my_rank*p+j]-WW2[my_rank*p+my_rank]+WW2[j*p+my_rank]-WW2[j*p+j];
			DP=recvord[my_rank*num_arg+1]-recvord[j*num_arg+1];
			DE=recvord[my_rank*num_arg]-recvord[j*num_arg];
			DS=(recvord[my_rank*num_arg+5]-P_sigma)*(recvord[my_rank*num_arg+5]-P_sigma)-(recvord[j*num_arg+5]-P_sigma)*(recvord[j*num_arg+5]-P_sigma);
			DA=(recvord[my_rank*num_arg+2]-ave)*(recvord[my_rank*num_arg+2]-ave)-(recvord[j*num_arg+2]-ave)*(recvord[j*num_arg+2]-ave);
			espo=(beta2[my_rank]-beta2[j])*(DE-beta*DP+beta_sigma*DS+beta_ave*DA)+DW;
			///fprintf(out,"tswap DE=%f -beta*DP=%lf beta=%f DP=%lf beta_sigma=%f -beta_sigma*DS=%lf DS=%lf network->tmp_sigma1=%f network->tmp_sigma2=%f P_sigma=%f\n DA=%f beta_ave=%f beta_ave*DA=%f newtork->ave1=%f network->matrix_ave2=%f ave=%f",DE,-beta*DP,beta,DP,beta_sigma,-beta_sigma*DS,DS,recvord[my_rank*num_arg+5],recvord[j*num_arg+5],P_sigma,DA,beta_ave,beta_ave*DA,recvord[my_rank*num_arg+2],recvord[j*num_arg+2],ave);fflush(out);
			if(espo<50.0){
				espo1=-log(1+exp(espo));
			}else{
				espo1=-espo;
			}
			
			if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
				fprintf(out,"Cavolo TSWAP 1  %llu my_rank=%d beta2[my_rank]=%lf beta2[%d]=%lf espo=%lf\n",icycl,my_rank,beta2[my_rank],j,beta2[j],espo);
				fprintf(out,"E_myrank=%lf E_j=%lf\n",recvord[my_rank*num_arg],recvord[j*num_arg]);
				fprintf(out,"WW2[my_rank*p+j]=%lf WW2[my_rank*p+my_rank]=%lf WW2[j*p+my_rank]=%lf WW2[j*p+j]=%lf\n",WW2[my_rank*p+j],WW2[my_rank*p+my_rank],WW2[j*p+my_rank],WW2[j*p+j]);
				
	
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			#ifdef SIGMA_ON
			if ((fabs(recvord[j*num_arg+5]-P_sigma)<1e-2)&&(fabs(recvord[j*num_arg+2]-ave)<1e-3)){
			  sampling(recvord[j*num_arg],recvord[j*num_arg+1],espo+espo1);
			  matrix_on++;
			  if (fabs(recvord[j*num_arg+5]-P_sigma)<1e-2) sigma_on++;
			  if (fabs(recvord[j*num_arg+2]-ave)<1e-3) ave_on++;
			  
			}
			  if ((fabs(recvord[my_rank*num_arg+5]-P_sigma)<1e-2)&&(fabs(recvord[my_rank*num_arg+2]-ave)<1e-3)){
			  sampling(recvord[my_rank*num_arg],recvord[my_rank*num_arg+1],espo1);	  
			}
			#else
			sampling(recvord[j*num_arg],recvord[j*num_arg+1],espo+espo1);
		
			sampling(recvord[my_rank*num_arg],recvord[my_rank*num_arg+1],espo1);	
                        #endif
			
		}    
	}
	
	
	
	
	if(P==NULL) P=(int*)malloc(N*sizeof(int));    	    
	
	
	if(buff==NULL){
		
		
		MPI_Pack_size(N,MPI_INT,MPI_COMM_WORLD,&kk);
		kkt=kkt+kk;
		buff=(char *)malloc(kkt);
	}
	
	for(j=0;j<N;j++){    	  	
		P[j]=0;    	      	
	}
	ntempswap++;
	a=(int)(ran35(&seed2)*2);
	
	rand=ran37(&seed4);
	for (i=a;i<p-1;i+=2){
		if (my_rank==i){
			
			for(j=0;j<N;j++){
				P[j]=network->genome[j].identity;
			}
			
			position=0;
			
			MPI_Pack(P,N,MPI_INT,   	buff,kkt,&position,MPI_COMM_WORLD);
			
			MPI_Sendrecv_replace(buff,kkt,MPI_PACKED,i+1,tag+50,i+1,tag+55,MPI_COMM_WORLD,&status);
			position=0;
			
			MPI_Unpack(buff,kkt,&position,	P,N,MPI_INT,MPI_COMM_WORLD);
			
			DE=recvord[(i+1)*num_arg]-recvord[i*num_arg];
			DP=recvord[(i+1)*num_arg+1]-recvord[i*num_arg+1];
			DW=WW2[(i+1)*p+i]-WW2[(i+1)*p+(i+1)]+WW2[i*p+(i+1)]-WW2[i*p+i];
			DS=(recvord[(i+1)*num_arg+5]-P_sigma)*(recvord[(i+1)*num_arg+5]-P_sigma)-(recvord[i*num_arg+5]-P_sigma)*(recvord[i*num_arg+5]-P_sigma);
			DA=(recvord[(i+1)*num_arg+2]-ave)*(recvord[(i+1)*num_arg+2]-ave)-(recvord[i*num_arg+2]-ave)*(recvord[i*num_arg+2]-ave);
			acc=exp((beta2[i+1]-beta2[i])*(DE-beta*DP+beta_sigma*DS+beta_ave*DA)+DW);
			acc=acc/(1+acc); 


			if (rand<acc){
				tempHisto++;
				for(j=0;j<N;j++) {
					network->genome[j].old_identity=network->genome[j].identity=P[j];
				}
				for(j=0;j<S;j++) {
					network->np[j]=network->p[j]=1;
					///network->types[j]=0;
					
				}
				for(j=1;j<N;j++){
				  network->p[network->genome[j].identity]++;
				}
				/*for(j=1;j<N;j++){
					
					simil=0;		  
					for(k=0;k<l;k++){
						if(P[j]==network->types[k]){
							simil++;
							network->p[k]++;
						}		    
					}		   
					if(simil==0){		     
						network->types[l]=P[j];
						network->p[l]++;
						l++;
					}
					
				}*/
				for(j=0;j<S;j++) {
					
					network->np[j]=network->p[j];
					///network->ntypes[j]=network->types[j];
					
				}
				/*for(ii=1;ii<N;ii++){
					simul2=0;
					for(k=0;k<N;k++){
						if(network->genome[ii].identity==network->types[k]){ 
							simul2++;
							kkk=k;
						}
						
						
					}
					if(simul2==0){
						fprintf(out,"Tswap non e' possibile\n");
						fprintf(out,"network->genome[%d].identity=%d \n",ii,network->genome[ii].identity);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}else{
						if(network->np[kkk]==1){
							fprintf(out," Tswap non e' possibile\n");
							fprintf(out,"%llu network->genome[%d].identity=%d network->np[%d]=%d\n",icycl,ii,network->genome[ii].identity,kkk,network->np[kkk]);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
					}
				}*/
				
				
				network->Energy=recvord[(i+1)*num_arg];
				network->order=recvord[(i+1)*num_arg+1];
				network->matrix_ave=recvord[(i+1)*num_arg+2];
				network->Etype=recvord[(i+1)*num_arg+3];
				network->E2type=recvord[(i+1)*num_arg+4];
				network->sigma=recvord[(i+1)*num_arg+5];
				if(lround(network->order)<0){ fprintf(out,"Tswap %llu cazzo order e' negativo %f\n",icycl,network->order);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				} 
				/*for(j=1;j<N;j++){
					network->genome[j].Energy=energy(network->genome[j].identity,j);
				}*/	
			}
		}
		
		
		if (my_rank==i+1){
			
			
			
			for(j=0;j<N;j++){
				P[j]=network->genome[j].identity;
			}
			position=0;
			
			MPI_Pack(P,N,MPI_INT,   	buff,kkt,&position,MPI_COMM_WORLD);
			
			MPI_Sendrecv_replace(buff,kkt,MPI_PACKED,i,tag+55,i,tag+50,MPI_COMM_WORLD,&status);
			
			position=0;
			
			MPI_Unpack(buff,kkt,&position,	P,N,MPI_INT,MPI_COMM_WORLD);
			
			
			DE=recvord[(i+1)*num_arg]-recvord[i*num_arg];
			DP=recvord[(i+1)*num_arg+1]-recvord[i*num_arg+1];
			DW=WW2[(i+1)*p+i]-WW2[(i+1)*p+(i+1)]+WW2[i*p+(i+1)]-WW2[i*p+i];
			DS=(recvord[(i+1)*num_arg+5]-P_sigma)*(recvord[(i+1)*num_arg+5]-P_sigma)-(recvord[i*num_arg+5]-P_sigma)*(recvord[i*num_arg+5]-P_sigma);
			DA=(recvord[(i+1)*num_arg+2]-ave)*(recvord[(i+1)*num_arg+2]-ave)-(recvord[i*num_arg+2]-ave)*(recvord[i*num_arg+2]-ave);
			acc=exp((beta2[i+1]-beta2[i])*(DE-beta*DP+beta_sigma*DS+beta_ave*DA)+DW);
			acc=acc/(1+acc);
			if (rand<acc){
				tempHisto++;
				for(j=0;j<N;j++) {
					network->genome[j].old_identity=network->genome[j].identity=P[j];
				}
				for(j=0;j<S;j++) {
					network->np[j]=network->p[j]=1;
					///network->types[j]=0;
					
				}
				/*for(j=1;j<N;j++){
					
					simil=0;		  
					for(k=0;k<l;k++){
						if(P[j]==network->types[k]){
							simil++;
							network->p[k]++;
						}		    
					}		   
					if(simil==0){		     
						network->types[l]=P[j];
						network->p[l]++;
						l++;
					}
					
				}*/
				for(j=1;j<N;j++){
				  network->p[network->genome[j].identity]++;
				}
				for(j=0;j<S;j++) {
					
					network->np[j]=network->p[j];
					///network->ntypes[j]=network->types[j];
					
					
				}
				/*for(ii=1;ii<N;ii++){
					simul2=0;
					for(k=0;k<N;k++){
						if(network->genome[ii].identity==network->types[k]){ 
							simul2++;
							kkk=k;
						}
						
						
					}
					if(simul2==0){
						fprintf(out,"Tswap non e' possibile\n");
						fprintf(out,"network->genome[%d].identity=%d \n",ii,network->genome[ii].identity);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}else{
						if(network->np[kkk]==1){
							fprintf(out," Tswap non e' possibile\n");
							fprintf(out,"%llu network->genome[%d].identity=%d network->np[%d]=%d\n",icycl,ii,network->genome[ii].identity,kkk,network->np[kkk]);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
					}
				}*/
				
				network->Energy=recvord[(i)*num_arg];
				network->order=recvord[(i)*num_arg+1];
				network->matrix_ave=recvord[(i)*num_arg+2];
				network->Etype=recvord[(i)*num_arg+3];
				network->E2type=recvord[(i)*num_arg+4];
				network->sigma=recvord[(i)*num_arg+5];
				if(lround(network->order)<0){ fprintf(out,"Tswap %llu cazzo order e' negativo %f\n",icycl,network->order);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				} 
				/*for(j=1;j<N;j++){
					network->genome[j].Energy=energy(network->genome[j].identity,j);
					
				}*/	
			}
			
		}
		
	}    
}

void sampling (double E,double order,double espo){
	
	int index=0,index2=0,indexS=0,i,indexM;
	index=lround(E*E_Bin+SIZE/2);
	if(index<0) index=0;
	if(index>=SIZE) index=SIZE-1;
	index2=lround(order);
	
	
	if(index<SIZE){
		ndata++;
		if(espo>=Whisto[index][index2]){
			
			Whisto[index][index2]=espo+
			log(1+exp(Whisto[index][index2]-espo));
			
		}else{
			Whisto[index][index2]=Whisto[index][index2]+
			log(1+exp(espo-Whisto[index][index2]));
		}
		Wsamp[index][index2]=1;
		
		
		
		if(icycl>Equi1){	    
			
			espo=espo-Wpot[index][index2];	
			
			if(espo>=ndataen){
				ndataen=espo+
				log(1+exp(ndataen-espo));
			}else{
				ndataen=ndataen+
				log(1+exp(espo-ndataen));
			}
			
			
			hsamp[index][index2]=1;
			
			if(index2>size){ fprintf(out,"%llu cazzo index2 e' troppo grande %d %d %f\n",icycl,index2,size,order);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			} 
			
			
			if(index2<0){ fprintf(out,"%llu cazzo index2 e' troppo piccolo %d %f\n",icycl,index2,order);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			} 
			
			 
			
			if(espo>=histo[index][index2]){
				histo[index][index2]=espo+
				log(1+exp(histo[index][index2]-espo));
			}else{
				histo[index][index2]=histo[index][index2]+
				log(1+exp(espo-histo[index][index2]));
			}

			
		}
	}
	
	
}
void W (void){
	int i,j;
	
	/*int minWimean=0,minWjmean=0;*/
	double minW=0,minWmean=0;
	
	static unsigned long long int iter=0;
	double bin_x;
	char message[200];
	FILE *fp;
	
	
	iter++;
	minW_nobias=1e10;
	minW=1e10;
	minWmean=1e10;
	for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			Wpot[i][j]-=umbrella*(Whisto[i][j]);
			if(Wpot[i][j]<minW) {
				minW=Wpot[i][j];
				minW_nobias=beta2[my_rank]*beta*j;
			}
			
		}
	}
	for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			Wpot[i][j]=Wpot[i][j]-minW;
			if(Wpot[i][j]>1e10){
				fprintf(out,"Cavolo Wpot[%d][%d]=%f\n",i,j,Wpot[i][j]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
		}
	}
	/*minSIZE=SIZE;
	minsize=size;
	maxSIZE=0;
	maxsize=0;
	for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			if(Wsamp[i][j]==1){
				if(minSIZE>=i) minSIZE=i;
				if(maxSIZE<=i) maxSIZE=i;
				if(minsize>=j) minsize=j;
				if(maxsize<=j) maxsize=j;
			}
		}
	}*/
	
	/*if(iter==Niteration-1){
		for(i=0;i<SIZE;i++){	
			for(j=0;j<size;j++){	   
				unbias[i][j]=-Wpot[i][j];
				
			}
		}	
	}*/
	/*for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			if(Wsamp[i][j]==1){
				if(minSIZE>=i) minSIZE=i;
				if(maxSIZE<=i) maxSIZE=i;
				if(minsize>=j) minsize=j;
				if(maxsize<=j) maxsize=j;
			}
		}
	}
	fprintf(fHisto_Ord,"# Dy=%d\n",(maxsize-minsize));
	for(i=minSIZE;i<maxSIZE;i++){
		for(j=minsize;j<maxsize;j++){
			
			fprintf(fHisto_Ord,"%5llu %5d %5d %15.10f %15.10f \n",iter,i-(int)(SIZE/2.0),j,Wpot[i][j],Whisto[i][j]);
		}
		fprintf(fHisto_Ord,"\n");
	}
	fprintf(fHisto_Ord,"# icycl=%10llu ndata=%10.5f\n",icycl,ndata);
	fprintf(fHisto_Ord,"\n\n");
	fflush(fHisto_Ord);*/
	sprintf(message,"Wpot-T-%2.3f.dat",beta2[my_rank]);
	fp=fopen(message,"w");
	fwrite(&SIZE,sizeof(int),1,fp);
	fwrite(&size,sizeof(int),1,fp);
	
	for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			
			bin_x=Wpot[i][j];
			fwrite(&bin_x,sizeof(double),1,fp);
			
		}
		
	}
	fclose(fp);
	/* test to write text wpot
	sprintf(message,"Wpot-T-%2.3f-out.dat",beta2[my_rank]);
	fp=fopen(message,"w");
	fprintf(fp,"%d \n",SIZE);
	fprintf(fp,"%d \n",size);
	for(i=0;i<SIZE;i++){
		for(j=0;j<size;j++){
			
			bin_x=Wpot[i][j];
			fprintf(fp,"%15.10f %5d %15.10f\n",(double)(i-SIZE/2)/E_Bin,j,bin_x);
			
		}
		fprintf(fp,"\n");
	}
	fclose(fp);*/
	
}


void O_minFE_Sampling (double E,double order,double espo){
	
	int index=0,index2=0,i,flag,j,flag2;
	static int counter=0;
	index=lround(E*E_Bin+SIZE/2);
	index2=lround(order);
	if((index>0)&&(index<SIZE)){
		if(icycl>Niteration*(Equi2+Equi3)+Equi1){
			
			FE=(Wpot[index][index2]);
			
			if (FE<=Ref_FE){    		    
				for(i=0;i<N;i++){
					minFEseq[i]=network->genome[i].old_identity;
					minFEseqHisto[i][network->genome[i].identity]++;
				}
				if(minFE>=FE){
				minFE=FE;
				minE_FE=E;
				minord_FE=order;
			}
				counter++;
				if((counter==500)&&(Nseq_reduced<Nseq)){
				//espo=espo-Wpot[index][index2]-beta2[my_rank]*beta*order;			
					
					flag=0;
				for(j=0;j<Nseq_reduced;j++){
					flag2=0;
					for(i=1;i<N;i++){
			
								
								if(Sequences_in_FEmin[j][i]!=minFEseq[i]) flag2++;


						}
					if(flag2==0){
						/*if(espo>=seq_list[j]){			
							seq_list[j]=espo+log(1+exp(seq_list[j]-espo));
						}else{
							seq_list[j]=seq_list[j]+log(1+exp(espo-seq_list[j]));
						}
						if(espo>=seq_list_ndataen){
							seq_list_ndataen=espo+
							log(1+exp(seq_list_ndataen-espo));
						}else{
							seq_list_ndataen=seq_list_ndataen+
							log(1+exp(espo-seq_list_ndataen));
						}	*/				
						flag=1;	
						j=Nseq_reduced;					
					}
				}
					if(flag==0){
						
						if(Nseq_reduced<Nseq){
							for(i=0;i<N;i++) {
								Sequences_in_FEmin[Nseq_reduced][i]=minFEseq[i];								
							}
							Sequences_in_FEmin_orders[Nseq_reduced][0]=E;
							Sequences_in_FEmin_orders[Nseq_reduced][1]=order;
							seq_list[Nseq_reduced]=histo[index][index2];
							seq_list_ndataen=ndataen;
							/*if(espo>=seq_list[Nseq_reduced]){			
								seq_list[Nseq_reduced]=espo+log(1+exp(seq_list[Nseq_reduced]-espo));
							}else{
								seq_list[Nseq_reduced]=seq_list[Nseq_reduced]+log(1+exp(espo-seq_list[Nseq_reduced]));
							}
							if(espo>=seq_list_ndataen){
								seq_list_ndataen=espo+
								log(1+exp(seq_list_ndataen-espo));
							}else{
								seq_list_ndataen=seq_list_ndataen+
								log(1+exp(espo-seq_list_ndataen));
							}	*/							
						}
						Nseq_reduced++;
					}
				
					//writeBinaryMin(fMinFEseq);
					counter=0;
				}
			}
			
			
		}  
	}  	
	
}

void N_minFE_Sampling (double E,double order,double espo){
	
	int index=0,index2=0,i,flag,j,flag2;
	static int counter=0;
	index=lround(E*E_Bin+SIZE/2);
	index2=lround(order);
	if((index>0)&&(index<SIZE)){
		if(icycl>Niteration*(Equi2+Equi3)+Equi1){
			
			FE=(Wpot[index][index2]);
			if (FE<=Ref_FE){    		    
				for(i=0;i<N;i++){
					minFEseq[i]=network->genome[i].identity;
					minFEseqHisto[i][network->genome[i].identity]++;
				}
				minFE=FE;
				minE_FE=E;
				minord_FE=order;
				counter++;
				if((counter==500)&&(Nseq_reduced<Nseq)){
				//espo=espo-Wpot[index][index2]-beta2[my_rank]*beta*order;		
				
					
					flag=0;
				for(j=0;j<Nseq_reduced;j++){
					flag2=0;
					for(i=1;i<N;i++){
			
								
								if(Sequences_in_FEmin[j][i]!=minFEseq[i]) flag2++;


						}
					if(flag2==0){
						/*if(espo>=seq_list[j]){			
							seq_list[j]=espo+log(1+exp(seq_list[j]-espo));
						}else{
							seq_list[j]=seq_list[j]+log(1+exp(espo-seq_list[j]));
						}
						if(espo>=seq_list_ndataen){
							seq_list_ndataen=espo+
							log(1+exp(seq_list_ndataen-espo));
						}else{
							seq_list_ndataen=seq_list_ndataen+
							log(1+exp(espo-seq_list_ndataen));
						}	*/				
						flag=1;	
						j=Nseq_reduced;					
					}
				}
					if(flag==0){
						
						if(Nseq_reduced<Nseq){
							for(i=0;i<N;i++) {
								Sequences_in_FEmin[Nseq_reduced][i]=minFEseq[i];								
							}
							Sequences_in_FEmin_orders[Nseq_reduced][0]=E;
							Sequences_in_FEmin_orders[Nseq_reduced][1]=order;
							seq_list[Nseq_reduced]=histo[index][index2];
							seq_list_ndataen=ndataen;
							/*if(espo>=seq_list[Nseq_reduced]){			
								seq_list[Nseq_reduced]=espo+log(1+exp(seq_list[Nseq_reduced]-espo));
							}else{
								seq_list[Nseq_reduced]=seq_list[Nseq_reduced]+log(1+exp(espo-seq_list[Nseq_reduced]));
							}
							if(espo>=seq_list_ndataen){
								seq_list_ndataen=espo+
								log(1+exp(seq_list_ndataen-espo));
							}else{
								seq_list_ndataen=seq_list_ndataen+
								log(1+exp(espo-seq_list_ndataen));
							}	*/							
						}
						Nseq_reduced++;
					}
				
					//writeBinaryMin(fMinFEseq);
					counter=0;
				}
				
			}
			
			
		} 
	}   	
	
}

void writeBinaryMin (FILE *outFile) {
	
	int id;
	int i,j;
	double x;
	/* open the file we are writing to */
	id=Nseq_reduced;
   fwrite(&id,sizeof(int),1,outFile);
   id=N;
   fwrite(&id,sizeof(int),1,outFile);
	for(i=0;i<Nseq_reduced;i++) {
			for(j=1;j<N;j++) {
				
				id=Sequences_in_FEmin[i][j];
			
					fwrite(&id,sizeof(int),1,outFile);
		
				}
				x=(-seq_list[i]+seq_list_ndataen);
				fwrite(&x,sizeof(double),1,outFile);
		}
	
	
	
	return ;
}

void Order_Conf_Sampling (double E,double order_P) {
  int j=0;
  char message[200];
  FILE *fp=NULL;
  ///char codice[]="OACDEFGHIKLMNPQRSTVWY"; 
  int index_E, index_P;
  index_E=lround(E*E_Bin+SIZE/2);
  index_P=lround(order_P);

  if((index_E<SIZE)&&(index_P<size)){
    if(MinEner[index_P]>=E){
      MinEner[index_P]=E;
      sprintf(message, "OP-%d/O-%04d.seq",my_rank,index_P);
      fp=fopen(message,"w");
      fprintf(fp,"HEADER    E_CA=%lf Permutations=%lf\n",E,order_P);
      for(j=0;j<N-1;j++) {
	fprintf (fp,"%d-",network->genome[j].identity);fflush(out);
      }
      fprintf (fp,"%d ",network->genome[N-1].identity);fflush(out);

      fprintf(fp," %d",N-1);
      fclose(fp);
    }
  }
}
void fastadecoder2 (int *Dec, char *Enc,int Length){
	int i;
	
	for(i=0;i<Length-1;i++){
		switch (Enc[i]) {
			case 'A':
			Dec[i]=1;
			break;
			case 'C':
			Dec[i]=2;
			break;
			case 'D':
			Dec[i]=3;
			break;
			case 'E':
			Dec[i]=4;
			break;
			case 'F':
			Dec[i]=5;
			break;
			case 'G':
			Dec[i]=6;
			break;
			case 'H':
			Dec[i]=7;
			break;
			case 'I':
			Dec[i]=8;
			break;
			case 'K':
			Dec[i]=9;
			break;
			case 'L':
			Dec[i]=10;
			break;
			case 'M':
			Dec[i]=11;
			break;
			case 'N':
			Dec[i]=12;
			break;
			case 'P':
			Dec[i]=13;
			break;
			case 'Q':
			Dec[i]=14;
			break;
			case 'R':
			Dec[i]=15;
			break;
			case 'S':
			Dec[i]=16;
			break;
			case 'T':
			Dec[i]=17;
			break;
			case 'V':
			Dec[i]=18;
			break;
			case 'W':
			Dec[i]=19;
			break;
			case 'Y':
			Dec[i]=20;
			break;
			case 'X':
			Dec[i]=1;
			break;
			default:
			fprintf(out,"errore nel codice della Sequenza Residuo[%d]=%c unknown\n",i,Enc[i]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
	}
}

 void sigma_calculator (double *tmp_sigma,double *tmp_ave){
  int i,j,type,count,sigma_ok=0;
  int *reduced_matrix=NULL;
	
  reduced_matrix=(int *)calloc(S,sizeof(int)); //has the same length as the alphabet and the element i is network->genome[i].identity if the letter is used, 0 otherwise
  *tmp_sigma=0.;
  *tmp_ave=0.;
  for(i=1;i<N;i++){	
    type=network->genome[i].identity;
    reduced_matrix[type]=type;
  }
  count=0;
  for(i=1;i<S;i++){
    for(j=i;j<S;j++){
      if((reduced_matrix[i]!=0)&&(reduced_matrix[j]!=0)){
      *tmp_ave+=M[reduced_matrix[i]][reduced_matrix[j]];
      count++;
      }
      //fprintf(out,"tmp_sigma============%f  count=%d \n", tmp_sigma,count);
    }
  }
  *tmp_ave=*tmp_ave/count;
  //fprintf(out,"P_sigma=%e  tmp_ave=================%e  ave=%e   \n",P_sigma,*tmp_ave,ave);
  
  count=0;
  for(i=1;i<S;i++){
    for(j=i;j<S;j++){
      if((reduced_matrix[i]!=0)&&(reduced_matrix[j]!=0)){
	*tmp_sigma+=(M[reduced_matrix[i]][reduced_matrix[j]]-*tmp_ave)*(M[reduced_matrix[i]][reduced_matrix[j]]-*tmp_ave);
	count++;
	//fprintf(out,"tmp_sigma============%f  count=%d \n", tmp_sigma,count);
      }
    }
  }
  *tmp_sigma=*tmp_sigma/count;
  *tmp_sigma=sqrt(*tmp_sigma);
  ///if (fabs(tmp_sigma-P_sigma)<1e-2) sigma_ok=1;
  //fprintf(out,"icycl=%llu tmp_sigma============%e \n",icycl,*tmp_sigma);
  free(reduced_matrix);
}
	
