/******************************************************
 * 		THIS PROGRAM GENERATE AN INTERACTION MATRIX
 * 		WITH MEAN VALUE 0. IT TAKES AS INPUT THE NUMBER
 * 		OF INTERACTING PARTICLES, THE SEED AND THE WIDTH
 * 		OF THE GAUSSIAN
 * 				     
 *					Valentino Bianco	
 * 		
 * 					4 FEBRUARY 2015			
******************************************************/


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

/******************************************************************/
/**   NECESSARY FOR THE MARCENNE TWISTER RANDOM NUMBER GENERATOR **/
/******************************************************************/

typedef long uint32;

#define N              (624)                 // length of state vector
#define M              (397)                 // a period parameter
#define K              (0x9908B0DFU)         // a magic constant
#define hiBit(u)       ((u) & 0x80000000U)   // mask all but highest   bit of u
#define loBit(u)       ((u) & 0x00000001U)   // mask all but lowest    bit of u
#define loBits(u)      ((u) & 0x7FFFFFFFU)   // mask     the highest   bit of u
#define mixBits(u, v)  (hiBit(u)|loBits(v))  // move hi bit of u to hi bit of v
#define LARGEST_NUMBER 4294967295		 // range of integer number

static uint32   state[N+1];     // state vector + 1 extra to not violate ANSI C
static uint32   *next;          // next random value is computed from here
static int      left = -1;      // can *next++ this many times before reloading

void seedMT(uint32 seed);
uint32 reloadMT(void);
uint32 randomMT(void);

float gausdev();

int main(int argc, char *argv[])
{	
  int 	i, j, alphabet, seed, cont=0;
  double 	sigma, HOH_Burried_Threshold, Hydropathy_Scale, x, num[1000000], avg;
	
	/// those parameters are necessary in order to account for the implicit solvent interaction ... see the SEEK code
	HOH_Burried_Threshold=0.;	
	Hydropathy_Scale=0.;
	
	alphabet=atoi(argv[1]);
	sigma=atof(argv[2]);
	seed=atoi(argv[3]);
	
	seedMT(seed);
	
	printf("%d\n%lf\n%lf\n\n",alphabet,HOH_Burried_Threshold,Hydropathy_Scale);

	do{
	  cont=0;
	  avg=0;
	  for (i=1;i<=alphabet;i++)
	    for (j=i; j<=alphabet;j++){
	      num[cont]=sigma*gausdev();
	      cont++;
	    }
	  for(i=0;i<cont;i++)
	    avg+=num[i];
	  avg=avg/cont;
	      
	}while(fabs(avg)>0.001);

	cont=0;
	for (i=0;i<=alphabet;i++)	{
		printf("\n");
		for (j=i; j<=alphabet;j++)	{
			if (i==0)	
				printf("0.0\n");		/// we set the default interaction with the solvent equal to 0
			else	{
				printf("%lf\n",num[cont]);
				cont++;
			}
		}
	}

	return 0;
}

/***********************************************************/
/** GAUSSIAN NUMBER GENERATOR, WITH MEAN 0 AND VARIANCE 1 **/
/***********************************************************/
float gausdev()	{    //Simpler Gaussian Generator
	float v1, v2, r2, fac;
	do	{
		v1=2.0*(float)randomMT()/LARGEST_NUMBER-1.0;
		v2=2.0*(float)randomMT()/LARGEST_NUMBER-1.0;
		r2=v1*v1+v2*v2;
	} while (r2>=1.0 || r2 == 0.0);
	fac=sqrt(-2.0*log(r2)/r2);
	return (v1/fabs(v1))*v2*fac;
}

/**********************************************/
/** RANDOM NUMBER GENERATOR MARCENNE TWISTER **/
/**********************************************/
void seedMT(uint32 seed)	{
	register uint32 x = (seed | 1U) & 0xFFFFFFFFU, *s = state;
    register int    j;

    for(left=0, *s++=x, j=N; --j;
        *s++ = (x*=69069U) & 0xFFFFFFFFU);
}

uint32 reloadMT(void)	{
    register uint32 *p0=state, *p2=state+2, *pM=state+M, s0, s1;
    register int    j;

    if(left < -1)
        seedMT(4357U);

    left=N-1, next=state+1;

    for(s0=state[0], s1=state[1], j=N-M+1; --j; s0=s1, s1=*p2++)
        *p0++ = *pM++ ^ (mixBits(s0, s1) >> 1) ^ (loBit(s1) ? K : 0U);

    for(pM=state, j=M; --j; s0=s1, s1=*p2++)
        *p0++ = *pM++ ^ (mixBits(s0, s1) >> 1) ^ (loBit(s1) ? K : 0U);

    s1=state[0], *p0 = *pM ^ (mixBits(s0, s1) >> 1) ^ (loBit(s1) ? K : 0U);
    s1 ^= (s1 >> 11);
    s1 ^= (s1 <<  7) & 0x9D2C5680U;
    s1 ^= (s1 << 15) & 0xEFC60000U;
    return(s1 ^ (s1 >> 18));
}

uint32 randomMT(void)	{
    uint32 y;

    if(--left < 0)
        return(reloadMT());

    y  = *next++;
    y ^= (y >> 11);
    y ^= (y <<  7) & 0x9D2C5680U;
    y ^= (y << 15) & 0xEFC60000U;
    return(y ^ (y >> 18));
}

