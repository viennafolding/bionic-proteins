/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) aqueny later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


WITHOUT AVERAGE LINKING NUMBER!

*/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#define _ISOC9X_SOURCE 1
#define _ISOC99_SOURCE 1
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>
void stacksize_()
{
int res;
struct rlimit rlim;

getrlimit(RLIMIT_STACK, &rlim);
printf("Before: cur=%d,hard=%d\n",(int)rlim.rlim_cur,(int)rlim.rlim_max);

rlim.rlim_cur=RLIM_INFINITY;
rlim.rlim_max=RLIM_INFINITY;
res=setrlimit(RLIMIT_STACK, &rlim);

getrlimit(RLIMIT_STACK, &rlim);
printf("After: res=%d,cur=%d,hard=%d\n",res,(int)rlim.rlim_cur,(int)rlim.rlim_max);
} 
 
#define REJECTSELF 1
#define remove_old_water_bonds(pr,i,prt,j,bond) prot[pr].ammino[i].new_water_contacts-=bond;prot[prt].ammino[j].new_water_contacts-=bond;
#define add_new_water_bonds(pr,i,prt,j,bond) prot[pr].ammino[i].new_water_contacts+=bond;prot[prt].ammino[j].new_water_contacts+=bond;

#define Potential_Seq(pr,i,prt,j,energCA) M[prot[pr].ammino[i].residue][prot[prt].ammino[j].residue]*energCA + add_ener	 /// M is the interaction matrix

#define P_Dist(dx) (dx-lround(dx/BoxSize)*BoxSize)
#define N1 (1.0/N)
#define SMAX 99
#ifdef TUBE
#define EXCLUDED_NEIGH 1
#else
#define EXCLUDED_NEIGH 2
#endif
/*#ifdef HP
	#define S 3 
#else
	#ifdef VELCRO
		#define S 4
	#else
		#define S 21
	#endif
	#endif*/

#define ATOM_CA 0
#define ATOM_H 1

#define Nbgs_Max2 24
#define Nbgs_Max 12
#define MAXAM 1
#define MAXPROPAG 0
#define Neigh 27
#define Spring_Constant 0.0
///#define TOPO_MAX 20
#define NEIGH_MAX 200
#define PROGRESS_STEP_SKIP 0				   
#define OLDWATER 1
#define NEWWATER 0
#define SUBSTRATE NPROT //Il substrato sara sempre l'ultima proteina
//#define NPROT NPROT+1 //include il substrato nei Loops
//#define  HOH_Burried_Threshold 16.0 /*Average number of contacts of a non burried aminoacid from Sanne. I have measured around 20 but must be verified*/

#define O_VDWR2 9.2416 /*Van der Waals Radius of Oxygene =1.52 */
#define Alpha 4*PI*2.4 /*proportionality prefactor between the dipole moment and the external field*/
#define DIPOLE_SFC_THRESHOLD 1e-1 /*minimum energy difference to considere self consistent equilibration of the dipoles completed*/
#define DIPOLE_BOND_THRESHOLD -0.0015 /*energy threshold to count dipole bonds*/
#define clust_MAXNEIGH 30 //Cluster maximum number of neighbours
#define clust_maxsize NPROT //Cluster maximum size; The maximum cluster cnanot have more than the total number of chains in it
#define KNOT_END_COND 1.2
#define ALN_PREFACTOR 1./(2*3.141592653589793)

int S=0;
double Mean_CA_Bonds_bin=1.0;
double Mean_H_Bonds_bin=1.0;
double OEND_bin=1.0;
double OALN_bin=1.0;
double bin_energy=1.0;
double Max_energy=0.0;
double Min_energy=0.0;
double ENDMax=10.0;
double dumm1=0.;
int fread_out=0;
int ALN_MAX=0;
char *fgets_out=NULL;
int mossaid=0;
int tube_cont=0;

struct part{
	double x;
	double y;
	double z;
	double E,En;
	int O,C;
	int On,Cn;
	int residue,oorder,ocontact,id;
	int lbw_pr,lbw_i;
	int lfw_pr,lfw_i;
	int lbw_SAW_pr,lbw_SAW_i;
	int lfw_SAW_pr,lfw_SAW_i;
	unsigned long int indice,indice_SAW;
	double old_water_contacts,new_water_contacts;
	int spring_anchor;
	double dipole_x,dipole_y,dipole_z;
	double ndipole_x,ndipole_y,ndipole_z;
};

struct proteina{
	struct part *ammino;
	///char topology[2000];
	///char Otopology[2000];
	double end;
	///int topo_indice;
	double ALN;
};

/**struct part_knot{
	double x;
	double y;
	double z;
	int residue, id;
};

struct proteina_knot{
	struct part_knot *bead;
};**/

struct GlobalSistem{
	double ECA;
  double E;
  double add_ener_tot;
	double EDip,EDip_New;
	int dip_contact,ndip_contact;
	double order;
	int touch;
	int contact;
	double density;
	double nE;
	double packing;
	double norder;
	int ntouch;
	int ncontact;
	int RMSD;
	int Mean_H_Bonds;
	int Mean_CA_Bonds;
	double ndensity;
	double Rmin2,Rint2,Rmax2;
	double Rmin_CAsaw,Rmin_CAsaw2;
	double Rmin,Rint,Rmax;
	double RintD,RintD2;
	double Rmin6,Rmin12,Bbond[6],RintH2,RintH,DotProd[3];
	char Atoms[5][10];
	char Amminoacids[SMAX][4];
	int count;
	double NP;
	int p[SMAX];
	int np[SMAX];
	int **Cluster_Index; //List of clusters
	int *Cluster_Size; // Cluster sizes
	int Cluster_N; // Number of clusters
	double *clust_CM_x;
	double *clust_CM_y;
	double *clust_CM_z; // clusters Center of Mass
	///double Topo_indice;
	double ALN_tot;
	double End;
  double Emax_H;
  double Emin_H;
};

/********DIPOLE PARAMETERS*********/
double E_Dip=0; // On Off switch
double DIP_Scale=0; // alpha^2 E^2 /(4 pi epsilon0)

/*********PATCHY PARAMETER*********/
int NSPOTS=4,NATOM=5;
int SPRING_MODEL=1;
int SPG_PATCH2=4;
double CA_Scale=1.0; /*Is 2.0 because the average energy for a designed seqeunce is aroung 300 for lenght 50 and is 600 if we use -1 for all contacts*/

/********************************/
/***********SEQEUNCE EVOLUTION***********/
void fseq (void );
void Pswap (void );
double Seq_energy_SP (int pr, int i);
double Seq_energy_SP_pswap(int pr, int i, int ii);
/****************************************/

double 	BestH=-100000.0, BestHO=100000.0;
double 	MinEO=100000.0, MinEH=100000.0;
double 	MinE=100000.0, BestHE=100000.0;
double 	HalfN2=240.25;
double  shift_potential_h=0.0, shift_potential_h2=0.0;
double 	Bond_factor=10, Bond_extension=1.0;
double 	**DistMap1=NULL, **DistMap2=NULL;
double 	MINR=1000.0;
double 	SHIFT=4.0*5.0/6.0;
double 	sigmahb12=0, sigmahb10=0, sigma6=0, sigma_tube6=0, sigma_tube12=0;
double 	*beta=NULL;
double 	umbrella=0.2, Enative=0;
double 	Rmin[SMAX], Rmin3[SMAX], Rmin_init=2.0;	/// Rmin[S] cosa é ????
double 	Numberofsubstrates=0;
double 	LJpower=3.0;
double 	beta2=0;
double 	HOH_Burried_Threshold=16.0;
double 	MAX_ERROR=10000.0;
double 	subwaterinter=0,TempBias=0;
double 	HydrogenBond1=0,HydrogenBond2=0,HydrogenBond3=0,HydrogenBond4=0,HydrogenBond_r=5.0,HydrogenBond5=0,HydrogenBond6=0;
double 	EHmin=0,EHmin2=0;
double 	LJ2_a=0,LJ2_b=0,LJ2_c=0;
double 	HB_E_Scale=0,CA_E_Scale=0;	
double 	bin_x=0;
double 	PI=0, PI_2=0, PI2=0, PI_180=0;
double 	BoxSize=0, BoxSize2=0;
double 	EH_Power=2.0, ECA_Range=7.0, ECA_Range2, RMS_Range=1.0;
double 	EH_LJPower1=12.0, EH_LJPower1_2=6.0, EH_LJPower2=3.0, EH_LJPower2_2=1.5;
double 	*rndataen2=NULL, *ndataen2=NULL, *Density=NULL, *rDensity=NULL;
double 	*Whisto=NULL, *histoC=NULL, ***Wpot=NULL, *rhistoC=NULL, *histoSC=NULL, *rhistoSC=NULL,*rhistosc_norm=NULL,*histosc_norm=NULL;
/** Non calncellare C&V **/
/** double 	*histo_Topo=NULL, *Whisto_Topo=NULL, ***Wpot_Topo=NULL,*rhisto_Topo=NULL, *histo=NULL, *histofill=NULL, *rhisto=NULL, *rhistofill=NULL; **/
double  *histo_energy=NULL, *rhisto_energy=NULL;
double 	*histo_ALN=NULL, *Whisto_ALN=NULL, ***Wpot_ALN=NULL, *rhisto_ALN=NULL;
double 	**MinEner=NULL;
double 	*ntempswap=NULL, *temthetasto=NULL;
double 	*rntempswap=NULL, *rtemthetasto=NULL;
double 	*rootminE=NULL, *rootminO=NULL, *rootminEH=NULL, *rootminEO=NULL, *rootminOH=NULL, *rootminOE=NULL, nmean=0;
double 	**M=NULL;
double  test_dist;
char 	VFlag=0;
char 	**Seq=NULL;
char 	*SubSeq=NULL;

long 	seed=-1;
long 	Psize=0;

int 	process=0, Rmin_flag=1;
int 	N=30, SubN=0, NPROT=3, TOTLENGTH=90, ProtN=30, HalfN=15, flipped=0, LBox=3;
int 	*Plength=NULL, *Plength_half=NULL;
int 	Window_Flag=0;
int 	Window_Inc=200, Window_Inc2=0;
int 	betaindice=0;
int 	Ustart_Flag=0;
int 	NLINKS=0;
int 	native1=0,	native2=0;
int 	minO=0,	maxO=0, maxE=0, minE=0;
int 	sizeZ=0;
int 	sizex=0,sizeC=0,p=0,my_rank=0,err5=0,spacing=2;
int 	sizeEnd=0,sizeALN=0;	///sizeTopo=0
int     sizeEnergy=0; 

unsigned long int ntest=0, icycl=0, Niteration=0;
unsigned long int ncycl=0, dur=0, deltacycle=0;
unsigned long int Equi1=0, Equi2=0, Equi3=0, nsamp=0;

struct proteina *prot=NULL;
struct proteina *old_prot=NULL;
struct proteina *bestOprot=NULL;
struct proteina *maxOprot=NULL;
struct proteina *minprot=NULL;
struct proteina *sub=NULL;
struct GlobalSistem *sist=NULL;

FILE *fSeq=NULL, *fEner=NULL, *fTempControl=NULL, *fHisto_Ord=NULL, *out=NULL, *EHout=NULL, *EHout2=NULL, *ECAout=NULL, *EDOut=NULL;
FILE *Accepted=NULL, *Rejected=NULL;

MPI_Op myExpSum;

/*****************CELL LIST******************/
unsigned int *hoc_CA=NULL;
unsigned int *hoc_H=NULL;
unsigned int *hoc_SAW=NULL;
double cell_CA_size=0,cell_SAW_size=0,cell_H_size=0,cell_O_size=0;
double cell_CA_factor=0,cell_SAW_factor=0,cell_H_factor=0,cell_O_factor=0;
int N_SAW_cells=0, N_SAW_cells2=0, N_SAW_cells3=0, N_CA_cells=0,N_H_cells=0,N_O_cells=0;
double *neighcell_CA=NULL,*neighcell_H=NULL,*neighcell_SAW=NULL;
int *neighcells=NULL;
double period_factor_CA=0,period_factor_H=0,period_factor_SAW=0;
double inv_period_factor_CA=0,inv_period_factor_H=0,inv_period_factor_SAW=0;
/********************************************/
/******************Water*******************/
int NWater_Changed=0;
int *Water_Changed_pr=NULL,*Water_Changed_i=NULL;
/********************************************/
/************RMSD****************************/
int *RMSD_neigh=NULL,*RMSD_Neigh=NULL;
double DNPROT=0;
int AmminoN=0;
/********************************************/
/******KNOT ID from Mark*********************/
/** C&V
void whatknot(struct proteina_knot *config, long npart, char *topology, int *topo_indice);
struct proteina_knot *prot_knot=NULL;
int indice_knot=0; **/
///void Linking_Number ( struct proteina *polymer, int polymer_indice, int atom_CA, int node1, int node2, int plength, int natom, char *c, double *aln_partial);
void Linking_Number ( struct proteina *, int,  int, int, int, int, int, char *, double *);

/********************************************/


/*************Platonic Solids***************/
void Tetrahedron (double **vertices);
void Cube (double **vertices);
void Dodecahedron (double **vertices);
void Octahedron (double **vertices);
void Icosahedron (double **vertices);
/********************************************/
void Rotation_Random_Sphere (int pr,int k);
char * strcpy ( char * destination, const char * source );
int partint (double e);
void native (void);
void finit	(void);

/*************Bond energy functions***************/
double Bond_Energy_fw (int pr,int input_k);
double Bond_Energy_bw (int pr,int input_k);
double Bond_Energy_SP (int pr,int input_k);

/*************Interaction energies***************/
double energy (void);
double energy_SP_brot_local (int pr,int i,int min,int max,int old);
double energy_SP (int pr,int i,int old);
double energy_SP_Pivot_fw (int pr,int i,int old);
double energy_SP_Pivot_bw (int pr,int i,int old);
/********************************************/

/*************Self-avoiding Checks***************/
double energy_SAW (int pr,int i);
double energy_SAW_All_rot_trasl (int pr,int i);
double energy_SAW_output (int pr,int i);
double energy_SAW_bw (int pr,int i);
double energy_SAW_SP (int pr,int i);
void Overalp_Test (int err_pos, int reject_status);
void Perm_Test (int err_pos);
double E_Water_Old (void);
double E_Water_New (void);
void Water_Update_Accept (void);
void Water_Update_Reject (void);
void Water_Update_Test (void);
/********************************************/

void fsamp		(void);
void rotor		(void);
void trasl		(void);
void rotor2 	(void);
void flip 		(void);
void hanch		(void);
void Tswap		(void);
void order 		(void);
void order_SP (int pr, int i);
void order_test(int *C, double *O, double *E);
void order_test_water(double *E);
double gasdev(long *idum);
int Cluster_Builder (int **clust_indx, int *clust_size);
void Minimum_Energy (double E,int order,int contact);
void Order_Conf_Sampling (double E,int order,int contact);

void adjustCCAN_bw (int pr,int input_k);
void adjustCCAN_fw (int pr,int input_k);
int MC_SP_trasl (int pr);
int MC_SP_trasl_fseq (int pr);
int MC_SP_rot(int pr);
int MC_rot(int pr);
int MC_All_trasl (int pr);
int MC_Cluster_trasl (void );
int MC_All_rot (int pr);
int MC_Pivot_fw(int,int);
int MC_Crankshaft(int);
int MC_Crankshaft_2 (int pr);
int MC_Pivot_bw(int,int);
int MC_trot_fw(int);
int MC_trot_bw(int);
void MC_brot_bgs(int pr);
void Rescaler (void);
void Inflate (void);

/*****************CELL LIST******************/
void new_cell_list (void);
void new_cell_list_SAW (void);
void updt_cell_list (int pr, int i);
void updt_cell_list_SAW (int pr, int i);
void test_celllist_SAW(int dove);
void test_celllist_CA(void);
void test_celllist_H(void);
int  P_Cell (int x, int N_cels);
double  P_Cd (double x);
/*******************************************/

/************RMSD****************************/
double RMSD_CA_local(int pr,int i,int min, int max);
double RMSD_CA_SP (int pr,int i);
double RMSD_CA_fw (int pr,int i);
double RMSD_CA_bw (int pr,int i);
/*******************************************/
double Hydropathy_Scale=1.0;

/********Dipoles*******/

double Dipole_Self_Cons (void);
void Dipole_Accept(void);
void Dipole_Reject(void);
/*******************************************/

double Potential_H (int pr,int i,int prt,int j,int *touch );
double Potential_Dip (int pr,int i,int prt,int j,int *touch );
double Potential (double r,int pr,int i,int prt,int j,int resi,int resj);
double Potential_CA (int pr,int i,int prt,int j,double *add_ener);
void W		(void);
void W_Topo	(void);
double gasdev		(long *idum);
double ran3		(long *idum);
double factorial(int Nfact);
long lround(double x);
void fastadecoder 	(char **Dec, char **Enc);
void fastadecoder2 (char *Dec, char *Enc);
void sampling 		(double density, double E,int order,int touch,int contact,int end, int topo, double espo);
void order_packing (void);
void ExpSum (void *in,void *inout, int *len, MPI_Datatype *dptr);
void Rotation (int pr,int k,double alpha,double u1,double u2,double u3,int flag);
int writeBinary(FILE *);
int readBinary(FILE *);
int write_psf(FILE *fPSFvmd);

unsigned long int local_accepted=0,all_trasl_accepted=0,all_rot_accepted=0,cluster_trasl_accepted=0,rot_accepted=0,trasl_fseq_accepted=0,trasl_accepted=0,fw_accepted=0,bw_accepted=0,fseq_accepted=0;
unsigned long int local_rejectedener=0,local_rejectedself=0,all_trasl_rejectedself=0,all_trasl_rejectedener=0,all_rot_rejectedself=0,all_rot_rejectedener=0,cluster_trasl_rejectedener=0,cluster_trasl_rejectedself=0,rot_rejectedener=0,trasl_fseq_rejectedself=0,trasl_fseq_rejectedener=0,trasl_rejectedself=0,trasl_rejectedener=0,fw_rejectedself=0,fw_rejectedener=0,bw_rejectedself=0,bw_rejectedener=0,fseq_rejectedener=0;
unsigned long int brot_local_tried=0,trasl_tried=0,trasl_fseq_tried=0,rot_tried=0,bw_tried=0,fw_tried=0,fseq_tried=0,all_trasl_tried=0,all_rot_tried=0,cluster_trasl_tried=0;
double EH_Range=0.5;
double BGS_Av_Displacement=0.0,BGS_Sigma_Displacement=0.0,BGS_Trial_Displacement=0.0; 
double BGS_Av_Displacement2=0.0,BGS_Sigma_Displacement2=0.0;
double starttime=0,endtime=0;
double HR_Angle1=90.0,HR_Angle2=90.0;
int minH,maxH;
int IminH,ImaxH,IminO,ImaxO;
int SminH,SmaxH,SminO,SmaxO;
int JOBID;
int second_minimum_CA=0;
double shift_potential_CA=0.0,shift_potential_CA2=0.0,zero_potential_CA2=0.0;
int main(int argc, char** argv){
#define sigma_tube 6.5
#define Rtube 81
  double r2=0.,r6=0.,r12=0.,energhor=0.,energh=0.,Emin_h=0.,Emax_h=0.;
	double OEnd_Old=0, OALN_Old=0; ///OTopo_indice_Old=0;
	int self_rejected_flag=0;
	int mossa=0;
	#ifdef 	ORDER_TEST_WATER
	int ORDER_TEST_WATER_FLAG=0;
	#endif
	int traslator=3.0;
	int ii=0,i=0,j=0,k=0,pr=0,ppr=0,kk=0,last=0;
	double pass=0,R=0;
	double totalvolume=0.0;
	int PTFreq=0;
	double dx=0,dy=0,dz=0,rCA2_end=0, aln_partial=0.;
	char message[50],line[50], info_ALN[20];
	int testC=0;
	double testE=0,testO=0;
	double testEWater=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	FILE *fp=NULL;
	FILE *fHisto_Topo=NULL,*fHisto_contact=NULL,*fHisto_contacta=NULL;
	/// FILE *fHisto_2Dnt=NULL,*fHisto_2Dt=NULL,*fHisto_3Dnt=NULL,*fHisto_3Dt=NULL;
	FILE *fHisto_2D_Energy=NULL,*fHisto_entropy=NULL,*fHisto_entropy_txt=NULL;
	MPI_Init(&argc,&argv);
	printf("Init Init\n");fflush(NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	stacksize_();
	starttime=MPI_Wtime();
		
	#ifdef RUN_ON_SCRATCH	
	fp=fopen("jobid.dat","r");	
	fgets_out=fgets(line,sizeof(line),fp);
	sscanf (line,"%d\n",&JOBID);
	printf("JobID=%d\n",JOBID);
	fflush(NULL);
	sprintf(message,"/scratch/ic247/%d/",JOBID);
	
	if(chdir(message)!=0){
		printf("%d %s does not exists\n",my_rank,message);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	fclose(fp);	
	#endif		
	Window_Inc2=Window_Inc/2;
	sprintf(message,"OUT-P-%d.dat",my_rank);
	out=fopen(message,"w");
	
	sprintf(message,"EHOUT-P-%d.dat",my_rank);
	EHout=fopen(message,"w");
	sprintf(message,"EDOut-P-%d.dat",my_rank);
	EDOut=fopen(message,"w");
	
	sprintf(message,"EHOUT2-P-%d.dat",my_rank);
	EHout2=fopen(message,"w");
	sprintf(message,"ECAOUT-P-%d.dat",my_rank);
	ECAout=fopen(message,"w");
	
	sprintf(message, "Accepted-P-%d.dat",my_rank);
	Accepted=fopen(message,"w");
	sprintf(message, "RejectedP-%d.dat",my_rank);
	Rejected=fopen(message,"w");
	
	fprintf(Accepted,"#Temp,icycl,all_trasl_accepted,all_trasl_tried,all_rot_accepted,all_rot_tried,rot_accepted,rot_tried,trasl_fseq_accepted,trasl_fseq_tried,trasl_accepted,trasl_tried,fw_accepted,fw_tried,bw_accepted,bw_tried,fseq_accepted,fseq_tried\n");

	fprintf(Rejected,"#Temp icycl local_rejectedener local_rejectedself all_trasl_rejectedself all_trasl_rejectedener all_rot_rejectedself all_rot_rejectedener rot_rejectedener trasl_fseq_rejectedself trasl_fseq_rejectedener trasl_rejectedself trasl_rejectedener fw_rejectedself fw_rejectedener bw_rejectedself bw_rejectedener fseq_rejectedener\n");
	
	PI=atan(1)*4;
	PI_2=atan(1)*2;
	PI2=atan(1)*8;
	PI_180=atan(1)*4/180;
	MPI_Op_create(&ExpSum,1,&myExpSum);
	sist= (struct GlobalSistem *)calloc(1,sizeof(struct GlobalSistem));
	
	fp=fopen("aapot.dat","r");
	if ( fp == NULL) {
	  printf ("File aapot.dat not found!!!\n");
	  fflush(NULL);
	  MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
	  fscanf(fp,"%d",&S);
	  S=S+1;// we include into the alphabet also the solvent molecule
	  fclose(fp);
	}

	if(S>SMAX){
		fprintf(out,"THE ALPHABET IS TOO BIG!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	fp=fopen("patches_structure.dat","r");
	fscanf(fp,"%d",&NSPOTS);
	SPG_PATCH2=NSPOTS;
	fclose(fp);


	M=(double**)calloc(S,sizeof(double*));
	if (M == NULL) {
		fprintf(out,"Allocation M Failed\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	for(i=0;i<S;i++){
		M[i]=(double*)calloc(S,sizeof(double));
		if (M[i] == 0) {
			fprintf(out,"Allocation M[%d] Failed\n",i);
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}
		
	beta=(double *)calloc(p,sizeof(double));
	if(beta==NULL){
		fprintf(out,"allocation of beta failed in main\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/************************************************
	* 	Raccogliamo i parametri per la simulazione	*
	*************************************************/
	fp=fopen("param.dat","r");
	if ( fp == NULL) {
		printf ("File param.dat not found!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		while(feof(fp)==0){
			fgets_out=fgets(line,sizeof(line),fp);
			switch (line[0]){
			  case 'F':
			    sscanf (line,"F%d\n",&second_minimum_CA);
			    break;


			case 'M':
			  switch (line[1]){
			  case 'H':
			    sscanf (line,"MH%lf\n",&shift_potential_h);
			    break;
			  case 'C':
			    sscanf (line,"MC%lf\n",&shift_potential_CA);
			    break;
			  }


			case 'A':
			  switch (line[1]){
					case 's':
					sscanf (line,"As%lf\n",&pass);
					Rmin[1]=pass;
					break;
					case 'L':
					sscanf (line,"AL%lf\n",&pass);
					Rmin[2]=pass;
					
					break;
					case '1':
					sscanf (line,"A1%lf\n",&pass);
					HR_Angle1=pass;
					break;
					case '2':
					sscanf (line,"A2%lf\n",&pass);
					HR_Angle2=pass;
					break;
				}
				break;
				case 'N':
				switch (line[1]){
					case 'C':
					sscanf (line,"NC%lu\n",&ncycl);
					break;
					case 'I':
					sscanf (line,"NI%lu\n",&Niteration);
					break;
					case 'P':
					sscanf (line,"NP%d\n",&NPROT);
					DNPROT=(double)(NPROT);
					break;
					/*case 'S':
					sscanf (line,"NS%d\n",&NSPOTS);
					SPG_PATCH2=NSPOTS;
					break;*/
				}
				break;
				case 'P':
				sscanf (line,"P%d\n",&PTFreq);
				break;
				case 'C':
				sscanf (line,"C%lu\n",&nsamp);
				break;
				case 'R':
				sscanf (line,"R%ld\n",&seed);
				process=(int)(seed);
				seed=-seed;
				break;
				case 'U':
				switch (line[1]){
					case 'h':
					sscanf (line,"Uh%d\n",&IminH);
					break;
					case 'H':
					sscanf (line,"UH%d\n",&ImaxH);
					break;
					case 'm':
					sscanf (line,"Um%d\n",&IminO);
					break;
					case 'M':
					sscanf (line,"UM%d\n",&ImaxO);
					break;
					case 'E':
					sscanf (line,"UE%lf\n",&Max_energy);
					break;
					case 'e':
					sscanf (line,"Ue%lf\n",&Min_energy);
					break;
					
				}
				break;
				case 's':
				switch (line[1]){
					case 'h':
					sscanf (line,"sh%d\n",&SminH);
					break;
					case 'H':
					sscanf (line,"sH%d\n",&SmaxH);
					break;
					case 'm':
					sscanf (line,"sm%d\n",&SminO);
					break;
					case 'M':
					sscanf (line,"sM%d\n",&SmaxO);
					break;
				}
				break;
				case 'S':
				sscanf (line,"S%d\n",&spacing);
				break;
				case 'D':
				sscanf (line,"D%lf\n",&E_Dip);
				break;
				
				case 'E':
				switch (line[1]){
					case '1':
					sscanf (line,"E1%lu\n",&Equi1);
					break;
					case '2':
					sscanf (line,"E2%lu\n",&Equi2);
					break;
					case '3':
					sscanf (line,"E3%lu\n",&Equi3);
					break;
					
					case 'S':
					sscanf (line,"ES%lf\n",&CA_Scale);
					break;
					case 'D':
					sscanf (line,"ED%lf\n",&DIP_Scale);
					break;
					case 'H':
					switch (line[2]){
						case '1':
						sscanf (line,"EH1%lf\n",&EH_Power);
						break;
						case '2':
						sscanf (line,"EH2%lf\n",&EH_LJPower2);
						break;
						case 'r':
						sscanf (line,"EHr%lf\n",&EH_Range);
						break;
						case 'S':
						sscanf (line,"EHS%lf\n",&HB_E_Scale);
						break;
					}
					break;
					case 'R':
					sscanf (line,"ER%lf\n",&ECA_Range);
					break;
				}
				case 'B':
				switch (line[1]){
					case 'H':
					sscanf (line,"BH%lf\n",&Mean_H_Bonds_bin);
					break;
					case 'C':
					sscanf (line,"BC%lf\n",&Mean_CA_Bonds_bin);
					break;
					case 'D':
					sscanf (line,"BD%lf\n",&OEND_bin);
					break;
					case 'L':
					sscanf (line,"BL%lf\n",&OALN_bin);
					break;
					case 'F':
					sscanf (line,"BF%lf\n",&Bond_factor);
					break;
					case 'E':
					sscanf (line,"BE%lf\n",&bin_energy);
					break;
					case 'S':
					sscanf (line,"BS%lf\n",&BoxSize);
					break;
					case '2':
					sscanf (line,"B2%lf\n",&beta2);
					break;					
					case 'M':
					sscanf (line,"BM%d\n",&SPRING_MODEL);
					break;
				}
				break;
				case 'L':
				sscanf (line,"L%ld\n",&Psize);
				break;
				
				default:
				fprintf(out,"Usage:\n");
				fprintf(out,"N number of cycles \n");
				fprintf(out,"C samplings step \n");
				fprintf(out,"S spacing between substrate patches \n");
				fprintf(out,"R ramdom seed positive \n");
				fprintf(out,"U strenght of the umbrella potential \n");
				fprintf(out,"O position of the order parameter \n");
				fprintf(out,"E1 2 3equilibration delays \n");
				fprintf(out,"I number of multicanonical iterations \n");
				fprintf(out,"L Box Size in chain legth units \n");
				fprintf(out,"P Parallel Tempering Frequency \n");
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
		fclose(fp);
	}
	NATOM=NSPOTS+1;
	ALN_MAX=(Psize-1)/5;
	ECA_Range2=ECA_Range*ECA_Range;
	if(Max_energy<Min_energy){
	  printf("Max_energy<Min_energy check input \n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(ImaxO<IminO){
	  printf("ImaxO<IminO check input \n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(ImaxH<IminH){
	  printf("ImaxO<IminO check input \n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/****This is because the strenght is proportional to the radius cube of the particle size****/
	/* The problem is that this ignores the field genrated by the other dipoles!!!!*/
	#ifdef HP
	#else
	for(i=0;i<S;i++){
		Rmin[i]=Rmin[1];
	}
	#endif
	Rmin3[1]=pow(Rmin[1],3);
	Rmin3[2]=pow(Rmin[2],3);
	
	#ifdef TUBE
	sigma_tube6=pow(sigma_tube,6);
	sigma_tube12=pow(sigma_tube,12);
	#endif
	shift_potential_h2=shift_potential_h*shift_potential_h;
	shift_potential_CA=shift_potential_CA+2.0;
	zero_potential_CA2 = ( sqrt(sigmahb12/sigmahb10) + shift_potential_CA ) * ( sqrt(sigmahb12/sigmahb10) + shift_potential_CA) ;
	shift_potential_CA2=shift_potential_CA*shift_potential_CA;
	sist->Rmin_CAsaw=Rmin[1];
	if(sist->Rmin_CAsaw<Rmin[2]) sist->Rmin_CAsaw=Rmin[2];
	sist->Rmin_CAsaw*=2.0;
	sist->Rmin=sist->Rmin_CAsaw;
		i=1;
	do	{
	        sprintf(sist->Amminoacids[i],"%03d",i);
		i++;
	} while (i<S);
	
	/*************************************Sampling Interval*/
	maxO=Mean_CA_Bonds_bin*ImaxO;
	minO=Mean_CA_Bonds_bin*IminO;
	sizex=maxO-minO;
	sizeEnd=(int)(ENDMax*OEND_bin)+1;
	///sizeTopo=TOPO_MAX;
	sizeALN=(int)(ALN_MAX*OALN_bin)+1;
	maxE=(int)(Max_energy*bin_energy);
	minE=(int)(Min_energy*bin_energy);
	sizeEnergy=maxE-minE;
	
	maxH=Mean_H_Bonds_bin*ImaxH;
	minH=Mean_H_Bonds_bin*IminH;
	sizeC=maxH-minH;
	fprintf(out,"NUMBER OF EXCLUDED NEIGHBOURS from isotropic energy calculation = %d", EXCLUDED_NEIGH);
	fprintf(out,"####################\n");
	fprintf(out,"sizeEnd=%d OEND_bin=%lf sizeALN=%d OALN_bin=%lf \n",sizeEnd,OEND_bin,sizeALN,OALN_bin);
	fprintf(out,"sizex=%d minO=%d sizeC=%d  minH=%d \n",sizex,minO,sizeC,minH);
	fprintf(out,"Mean_CA_Bonds_bin=%lf Mean_H_Bonds_bin=%lf \n",Mean_CA_Bonds_bin,Mean_H_Bonds_bin);
	fprintf(out,"ImaxH=%d ImaxO=%d \n",ImaxH,ImaxO);
	fprintf(out,"IminH=%d IminO=%d \n",IminH,IminO);
	//fprintf(out,"SmaxH=%d SmaxO=%d \n",SmaxH,SmaxO);
	//fprintf(out,"SminH=%d SminO=%d \n",SminH,SminO);
	fflush(out);
	/*****************************************************/
		
	strcpy(sist->Atoms[ATOM_CA],"CA ");
	strcpy(sist->Atoms[ATOM_H],"H  ");
	
	sist->Bbond[0]=1.4500000;
	sist->Bbond[1]=sist->Rmin_CAsaw/2.0;
	
	sist->Rint=ECA_Range+5.0;
	sist->Rmax=sist->Rmin;
	sist->RintD=20.0;
	sist->RintD2=20.0*20.0;
		
	if(HR_Angle1>140) {
		HydrogenBond3=cos((HR_Angle1/180.0)*PI);
	}else{
		HydrogenBond3=cos((140.0/180.0)*PI);
	}
	if(HR_Angle2>140) {
		HydrogenBond4=cos((HR_Angle2/180.0)*PI);
	}else{
		HydrogenBond4=cos((140.0/180.0)*PI);
	}
	
	HydrogenBond1=cos((HR_Angle1/180.0)*PI);
	HydrogenBond2=cos((HR_Angle2/180.0)*PI);
	HydrogenBond_r=EH_Range*1.25; // This is the threshold to recognize a patch bond so I increase by 25% the equilibrium bond distance
	HydrogenBond_r*=HydrogenBond_r;
	
	sist->RintH=6.0;
	
	sist->Rmin2=sist->Rmin*sist->Rmin;
	sist->Rmin_CAsaw2=sist->Rmin_CAsaw*sist->Rmin_CAsaw;
	sist->Rint2=sist->Rint*sist->Rint;
	sist->RintH2=sist->RintH*sist->RintH;
	sist->Rmax2=sist->Rmax*sist->Rmax;
	
	sigma6=pow(sist->Rmin,6.0);
	EHmin=3.1*HB_E_Scale;
	EHmin2=-EHmin/2.0;
	
	EH_LJPower1_2=EH_LJPower1/2.0;
	EH_LJPower2_2=EH_LJPower2/2.0;
	sigmahb10=pow(EH_Range,EH_LJPower2)*EH_LJPower1_2*EHmin/(EH_LJPower1_2-EH_LJPower2_2);
	sigmahb12=pow(EH_Range,EH_LJPower1)*EH_LJPower2_2*EHmin/(EH_LJPower1_2-EH_LJPower2_2);

	r2=EH_Range*EH_Range;
	r6=pow(r2,EH_LJPower2_2);
	r12=pow(r2,EH_LJPower1_2);
	Emin_h=(sigmahb12/r12-sigmahb10/r6);
	
        r6=pow(HydrogenBond_r,EH_LJPower2_2);
        r12=pow(HydrogenBond_r,EH_LJPower1_2);
        energhor=pow(HydrogenBond3*HydrogenBond4,EH_Power);
        energh=(sigmahb12/r12-sigmahb10/r6);
	Emax_h=(energh)*energhor;


	ncycl=ncycl+Equi1+(Equi2+Equi3)*Niteration;
	///dur=partint(ncycl/50);
	///if (dur==0) dur=1;
	
	fprintf(out,"prima di finit ok\n");
	/********************
	* File Creation *
	*********************/
	sprintf(message, "Seq-P-%d-%d.xyz",my_rank,process);
	fSeq=fopen(message,"w");
	
	if(my_rank==0){
	  sprintf(message,"TempControl-P-%d.dat",process);
	  fTempControl=fopen(message,"w");
	  if(fTempControl==NULL){
	    fprintf(out,"fTempControl creation failed\n");fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	}
	sprintf(message, "Ener-P-%d-%d.dat",my_rank,process);
	fEner=fopen(message,"w");
	
	if(fEner==NULL){
		fprintf(out,"fEner creation failed\n");fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if(fSeq==NULL){
		fprintf(out,"fSeq creation failed\n");fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/************************************************/
	
	/************************************
	* 	Inizializiamo le Variabili	* 
	*************************************/
	finit();	/// here we initialize the variables
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	fprintf(out,"finit ok\n");
	fflush(out);
	Perm_Test(112);
	
	sist->E=energy();
	
	fprintf(out,"sist->E=%lf without bond energy \n",sist->E);fflush(out);
	sist->order=sist->norder;      /// order is the energy of CA bonds but without specificity=compactness
	sist->contact=sist->ncontact; /// number of total contacts between H atoms (H-bonds)
	sist->touch=sist->ntouch; /// touch is 1 if there is at least one hydrogen bond
	/// n is the partial order parameter (the part that changes in the moves) and without n is the total one
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;

fprintf(out,"sist->order=%lf sist->contact=%d DNPROT=%lf\n",sist->order,sist->contact,DNPROT);fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			sist->E+=Bond_Energy_fw(pr,i);  /// sist->E is the total energy!
		}
		kk=0;
	}
	fprintf(out,"sist->E=%lf with bond energy\n",sist->E);fflush(out);
	HalfN2=((ProtN+1)/2)*((ProtN+1)/2);
	
	temthetasto=(double *)calloc(p,sizeof(double)); /// these are for acceptance probability in parallel tempering
	if(temthetasto==NULL){ /// temthetasto= number of accepted moves in REM for each betaindice
		fprintf(out,"allocation of roottemthetasto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	ntempswap=(double *)calloc(p,sizeof(double)); /// temthetasto= number of attempted moves in REM for each betaindice
	if(ntempswap==NULL){
		fprintf(out,"allocation of roottemthetasto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	rtemthetasto=(double *)calloc(p,sizeof(double));  /// with r are the same but avaraged on all the processors
	if(rtemthetasto==NULL){
		fprintf(out,"allocation of roottemthetasto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	rntempswap=(double *)calloc(p,sizeof(double));
	if(rntempswap==NULL){
		fprintf(out,"allocation of roottemthetasto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	rootminE=(double *)calloc(p,sizeof(double));
	rootminEH=(double *)calloc(p,sizeof(double));
	rootminO=(double *)calloc(p,sizeof(double));
	rootminOE=(double *)calloc(p,sizeof(double));
	rootminOH=(double *)calloc(p,sizeof(double));
	rootminEO=(double *)calloc(p,sizeof(double));
	
	rDensity=(double *)calloc(3*p,sizeof(double));	
	Density=(double *)calloc(3*p,sizeof(double));
	
	rndataen2=(double *)calloc(p,sizeof(double));	
	ndataen2=(double *)calloc(p,sizeof(double));
	rhistosc_norm=(double *)calloc(p,sizeof(double));
	histosc_norm=(double *)calloc(p,sizeof(double));

	Whisto=(double *)calloc(p*sizeC*sizex,sizeof(double));
	/** NON CANCELLARE: commentato da Chiara e Valo **/
	/** Whisto_Topo=(double *)calloc(p*sizeEnd*sizeTopo,sizeof(double)); **/
	Whisto_ALN=(double *)calloc(p*sizeEnd*sizeALN,sizeof(double));
	
	Wpot=(double ***)calloc(p,sizeof(double **));
	if( Wpot==NULL){
		fprintf(out,"allocation of Wpot failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	for(i=0;i<p;i++){
		Wpot[i]=(double **)calloc(sizeC,sizeof(double*));
		for(j=0;j<sizeC;j++){
			Wpot[i][j]=(double *)calloc((sizex),sizeof(double));	
		}	
	}
	
	/** NON CANCELLARE: commentato da Chiara e Valo **/
	/**Wpot_Topo=(double ***)calloc(p,sizeof(double **));
	if( Wpot_Topo==NULL){
		fprintf(out,"allocation of Wpot_Topo failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	for(i=0;i<p;i++){
		Wpot_Topo[i]=(double **)calloc(sizeEnd,sizeof(double*));
		for(j=0;j<sizeEnd;j++){
			Wpot_Topo[i][j]=(double *)calloc((sizeTopo),sizeof(double));	
		}	
	}**/
	
	Wpot_ALN=(double ***)calloc(p,sizeof(double **));
	if( Wpot_ALN==NULL){
		fprintf(out,"allocation of Wpot_ALN failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	for(i=0;i<p;i++){
		Wpot_ALN[i]=(double **)calloc(sizeEnd,sizeof(double*));
		for(j=0;j<sizeEnd;j++){
			Wpot_ALN[i][j]=(double *)calloc((sizeALN),sizeof(double));	
		}	
	}
	
	MinEner=(double **)calloc(sizeC,sizeof(double*));
	for(j=0;j<sizeC;j++){
		MinEner[j]=(double *)calloc((sizex),sizeof(double));	
	}
	for(i=0;i<sizeC;i++){
		for(j=0;j<sizex;j++){		
			MinEner[i][j]=100000.0;	
		}	 
	}
	
	rhistoC=(double *)calloc(p*sizeC*sizex,sizeof(double ));
	/// rhisto_Topo=(double *)calloc(p*sizeEnd*sizeTopo,sizeof(double ));	
	rhisto_ALN=(double *)calloc(p*sizeEnd*sizeALN,sizeof(double ));
	///rhisto=(double *)calloc(p*2*sizex*1000,sizeof(double ));		
	///rhistofill=(double *)calloc(p*2*sizex,sizeof(double ));
	rhisto_energy=(double *)calloc(p*sizeEnergy,sizeof(double ));
	rhistoSC=(double *)calloc(p*sizeC*sizex,sizeof(double ));

	histoC=(double *)calloc(p*sizeC*sizex,sizeof(double ));
	///histo_Topo=(double *)calloc(p*sizeEnd*sizeTopo,sizeof(double ));
	histo_ALN=(double *)calloc(p*sizeEnd*sizeALN,sizeof(double ));
	///histo=(double *)calloc(p*2*sizex*1000,sizeof(double ));
	///histofill=(double *)calloc(p*2*sizex,sizeof(double ));
	histo_energy=(double *)calloc(p*sizeEnergy,sizeof(double ));
	histoSC=(double *)calloc(p*sizeC*sizex,sizeof(double ));

	deltacycle=Equi1;
	
	fprintf(out,"allocation ok\n");
	for(i=0;i<3*p;i++){
		Density[i]=-1000000.0;
	}
	for(i=0;i<p;i++){
		ndataen2[i]=-1000000.0;
		histosc_norm[i]=-1000000.0;
	}
	
	for(i=0;i<p*sizeC*sizex;i++){
		Whisto[i]=0.0;
		histoC[i]=-1000000.0;
	}
	for(i=0;i<p*sizeC*sizex;i++){
	  histoSC[i]=-1000000.0;
        }
	/**for(i=0;i<p*sizeEnd*sizeTopo;i++){
		Whisto_Topo[i]=0.0;
		histo_Topo[i]=-1000000.0;
	}**/
	for(i=0;i<p*sizeEnd*sizeALN;i++){
		Whisto_ALN[i]=0.0;
		histo_ALN[i]=-1000000.0;
	}
	/*for(k=0;k<p*2*sizex*1000;k++){
		histo[k]=-1000000.0;		
	}
	for(k=0;k<p*2*sizex;k++){
		histofill[k]=-1000000.0;		
	}*/
	for(k=0;k<p*sizeEnergy;k++){
		histo_energy[k]=-1000000.0;		
	}
	for(i=0;i<p;i++){
		temthetasto[i]=0.0;
		ntempswap[i]=1.0;
	}
	fprintf(out,"Histo Init ok\n");
	fflush(out);

	for(k=0;k<p;k++){
		sprintf(message,"Wpot-T-%2.3f.dat",1/beta[k]);
		fp=fopen(message,"r");
		
		if ( fp == NULL) {
			for(i=0;i<sizeC;i++){
				for(j=0;j<sizex;j++){		
					Wpot[k][i][j]=0.0;	
				}	 
			}
		}else{
			fread_out=fread(&i,sizeof(int),1,fp);
			if(sizeC>i){
				fprintf(out,"Init Wpot reading on file %s SizeC higher than previous simulation I use old one old sizeC=%d new sizeC=%d\n",message,i,sizeC);
				sizeC=i;
			}else{
			  if(sizeC<i) {
			    fprintf(out,"Init Wpot reading on file %s SizeC lower than previous simulation old sizeC=%d new sizeC=%d, I go on with new one\n",message,i,sizeC);
			  }
			}
			fread_out=fread(&i,sizeof(int),1,fp);
			if(minH<i){
				fprintf(out,"Init Wpot reading on file %s minH lower than previous simulation I use old one old minHH=%d new minHH=%d\n",message,i,minH);
				minH=i;
			}else{
			  if (minH>i){
			    fprintf(out,"Init Wpot reading on file %s minH higher than previous simulation old minH=%d new minH=%d, I use new one \n",message,i,minH);
			  }
			}
			fread_out=fread(&dumm1,sizeof(double),1,fp);
			if(dumm1!=Mean_H_Bonds_bin){
				fprintf(out,"Init Wpot reading on file %s wrong Mean_H_Bonds_bin I use old one old=%lf new=%lf\n",message,dumm1,Mean_H_Bonds_bin);
				Mean_H_Bonds_bin=dumm1;
			}
		
			fread_out=fread(&i,sizeof(int),1,fp);
			if(sizex>i){
				fprintf(out,"Init Wpot reading on file %s sizex higher than previous simulation I use old one old sizex=%d new sizex=%d\n",message,i,sizex);
				sizex=i;
			}else{
			  if(sizex<i) {
                            fprintf(out,"Init Wpot reading on file %s Sizex lower than previous simulation old sizex=%d new sizex=%d, I use new one \n",message,i,sizex);
			  }
			  
			}
			fread_out=fread(&i,sizeof(int),1,fp);
			if(minO<i){
				fprintf(out,"Init Wpot reading on file %s minO lower than previous simulation old minO=%d new minO=%d I use old one\n",message,i,minO);
				minO=i;
			}else{
			  if (minO>i){
			    fprintf(out,"Init Wpot reading on file %s minO higher than previous simulation old minO=%d new minO=%d, I use new one \n",message,i,minH);
			  }
			}

			fread_out=fread(&dumm1,sizeof(double),1,fp);
			if(dumm1!=Mean_CA_Bonds_bin){
				fprintf(out,"Init Wpot reading on file %s wrong Mean_CA_Bonds_bin I use old one old=%lf new=%lf\n",message,dumm1,Mean_CA_Bonds_bin);
				Mean_CA_Bonds_bin=dumm1;
			}

			for(i=0;i<sizeC;i++){
				for(j=0;j<sizex;j++){
					fread_out=fread(&bin_x,sizeof(double),1,fp);
					Wpot[k][i][j]=bin_x;
					
				}
			}
			fclose(fp);
		}
	}
	
	for(k=0;k<p;k++){
	/**	sprintf(message,"Wpot_Topo-T-%2.3f.dat",1/beta[k]);
		fp=fopen(message,"r");
		if ( fp == NULL) {
			for(i=0;i<sizeEnd;i++){
				for(j=0;j<sizeTopo;j++){		
					Wpot_Topo[k][i][j]=0.0;	
				}	 
			}
		} **/
		sprintf(message,"Wpot_ALN-%2.3f.dat",1/beta[k]);
		fp=fopen(message,"r");
		
		if ( fp == NULL) {
			for(i=0;i<sizeEnd;i++){   /// initialize Wpot if there is no previous file from previous simulation to continue
				for(j=0;j<sizeALN;j++){		
					Wpot_ALN[k][i][j]=0.0;	
				}	 
			}
		}
		
		else{
			fread_out=fread(&i,sizeof(int),1,fp);  /// reading Wpot from file and check if has the correct lenght
			if(i!=sizeEnd){
				fprintf(out,"Init Wpot_ALN reading on file %s wrong sizeEnd %d %d\n",message,i,sizeEnd);
				
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			fread_out=fread(&dumm1,sizeof(int),1,fp);  /// reading Wpot from file and check if has the correct lenght
			if(dumm1!=OEND_bin){
				fprintf(out,"Init Wpot_ALN reading on file %s wrong OEND_bin %lf %lf\n",message,dumm1,OEND_bin);
				
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			fread_out=fread(&i,sizeof(int),1,fp);
			/**if(i!=sizeTopo){
				fprintf(out,"Init Wpot_Topo reading on file %s wrong sizeTopo %d %d\n",message,i,sizeTopo);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}**/
			if(i!=sizeALN){
				fprintf(out,"Init Wpot_ALN reading on file %s wrong sizeALN %d %d\n",message,i,sizeALN);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			fread_out=fread(&dumm1,sizeof(int),1,fp);  /// reading Wpot from file and check if has the correct lenght
			if(dumm1!=OALN_bin){
				fprintf(out,"Init Wpot_ALN reading on file %s wrong OEND_bin %lf %lf\n",message,dumm1,OALN_bin);
				
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			/**for(i=0;i<sizeEnd;i++){
				for(j=0;j<sizeTopo;j++){		
					fread_out=fread(&bin_x,sizeof(double),1,fp);
					Wpot_Topo[k][i][j]=bin_x;
					if(Wpot_Topo[k][i][j]>1e9){
						fprintf(out,"Cavolo Wpot_Topo[%d][%d][%d]=%lf\n",k,i,j,Wpot_Topo[k][i][j]);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
					}
				}
			} **/
			for(i=0;i<sizeEnd;i++){
				for(j=0;j<sizeALN;j++){		
					fread_out=fread(&bin_x,sizeof(double),1,fp);
					Wpot_ALN[k][i][j]=bin_x;
					if(Wpot_ALN[k][i][j]>1e9){
						fprintf(out,"Cavolo Wpot_ALN [%d][%d][%d]=%lf\n",k,i,j,Wpot_ALN[k][i][j]);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
					}
				}
			}
			fclose(fp);
		}
	}
	
	Numberofsubstrates=(double)(SubN);
	totalvolume=(LBox*(ProtN)+1)*(LBox*(ProtN)+1)*((LBox-1)*(ProtN)+1);
	
	if(my_rank==0){
		fp=fopen("SimPar","a");
		fprintf(fp,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
		fprintf(fp,"Number of cycles\t\t\t\t=\t %15lu\n",ncycl);
		fprintf(fp,"Number of samplings\t\t\t\t=\t %15lu\n",nsamp);
		fprintf(fp,"Umbrella strenght \t\t\t\t=\t %15.5g\n",umbrella);
		fprintf(fp,"Equilibrium times \tE1\t\t\t=\t %15lu\n",Equi1);
		fprintf(fp,"Equilibrium times \tE2\t\t\t=\t %15lu\n",Equi2);
		fprintf(fp,"Equilibrium times \tE3\t\t\t=\t %15lu\n",Equi3);
		fprintf(fp,"Number of multicanonical iterations I\t\t=\t %15lu\n",Niteration);
		fprintf(fp,"Box Size\t\t=\t %lf\n",BoxSize);
		fprintf(fp,"\n Bulk Volume %lf\nNumber of Substrates %lf\n Surface Volume ratio %lf \n",totalvolume,Numberofsubstrates,Numberofsubstrates/totalvolume);
		fprintf(fp,"Temperatures\n");
		for(i=0;i<p;i++)
			fprintf(fp,"%lf ",1/beta[i]);
		
		fprintf(fp,"\n");
		fclose(fp);
	}
	
	fprintf(out,"Before fsamp\n");
	fflush(out);
	
	fp=fopen("Topology.psf","w");
	write_psf(fp);
	fclose(fp);
		
	fsamp();
		
	fprintf(out,"Before Barrier\n");
	fflush(out);
	
	Perm_Test(113);
	
	sist->E=energy(); /// perche` di nuovo???
	sist->order=sist->norder;
	sist->contact=sist->ncontact;
	mossaid=0;
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		sist->EDip=sist->EDip_New=Dipole_Self_Cons();
		sist->E+=sist->EDip_New;
		sist->dip_contact=sist->ndip_contact;
		sist->contact+=sist->dip_contact;
		Dipole_Accept();
	}
	#endif
	
	fprintf(out,"sist->E=%lf \n",sist->E);fflush(out);
	
	Perm_Test(114);
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO; /// perche` di nuovo???
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	///sist->Topo_indice=sist->End=0.0;
	sist->ALN_tot=sist->End=0.0;
	fprintf(out,"sist->order=%lf DNPROT=%lf\n",sist->order,DNPROT);fflush(out);
	
	strcpy(info_ALN,"initial");
	for(pr=0;pr<NPROT;pr++){	/// Calculate the initial value of end distance
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			sist->E+=Bond_Energy_fw(pr,i);
		}
		/*last=Plength[pr]-NSPOTS;
		dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
		dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
		dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
		dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		prot[pr].end=sqrt(dx*dx+ dy*dy + dz*dz);
		sist->End+=prot[pr].end;*/
		
		/*** COMMENTATO DA VALO E CHIARA : NON CANCELLARE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! **/
		/**if (prot[pr].end<KNOT_END_COND*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue])){
			indice_knot=0;
			for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
<				if(indice_knot>=Psize){
					fprintf(out,"whatknot out of boudaries Init %lu mossaid= %d indice_knot= %d Psize= %ldk= %d Plength[%d]= %d NATOM= %d\n",icycl,mossaid,indice_knot,Psize,k,pr,Plength[pr],NATOM);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				prot_knot->bead[indice_knot].x=prot[pr].ammino[k].x;
				prot_knot->bead[indice_knot].y=prot[pr].ammino[k].y;
				prot_knot->bead[indice_knot].z=prot[pr].ammino[k].z;
				indice_knot++;
			}
			whatknot(prot_knot, Psize, prot[pr].topology,&prot[pr].topo_indice);
			if(prot[pr].topo_indice<0) prot[pr].topo_indice=sizeTopo-1;
				sist->Topo_indice+=prot[pr].topo_indice;
		}else{
			prot[pr].topo_indice=0;
		}*/
		
		/*Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial); /// Calculate the initial value of end
		  prot[pr].ALN=aln_partial;*/
		//sist->ALN_tot+=prot[pr].ALN;
	}
	
	for(ppr=0;ppr<NPROT;ppr++){	/// Check the overlap between the beads
		for(ii=ATOM_CA;ii<Plength[ppr];ii+=NATOM){
			if(energy_SAW(ppr,ii)>1000.0){
				fprintf(out,"OVERLAP ppr=%d ii=%d\n",ppr,ii);
				fflush(out);
				energy_SAW_output(ppr,ii);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
	}
	Perm_Test(115);
	#ifdef WATER_UP_TEST
	mossaid=123;
	Water_Update_Test();
	#endif
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		testC+=sist->ndip_contact;
	}
	#endif
	if(sist->contact!=testC){
		fprintf(out,"CC INIT   %d %d\n",sist->contact,testC);
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(sist->order-testO)>1e-5){
		fprintf(out,"CC b INIT %10.5lf %10.5lf\n",sist->order,testO);
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(sist->E-testE)>1e-8){
		order_test_water(&testEWater);
		fprintf(out,"CC c INIT %15.10lf %15.10lf Water %15.10lf %15.10lf Dip %15.10lf\n",sist->E,testE,E_Water_New(),testEWater,sist->EDip_New);
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	
	fsamp();
	fprintf(out,"ENERGIAAAAAAA###########%lf\n",sist->E);
	fflush(out);
	Overalp_Test(0,0);
	
	if(my_rank==0){
		fp=fopen("SimPar","a");
		fprintf(fp,"Energia iniziale sist->E=%lf NPROT=%d\n",sist->E,NPROT);
		fclose(fp);
	}
	
	fprintf(out,"SIMULATION\n");fflush(out);
	
	/*******************************
	 * 
	 * ESECUZIONI MOSSE MONTECARLO
	 * 
	 * ****************************/
		
	for(icycl=1;icycl<=ncycl;icycl++){
		#ifdef PROGRESS			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############ORDER_TEST %lu\n",icycl);fflush(out);}	
		#endif	
		#ifdef ORDER_TEST
		for(pr=0;pr<NPROT;pr++){
			for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
				if(sist->p[prot[pr].ammino[k].residue]<=1){
					fprintf(out,"icycl=%lu sist->p[%d]=%d  prot[%d].ammino[%d].residue=%d\n",icycl,prot[pr].ammino[k].residue,sist->p[prot[pr].ammino[k].residue],pr,k,prot[pr].ammino[k].residue);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				if(sist->p[prot[pr].ammino[k].residue]!=sist->np[prot[pr].ammino[k].residue]){
					fprintf(out,"icycl=%lu sist->p[%d]=%d sist->np[%d]=%d prot[%d].ammino[%d].residue=%d\n",icycl,prot[pr].ammino[k].residue,sist->p[prot[pr].ammino[k].residue],prot[pr].ammino[k].residue,sist->np[prot[pr].ammino[k].residue],pr,k,prot[pr].ammino[k].residue);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
		for(k=0;k<S;k++){
			if(sist->p[k]!=sist->np[k]){
				fprintf(out,"icycl=%lu sist->p[%d]=%d sist->np[%d]=%d\n",icycl,k,sist->p[k],k,sist->np[k]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
		#endif
		#ifdef PROGRESS			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############ORDER_TEST OK %lu\n",icycl);fflush(out);}	
		#endif		
		
		/***************************************************************************************
		 * We divide the moves into groups that either affect or not the topology of the chain
		 * The ones that affect che topology are:
		 * 1) MC_Pivot_fw (Rotation of the end branch of the chain)
		 * 2) MC_Pivot_bw (Rotation of the beginning branch of the chain)
		 * 3) MC_Rotation of a loop cranckshaft)
		 * 
		 * While the ones that do not affect the topology are
		 * 1) MC_SP_trasl (single particle translation) it does not affect the topology because the maximum distance
		 *    between two particles does not allow for crossing
		 * 2) MC_SP_rot (single particle rotation)
		 * 3) fseq (Point mutation) 
		 * 4) MC_All_trasl (translation of an entire chain)
		 * 5) MC_All_rot (rotation of an entire chain)
		 * 6) MC_Cluster_trasl (translation of a cluster of chains)
		 * *************************************************************************************/
		
		
		if(icycl%PTFreq!=0){	/// EXCLUDING the parallel tempering move
			pr=(int)(ran3(&seed)*NPROT);	/// chose the random protein
			self_rejected_flag=0;
			if(ran3(&seed)>0.8){	/// 20% of probability to make a topology change move
				/********** Topology Changers************/
				traslator=2.0;
				mossa=(int)(ran3(&seed)*traslator);	/// choose between Pivot and Crankshaft
				
				#ifdef PROGRESS			
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MOVE CHOOSEN %lu mossa=%d\n",icycl,mossa);fflush(out);}	
				#endif	
				switch(mossa){
					
					case 0: 		/// PIVOT
					if(Plength[pr]/NATOM>10){
						kk=(int)(ran3(&seed)*((Plength[pr]/NATOM-10)))+5;
						
						if(kk<Plength[pr]/(2.0*NATOM))	{	/// see if the node is closer to the polymer head
							#ifdef PROGRESS
							if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Pivot_bw %lu energy %lf\n",icycl,sist->E);fflush(out);} 
							#endif
							mossaid=3;
							self_rejected_flag=MC_Pivot_bw(pr,kk);
							#ifdef WATER_UP_TEST
							mossaid=3;
							Water_Update_Test();
							#endif
							#ifdef OTEST
							Overalp_Test(3,self_rejected_flag);
							#endif
							bw_tried++;
							#ifdef PROGRESS
							if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Pivot_bw ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
							#endif
						}else{	/// ... the node is closer to the polymer tail
							#ifdef PROGRESS
							if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Pivot_fw %lu energy %lf\n",icycl,sist->E);fflush(out);	} 
							#endif
							mossaid=4;
							self_rejected_flag=MC_Pivot_fw(pr,kk);
							#ifdef WATER_UP_TEST
							mossaid=4;
							Water_Update_Test();
							#endif
							#ifdef OTEST
							Overalp_Test(4,self_rejected_flag);
							#endif
							fw_tried++;
							#ifdef PROGRESS
							if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Pivot_fw ok %lu energy %lf\n",icycl,sist->E);fflush(out);	}  
							#endif
						}
					}
					break;
					
					case 1: 	/// CRANCKSHAFT
					if(Plength[pr]/NATOM>10){
						#ifdef PROGRESS					
						if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Crankshaft %lu energy %lf\n",icycl,sist->E);fflush(out);}	 
						#endif
						mossaid=7;
						self_rejected_flag=MC_Crankshaft(pr);
						#ifdef WATER_UP_TEST
						mossaid=7;
						Water_Update_Test();
						#endif
						#ifdef OTEST
						Overalp_Test(6,self_rejected_flag);
						#endif
						brot_local_tried++;
						#ifdef PROGRESS
						if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Crankshaft ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
						#endif
					}
					break;	
				}
			}else{
				/********** Topology Inhert************/
				
				if((ran3(&seed)<0.1)&&(NPROT>=2)) { /// Total protein moves mossa => 3, only if we have just 1 protein, in the 10% of these cases!
					traslator=3.0;
					mossa=(int)(ran3(&seed)*traslator)+3;
				}else{                             ///  Single particle moves mossa < 3 
					traslator=3.0;
					mossa=(int)(ran3(&seed)*traslator);
				}
				
				#ifdef PROGRESS			
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MOVE CHOOSEN %lu mossa=%d\n",icycl,mossa);fflush(out);}	
				#endif	
				
				switch(mossa){
					
					case 0: 		/// SINGLE PARTICLE TRANSLOCATION
					#ifdef PROGRESS			
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_SP_trasl %lu energy %lf\n",icycl,sist->E);fflush(out);}	
					#endif
					mossaid=1;
					self_rejected_flag=MC_SP_trasl(pr);
					#ifdef WATER_UP_TEST
					mossaid=1;
					Water_Update_Test();
					#endif
					#ifdef OTEST
					Overalp_Test(1,self_rejected_flag);
					#endif
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_SP_trasl ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
					#endif
					trasl_tried++;
					break;
					
					case 1: 		
					#ifdef PROGRESS			
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_SP_rot %lu energy %lf\n",icycl,sist->E);fflush(out);}	
					#endif
					
					#ifdef TUBE
					#else
					mossaid=2;
					self_rejected_flag=MC_SP_rot(pr);	/// SINGLE PARTICLE ROTATION
					#endif

                                        #ifdef WATER_UP_TEST
					mossaid=2;
					Water_Update_Test();
					#endif
					#ifdef OTEST
					Overalp_Test(2,self_rejected_flag);
					#endif		
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_SP_rot ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
					#endif
					rot_tried++;
					break;
							
					case 2:
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############fseq %lu energy %lf\n",icycl,sist->E);fflush(out);}     
					#endif
					mossaid=11;
					fseq();			/// SINGLE POINT MUTATION
					#ifdef WATER_UP_TEST
					mossaid=11;
					Water_Update_Test();
					#endif
					fseq_tried++;
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############fseq ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	
					#endif
					break;
					
					case 3: 
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_All_trasl %lu energy %lf\n",icycl,sist->E);fflush(out);}	 
					#endif
					mossaid=5;	
					self_rejected_flag=MC_All_trasl(pr);	/// TRASLOCATION OF ONE PROTEINS
					#ifdef WATER_UP_TEST
					mossaid=5;
					Water_Update_Test();
					#endif
					#ifdef OTEST
					Overalp_Test(60,self_rejected_flag);
					#endif
					all_trasl_tried++;
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_All_trasl ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
					#endif
					break;
							
					case 4: 	
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_All_rot %lu energy %lf\n",icycl,sist->E);fflush(out);}	 
					#endif
					mossaid=6;
					self_rejected_flag=MC_All_rot(pr);	/// ROTATION OF ALL PROTEINS
					#ifdef WATER_UP_TEST
					mossaid=6;
					Water_Update_Test();
					#endif
					all_rot_tried++;
					#ifdef OTEST
					Overalp_Test(61,self_rejected_flag);
					#endif
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_All_rot ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
					#endif
					break;
							
					case 5:					
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Cluster_trasl %lu energy %lf\n",icycl,sist->E);fflush(out);}	 
					#endif
					mossaid=8;	
					self_rejected_flag=MC_Cluster_trasl();	/// TRASLOCATION OF A PROTEIN GROUP
					#ifdef WATER_UP_TEST
					mossaid=8;
					Water_Update_Test();
					#endif
					#ifdef OTEST
					Overalp_Test(62,self_rejected_flag);
					#endif
					cluster_trasl_tried++;
					#ifdef PROGRESS
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############MC_Cluster_trasl ok %lu energy %lf\n",icycl,sist->E);fflush(out);}	  
					#endif
					break;		
				}
			}
			
			if(self_rejected_flag==REJECTSELF){
				OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
				///OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);		
				OALN_Old=lround(fabs(sist->ALN_tot/NPROT));
				
				if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;				
				///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
				if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
				
				///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,0);
				sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,0);
			}



 			/*last=Plength[pr]-NSPOTS;
			dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
			dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
			dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
			test_dist=sqrt(dx*dx+ dy*dy + dz*dz);
			printf("%lu\t%lf\t%lf\t%d\t%lf\n",icycl,sist->End,prot[pr].end,mossaid,test_dist);*/
			


			#ifdef 	ORDER_TEST_WATER
			mossaid+=100;
			order_test_water(&testEWater);
			for (i=ATOM_CA;i<Plength[pr];i+=NATOM)
				energy_SP_Pivot_fw(pr,i,OLDWATER);
			for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
				if(fabs(prot[pr].ammino[i].new_water_contacts)>1e-8){
					fprintf(out,"Main test Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf prot[%d].ammino[%d].old_water_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,pr,i,prot[pr].ammino[i].old_water_contacts);fflush(out);
					fsamp();
					fp=fopen("final-err.bin","w");
					writeBinary(fp);
					test_celllist_SAW(1);
					test_celllist_CA();
					test_celllist_H();
					ORDER_TEST_WATER_FLAG=1;
				}	
			}
			for (i=ATOM_CA;i<Plength[pr];i+=NATOM)
				energy_SP_Pivot_fw(pr,i,NEWWATER);
			if(ORDER_TEST_WATER_FLAG==1)
				MPI_Abort(MPI_COMM_WORLD,err5);
			#endif
			#ifdef PROGRESS
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Tswap notry %lu energy %lf\n\n",icycl,sist->E);fflush(out);} 
			#endif
			
		}else{
			#ifdef PROGRESS
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Tswap try %lu energy %lf\n",icycl,sist->E);fflush(out);} 
			#endif
			mossaid=8;
			Tswap();
			#ifdef WATER_UP_TEST
			mossaid=8;
			Water_Update_Test();
			#endif
			#ifdef OTEST
			Overalp_Test(7,self_rejected_flag);
			#endif
			#ifdef PROGRESS
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Tswap ok %lu energy %lf\n",icycl,sist->E);fflush(out);} 
			#endif
		}
		
		if(icycl%100000==0){
			mossaid=9;
			Rescaler();
			sist->EDip_New=sist->EDip=0.0;
			sist->dip_contact=sist->ndip_contact=0;
			#ifdef DIPOLE_SFC
			if(E_Dip>0){
				sist->EDip_New=sist->EDip=Dipole_Self_Cons();
				sist->dip_contact=sist->ndip_contact;
				Dipole_Accept();
			}
			#endif	
			sist->E=energy()+sist->EDip;
			#ifdef WATER_UP_TEST
			mossaid=9;
			Water_Update_Test();
			#endif
			fprintf(out,"################## Rescaled Energy ok %lu\n",icycl);fflush(out);
			
			sist->order=sist->norder;
			sist->contact=sist->ncontact+sist->dip_contact;
			
			sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
			if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
			
			sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
			if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
			
			fprintf(out,"################## Rescaled Orders ok %lu\n",icycl);fflush(out);
			
			#ifdef 	CLIST_TEST	
			test_celllist_SAW(46);
			test_celllist_CA();
			test_celllist_H();
			#endif	
			
			///sist->Topo_indice=sist->End=0;
			sist->ALN_tot=sist->End=0;
			for(pr=0;pr<NPROT;pr++){
				for(k=ATOM_CA;k<Plength[pr];k+=NATOM)
					sist->E+=Bond_Energy_fw(pr,k);
					
				fprintf(out,"################## Rescaled Energy Bond %lu pr= %d Ok\n",icycl,pr);fflush(out);
				
				/*last=Plength[pr]-NSPOTS;
				dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
				dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
				dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				rCA2_end=sqrt(dx*dx+ dy*dy + dz*dz);
				prot[pr].end=rCA2_end;
				sist->End+=prot[pr].end;*/
				
				///fprintf(out,"################## Rescaled End %lu pr= %d Ok\n",icycl,pr);fflush(out);
				/**if (rCA2_end<KNOT_END_COND*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue])){
					indice_knot=0;
					for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
						if(indice_knot>=Psize){
							fprintf(out,"whatknot out of boudaries Rescale %lu mossaid= %d indice_knot= %d Psize= %ld k= %d Plength[%d]= %d NATOM= %d\n",icycl,mossaid,indice_knot,Psize,k,pr,Plength[pr],NATOM);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
						prot_knot->bead[indice_knot].x=prot[pr].ammino[k].x;
						prot_knot->bead[indice_knot].y=prot[pr].ammino[k].y;
						prot_knot->bead[indice_knot].z=prot[pr].ammino[k].z;
						indice_knot++;
					}
					fprintf(out,"################## ALN preparation %lu pr= %d Ok\n",icycl,pr);fflush(out);
					whatknot(prot_knot, Psize, prot[pr].topology,&prot[pr].topo_indice);
					
					fprintf(out,"################## ALN %lu pr= %d Ok\n",icycl,pr);fflush(out);
					if(prot[pr].topo_indice<0) prot[pr].topo_indice=sizeTopo-1;
					sist->Topo_indice+=prot[pr].topo_indice;
					Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
					fprintf(out,"################## Rescaled Order %lu pr= %d Ok\n",icycl,pr);fflush(out);
				}else{
					prot[pr].topo_indice=0;
				}**/
								
				



				
				/*strcpy(info_ALN,"initial");
				fprintf(out,"################## ALN preparation %lu pr= %d Ok\n",icycl,pr);fflush(out);
				Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
				fprintf(out,"################## ALN %lu pr= %d Ok\n",icycl,pr);fflush(out);
				prot[pr].ALN=aln_partial;
				sist->ALN_tot+=prot[pr].ALN;*/
				
				Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
				fprintf(out,"################## Rescaled Order %lu pr= %d Ok\n",icycl,pr);fflush(out);
			}
			
			#ifdef 	CLIST_TEST	
			test_celllist_SAW(47);
			test_celllist_CA();
			test_celllist_H();
			#endif	
			
			Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
			fprintf(out,"################## Rescaled Minimum Energy %lu Ok\n",icycl);fflush(out);
		}
		
		/********************
		* Data sampling *
		*********************/
		
		if (icycl%nsamp==0) {
		  fsamp();
		  for(i=0;i<p;i++){
		    rtemthetasto[i]=0.0;
		    rntempswap[i]=0.0;
		    }
		  MPI_Reduce(temthetasto,rtemthetasto,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
		  MPI_Reduce(ntempswap,rntempswap,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
		  if(my_rank==0){
		    
		    ///sprintf(message,"TempControl-O-%d-P-%d.dat",Lattice,process);
		    ///fTempControl=fopen(message,"w");
		    for(i=0;i<p;i++){
		      fprintf(fTempControl,"%2.4f %2.4f %2.4f\n",1/beta[i],rtemthetasto[i],rntempswap[i]);fflush(fTempControl);
		    }
		    fprintf(fTempControl,"##########################################\n");fflush(fTempControl);
		    ///fclose(fTempControl);
		  }
		}
		if (icycl%2000==0){
			
			#ifdef 	CLIST_TEST	
			test_celllist_SAW(44);
			test_celllist_CA();
			test_celllist_H();
			#endif	
			///fprintf(out,"################## End A %lu\n",icycl);fflush(out);
			///sist->Topo_indice=sist->End=0;
			sist->ALN_tot=sist->End=0;
			
			for(pr=0;pr<NPROT;pr++){
				
			  /*last=Plength[pr]-NSPOTS;
				dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
				dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
				dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				rCA2_end=sqrt(dx*dx+ dy*dy + dz*dz);
				prot[pr].end=rCA2_end;
				sist->End+=prot[pr].end;*/
				
				#ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"pr= %d last= %d rCA2_end= %lf Dist_Ref= %lf\n",pr,last,rCA2_end,10.0*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue]));fflush(out);}
				#endif
				
				///fprintf(out,"################## End B %lu pr= %d Ok\n",icycl,pr);fflush(out);
				
				/**if (rCA2_end<KNOT_END_COND*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue])){
					 indice_knot=0;
					for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
						if(indice_knot>=Psize){
							fprintf(out,"whatknot out of boudaries Main %lu mossaid= %d indice_knot= %d Psize= %ldk= %d Plength[%d]= %d NATOM= %d\n",icycl,mossaid,indice_knot,Psize,k,pr,Plength[pr],NATOM);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
						prot_knot->bead[indice_knot].x=prot[pr].ammino[k].x;
						prot_knot->bead[indice_knot].y=prot[pr].ammino[k].y;
						prot_knot->bead[indice_knot].z=prot[pr].ammino[k].z;
						indice_knot++;
					}
					fprintf(out,"################## ALN preparation B %lu pr= %d Ok\n",icycl,pr);fflush(out);
					whatknot(prot_knot, Psize, prot[pr].topology,&prot[pr].topo_indice);
					fprintf(out,"################## ALN B %lu pr= %d Ok\n",icycl,pr);fflush(out);
					if(prot[pr].topo_indice<0) prot[pr].topo_indice=sizeTopo-1;
					sist->Topo_indice+=prot[pr].topo_indice;
					#ifdef PROGRESS_KNOT
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"whatknot says %s\n",prot[pr].topology);fflush(out);}
					#endif
					
					sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
					if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
					
					sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
					if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
					
					Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
					fprintf(out,"################## ALN C %lu pr= %d Ok\n",icycl,pr);fflush(out);
					#ifdef PROGRESS_KNOT
					if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Order_Conf_Sampling %lf %d %d\n",sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);fflush(out);}
					#endif
				}else{
					prot[pr].topo_indice=0;
				}**/
				
				




				/*strcpy(info_ALN,"initial");
				fprintf(out,"################## ALN preparation %lu pr= %d Ok\n",icycl,pr);fflush(out);
				Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
				fprintf(out,"################## ALN %lu pr= %d Ok\n",icycl,pr);fflush(out);
				prot[pr].ALN=aln_partial;
				sist->ALN_tot+=prot[pr].ALN;*/
				
				/** #ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"whatknot says %s\n",prot[pr].topology);fflush(out);}
				#endif **/
				#ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Linking_number says %lf\n",prot[pr].ALN);fflush(out);}
				#endif
				
				sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
				if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
					
				sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
				if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
					
				Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
				///fprintf(out,"################## ALN C %lu pr= %d Ok\n",icycl,pr);fflush(out);
				#ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Order_Conf_Sampling %lf %d %d\n",sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);fflush(out);}
				#endif
				
				///fprintf(out,"################## End C %lu pr= %d Ok\n",icycl,pr);fflush(out);
				
			}
			#ifdef 	CLIST_TEST	
			test_celllist_SAW(45);
			test_celllist_CA();
			test_celllist_H();
			#endif	
		}
				
		/*****************************
		* Multicanonical iteration
		******************************/
		
		if((icycl>Equi1)&&(icycl<Niteration*(Equi2+Equi3)+Equi1)){
			if(icycl==deltacycle+Equi2){
				if(icycl > 0) fprintf(out,"init Whisto %lu\n",icycl);
				for(i=0;i<p*sizeC*sizex;i++){		
					Whisto[i]=0.0;	
				}
				if(icycl > 0) fprintf(out,"init Whisto ok %lu\n",icycl);
				
				/**for(i=0;i<p*sizeEnd*sizeTopo;i++){		
					Whisto_Topo[i]=0.0;	
				}**/
				for(i=0;i<p*sizeEnd*sizeALN;i++){		
					Whisto_ALN[i]=0.0;	
				}
			}
			
			if(icycl==deltacycle+Equi3+Equi2){
				deltacycle=icycl;
				if(icycl > 0) fprintf(out,"try W %lu\n",icycl);
				W();
				if(icycl > 0) fprintf(out,"try W ok %lu\n",icycl);
				if(icycl > 0) fprintf(out,"try W_Topo %lu\n",icycl);
				///W_Topo();
				if(icycl > 0) fprintf(out,"try W_Topo ok %lu\n",icycl);
			}
		}
		
		if(icycl==Equi1){
			
			/// Here we reset the histograms accumulated during the equilibration of the system
			
			for(i=0;i<3*p;i++){
				rDensity[i]=-10000.0;
			}
			for(i=0;i<p*sizeC*sizex;i++){
				rhistoC[i]=-1000000.0;
			}
			for(i=0;i<p*sizeC*sizex;i++){
			  rhistoSC[i]=-1000000.0;
                        }

			/**for(i=0;i<p*sizeEnd*sizeTopo;i++){
				rhisto_Topo[i]=-1000000.0;
			}**/
			for(i=0;i<p*sizeEnd*sizeALN;i++){
				rhisto_ALN[i]=-1000000.0;
			}
			/*for(i=0;i<p*2*sizex*1000;i++){
				rhisto[i]=-1000000.0;		
			}
			for(i=0;i<p*2*sizex;i++){
				rhistofill[i]=-1000000.0;		
			}*/
			for(i=0;i<p*sizeEnergy;i++){
				rhisto_energy[i]=-1000000.0;		
			}
			for(i=0;i<p;i++){
				rndataen2[i]=-1000000.0;
				rhistosc_norm[i]=-1000000.0;
			}
			fprintf(out,"MPI_Reduce INIT %lu\n",icycl);fflush(out);
			MPI_Reduce(histoC,rhistoC,p*sizeC*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			MPI_Reduce(histoSC,rhistoSC,p*sizeC*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			fprintf(out,"MPI_Reduce INIT %lu OK histoC\n",icycl);fflush(out);
			///MPI_Reduce(histo_Topo,rhisto_Topo,p*sizeEnd*sizeTopo,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			
			/// rhisto is the global histogram obtained summing histog (myExpSum) over all the nodes
			/// histo is the local (on the single node) unbiased histogram 
			MPI_Reduce(histo_ALN,rhisto_ALN,p*sizeEnd*sizeALN,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			
			///fprintf(out,"MPI_Reduce INIT %lu OK histo_Topo\n",icycl);fflush(out);
			fprintf(out,"MPI_Reduce INIT %lu OK histo_ALN\n",icycl);fflush(out);
			
			MPI_Reduce(histo_energy,rhisto_energy,p*sizeEnergy,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			
			///MPI_Reduce(histofill,rhistofill,p*2*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			///fprintf(out,"MPI_Reduce INIT %lu OK histofill\n",icycl);fflush(out);
			MPI_Reduce(ndataen2,rndataen2,p,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			MPI_Reduce(histosc_norm,rhistosc_norm,p,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			fprintf(out,"MPI_Reduce INIT %lu ndataen2 OK\n",icycl);fflush(out);
			MPI_Reduce(Density,rDensity,p*3,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			
			fprintf(out,"MPI_Reduce INIT %lu Density OK\n",icycl);fflush(out);
			
			for(i=0;i<3*p;i++){
				Density[i]=-1000000.0;
			}
			for(i=0;i<p*sizeC*sizex;i++){
				histoC[i]=-1000000.0;
			}
			for(i=0;i<p*sizeC*sizex;i++){
			  histoSC[i]=-1000000.0;
                        }
			/**for(i=0;i<p*sizeEnd*sizeTopo;i++){
				histo_Topo[i]=-1000000.0;
			}**/
			for(i=0;i<p*sizeEnd*sizeALN;i++){
				histo_ALN[i]=-1000000.0;
			}
			/*for(i=0;i<p*2*sizex*1000;i++){
				histo[i]=-1000000.0;		
			}
			for(i=0;i<p*2*sizex;i++){
				histofill[i]=-1000000.0;		
			}*/
			for(i=0;i<p;i++){
				ndataen2[i]=-1000000.0;
				histosc_norm[i]=-1000000.0;
			}
			
			for(i=0;i<p*sizeEnergy;i++){   /// perche` qui si riinizializza dopo MPI_REDUCE???
				histo_energy[i]=-1000000.0;		
			}
			
			if(my_rank==0){
				sprintf(message,"Density-P-%d-init.dat",process);
				fp=fopen(message,"w");
				
				for(i=0;i<p;i++){
					R=exp(rDensity[i*3]-rDensity[i*3+1]);
					fprintf(fp,"%2.4f %10.4g %10.4g\n",1/beta[i],R*(totalvolume/Numberofsubstrates),
					1/(R*(totalvolume/Numberofsubstrates)));
					
				}
				fprintf(fp,"\n\n");		
				fclose(fp);
				
				for(k=0;k<p;k++){
					/*sprintf(message,"Histogram_3D-T-%2.3f-%d-nt-init.dat",1/beta[k],process);
					fHisto_3Dnt=fopen(message,"w");
					
					sprintf(message,"Histogram_2D-T-%2.3f-%d-nt-init.dat",1/beta[k],process);
					fHisto_2Dnt=fopen(message,"w");
					
					sprintf(message,"Histogram_2D-T-%2.3f-%d-t-init.dat",1/beta[k],process);
					fHisto_2Dt=fopen(message,"w");
					
					sprintf(message,"Histogram_3D-T-%2.3f-%d-t-init.dat",1/beta[k],process);
					fHisto_3Dt=fopen(message,"w");*/
					
					sprintf(message,"Histogram_2D-E-%2.3f-%d-init.dat",1/beta[k],process);
					fHisto_2D_Energy=fopen(message,"w");
										
					sprintf(message,"Histogram_C-T-%2.3f-%d-init.dat",1/beta[k],process);
					fHisto_contact=fopen(message,"w");
					
					///sprintf(message,"Histogram_Topo-T-%2.3f-%d-init.dat",1/beta[k],process);
					///fHisto_Topo=fopen(message,"w");
					sprintf(message,"Histogram_ALN-%2.3f-%d-init.dat",1/beta[k],process);
					fHisto_Topo=fopen(message,"w");
					sprintf(message,"Entropy_C-T-%2.3f-%d-init.dat",1/beta[k],process);
					fHisto_entropy=fopen(message,"w");  
					
					/* fwrite(&sizex,sizeof(int),1,fHisto_2Dnt);            C & V
					fwrite(&minO,sizeof(int),1,fHisto_2Dnt);
					bin_x=rndataen2[k];
					//fprintf(out,"NT %d %d %lf\n",sizex,minO,rndataen2[k]);
					fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
					
					fwrite(&sizex,sizeof(int),1,fHisto_2Dt);
					fwrite(&minO,sizeof(int),1,fHisto_2Dt);	
					bin_x=rndataen2[k];
					//fprintf(out,"T %d %d %lf\n",sizex,minO,rndataen2[k]);
					fflush(out);
					fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
					for(i=0;i<sizex;i++){		
						
						bin_x=-rhistofill[k*2*sizex+i]; /// rhistofill is the one with touch
						//fprintf(out,"		%lf %lf\n",bin_x,(bin_x+rndataen2[k])/beta[k]);
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
						
						bin_x=-rhistofill[k*2*sizex+sizex+i];
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
						
					}*/
					/// Print in binary 2D energy istogram
					///fwrite(&sizex,sizeof(int),1,fHisto_2D);
					///fwrite(&minO,sizeof(int),1,fHisto_2D);	//
					fwrite(&sizeEnergy,sizeof(int),1,fHisto_2D_Energy);
					bin_x=rndataen2[k];
					fflush(out);
					fwrite(&minE,sizeof(int),1,fHisto_2D_Energy);
					fwrite(&bin_energy,sizeof(double),1,fHisto_2D_Energy);
					fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
				   
					for(i=0;i<sizeEnergy;i++){		
						
						bin_x=-rhisto_energy[k*sizeEnergy+i]; /// k is the temperature
						fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
						
					}
					
					fwrite(&sizeC,sizeof(int),1,fHisto_contact);
					fwrite(&minH,sizeof(int),1,fHisto_contact);
					fwrite(&Mean_H_Bonds_bin,sizeof(double),1,fHisto_contact);
					fwrite(&sizex,sizeof(int),1,fHisto_contact);
					fwrite(&minO,sizeof(int),1,fHisto_contact);
					fwrite(&Mean_CA_Bonds_bin,sizeof(double),1,fHisto_contact);
					
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_contact);
					for(i=0;i<sizeC;i++){
						for(j=0;j<sizex;j++){
							bin_x=-rhistoC[k*sizeC*sizex+i*sizex+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_contact);
						}
					}
					fwrite(&sizeC,sizeof(int),1,fHisto_entropy);
					fwrite(&minH,sizeof(int),1,fHisto_entropy);
                                        fwrite(&Mean_H_Bonds_bin,sizeof(double),1,fHisto_entropy);
					fwrite(&sizex,sizeof(int),1,fHisto_entropy);
                                        fwrite(&minO,sizeof(int),1,fHisto_entropy);
                                        fwrite(&Mean_CA_Bonds_bin,sizeof(double),1,fHisto_entropy);

                                        bin_x=rhistosc_norm[k];
                                        fwrite(&bin_x,sizeof(double),1,fHisto_entropy);
                                        for(i=0;i<sizeC;i++){
					  for(j=0;j<sizex;j++){
					    bin_x=-rhistoSC[k*sizeC*sizex+i*sizex+j];
					    fwrite(&bin_x,sizeof(double),1,fHisto_entropy);
					  }
                                        }

					fwrite(&sizeEnd,sizeof(int),1,fHisto_Topo);
					///fwrite(&sizeTopo,sizeof(int),1,fHisto_Topo);
					fwrite(&OEND_bin,sizeof(double),1,fHisto_Topo);
					fwrite(&sizeALN,sizeof(int),1,fHisto_Topo);
					fwrite(&OALN_bin,sizeof(double),1,fHisto_Topo);
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_Topo);
					for(i=0;i<sizeEnd;i++){
						/**for(j=0;j<sizeTopo;j++){
							bin_x=-rhisto_Topo[k*sizeEnd*sizeTopo+i*sizeTopo+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_Topo);
						}**/
						for(j=0;j<sizeALN;j++){
							bin_x=-rhisto_ALN[k*sizeEnd*sizeALN+i*sizeALN+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_Topo);
						}
					}
					/*sprintf(message,"Histogram_ALN-T-%2.3f-%d.txt",1/beta[k],process);
					fHisto_Topo_txt=fopen(message,"w");
					fprintf(fHisto_Topo_txt,"sizeEnd %d  sizeALN %d\n",sizeEnd,sizeALN);fflush(NULL);
					for(i=0;i<sizeEnd;i++){
					  for(j=0;j<sizeALN;j++){
					    fprintf(fHisto_Topo_txt,"%5d %5d %15.10f\n",j,i,(-rhisto_ALN[k*sizeEnd*sizeALN+i*sizeALN+j]+rndataen2[k])/beta[k]);fflush(NULL);
					  }
					  fprintf(fHisto_Topo,"\n");
					  }*/
					
					/*fwrite(&sizex,sizeof(int),1,fHisto_3Dt);
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
					fwrite(&sizex,sizeof(int),1,fHisto_3Dnt);		
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
					
					for(i=0;i<sizex;i++){	
						for(j=0;j<1000;j++){
							bin_x=-rhisto[k*2*sizex*1000+sizex*1000+i*1000+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
							bin_x=-rhisto[k*2*sizex*1000+i*1000+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
						}
					}*/
					fclose(fHisto_contact);
					fclose(fHisto_entropy);
					fclose(fHisto_Topo);
					fclose(fHisto_2D_Energy);
					/*fclose(fHisto_3Dnt);
					fclose(fHisto_2Dt);
					fclose(fHisto_3Dt);*/
					
				}
			}
			
			
		}
		if((icycl>10*(Equi2+Equi3)+Equi1)&&(icycl%nsamp==0)){
			
			#ifdef Histogram_C_TEST
			for(k=0;k<p;k++){
				sprintf(message,"Histogram_C-P-%d-%d-T-%2.3f.dat",my_rank,process,1/beta[k]);
				fHisto_contacta=fopen(message,"w");
				for(i=0;i<sizeC;i++){
					for(j=0;j<sizex;j++){
						fprintf(fHisto_contacta,"%10.5lf %10.5lf %15.10f\n",j/Mean_CA_Bonds_bin,i/Mean_H_Bonds_bin,(-histoC[k*sizeC*sizex+i*sizex+j]+ndataen2[k])/beta[k]);
					}
					fprintf(fHisto_contacta,"\n");
				}
				fclose(fHisto_contacta);
			}
			#endif
			//fprintf(out,"Histogram_C Write %lu OK\n",icycl);fflush(out);
			
			for(i=0;i<3*p;i++){
				rDensity[i]=-10000.0;
			}
			for(i=0;i<p*sizeC*sizex;i++){
				rhistoC[i]=-1000000.0;
			}
			for(i=0;i<p*sizeC*sizex;i++){
			  rhistoSC[i]=-1000000.0;
                        }
			/**for(i=0;i<p*sizeEnd*sizeTopo;i++){
				rhisto_Topo[i]=-1000000.0;
			}**/
			for(i=0;i<p*sizeEnd*sizeALN;i++){
				rhisto_ALN[i]=-1000000.0;
			}
			/*for(i=0;i<p*2*sizex*1000;i++){
				rhisto[i]=-1000000.0;		
			}
			for(i=0;i<p*2*sizex;i++){
				rhistofill[i]=-1000000.0;		
			}*/
			for(i=0;i<p*sizeEnergy;i++){
				rhisto_energy[i]=-1000000.0;		
			}
			for(i=0;i<p;i++){
				rndataen2[i]=-1000000.0;
				rhistosc_norm[i]=-1000000.0;
			}
			fprintf(out,"Histo Init %lu OK\n",icycl);fflush(out);
			MPI_Reduce(histoC,rhistoC,p*sizeC*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			MPI_Reduce(histoSC,rhistoSC,p*sizeC*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);

			fprintf(out,"MPI_Reduce %lu OK histoC\n",icycl);fflush(out);
			
			///MPI_Reduce(histo_Topo,rhisto_Topo,p*sizeEnd*sizeTopo,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			MPI_Reduce(histo_ALN,rhisto_ALN,p*sizeEnd*sizeALN,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			
			///fprintf(out,"MPI_Reduce %lu OK histo_Topo\n",icycl);fflush(out);
			fprintf(out,"MPI_Reduce %lu OK histo_ALN\n",icycl);fflush(out);
			///MPI_Reduce(histofill,rhistofill,p*2*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			///fprintf(out,"MPI_Reduce %lu OK histofill\n",icycl);fflush(out);
			
			MPI_Reduce(histo_energy,rhisto_energy,p*sizeEnergy,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			fprintf(out,"MPI_Reduce %lu OK histo_energy\n",icycl);fflush(out);
			
			MPI_Reduce(ndataen2,rndataen2,p,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			MPI_Reduce(histosc_norm,rhistosc_norm,p,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			fprintf(out,"MPI_Reduce %lu OK ndataen2\n",icycl);fflush(out);
			MPI_Reduce(Density,rDensity,p*3,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
			
			fprintf(out,"MPI_Reduce %lu OK Density\n",icycl);fflush(out);
			
			if(my_rank==0){
				sprintf(message,"Density-P-%d.dat",process);
				fp=fopen(message,"w");
				
				for(i=0;i<p;i++){
					R=exp(rDensity[i*3]-rDensity[i*3+1]);
					fprintf(fp,"%2.4f %10.4g %10.4g\n",1/beta[i],R*(totalvolume/Numberofsubstrates),
					1/(R*(totalvolume/Numberofsubstrates)));
					
				}
				fprintf(fp,"\n\n");		
				fclose(fp);
				
				for(k=0;k<p;k++){
					/*sprintf(message,"Histogram_3D-T-%2.3f-%d-nt.dat",1/beta[k],process);
					fHisto_3Dnt=fopen(message,"w");
					
					sprintf(message,"Histogram_2D-T-%2.3f-%d-nt.dat",1/beta[k],process);
					fHisto_2Dnt=fopen(message,"w");
					
					sprintf(message,"Histogram_3D-T-%2.3f-%d-t.dat",1/beta[k],process);
					fHisto_3Dt=fopen(message,"w");
								
					sprintf(message,"Histogram_2D-T-%2.3f-%d-t.dat",1/beta[k],process);
					fHisto_2Dt=fopen(message,"w"); C & V */
					
					sprintf(message,"Histogram_2D-E-%2.3f-%d.dat",1/beta[k],process);
					fHisto_2D_Energy=fopen(message,"w");
									
					sprintf(message,"Histogram_C-T-%2.3f-%d.dat",1/beta[k],process);
					fHisto_contact=fopen(message,"w");
					sprintf(message,"Entropy_C-T-%2.3f-%d.dat",1/beta[k],process);
					fHisto_entropy=fopen(message,"w");

					
					
					///sprintf(message,"Histogram_Topo-T-%2.3f-%d.dat",1/beta[k],process);
					sprintf(message,"Histogram_ALN-%2.3f-%d.dat",1/beta[k],process);
					fHisto_Topo=fopen(message,"w");					
					
					/*fwrite(&sizex,sizeof(int),1,fHisto_2Dnt);    C & V
					fwrite(&minO,sizeof(int),1,fHisto_2Dnt);
					bin_x=rndataen2[k];
					//fprintf(out,"NT %d %d %lf\n",sizex,minO,rndataen2[k]);
					fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
					
					fwrite(&sizex,sizeof(int),1,fHisto_2Dt);
					fwrite(&minO,sizeof(int),1,fHisto_2Dt);	
					bin_x=rndataen2[k];
					//fprintf(out,"T %d %d %lf\n",sizex,minO,rndataen2[k]);
					//fflush(out);
					fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
					for(i=0;i<sizex;i++){		
						
						bin_x=-rhistofill[k*2*sizex+i];
						//fprintf(out,"		%lf %lf\n",bin_x,(bin_x+rndataen2[k])/beta[k]);
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
						
						bin_x=-rhistofill[k*2*sizex+sizex+i];
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
						
					}*/
					
					fwrite(&sizeEnergy,sizeof(int),1,fHisto_2D_Energy);
					bin_x=rndataen2[k];
					fflush(out);
					fwrite(&minE,sizeof(int),1,fHisto_2D_Energy);
					fwrite(&bin_energy,sizeof(double),1,fHisto_2D_Energy);
					fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
					for(i=0;i<sizeEnergy;i++){		
						
						bin_x=-rhisto_energy[k*sizeEnergy+i]; /// k is the temperature, is the indice ok???
						fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
						
					}
					
					
					fwrite(&sizeC,sizeof(int),1,fHisto_contact);
					fwrite(&minH,sizeof(int),1,fHisto_contact);
					fwrite(&Mean_H_Bonds_bin,sizeof(double),1,fHisto_contact);
					fwrite(&sizex,sizeof(int),1,fHisto_contact);
					fwrite(&minO,sizeof(int),1,fHisto_contact);
					fwrite(&Mean_CA_Bonds_bin,sizeof(double),1,fHisto_contact);
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_contact);
					for(i=0;i<sizeC;i++){
						for(j=0;j<sizex;j++){
							
							bin_x=-rhistoC[k*sizeC*sizex+i*sizex+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_contact);
							
						}
						
					}
					
					fwrite(&sizeC,sizeof(int),1,fHisto_entropy);
                                        fwrite(&minH,sizeof(int),1,fHisto_entropy);
                                        fwrite(&Mean_H_Bonds_bin,sizeof(double),1,fHisto_entropy);
                                        fwrite(&sizex,sizeof(int),1,fHisto_entropy);
					fwrite(&minO,sizeof(int),1,fHisto_entropy);
                                        fwrite(&Mean_CA_Bonds_bin,sizeof(double),1,fHisto_entropy);

                                        bin_x=rhistosc_norm[k];
                                        fwrite(&bin_x,sizeof(double),1,fHisto_entropy);
                                        for(i=0;i<sizeC;i++){
                                          for(j=0;j<sizex;j++){
                                            bin_x=-rhistoSC[k*sizeC*sizex+i*sizex+j];
                                            fwrite(&bin_x,sizeof(double),1,fHisto_entropy);
					  }
                                        }
					/*****************TESTS FOR ENTROPY print txt**************/

					/*sprintf(message,"Entropy-C-T-%2.3f-%d.txt",1/beta[k],process);
					fHisto_entropy_txt=fopen(message,"w");
					fprintf(fHisto_entropy_txt,"sizeC %d  minH %d Mean_H_Bonds_bin %lf sizex %d minO %d Mean_CA_Bonds_bin %lf\n",sizeC,minH,Mean_H_Bonds_bin,sizex,minO,Mean_CA_Bonds_bin);fflush(fHisto_entropy_txt); 
					for(i=0;i<sizeC;i++){
					  for(j=0;j<sizex;j++){
					    fprintf(fHisto_entropy_txt,"%5d %5d %15.10f\n",(int)((i+minH)/Mean_H_Bonds_bin),(int)((j+minO)/Mean_CA_Bonds_bin),(-rhistoSC[k*sizeC*sizex+i*sizex+j])/beta[k]);
					  }
					  fprintf(fHisto_entropy_txt,"\n");
					}
					fclose(fHisto_entropy_txt);*/
					
					/********************************************/

					fwrite(&sizeEnd,sizeof(int),1,fHisto_Topo);
					fwrite(&OEND_bin,sizeof(double),1,fHisto_Topo);
					///fwrite(&sizeTopo,sizeof(int),1,fHisto_Topo);
					fwrite(&sizeALN,sizeof(int),1,fHisto_Topo);
					fwrite(&OALN_bin,sizeof(double),1,fHisto_Topo);
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_Topo);
					for(i=0;i<sizeEnd;i++){
					/***for(j=0;j<sizeTopo;j++){
							
							bin_x=-rhisto_Topo[k*sizeEnd*sizeTopo+i*sizeTopo+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_Topo);
							
						} **/
						for(j=0;j<sizeALN;j++){
							
							bin_x=-rhisto_ALN[k*sizeEnd*sizeALN+i*sizeALN+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_Topo);
							
						}
						
						
					}
					
					/*fwrite(&sizex,sizeof(int),1,fHisto_3Dt);
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
					
					
					fwrite(&sizex,sizeof(int),1,fHisto_3Dnt);		
					bin_x=rndataen2[k];
					fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
					
					for(i=0;i<sizex;i++){	
						for(j=0;j<1000;j++){
							
							bin_x=-rhisto[k*2*sizex*1000+sizex*1000+i*1000+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
							bin_x=-rhisto[k*2*sizex*1000+i*1000+j];
							fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
							
						}
						
					}*/
					fclose(fHisto_contact);
					fclose(fHisto_Topo);
					fclose(fHisto_2D_Energy);
					fclose(fHisto_entropy);
					/*fclose(fHisto_2Dnt);
					fclose(fHisto_3Dnt);
					fclose(fHisto_2Dt);
					fclose(fHisto_3Dt);*/
					
					
				}
			}
		}
		
		
		/****************************/
		/*Test continuo sulla catena*/
		/****************************/
		/*if (icycl%1 ==0) test();*/
		
		
	}
	/*if(icycl > 0) fprintf(out,"alora va bene \n");
	fflush(NULL);*/
	/******************************/
	/*Saving Iteration potentials */
	/******************************/
	W();
	if(my_rank==0){
		for(k=0;k<p;k++){
			sprintf(message,"Wpot-T-%2.3f.dat",1/beta[k]);
			fp=fopen(message,"w");
			fwrite(&sizeC,sizeof(int),1,fp);
			fwrite(&minH,sizeof(int),1,fp);
			fwrite(&Mean_H_Bonds_bin,sizeof(double),1,fp);
			fwrite(&sizex,sizeof(int),1,fp);
			fwrite(&minO,sizeof(int),1,fp);
			fwrite(&Mean_CA_Bonds_bin,sizeof(double),1,fp);
			for(i=0;i<sizeC;i++){
				for(j=0;j<sizex;j++){
					
					bin_x=Wpot[k][i][j];
					fwrite(&bin_x,sizeof(double),1,fp);
					
				}
				
			}
			fclose(fp);
		}
		for(k=0;k<p;k++){
			///sprintf(message,"Wpot_Topo-T-%2.3f.dat",1/beta[k]);
			sprintf(message,"Wpot_ALN-%2.3f.dat",1/beta[k]);
			fp=fopen(message,"w");
			fwrite(&sizeEnd,sizeof(int),1,fp);
			fwrite(&OEND_bin,sizeof(double),1,fp);
			///fwrite(&sizeTopo,sizeof(int),1,fp);
			fwrite(&sizeALN,sizeof(int),1,fp);
			fwrite(&OALN_bin,sizeof(double),1,fp);
			for(i=0;i<sizeEnd;i++){
			/**	for(j=0;j<sizeTopo;j++){
					bin_x=Wpot_Topo[k][i][j];
					fwrite(&bin_x,sizeof(double),1,fp);
					
				} **/
				for(j=0;j<sizeALN;j++){
					bin_x=Wpot_ALN[k][i][j];
					fwrite(&bin_x,sizeof(double),1,fp);
					
				}
			}
			fclose(fp);
		}
	}
	/******************************/
	/*Sampling result		 */
	/******************************/
	
	
	/****************************************/
	/*Saving Parallel Tempering Performance */
	/****************************************/
	//MPI_Gather(&temthetasto,1,MPI_DOUBLE,roottemthetasto,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	for(i=0;i<p;i++){
		rtemthetasto[i]=0.0;
		rntempswap[i]=0.0;
	}
	fprintf(out,"MPI_Reduce %lu FINAL\n",icycl);fflush(out);
	MPI_Reduce(temthetasto,rtemthetasto,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	fprintf(out,"MPI_Reduce %lu temthetasto OK\n",icycl);fflush(out);
	MPI_Reduce(ntempswap,rntempswap,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	fprintf(out,"MPI_Reduce %lu ntempswap OK\n",icycl);fflush(out);
	if(my_rank==0){
		
	  ///sprintf(message,"TempControl-P-%d.dat",process);
	  ///	fTempControl=fopen(message,"w");
		for(i=0;i<p;i++){
			fprintf(fTempControl,"%2.4f %2.4f %2.4f\n",1/beta[i],rtemthetasto[i],rntempswap[i]);
		}
		fprintf(fTempControl,"\n");
		
		fclose(fTempControl);
	}
	
	
	fsamp();
	fclose(fSeq);
	fclose(fEner);
	
	
	
	
	
	//MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gather(&MinE,1,MPI_DOUBLE,rootminE,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&BestH,1,MPI_DOUBLE,rootminO,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinEH,1,MPI_DOUBLE,rootminEH,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&BestHE,1,MPI_DOUBLE,rootminOE,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinEO,1,MPI_DOUBLE,rootminEO,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&BestHO,1,MPI_DOUBLE,rootminOH,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	
	
	
	
	if(my_rank==0){
		
		
		
		for(i=0;i<p;i++){
			fprintf(out,"%d MinE =%lf MinEO=%lf MinEH=%lf\n",i,rootminE[i],rootminEO[i],rootminEH[i]);
			fprintf(out,"%d BestH =%lf BestHE=%lf BestHO=%lf\n",i,rootminO[i],rootminOE[i],rootminOH[i]);
		}
		
		endtime=MPI_Wtime();
		fprintf(out,"Simulation Time %lf (seconds)\n",endtime-starttime);
	}
	MPI_Finalize();
	
	
	return(0);
	
}

int writeBinary(FILE *outFile) {
	
	double x,y,z;
	int i,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=prot[pr].ammino[i].x;
			y=prot[pr].ammino[i].y;
			z=prot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}

int writeBinaryMin(FILE *outFile) {
	
	double x,y,z;
	int i,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=minprot[pr].ammino[i].x;
			y=minprot[pr].ammino[i].y;
			z=minprot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}

int writeBinaryMinO(FILE *outFile) {
	
	double x,y,z;
	int i,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=bestOprot[pr].ammino[i].x;
			y=bestOprot[pr].ammino[i].y;
			z=bestOprot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}

int writeBinaryMaxO(FILE *outFile) {
	
	double x,y,z;
	int i,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=maxOprot[pr].ammino[i].x;
			y=maxOprot[pr].ammino[i].y;
			z=maxOprot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}

int readBinary (FILE *inFile) {
	
	double x,y,z;
	int i,j,k,pr,id,indice;
	
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			fread_out=fread(&x,sizeof(double),1,inFile);
			fread_out=fread(&y,sizeof(double),1,inFile);
			fread_out=fread(&z,sizeof(double),1,inFile);
			fread_out=fread(&id,sizeof(int),1,inFile);
			prot[pr].ammino[i].x=x;
			prot[pr].ammino[i].y=y;
			prot[pr].ammino[i].z=z;
			prot[pr].ammino[i].id=id;
			
			if((isnan(prot[pr].ammino[i].x))||(isnan(1./prot[pr].ammino[i].x))){
				fprintf(out,"Coord NAN %lu after readBinary prot[%d].ammino[%d].x=%lf\n",icycl,pr,i,prot[pr].ammino[i].x);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
			if((isnan(prot[pr].ammino[i].y))||(isnan(1./prot[pr].ammino[i].y))){
				fprintf(out,"Coord NAN %lu after readBinary prot[%d].ammino[%d].y=%lf\n",icycl,pr,i,prot[pr].ammino[i].y);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
			if((isnan(prot[pr].ammino[i].z))||(isnan(1./prot[pr].ammino[i].z))){
				fprintf(out,"Coord NAN %lu after readBinary prot[%d].ammino[%d].z=%lf\n",icycl,pr,i,prot[pr].ammino[i].z);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
			
		}
	}
	/** for(j=0;j<NPROT;j++){
		
		for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			for(k=0;k<NSPOTS;k++){
				
				indice=i+k+1;
				if((k==0)||(k==SPG_PATCH2-1)){
					if(SPRING_MODEL==1) {
						prot[j].ammino[indice].spring_anchor=1;
					}else{
						prot[j].ammino[indice].spring_anchor=0;
					}
				}else{
					prot[j].ammino[indice].spring_anchor=0;
				}
			}
		}
	  }**/
	
	
	return 0;
}

void Rotation_Random_Sphere (int pr,int k){
	
	
	
	double  u1=0, u2=0, u3=0;
	double a=0,a2=0;
	double v1=0,v2=0,v3=0;
	double u12=0,u22=0,u32=0;
	double a11=0,a12=0,a13=0,a21=0,a22=0,a23=0,a31=0,a32=0,a33=0;
	double nx=0,ny=0,nz=0;
	double mod=0;
	int i=0,indice=0;
	
	
	/*****************RANDOM ROTATIONS**************/
	//Random Quaternion 
	a =gasdev(&seed);
	u1=gasdev(&seed);
	u2=gasdev(&seed);
	u3=gasdev(&seed);
	mod=sqrt(a*a+u1*u1+u2*u2+u3*u3);
	//Quaternion  Normalization
	a/=mod;
	u1/=mod;
	u2/=mod;
	u3/=mod; 
	
	
	/*if(fabs(a*a+u1*u1+u2*u2+u3*u3-1.0)>1e-10) {
		fprintf(out,"PDL1 %lu %30.25lf\n",icycl,a*a+u1*u1+u2*u2+u3*u3-1.0);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	a2=a*a;
	u12=u1*u1;
	u22=u2*u2;
	u32=u3*u3;
	
	
	
	a11=(a2+u12-u22-u32);
	a12=2*(u1*u2-a*u3);
	a13=2*(u1*u3+a*u2);
	
	a21=2*(u2*u1+a*u3);
	a22=(a2-u12+u22-u32);
	a23=2*(u2*u3-a*u1);
	
	a31=2*(u3*u1-a*u2);
	a32=2*(u3*u2+a*u1);
	a33=(a2-u12-u22+u32);
	
	
	
	
	//if(icycl>20) fprintf(out,"U %lf %lf %lf\n",u1,u2,u3);
	//if(icycl>20) fprintf(out,"V %lf %lf %lf %lf %lf\n",v1,v2,v3,a2,b2);
	
	
	
	for(i=0;i<NSPOTS;i++){
		indice=k+i+1;
		
		v1=prot[pr].ammino[indice].x-prot[pr].ammino[k].x;
		v2=prot[pr].ammino[indice].y-prot[pr].ammino[k].y;
		v3=prot[pr].ammino[indice].z-prot[pr].ammino[k].z;
		
		
		nx=a11*v1+a12*v2+a13*v3;
		ny=a21*v1+a22*v2+a23*v3;
		nz=a31*v1+a32*v2+a33*v3;
		
		prot[pr].ammino[indice].x=nx+prot[pr].ammino[k].x;
		prot[pr].ammino[indice].y=ny+prot[pr].ammino[k].y;
		prot[pr].ammino[indice].z=nz+prot[pr].ammino[k].z;
		
	}
	return;
	
}

void Rotation (int pr,int k,double alpha,double u1,double u2,double u3,int flag){
	
	static double a=0,a2=0,c=0;
	double v1=0,v2=0,v3=0;
	static double u12=0,u22=0,u32=0;
	static double a11=0,a12=0,a13=0,a21=0,a22=0,a23=0,a31=0,a32=0,a33=0;
	double nx=0,ny=0,nz=0;
	double modulo=0;
	
	v1=prot[pr].ammino[k].x;
	v2=prot[pr].ammino[k].y;
	v3=prot[pr].ammino[k].z;
	if(flag==1){
		a=cos(alpha);
		a2=a*a;
		
		c=sin(alpha);
		
		modulo=sqrt(u1*u1+u2*u2+u3*u3);
		u1/=modulo;
		u2/=modulo;
		u3/=modulo;
		if(fabs(u1*u1+u2*u2+u3*u3-1.0)>1e-10) {
			fprintf(out,"PDL1 %lu %30.25lf\n",icycl,u1*u1+u2*u2+u3*u3-1.0);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		u1*=c;
		u2*=c;
		u3*=c;
		u12=u1*u1;
		u22=u2*u2;
		u32=u3*u3;
		
		if(fabs(c*c+a2-1.0)>1e-10) {
			fprintf(out,"PDL 2 %lu %30.25lf\n",icycl,c*c+a2-1.0);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		a11=(a2+u12-u22-u32);
		a12=2*(u1*u2-a*u3);
		a13=2*(u1*u3+a*u2);
		
		a21=2*(u2*u1+a*u3);
		a22=(a2-u12+u22-u32);
		a23=2*(u2*u3-a*u1);
		
		a31=2*(u3*u1-a*u2);
		a32=2*(u3*u2+a*u1);
		a33=(a2-u12-u22+u32);
		
		
	}
	
	nx=a11*v1+a12*v2+a13*v3;
	ny=a21*v1+a22*v2+a23*v3;
	nz=a31*v1+a32*v2+a33*v3;
	
	prot[pr].ammino[k].x=nx;
	prot[pr].ammino[k].y=ny;
	prot[pr].ammino[k].z=nz;
	
}

/***********************************
  FINIT FUNCION
  INITIALIZE THE VARIABLES 
 ***********************************/

void finit (void){
	
	/************************************************
	* finit -- initialize the protein:	 		
	*	take the interaction matrix M form the file aapot.dat;
	*	take the protein struct from the file prot;					
	*	give a random sequence to the protein;	
	*   calculate the average crossing number for any protein
	* 						
	* Parameters: 				
	*	prot -- is a proteina type structure where is stored the protein.
	*	M -- is the interaction matrix	*
	*						
	* Return:					
	*	void						
	*************************************************/
	
       
    int 			i=0, j=0, k=0, pr=0, kk=0, dumm=0;
	int                     n1=0, p2=0;
	double 			n=0, x=0, y=0, z=0, module=0;
	int *connector;
	double 			**vertices=NULL;
	int 			subminx=10000, subminy=10000, subMAXx=-100000, subMAXy=-100000, NRx=0, NRy=0;
	unsigned int     	indice=0;
	long int 		indicen=0;
	char 			message[200], line[200];
	char 			**Res=NULL, *SubRes=NULL;
	FILE 			*fp=NULL, *fSeqvmd=NULL,*finitialconf=NULL;
	
	for(j=0;j<S;j++)	{
		sist->np[j]=1;
		sist->p[j]=1;
	}
	Perm_Test(116);
	
	fp=fopen("tempera.dat","r");
	if ( fp == NULL) {
		printf ("File tempera.dat not found!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else 	{
		for(j=0;j<p;j++)	{/// p is the number of used temperaures. I pass this number ($c) with the mpirun command in the script file .csh
			fgets_out=fgets(line,sizeof(line),fp);
			sscanf (line,"%lf\n",&x);
			beta[j]=1./x;
		}
		betaindice=my_rank;
		fclose(fp);
	}
	
	fp=fopen("aapot.dat","r");
	if ( fp == NULL)	{
		printf ("File aapot.dat not found!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else 	{
	        p2=fscanf (fp,"%d",&dumm); 
		p2=fscanf (fp,"%lf",&y);
		HOH_Burried_Threshold=y;
		p2=fscanf (fp,"%lf",&y);
		Hydropathy_Scale=y;
		
		for(j=0;j<S;j++)	{
			for(i=j;i<S;i++){
				p2=fscanf (fp,"%lf",&M[i][j]);
				//M[i][j]*=CA_Scale; /* Done later in the calculation */
				M[j][i]=M[i][j];
			}
		}
		for(i=0;i<S;i++)	{
			M[0][i]*=Hydropathy_Scale*CA_Scale;
			M[i][0]=M[0][i];
		}
	}
	fclose(fp);
        fprintf(out,"prima di no iso");fflush(out);
#ifdef NO_ISO /// switch off isotropic contributions
	for(i=0;i<S;i++){
	  for(j=0;j<S;j++){
	    M[i][j]=0;
	  }
	}
#endif
	fprintf(out,"dopo no iso");fflush(out);
	fprintf(out,"NPROT=%d\n",NPROT);
	prot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
	old_prot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
	minprot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
	bestOprot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
	maxOprot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
	///prot_knot= (struct proteina_knot *)calloc(1,sizeof(struct proteina_knot));
	///prot_knot->bead= (struct part_knot *)calloc(Psize,sizeof(struct part_knot));
		
	Seq=(char **)calloc(NPROT,sizeof(char*));	/// The matrix Seq[i][j] contains the sequence of the protein i. The dimension of Seq[i][j] is number_of_proteins X length_protein
	if(Seq==NULL){	
		fprintf(out,"allocation of Seq failed in finit\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	Res=(char **)calloc(NPROT,sizeof(char*));
	if(Res==NULL){
		fprintf(out,"allocation of Res failed in finit\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	Plength=( int *) calloc((NPROT+1),sizeof(unsigned int));
	Plength_half=( int *) calloc((NPROT+1),sizeof(unsigned int));
	
	ProtN=0;	/// ProtN is the max number of Plenght over all the chains
	pr=0;	/// Plength is the number of interacting elements including patchys per chain
	
	for(pr=0;pr<NPROT;pr++){
		Plength[pr]=( int)(Psize)*(NATOM);
		Plength_half[pr]=( int)(Psize/2.0)*NATOM;
		if(ProtN<=Plength[pr]) ProtN=Plength[pr];
	}
	AmminoN=ProtN/NATOM;
	fprintf(out,"ProtN=%d AmminoN=%d\n",ProtN,AmminoN);
	HalfN=partint(ProtN/2);
	sizeZ=partint(LBox*(ProtN)/2);
	
	for(j=0;j<NPROT;j++){
	  /*Seq[j]=(char *)calloc((Plength[j]/NATOM+1),sizeof(char));
		if(Seq[j]==NULL){
			fprintf(out,"allocation of Seq failed in finit\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		Res[j]=(char *)calloc((Plength[j]/NATOM+1),sizeof(char));
		if(Res[j]==NULL){
			fprintf(out,"allocation of Res failed in finit\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			}*/
		
		prot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		
		old_prot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		minprot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		bestOprot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		maxOprot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		if(prot[j].ammino==NULL){
			fprintf(out,"allocation of prot->ammino failed in finit\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}
	
	fprintf(out,"allocation of Seq ok\n");fflush(out);
	
	/*for(i=0;i<NPROT;i++){
	  for (k=0;k<(Plength[i]/NATOM+1);k++){
			Seq[i][k]='C';
	  }
	  }*/
		
	///fastadecoder(Res,Seq);
	fprintf(out,"fastadecoder of Seq ok\n");fflush(out);
	BoxSize*=(ProtN/NATOM+1.0)*(sist->Rmin_CAsaw*1.5); //the number 2.447976 is the bond length
	BoxSize2=BoxSize/2.0;
	
	Water_Changed_pr=(int *)calloc(NPROT*ProtN,sizeof(int));
	Water_Changed_i=(int *)calloc(NPROT*ProtN,sizeof(int));
	
	fp=fopen("sub","r");/// a che serve il file sub ???????
	if ( fp == NULL) {
		fprintf (out,"No sub found\n");
		fflush(out);
	}else{
		sub= (struct proteina *)calloc(1,sizeof(struct proteina));
		fgets_out=fgets(line,sizeof(line),fp);
		sscanf (line,"%*s %lf",&n);
		
		SubN=(int)(n);
		Plength[NPROT]=SubN;
		SubSeq=(char *)calloc((SubN+1),sizeof(char));
		SubRes=(char *)calloc((SubN+1),sizeof(char));
		if(SubSeq==NULL){
			fprintf(out,"allocation of SubSeq failed in finit\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		sub->ammino= (struct part *)calloc((Plength[NPROT]),sizeof(struct part));
		rewind(fp);
		
		fgets_out=fgets(line,sizeof(line),fp);
		sscanf (line,"%s %*f",SubSeq);
		
		fclose(fp); 
		fp=fopen("sub1","r");
		if ( fp == NULL) {
			printf ("Substrate sequence present but no substrate coordinates\n");
			
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}else{
			
			fgets_out=fgets(line,sizeof(line),fp);
			sscanf (line,"%d\n",&n1);
			fgets_out=fgets(line,sizeof(line),fp);
			
			for(i=0,j=0;i<n1;i++){
				fgets_out=fgets(line,sizeof(line),fp);
				p2=sscanf (line,"%*s%lf%lf%lf\n",&x,&y,&z);
				if(p2<3){
					fprintf(out,"Bad file Format prot3!!!\n");
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}else{
					if((i-1)%2){
						sub->ammino[j%SubN].x=x;
						sub->ammino[j%SubN].y=y;
						sub->ammino[j%SubN].z=z;
						j++;
					}
				}
			}
			fclose(fp);
		} 
		fprintf(out,"fastadecoder\n");
		fastadecoder2(SubRes,SubSeq);
		fprintf(out,"fastadecoder ok\n");
		for(i=0;i<SubN;i++){
			if(subminx>=sub->ammino[i].x) subminx=sub->ammino[i].x;
			if(subminy>=sub->ammino[i].y) subminy=sub->ammino[i].y;
			if(subMAXx<=sub->ammino[i].x) subMAXx=sub->ammino[i].x;
			if(subMAXy<=sub->ammino[i].y) subMAXy=sub->ammino[i].y;
		}
		NRx=partint(LBox*ProtN/(2*(subMAXx-subminx+1)));
		NRy=partint(LBox*ProtN/(2*(subMAXy-subminy+1)));
	}
	
	////////////////////////////////////////////////////////////
	vertices=(double **)calloc((NATOM),sizeof(double*));	/// qui memorizziamo le coordinate di tutto un bead, lette dal file 
	for(j=0;j<NATOM;j++){
	  vertices[j]=(double *)calloc(3,sizeof(double));
	}
	connector=(int *)calloc((NATOM),sizeof(int)); /// here I memorize at which patch is attached the spring
	for (i=0;i<NATOM;i++){
		connector[i]=0;
	}
	fp=fopen("patches_structure.dat","r");
	fscanf(fp,"%d",&k); /// this value is the number NSPOTS ... I've already red it
	vertices[0][0]=vertices[0][1]=vertices[0][2]=0.;	///coordinates of the C_alpha
	///connector[0]=0;
	for (k=1;k<NATOM;k++)	{
	  fscanf(fp,"%lf",&vertices[k][0]);
	  fscanf(fp,"%lf",&vertices[k][1]);
	  fscanf(fp,"%lf",&vertices[k][2]);
	  fscanf(fp,"%d",&connector[k]);
	  module=sqrt(vertices[k][0]*vertices[k][0] + vertices[k][1]*vertices[k][1] + vertices[k][2]*vertices[k][2]);
	  vertices[k][0]=(sist->Bbond[1])*vertices[k][0]/module;
	  vertices[k][1]=(sist->Bbond[1])*vertices[k][1]/module;
	  vertices[k][2]=(sist->Bbond[1])*vertices[k][2]/module;
	}
	fclose(fp);

	if(SPRING_MODEL==1)	{
	  if ( (connector[1]!=1) || (connector[SPG_PATCH2]!=1) || (vertices[1][2] > vertices[SPG_PATCH2][2]) ){
	    fprintf(out,"The first and last patches in patches_total.dat must be the binding onces if SPRING_MODEL=1, and in the right z order => the last patch must have a z > first patch!!! \n");
	    fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	}
	
	fprintf(out,"prot3\n");
	fflush(out);
	fp=fopen("prot3","r");
	finitialconf=fopen("initial_conf.xyz","w");
	if ( fp == NULL) {
	  for(j=0;j<NPROT;j++){
	    for(i=ATOM_CA;i<Plength[j];i+=NATOM){
	      for(k=0;k<NATOM;k++)	{
		prot[j].ammino[i+k].x=vertices[k][0] + j*spacing + 1.0;
		prot[j].ammino[i+k].y=vertices[k][1];
		prot[j].ammino[i+k].z=vertices[k][2] + sist->Rmin_CAsaw*1.1*i/NATOM + 1.0;
		if (k==0) fprintf(finitialconf,"%8.3lf%8.3lf%8.3lf\n",prot[j].ammino[i+k].x,prot[j].ammino[i+k].y,prot[j].ammino[i+k].z);
		if(SPRING_MODEL==1)	{
		  prot[j].ammino[i+k].spring_anchor=connector[k];
		}
		else{
			prot[j].ammino[i+k].spring_anchor=0;	
		}

		  	
		if (k==0)
		  prot[j].ammino[i].id=ATOM_CA;
		else
		  prot[j].ammino[i+k].id=ATOM_H;
	      }
	    }
	  }
	  
	  /***********************************************/
	  
	}else{
	  readBinary(fp);
	  for(j=0;j<NPROT;j++){
	    for(i=ATOM_CA;i<Plength[j];i+=NATOM){
	      for(k=0;k<NATOM;k++)	{
		if(SPRING_MODEL==1)
		  prot[j].ammino[i+k].spring_anchor=connector[k];
		else
		  prot[j].ammino[i+k].spring_anchor=0;
	      }
	    }
	  }
	}
	
	
	
	
	fprintf(out,"prot3 ok\n");
	fflush(out);
	fclose(finitialconf);
	fprintf(out,"contact \n");fflush(out);
	DistMap1=(double**)calloc(ProtN,sizeof(double*));
	for(i=0;i<ProtN;i++)
		DistMap1[i]=(double*)calloc(ProtN,sizeof(double));
	
	DistMap2=(double**)calloc(ProtN,sizeof(double*));
	for(i=0;i<ProtN;i++)
		DistMap2[i]=(double*)calloc(ProtN,sizeof(double));
		
	for(i=0;i<(NPROT);i++)
		fprintf(out,"Plengths[%d]=%d\n",i,Plength[i]);
		
	fflush(out);
	for(i=0;i<ProtN;i++)
		for(j=0;j<ProtN;j++){
			DistMap1[i][j]=-1; 
			DistMap2[i][j]=-1; 
		}
	
	fprintf(out,"contact ok\n");fflush(out);
	
	fprintf(out,"seq set \n");fflush(out);
	for(j=0;j<S;j++){	/// a che servono sist.p e sist.np ????
		sist->np[j]=1;
		sist->p[j]=1;
	}
	Perm_Test(117);
	for(j=0;j<NPROT;j++){
		for(i=0;i<Plength[j]/NATOM;i++){
			for(k=0;k<NATOM;k++)
			  //prot[j].ammino[i*NATOM+k].residue=Res[j][i]; /// cosa c'é in Res? numeri o lettere? qual é il range ????
			  prot[j].ammino[i*NATOM+k].residue=1;
			  
				
			if(j==0) sist->p[1]++;
		}
	}
	Rescaler();
	for(j=0;j<S;j++){	
		sist->np[j]=sist->p[j];
	}
	
	Perm_Test(118); 
	N=(int)(Plength[0]/NATOM)+1;
	if (N>100){
	  sist->NP=N*log(N)-N+0.5*log(2*PI*N);;}
	else{
	sist->NP=log(factorial(N));
	}
	for(j=0;j<S;j++){
	  if (sist->np[j]>100){
	    sist->NP-=(sist->np[j]*log(sist->np[j]))-sist->np[j]+0.5*log(2*PI*sist->np[j]);}
	  else{
	    sist->NP-=log(factorial(sist->np[j]));
	  }
	}
	sprintf(message, "VMD/Seqvmd-T-%lf-INIT.pdb",1/beta[betaindice]);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			for(kk=0;kk<NATOM;kk++){
			x=bestOprot[j].ammino[i+kk].x=minprot[j].ammino[i+kk].x=prot[j].ammino[i+kk].x;
			y=bestOprot[j].ammino[i+kk].y=minprot[j].ammino[i+kk].y=prot[j].ammino[i+kk].y;
			z=bestOprot[j].ammino[i+kk].z=minprot[j].ammino[i+kk].z=prot[j].ammino[i+kk].z;
			bestOprot[j].ammino[i+kk].id=minprot[j].ammino[i+kk].id=prot[j].ammino[i+kk].id;
				if(NPROT>1) {
					x=P_Cd(x);y=P_Cd(y);z=P_Cd(z);
				}
				
				fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",j*NPROT+i+1,sist->Atoms[prot[j].ammino[i+kk].id],sist->Amminoacids[prot[j].ammino[i].residue],i/NATOM+1,x,y,z);
			}
		}
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	
	fprintf(out,"seq set ok\n");fflush(out);
	
	N_SAW_cells=partint(BoxSize/(sist->Rmin*1.1));
	N_SAW_cells2=N_SAW_cells*N_SAW_cells;
	N_SAW_cells3=N_SAW_cells*N_SAW_cells2;
	cell_SAW_size=BoxSize/N_SAW_cells;
	
	N_CA_cells=partint(BoxSize/(sist->RintD*1.1));
	cell_CA_size=BoxSize/N_CA_cells;
	
	N_H_cells=N_O_cells=partint(BoxSize/(sist->RintH*1.1));
	cell_H_size=cell_O_size=BoxSize/N_O_cells;
	
	fprintf(out,"new_cell_list N_CA_cells=%d cell_CA_size=%lf BoxSize=%lf\n",N_CA_cells,cell_CA_size,BoxSize);fflush(out);
	fprintf(out,"new_cell_list N_H_cells=%d cell_H_size=%lf\n",N_H_cells,cell_H_size);fflush(out);
	fprintf(out,"new_cell_list N_SAW_cells=%d cell_SAW_size=%lf\n",N_SAW_cells,cell_SAW_size);fflush(out);
	
	neighcells=(int *)calloc(3*27,sizeof(int));
	
	indice=0;
	for(i=-1;i<2;i++){	/// here I store the displacement versors!!!!!
		for(j=-1;j<2;j++){
			for(k=-1;k<2;k++){
				neighcells[indice*3]=i;
				neighcells[indice*3+1]=j;
				neighcells[indice*3+2]=k;
				fprintf(out,"Finit indice=%d neighx=%d neighy=%d neighz=%d\n",indice,neighcells[indice*3],neighcells[indice*3+1],neighcells[indice*3+2]);fflush(out);
				indice++;
			}
		}
	}
		
	new_cell_list_SAW(); /// creates the cell list of the hard part of the potential of the CA atoms
	                     ///  from scratch, it will be not called again.
	test_celllist_SAW(0); /// check that the cell list is coherent, this will be called again
	new_cell_list(); /// same but for soft potential part of CA and H atoms
	test_celllist_CA();
	test_celllist_H();
	
	/**********RMSD INIT**************/
	
	RMSD_Neigh=(int*)calloc(AmminoN,sizeof(int));
	RMSD_neigh=(int*)calloc(AmminoN*NEIGH_MAX,sizeof(int));
	for(indicen=0;indicen<AmminoN;indicen++)
		RMSD_Neigh[indicen]=0;
		
	/**********************************/
	
	mossaid=100; 
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			bestOprot[pr].ammino[i].ndipole_x=minprot[pr].ammino[i].ndipole_x=prot[pr].ammino[i].dipole_x=prot[pr].ammino[i].ndipole_x=0;
			bestOprot[pr].ammino[i].ndipole_y=minprot[pr].ammino[i].ndipole_y=prot[pr].ammino[i].dipole_y=prot[pr].ammino[i].ndipole_y=0;
			bestOprot[pr].ammino[i].ndipole_z=minprot[pr].ammino[i].ndipole_z=prot[pr].ammino[i].dipole_z=prot[pr].ammino[i].ndipole_z=0;
		}
	}
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){ /// External field strenght 
		fprintf(out,"set Dipoles \n");fflush(out);
		for(pr=0;pr<NPROT;pr++){
			for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
				prot[pr].ammino[i].dipole_x=prot[pr].ammino[i].ndipole_x=0; /// ndipole e dipole sono uguali??
				prot[pr].ammino[i].dipole_y=prot[pr].ammino[i].ndipole_y=0;
				prot[pr].ammino[i].dipole_z=prot[pr].ammino[i].ndipole_z=Alpha*E_Dip*Rmin3[prot[pr].ammino[i].residue]; /// The strenght of the dipol is proportional to the cube of particle size
			}
		}
		fprintf(out,"set Dipoles A \n");fflush(out);
		sist->EDip_New=sist->EDip=Dipole_Self_Cons();
		fprintf(out,"set Dipoles B \n");fflush(out);
		sist->dip_contact=sist->ndip_contact;  /// ndipole e dipole sono uguali??
		for(pr=0;pr<NPROT;pr++){
			for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
				prot[pr].ammino[i].dipole_x=prot[pr].ammino[i].ndipole_x;
				prot[pr].ammino[i].dipole_y=prot[pr].ammino[i].ndipole_y;
				prot[pr].ammino[i].dipole_z=prot[pr].ammino[i].ndipole_z;
			}
		}
		fprintf(out,"set Dipoles C\n");fflush(out);
	}

	#endif
	fprintf(out,"set energy \n");fflush(out);
	sist->E=energy();
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			sist->E+=Bond_Energy_fw(pr,i);
		}
	}
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		sist->E+=sist->EDip;
	}
	#endif
	#ifdef WATER_UP_TEST
	mossaid=124;
	Water_Update_Test();
	#endif
		
	fprintf(out,"set energy ok\n");fflush(out);
	
	fprintf(out,"Cluster Analysys Set Up\n");fflush(out);
	
	sist->Cluster_Index=(int **) calloc(NPROT, sizeof(int *));
	for(i=0;i<NPROT;i++){    
		sist->Cluster_Index[i]=(int *) calloc(NPROT, sizeof(int));
		for(j=0;j<NPROT;j++){
			sist->Cluster_Index[i][j]=-1;
		}
	}
	sist->Cluster_Size=(int *) calloc(NPROT, sizeof(int));
	
	
	sist->clust_CM_x=(double *) calloc(clust_maxsize, sizeof(double));
	sist->clust_CM_y=(double *) calloc(clust_maxsize, sizeof(double));
	sist->clust_CM_z=(double *) calloc(clust_maxsize, sizeof(double));
	fprintf(out,"Cluster Allocation Ok\n");fflush(out);
	fprintf(out,"Initial Cluster calculation\n");fflush(out);
	
	sist->Cluster_N = Cluster_Builder(sist->Cluster_Index,sist->Cluster_Size);
	
	fprintf(out,"Initial Cluster calculation OK\n");fflush(out);
	
}	

double Potential (double r,int pr,int i,int prt,int j,int resi,int resj){
	
  double potential,energ=0.0;
	double LJ_repulsive;
	
	potential=(double)(M[resi][resj]);
	
	
	
	
	LJ_repulsive=-1.0/(1.0 + exp(2.5*(ECA_Range - r))) + 1.0;
	
	
	energ=potential*LJ_repulsive;
	
	
	if(icycl%nsamp==0)fprintf(ECAout,"%lu %lf %lf %lf %lf\n",icycl,1/beta[betaindice],r,LJ_repulsive,potential);
	///energ=0.; /// SWITCH OFF		
	return energ;
	
}

double Potential_CA (int pr,int i,int prt,int j, double *energy_CA_2){
	
	double energCA=0.0;
	double dx,dy,dz;
	double r,rCA2, r6, r12;
	tube_cont=0;
	dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
	dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
	dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	
	rCA2=(dx*dx+ dy*dy + dz*dz);
	*energy_CA_2 = 0.;
#ifdef NAN_CHECK
	if((isnan(rCA2)!=0)||(isnan(1.0/rCA2)!=0)||(rCA2==0)){
	  fprintf(out,"C Mossaid=%d non ok icycl=%lu, rCA2=%lf \n",mossaid,icycl,rCA2);
	  fprintf(out,"pr=%d i=%d coordinates %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
	  fprintf(out,"prt=%d j=%d coordinates %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
	  fflush(out);
	  MPI_Abort(MPI_COMM_WORLD,err5);
	}
#endif
	
	if((j < i-NATOM)&&(MINR>rCA2)){
	  MINR=rCA2; /// calculates the minimum rCA2
	}
	
	
        #ifdef TUBE
	if (rCA2<81) {
	  tube_cont=1;
	}else{
	  tube_cont=0;
	}
	if (rCA2<Rtube){
	  r6=pow(rCA2,3);
	  r12=pow(rCA2,6);
	  energCA=((sigma_tube12/r12)-(sigma_tube6/r6));
	  ///fprintf(out,"tube %llu i=%d j=%d rCA2=%lf enerCA=%lf\n",icycl,i,j,rCA2,energCA);fflush(out);
	  	  
	}
	return energCA;
	#endif
	
	if (rCA2<sist->Rint2){
	  r=sqrt(rCA2);
		
	  energCA=(-1.0/(1.0 + exp(2.5*(ECA_Range - r))) + 1.0);  /// CA isotropic interaction, it will be multiplied by the interaction
		///fprintf(out,"i=%d j=%d rCA2=%lf energyCA=%lf\n",i,j,rCA2,energCA);fflush(out); 
		if ( ( second_minimum_CA != 0 )&&(rCA2>zero_potential_CA2) ){
		  rCA2 = rCA2 - 2*r*shift_potential_CA + shift_potential_CA2;
		  r6=pow(rCA2,EH_LJPower2_2);
		  r12=pow(rCA2,EH_LJPower1_2);
		  *energy_CA_2=(sigmahb12/r12-sigmahb10/r6);
		  ///fprintf(out,"i=%d j=%d rCA2=%lf energy_CA_2=%lf\n",i,j,rCA2,*energy_CA_2);fflush(out);
		  if ( *energy_CA_2 > 0 ) {
		    fprintf(out,"Positive second minimum potentialCA energy i=%d j=%d prt=%d rCA2=%lf energy_CA_2=%lf\n",i,j,prt,rCA2,*energy_CA_2);
		  fflush(out);
		  MPI_Abort(MPI_COMM_WORLD,err5);
		  }
		   
		}
	}
	///fprintf(out,"i=%d j=%d rCA2=%lf enerCA=%lf\n",i,j,rCA2,energCA);fflush(out);
	return energCA;
} 

double Potential_Dip (int pr,int i,int prt,int j,int *touch ){
	
  double energ=0.0,energDip=0.0;
	//double oop=0;
	double dx,dy,dz;
	int ttouch=0;
	double r,rCA2;
	
	
	dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
	dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
	dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	
	rCA2=(dx*dx+ dy*dy + dz*dz);
	
	//fprintf(out,"rCA2=%lf\t\tdx=%lf dy=%lf dz=%lf\n",rCA2,dx,dy,dz);fflush(out);
	
	if (rCA2<sist->RintD2){
		r=sqrt(rCA2);
		energDip=Alpha*E_Dip*E_Dip*Rmin3[prot[pr].ammino[i].residue]*Rmin3[prot[prt].ammino[j].residue]*((1./(r*rCA2))*(1-3.0*(dz/r)*(dz/r)));
		//fprintf(EDOut,"%lf %lf %lf\n",r,dz,energDip);fflush(EDOut);
		if(energDip<=DIPOLE_BOND_THRESHOLD*Alpha*E_Dip*E_Dip*Rmin3[prot[pr].ammino[i].residue]*Rmin3[prot[prt].ammino[j].residue]) ttouch++;
		energ+=energDip;
	}
	*touch+=ttouch;
	///energ=0.; /// SWITCH OFF
	return energ;
	
}

double Dipole_Self_Cons (void){
	int pr,i,prt,j,flag=0;
	double dx,dy,dz;
	double r_x=0,r_y=0,r_z=0;
	double dot_1=0,dot_2=0,dot_3=0;
	double r,rCA2;
	int iteration=0;
	double Edip=0.0,Edip_old=0.0,energDip=0;
	#ifdef NOSFC_COMPARE	
	double Edip_Compare=0.0;	
	#endif
	
	while(flag<1){
		Edip_old=Edip;
		Edip=0;
		sist->ndip_contact=0;
		
		for(pr=0;pr<NPROT;pr++){
			for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
				
				/* This initialization should be here because is the first guess for this particular Dipole*/
				/*During the iteration each dipole will be recalculated taking into account the new dipoles generated before*/
				prot[pr].ammino[i].ndipole_x=0;
				prot[pr].ammino[i].ndipole_y=0;
				prot[pr].ammino[i].ndipole_z=Alpha*E_Dip*Rmin3[prot[pr].ammino[i].residue];
				
				for(prt=0;prt<NPROT;prt++){
					for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
						if((pr==prt)&&(i==j)){
						}else{
							
							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
							
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							
							rCA2=(dx*dx+ dy*dy + dz*dz);
							#ifdef NAN_CHECK							
							if((isnan(rCA2)!=0)||(isnan(1.0/rCA2)!=0)||(rCA2==0)){
								fprintf(out,"C Mossaid=%d non ok icycl=%lu, rCA2=%lf \n",mossaid,icycl,rCA2);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							#endif
							//fprintf(out,"rCA2=%lf\t\tdx=%lf dy=%lf dz=%lf\n",rCA2,dx,dy,dz);fflush(out);
							
							//if (rCA2<sist->RintD2){
								
								
								r=sqrt(rCA2);
								
								/*Distance versor*/
								#ifdef NAN_CHECK
								if((isnan(r)!=0)||(isnan(1.0/r)!=0)||(r==0)){
									fprintf(out,"A Mossaid=%d non ok icycl=%lu, r=%lf \n",mossaid,icycl,r);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								#endif
								r_x=dx/r;
								r_y=dy/r;
								r_z=dz/r;
								#ifdef NAN_CHECK
								if((isnan(r_x)!=0)||(isnan(1.0/r_x)!=0)){
									fprintf(out,"AA Mossaid=%d non ok icycl=%lu, r_x=%lf r=%lf\n",mossaid,icycl,r_x,r);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,r_x,r_y,r_z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								if((isnan(r_y)!=0)||(isnan(1.0/r_y)!=0)){
									fprintf(out,"AA Mossaid=%d non ok icycl=%lu, r_y=%lf r=%lf\n",mossaid,icycl,r_y,r);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,r_x,r_y,r_z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								if((isnan(r_z)!=0)||(isnan(1.0/r_z)!=0)){
									fprintf(out,"AA Mossaid=%d non ok icycl=%lu, r_z=%lf r=%lf\n",mossaid,icycl,r_z,r);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,r_x,r_y,r_z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								#endif
								
								/*Dot product between the second dipole and the distance versor*/
								dot_3=	prot[prt].ammino[j].ndipole_x*r_x+
								prot[prt].ammino[j].ndipole_y*r_y+
								prot[prt].ammino[j].ndipole_z*r_z;
								
								if(prot[pr].ammino[i].residue>=S){
									fprintf(out,"G Mossaid=%d non ok icycl=%lu, prot[pr].ammino[i].residue>=S=%d \n",mossaid,icycl,prot[pr].ammino[i].residue);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								prot[pr].ammino[i].ndipole_x+=Rmin3[prot[pr].ammino[i].residue]*(3*dot_3*r_x-prot[prt].ammino[j].ndipole_x)/(r*rCA2);
								prot[pr].ammino[i].ndipole_y+=Rmin3[prot[pr].ammino[i].residue]*(3*dot_3*r_y-prot[prt].ammino[j].ndipole_y)/(r*rCA2);
								prot[pr].ammino[i].ndipole_z+=Rmin3[prot[pr].ammino[i].residue]*(3*dot_3*r_z-prot[prt].ammino[j].ndipole_z)/(r*rCA2);
								
								
								if((prot[pr].ammino[i].ndipole_x>1e3)||(prot[pr].ammino[i].ndipole_y>1e3)||(prot[pr].ammino[i].ndipole_z>1e3)){
									fprintf(out,"I Mossaid=%d non ok icycl=%lu iteration=%d\n",mossaid,icycl,iteration);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								/*Total contribution to the energy of the Dipoles (Eq 4.7 Griffith) E_Dip[1] is the external field applied*/
								
								//}
							#ifdef NAN_CHECK
							if((isnan(prot[pr].ammino[i].ndipole_x)!=0)||(isnan(1.0/prot[pr].ammino[i].ndipole_x)!=0)){
								fprintf(out,"DD Mossaid=%d non ok icycl=%lu, prot[%d].ammino[%d].ndipole_x=%lf r_x=%lf r=%lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].ndipole_x,r_x,r);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							if((isnan(prot[pr].ammino[i].ndipole_y)!=0)||(isnan(1.0/prot[pr].ammino[i].ndipole_y)!=0)){
								fprintf(out,"DD Mossaid=%d non ok icycl=%lu, prot[%d].ammino[%d].ndipole_y=%lf r_y=%lf r=%lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].ndipole_y,r_y,r);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							if((isnan(prot[pr].ammino[i].ndipole_z)!=0)||(isnan(1.0/prot[pr].ammino[i].ndipole_z)!=0)){
								fprintf(out,"DD Mossaid=%d non ok icycl=%lu, prot[%d].ammino[%d].ndipole_z=%lf r_z=%lf r=%lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].ndipole_z,r_z,r);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							if(isnan(Edip)!=0){
								fprintf(out,"F Mossaid=%d non ok icycl=%lu iteration=%d prt=%d j=%d, Edip=%lf \n",mossaid,icycl,iteration,prt,j,Edip);
								fflush(out);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							#endif
						}
					}
					
				}
			}
		}	
		for(pr=0;pr<NPROT;pr++){
			for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
				#ifdef NAN_CHECK				
				if(isnan(Edip)!=0){
					fprintf(out,"D Mossaid=%d non ok icycl=%lu iteration=%d, Edip=%lf \n",mossaid,icycl,iteration,Edip);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				#endif
				Edip+=-E_Dip*prot[pr].ammino[i].ndipole_z;
				#ifdef NAN_CHECK				
				if((isnan(Edip)!=0)||(isnan(1.0/Edip)!=0)){
					fprintf(out,"D Mossaid=%d non ok icycl=%lu, Edip=%lf \n",mossaid,icycl,Edip);
					fprintf(out,"prot[%d].ammino[%d].ndipole_z=%lf\n",pr,i,prot[pr].ammino[i].ndipole_z);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				#endif
				
				for(prt=0;prt<NPROT;prt++){
					for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
						if((pr==prt)&&(i==j)){
						}else{
							
							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
							
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							
							rCA2=(dx*dx+ dy*dy + dz*dz);
							
							//fprintf(out,"rCA2=%lf\t\tdx=%lf dy=%lf dz=%lf\n",rCA2,dx,dy,dz);fflush(out);
							
							//if (rCA2<sist->RintD2){
								
								
								r=sqrt(rCA2);
								#ifdef NAN_CHECK								
								if((isnan(r)!=0)||(isnan(1.0/r)!=0)){
									fprintf(out,"B Mossaid=%d non ok icycl=%lu, r=%lf \n",mossaid,icycl,r);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								#endif
								/*Distance versor*/
								r_x=dx/r;
								r_y=dy/r;
								r_z=dz/r;
								
								
								
								/*Dot product between the dipoles*/
								dot_1=	prot[pr].ammino[i].ndipole_x*prot[prt].ammino[j].ndipole_x+
								prot[pr].ammino[i].ndipole_y*prot[prt].ammino[j].ndipole_y+
								prot[pr].ammino[i].ndipole_z*prot[prt].ammino[j].ndipole_z;
								
								/*Dot product between the first dipole and the distance versor*/
								dot_2=	prot[pr].ammino[i].ndipole_x*r_x+
								prot[pr].ammino[i].ndipole_y*r_y+
								prot[pr].ammino[i].ndipole_z*r_z;
								
								/*Dot product between the second dipole and the distance versor*/
								dot_3=	prot[prt].ammino[j].ndipole_x*r_x+
								prot[prt].ammino[j].ndipole_y*r_y+
								prot[prt].ammino[j].ndipole_z*r_z;
								
								
								
								/*Total contribution to the energy of the Dipoles (Eq 4.7 Griffith) E_Dip[1] is the external field applied*/
								energDip=(dot_1-3*dot_2*dot_3)/(r*rCA2*Alpha);
								#ifdef NAN_CHECK								
								if((isnan(energDip)!=0)||(isnan(1.0/energDip)!=0)){
									fprintf(out,"E Mossaid=%d non ok icycl=%lu iteration=%d, Edip=%lf \n",mossaid,icycl,iteration,energDip);
									fprintf(out,"dot_1=%lf dot_2=%lf dot_3=%lf r=%lf rCA2=%lf Alpha=%lf\n",dot_1,dot_2,dot_3,r,rCA2,Alpha);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								#endif
								//fprintf(EDOut,"%d %lf %lf %lf\n",iteration,Edip_old,Edip,energDip);fflush(EDOut);
								Edip+=energDip;
								#ifdef NAN_CHECK								
								if((isnan(Edip)!=0)||(isnan(1.0/Edip)!=0)){
									fprintf(out,"H Mossaid=%d non ok icycl=%lu iteration=%d, Edip=%lf energDip=%lf\n",mossaid,icycl,iteration,Edip,energDip);
									fprintf(out,"dot_1=%lf dot_2=%lf dot_3=%lf r=%lf rCA2=%lf Alpha=%lf\n",dot_1,dot_2,dot_3,r,rCA2,Alpha);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
									fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
									fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								#endif
								if(energDip<=DIPOLE_BOND_THRESHOLD*Alpha*E_Dip*E_Dip*Rmin3[prot[pr].ammino[i].residue]*Rmin3[prot[prt].ammino[j].residue]) sist->ndip_contact++;
								//}
						}
					}
					
				}
				
				
			}
		}
		flag=1;
		//fprintf(EDOut,"%lu %d %lf %lf\n",icycl,iteration,Edip_old,Edip);fflush(EDOut);
		/*Self consisten condition*/
		if(fabs(Edip_old-Edip)>DIPOLE_SFC_THRESHOLD) flag=0;
		iteration++;
		
	}
	
	#ifdef NOSFC_COMPARE	
	Edip_Compare=0.0;
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			Edip_Compare+=Alpha*E_Dip*E_Dip*Rmin3[prot[pr].ammino[i].residue];
			for(prt=0;prt<NPROT;prt++){
				for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
					
					
					if((pr==prt)&&(i==j)){
					}else{
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
						
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
						
						rCA2=(dx*dx+ dy*dy + dz*dz);
						
						//fprintf(out,"rCA2=%lf\t\tdx=%lf dy=%lf dz=%lf\n",rCA2,dx,dy,dz);fflush(out);
						
						//if (rCA2<sist->RintD2){
							
							
							r=sqrt(rCA2);
							#ifdef NAN_CHECK								
							if((isnan(r)!=0)||(isnan(1.0/r)!=0)){
								fprintf(out,"B Mossaid=%d non ok icycl=%lu, r=%lf \n",mossaid,icycl,r);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							#endif
							/*Distance versor*/
							r_x=dx/r;
							r_y=dy/r;
							r_z=dz/r;
							
							
							
							/*Dot product between the dipoles*/
							dot_1=	Alpha*E_Dip*Rmin3[prot[pr].ammino[i].residue]*Alpha*E_Dip*Rmin3[prot[pr].ammino[i].residue];
							
							/*Dot product between the first dipole and the distance versor*/
							dot_2=	Alpha*E_Dip*Rmin3[prot[pr].ammino[i].residue]*r_z;
							
							/*Dot product between the second dipole and the distance versor*/
							dot_3=	Alpha*E_Dip*Rmin3[prot[pr].ammino[i].residue]*r_z;
							
							
							
							/*Total contribution to the energy of the Dipoles (Eq 4.7 Griffith) E_Dip[1] is the external field applied*/
							energDip=(dot_1-3*dot_2*dot_3)/(r*rCA2*Alpha);
							#ifdef NAN_CHECK								
							if((isnan(energDip)!=0)||(isnan(1.0/energDip)!=0)){
								fprintf(out,"E Mossaid=%d non ok icycl=%lu iteration=%d, Edip_Compare=%lf \n",mossaid,icycl,iteration,energDip);
								fprintf(out,"dot_1=%lf dot_2=%lf dot_3=%lf r=%lf rCA2=%lf Alpha=%lf\n",dot_1,dot_2,dot_3,r,rCA2,Alpha);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							#endif
							//fprintf(EDOut,"%d %lf %lf %lf\n",iteration,Edip_Compare_old,Edip_Compare,energDip);fflush(EDOut);
							Edip_Compare+=energDip;
							#ifdef NAN_CHECK								
							if((isnan(Edip_Compare)!=0)||(isnan(1.0/Edip_Compare)!=0)){
								fprintf(out,"H Mossaid=%d non ok icycl=%lu iteration=%d, Edip_Compare=%lf energDip=%lf\n",mossaid,icycl,iteration,Edip_Compare,energDip);
								fprintf(out,"dot_1=%lf dot_2=%lf dot_3=%lf r=%lf rCA2=%lf Alpha=%lf\n",dot_1,dot_2,dot_3,r,rCA2,Alpha);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
								fprintf(out,"pr=%d i=%d %lf %lf %lf\n",pr,i,prot[pr].ammino[i].ndipole_x,prot[pr].ammino[i].ndipole_y,prot[pr].ammino[i].ndipole_z);
								fprintf(out,"prt=%d j=%d %lf %lf %lf\n",prt,j,prot[prt].ammino[j].ndipole_x,prot[prt].ammino[j].ndipole_y,prot[prt].ammino[j].ndipole_z);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							#endif
							
							//}
					}
				}
				
			}
		}
	}
	fprintf(EDOut,"%lu %d %lf %lf\n",icycl,iteration,Edip,Edip_Compare);fflush(EDOut);
	#endif
	
	Edip*=DIP_Scale;
	return(Edip);
	
}

void Dipole_Reject (void){
	int pr=0,i=0;
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].ndipole_x=prot[pr].ammino[i].dipole_x;
			prot[pr].ammino[i].ndipole_y=prot[pr].ammino[i].dipole_y;
			prot[pr].ammino[i].ndipole_z=prot[pr].ammino[i].dipole_z;
		}
	}
}

void Dipole_Accept (void){
	int pr=0,i=0;
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].dipole_x=prot[pr].ammino[i].ndipole_x;
			prot[pr].ammino[i].dipole_y=prot[pr].ammino[i].ndipole_y;
			prot[pr].ammino[i].dipole_z=prot[pr].ammino[i].ndipole_z;
			
			
		}
	}
	
	sist->EDip=sist->EDip_New;
	sist->dip_contact=sist->ndip_contact;
}

double RMSD_CA_local (int pr,int i,int min,int max){
	
	int j=0,k=0;
	double dx=0,dy=0,dz=0;
	double value=0,r=0,rCA2=0;
	long int indice=0,indicen=0,indicet=0;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_local icycl=%lu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
		
		if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
		}else{
			if((j>min)&&(j<max)){
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=(dx*dx+ dy*dy + dz*dz);
				
				
				r=sqrt(rCA2);
				
				value=DistMap1[i][j]-r;
				value*=value;
				op+=value/2.0;
			}else{
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=(dx*dx+ dy*dy + dz*dz);
				
				
				r=sqrt(rCA2);
				
				value=DistMap1[i][j]-r;
				value*=value;
				op+=value;
			}
		}
		
		
		
	}
	
	return op;
	
}

double RMSD_CA_SP (int pr,int i){
	
	int j=0,k=0;
	double dx=0,dy=0,dz=0;
	double value=0,r=0,rCA2=0;
	long int indice=0,indicen=0,indicet=0;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_fw icycl=%lu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
		
		if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
		}else{
			//fprintf(out,"RMSD_CA_SP yes  i=%d j=%d \n",i,j);fflush(out);
			dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
			dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
			dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;
			
			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
			
			rCA2=(dx*dx+ dy*dy + dz*dz);
			
			
			r=sqrt(rCA2);
			
			value=DistMap1[i][j]-r;
			value*=value;
			op+=value;
			
			
			//op++;				
		}/*else{
			fprintf(out,"RMSD_CA_fw no  i=%d j=%d \n",i,j);fflush(out);
		}*/
		
		
		
	}
	
	return op;
	
}

double RMSD_CA_fw (int pr,int i){
	
	int j=0,k=0;
	double dx=0,dy=0,dz=0;
	double value=0,r=0,rCA2=0;
	long int indice=0,indicen=0,indicet=0;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_fw icycl=%lu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
		
		if(j<i-NATOM*2){
			//fprintf(out,"RMSD_CA_fw yes  i=%d j=%d \n",i,j);fflush(out);
			dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
			dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
			dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;
			
			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
			
			rCA2=(dx*dx+ dy*dy + dz*dz);
			
			
			r=sqrt(rCA2);
			
			value=DistMap1[i][j]-r;
			value*=value;
			op+=value;
			
			
			//op++;				
		}/*else{
			fprintf(out,"RMSD_CA_fw no  i=%d j=%d \n",i,j);fflush(out);
		}*/
		
		
		
	}
	
	return op;
	
}

double RMSD_CA_bw (int pr,int i){
	
	int j=0,k=0;
	double dx=0,dy=0,dz=0;
	double value=0,r=0,rCA2=0;
	long int indice=0,indicen=0,indicet=0;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_bw icycl=%lu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
		
		if(j>i+NATOM*2){
			//fprintf(out,"RMSD_CA_bw yes  i=%d j=%d \n",i,j);fflush(out);
			dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
			dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
			dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;
			
			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
			
			rCA2=(dx*dx+ dy*dy + dz*dz);
			
			
			r=sqrt(rCA2);
			
			value=DistMap1[i][j]-r;
			value*=value;
			op+=value;
			
			
			//op++;				
		}/*else{
			fprintf(out,"RMSD_CA_bw no  i=%d j=%d \n",i,j);fflush(out);
		}*/
		
		
		
	}
	
	return op;
	
}



double Potential_H (int pr,int i,int prt,int j,int *touch ){
	
	/*************************************************************************
	 * CALCULATION OF THE DIRECTIONAL INTERACTION OF THE PATCHIES
	 * WE EXCLUDE THE INTERACTION BETWEEN BEADS BELONGING TO CONTIGUOUS BEADS
	 * COUNTING OF THE NUMBER OF PATCHY INTERACTIONS
	 ************************************************************************/
	
	int 		ttouch=0;
	int			indice_iCA=0, indice_jCA=0;
	double 		r2=0, r6=0, r12=0, dx=0, dy=0, dz=0, dx1=0, dy1=0, dz1=0;
	double 		energ=0, dotprod=0, dotprod2=0, energh=0, energhor=0;
	double 		rCA=0;
	
	
	if((prot[pr].ammino[i].spring_anchor!=1)&&(prot[prt].ammino[j].spring_anchor!=1)){
		/****i-H---j-H***/
		dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
		dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
		dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
		dx=P_Dist(dx); dy=P_Dist(dy); dz=P_Dist(dz);
		
		r2=(dx*dx+ dy*dy + dz*dz);
		if (r2<sist->RintH2){
			indice_iCA=floor(i/NATOM)*NATOM;
			indice_jCA=floor(j/NATOM)*NATOM;
			/****i-CA---i-H***/
			dx1=(prot[pr].ammino[indice_iCA].x-prot[pr].ammino[i].x);
			dy1=(prot[pr].ammino[indice_iCA].y-prot[pr].ammino[i].y);
			dz1=(prot[pr].ammino[indice_iCA].z-prot[pr].ammino[i].z);
			dx1=P_Dist(dx1);dy1=P_Dist(dy1);dz1=P_Dist(dz1); 
			/// Scalar product between the vector joining the two patchyes
			/// and the vector joining the first patch to the CA element of its bead (center of the bead i)
			dotprod=-(dx*dx1+dy*dy1+dz*dz1)/(sqrt(r2)*sist->Bbond[1]);
			
			/****j-CA---j-H***/
			dx1=(prot[prt].ammino[indice_jCA].x-prot[prt].ammino[j].x);
			dy1=(prot[prt].ammino[indice_jCA].y-prot[prt].ammino[j].y);
			dz1=(prot[prt].ammino[indice_jCA].z-prot[prt].ammino[j].z);
			dx1=P_Dist(dx1);dy1=P_Dist(dy1);dz1=P_Dist(dz1);
			/// Scalar product between the vector joining the two patchyes
			/// and the vector joining the second patch to the CA element of its bead (center of the bead j)
			dotprod2=(dx*dx1+dy*dy1+dz*dz1)/(sqrt(r2)*sist->Bbond[1]);								
			
			if((dotprod<=HydrogenBond1)&&(dotprod2<=HydrogenBond2)) {
			  if ((shift_potential_h!=0)){
			    if ((r2 > shift_potential_h2) || (shift_potential_h > 0)){
			      r2=r2-2*sqrt(r2)*shift_potential_h+shift_potential_h2;
			    }
			    else
			      {
				r2=0;
			      }
			  }
			  if (r2 > 0){
			    r6=pow(r2,EH_LJPower2_2);
			    r12=pow(r2,EH_LJPower1_2);
			    energhor=pow(dotprod*dotprod2,EH_Power);
			    energh=(sigmahb12/r12-sigmahb10/r6);
			    energ=(energh)*energhor;
			  }
			  else
			    energ = 100.;
			  
			  if(energ>100.0) energ=100.0;
			  if((HydrogenBond3>=dotprod)&&(HydrogenBond4>=dotprod2)&&(r2<HydrogenBond_r)) ttouch++;
			  
			  if(icycl%nsamp==0){
			    dx=prot[pr].ammino[indice_iCA].x-prot[prt].ammino[indice_jCA].x;
			    dy=prot[pr].ammino[indice_iCA].y-prot[prt].ammino[indice_jCA].y;
			    dz=prot[pr].ammino[indice_iCA].z-prot[prt].ammino[indice_jCA].z;
			    rCA=sqrt(dx*dx+ dy*dy + dz*dz);
			    fprintf(EHout2,"%lu %lf %lf %lf %lf %lf %lf %lf %lf %lf\n %d %d %d %d ->> RCA=%lf\n",
				    icycl,1/beta[betaindice],r2,sigmahb12,sigmahb10,dotprod,dotprod2,energh,energhor,energ,pr,i,prt,j,rCA);
			    fflush(EHout2);
			  }
			}
			if((dotprod>1+1e-05)||(dotprod<-1-1e-05)){
				fprintf(out,"1a fw CAZZOOOOOO %lu %30.20lf %lf \n",icycl,dotprod,HydrogenBond1);
				fprintf(out,"%lf ",r2);
				
				dx=(prot[pr].ammino[indice_iCA].x-prot[pr].ammino[i].x);
				dy=(prot[pr].ammino[indice_iCA].y-prot[pr].ammino[i].y);
				dz=(prot[pr].ammino[indice_iCA].z-prot[pr].ammino[i].z);
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				r2=(dx*dx+ dy*dy + dz*dz);
				fprintf(out,"indice_iCA=%d i=%d %lf %lf\n",indice_iCA,i,r2,sist->Bbond[1]);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			if((dotprod2>1+1e-05)||(dotprod2<-1-1e-05)){
				fprintf(out,"1b fw CAZZOOOOOO %lu %30.20lf %lf \n",icycl,dotprod2,HydrogenBond2);
				fprintf(out,"%lf ",r2);
				
				dx=(prot[prt].ammino[indice_jCA].x-prot[prt].ammino[j].x);
				dy=(prot[prt].ammino[indice_jCA].y-prot[prt].ammino[j].y);
				dz=(prot[prt].ammino[indice_jCA].z-prot[prt].ammino[j].z);
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				r2=(dx*dx+ dy*dy + dz*dz);
				fprintf(out,"%lf %lf\n",r2,sist->Bbond[1]);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}				
	}
	*touch+=ttouch; /// counting number of hydrogen bonds that will be sist-> contact
	#ifdef NO_PATCHES 
	energ=0; /// SWITCH OFF patches
        #endif
	return energ;
}

double energy (void){	/// this should be only the energy coming from the patches without the spring
	
	/************************************************
	* energy -- calculate the energy interaction    *
	* 	with the i residue and all the	            *
	*	neighbour residues 			                *
	* 						                        *
	* Parameters: 			                    	*
	*	prot -- is a proteina type structure	    *
	*		where is stored the protein 	        *
	*	i -- the reference residue 		            *
	*	res -- the type of the i residue	        *
	*	M -- is the interaction matrix	            *
	*						                        *
	* Return:					                    *
	*	energ -- the energy of the protein	        *
	*						                        *
	*************************************************/
	
	int 				icel_x, icel_y, icel_z;
	int					prt, jj;
	int 				pr, i, j;
	int 				touch=0, tmp_touch=0;
	double 				energ=0.,add_ener_tot=0.,energH=0.;
	double 				op=0.,iso_ener=0.;
	double 				energCA=0.0, energDip=0.0, add_ener=0.;
	unsigned long int 	indice, indice2;
	
	for(pr=0;pr<NPROT;pr++)
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM)
			prot[pr].ammino[i].old_water_contacts=0;
		
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			if(prot[pr].ammino[i].spring_anchor!=1){		
				switch(prot[pr].ammino[i].id){
					
					/*ATOMI CA*/
					case ATOM_CA:
					for(indice2=0;indice2<Neigh;indice2++){ /// this loop runs over the 27 neighbour cells. 
					///The vector "neigcells" contains the coordinates, ordered by thresome "delta_x delta_y delta_z",
					///of the displacement versors pointing at the neighbour cells.
						icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
						icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
						icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
						
						icel_x=P_Cell(icel_x,N_CA_cells);
						icel_y=P_Cell(icel_y,N_CA_cells);
						icel_z=P_Cell(icel_z,N_CA_cells);
						indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
						
						prt=hoc_CA[indice]-1;	/// the dimension of hoc_CA is double with respect the number of cells because it
						j=hoc_CA[indice+1]-1;	/// contains the indice of the protein (even) and of the CA atom (odd)
						
						while(j>=0){
							if(prt==pr){
							    if(j <i-NATOM*EXCLUDED_NEIGH){/// here we exclude the interaction with the contiguous beads. We consider
								  energCA=Potential_CA(pr,i,prt,j,&add_ener); /// only the previous beads of the chain.
								  ///fprintf(out,"add_ener=%lf\n",add_ener);fflush(out);
									prot[pr].ammino[i].old_water_contacts+=energCA;
									prot[prt].ammino[j].old_water_contacts+=energCA;
									#ifdef DIPOLE_SFC
									energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale; /// isotropic specific interaction
									/*energTUBE=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
									fprintf(out,"%llu i=%d j=%d energCA=%lf energTUBE=%lf prefact=%lf\n",icycl,i,j,energCA,energTUBE,M[prot[pr].ammino[i].residue][prot[prt].ammino[j].residue]);fflush(out);
									fprintf(out,"######################");fflush(out);*/
									add_ener_tot+=add_ener;
                                                                        #else /// each energyCA is multiplied by the interaction matrix element and summed
									energDip=Potential_Dip(pr,i,prt,j,&touch);	
									energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
									#endif
									
									#ifdef TUBE
									op+=tube_cont;
									#else
									op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
									#endif
								}				///  compact
							}else{   /// between different chains the interaction is divided for 2
							  energCA=Potential_CA(pr,i,prt,j,&add_ener)/2.0; 
								prot[pr].ammino[i].old_water_contacts+=energCA;
								prot[prt].ammino[j].old_water_contacts+=energCA;
								
								#ifdef DIPOLE_SFC
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
								#else
								energDip=Potential_Dip(pr,i,prt,j,&tmp_touch)/2.0;
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
								#endif
								
                                                                #ifdef TUBE
								op+=tube_cont;
                                                                #else
								op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                                #endif
							} 
							
							jj=prot[prt].ammino[j].lbw_i;
							prt=prot[prt].ammino[j].lbw_pr;
							j=jj;
						}
					}		
					break;
					
					/*ATOMI H*/
					case ATOM_H:
					for(indice2=0;indice2<Neigh;indice2++){
						icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
						icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
						icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
						
						icel_x=P_Cell(icel_x,N_H_cells);
						icel_y=P_Cell(icel_y,N_H_cells);
						icel_z=P_Cell(icel_z,N_H_cells);
						
						indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
						
						prt=hoc_H[indice]-1;
						j=hoc_H[indice+1]-1;
						
						while(j>=0){
							if(prt==pr)	{
								if(j <i-NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
							}
							else	{
								energ+=Potential_H(pr,i,prt,j,&tmp_touch)/2.0;
							}
							jj=prot[prt].ammino[j].lbw_i;
							prt=prot[prt].ammino[j].lbw_pr;
							j=jj;
						}
					}		
					break;
				}
			}  
		} /// end of the loop over the interacting elements of any protein
	} /// end of the loop over the number of proteins
	///	fprintf(out,"icycl=%llu energH=%lf\n", icycl, energH);fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].new_water_contacts=prot[pr].ammino[i].old_water_contacts;
			
			if(prot[pr].ammino[i].new_water_contacts<=HOH_Burried_Threshold)
				if(M[prot[pr].ammino[i].residue][0]>=0) 
					energ+=(HOH_Burried_Threshold-prot[pr].ammino[i].new_water_contacts)*M[prot[pr].ammino[i].residue][0];
				
			if(prot[pr].ammino[i].new_water_contacts>=HOH_Burried_Threshold)
				if(M[prot[pr].ammino[i].residue][0]<=0) 
					energ+=(HOH_Burried_Threshold-prot[pr].ammino[i].new_water_contacts)*M[prot[pr].ammino[i].residue][0];	
		}
	}
	
	if(tmp_touch%2 !=0){
		fprintf(out,"tmp_touch=%d not even\n",tmp_touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	touch+=tmp_touch/2;	/// touch counts the number of patchy directional interactions
	sist->ntouch=0; /// sist->touch is 1 if there is at least one hydrogen bond 
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch; /// number of hydrogen bonds
	sist->norder=op; /// compactness of the chain
	sist->add_ener_tot=add_ener_tot;
	return energ;
} 

double energy_SAW_test (){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int prt;
	double energ=0;
	int pr,i,j;
	double dx,dy,dz,rCA2;

	for(pr=0;pr<NPROT;pr++){
		for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
			for(prt=0;prt<NPROT;prt++){
				for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
					
					if(prt==pr){
						if(j <i){
							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
							
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							
							rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
							if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
								energ=99999.0;
								//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);
								
								return energ;
							}
						}
					}else{
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
						
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
						
						rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
						if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
							energ=99999.0;
							//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);
							
							return energ;
						}
					}
				}
			}
			
		}
	}
	
	
	
	
	
	
	
	
	return energ;
}

double energy_SAW_local (int pr,int i,int min , int max){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int prt,jj;
	double energ=0;
	int j;
	double dx,dy,dz,rCA2;
	

	
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
	for(indice2=0;indice2<Neigh;indice2++){
		/*icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);*/
		
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		
		indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
		
		prt=hoc_SAW[indice]-1;
		j=hoc_SAW[indice+1]-1;
		
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(j>=0){
			
			if(prt==pr){
				if((j >=i)&&(j <=i)){
				}else{
					if((j>min)&&(j<max)){
					}else{
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
						
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
						
						rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
						if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
							energ=99999.0;
							//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);
							
							return energ;
						}
					}
				}
			}else{
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
				if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
					energ=99999.0;
					//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);
					
					return energ;
				}
			}
			
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
	
	
	
	
	
	
	return energ;
}

double energy_SAW_output (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int prt,jj;
	double energ=0;
	int j;
	double dx,dy,dz,rCA2;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
	for(indice2=0;indice2<Neigh;indice2++){
		/*icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);*/
		
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		
		indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
		
		prt=hoc_SAW[indice]-1;
		j=hoc_SAW[indice+1]-1;
		
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(j>=0){
			
			if(prt==pr){
				if(j <i){
					
					dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
					dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
					dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
					
					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					
					rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
					if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
						energ=99999.0;
						fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
						fprintf(out,"%lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
						fprintf(out,"%lf %lf %lf\n",prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
						fprintf(out,"r=%lf rmin=%lf\n",sqrt(rCA2),sist->Rmin);
						return energ;
					}
				}
			}else{
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
				if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
					energ=99999.0;
					fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
					
					return energ;
				}
			}
			
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
	
	
	
	
	
	
	return energ;
}

double energy_SAW_SP (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int prt,jj;
	double energ=0;
	int j;
	double dx,dy,dz,rCA2;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
	for(indice2=0;indice2<Neigh;indice2++){
		/*icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);*/
		
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		
		indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
		
		prt=hoc_SAW[indice]-1;
		j=hoc_SAW[indice+1]-1;
		
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(j>=0){
			
			if(prt==pr){
				if(j !=i){
					
					dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
					dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
					dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
					
					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					
					rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
					if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
						energ=99999.0;
						//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
						
						return energ;
					}
				}
			}else{
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
				if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
					energ=99999.0;
					//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
					
					return energ;
				}
			}
			
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
	
	
	
	
	
	
	return energ;
}

double energy_SAW_All_rot_trasl (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int prt,jj;
	double energ=0;
	int j;
	double dx,dy,dz,rCA2;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
	for(indice2=0;indice2<Neigh;indice2++){
		/*icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);*/
		
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		
		indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
		
		prt=hoc_SAW[indice]-1;
		j=hoc_SAW[indice+1]-1;
		
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(j>=0){
			
			if(prt!=pr){
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
				if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
					energ=99999.0;
					//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
					
					return energ;
				}
			}
			
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
	
	
	
	
	
	
	return energ;
}

double energy_SAW (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int prt,jj;
	double energ=0;
	int j;
	double dx,dy,dz,rCA2;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
	for(indice2=0;indice2<Neigh;indice2++){
		/*icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);*/
		
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		
		indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
		
		prt=hoc_SAW[indice]-1;
		j=hoc_SAW[indice+1]-1;
		
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(j>=0){
			
			if(prt==pr){
				if(j <i){
					
					dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
					dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
					dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
					
					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					
					rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
					if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
						energ=99999.0;
						//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
						
						return energ;
					}
				}
			}else{
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
				if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
					energ=99999.0;
					//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
					
					return energ;
				}
			}
			
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
	
	
	
	
	
	
	return energ;
}

double energy_SAW_bw (int pr,int i){
	
	/************************************************
	* In this routine I evaluate if two neighbour amino-acids overlap. In that case I exit from the routine returning a value larger than 0
	* 
	* energy -- calculate the energy interaction
	* 	with the i residue and all the	
	*	neighbour residues 			
	* 						
	* 						
	* Parameters: 				
	*	prot -- is a proteina type structure	
	*		where is stored the protein 	
	*	i -- the refernce residue 		
	*	res -- the type of the i residue	
	*	M -- is the interaction matrix	
	*						
	* Return:					
	*	energ -- the energy of the protein	
	*						
	*************************************************/
	
	int 	icel_x, icel_y, icel_z;
	int 	prt, j, jj;
	double 	energ=0;
	double 	dx, dy, dz, rCA2;
	unsigned long int indice,indice2;
			
	for(indice2=0;indice2<Neigh;indice2++){
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		indice=icel_x*2*N_SAW_cells2 + icel_y*2*N_SAW_cells + icel_z*2;
		if(indice>=(unsigned int) (2*N_SAW_cells3)){
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		prt=hoc_SAW[indice]-1;
		j=hoc_SAW[indice+1]-1;
		
		while(j>=0){
			if(prt==pr){
				if(j >i){
					dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
					dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
					dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
					if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
						energ=99999.0;
						return energ;
					}
				}
			}else{
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				rCA2=sqrt(dx*dx+ dy*dy + dz*dz);
				if (rCA2<(Rmin[prot[pr].ammino[i].residue]+Rmin[prot[prt].ammino[j].residue])){
					energ=99999.0;
					return energ;
				}
			}
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
		}
	}		
	
	return energ;
}

double energy_SP_brot_local (int pr,int i,int min,int max,int old){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int j,prt,jj;
	double energ=0, add_ener_tot=0;
	double op=0;
	int temp_touch=0;
	int touch=0;
	double energCA=0.0,energDip=0.0,energ_ext=0.0, add_ener = 0.,energTUBE=0.;
	//int numero_partners;
	//double opCA=0.0;
	
	
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	//fprintf(out,"Energ Local %lu i=%d id=%d\n",icycl,i,prot[pr].ammino[i].id);fflush(out);
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			//opCA=RMSD_CA_local(pr,i,min,max);
			//op+=opCA;
			//fprintf(out,"Energ_local %lu op[%d][%d]=%lf\n",icycl,pr,i,opCA);fflush(out);
			
			for(indice2=0;indice2<Neigh;indice2++){
				//fprintf(out,"Energ Local CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
				/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				//fprintf(out,"Energ Local CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);*/
				
				
				icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				//fprintf(out,"Energ Local CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				
				prt=hoc_CA[indice]-1;
				j=hoc_CA[indice+1]-1;
				
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				
				//fprintf(out,"Energ Local CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					//fprintf(out,"Energ Local CA loop j=%d prt=%d\n",j,prt);fflush(out);
					if(prt==pr){
						
						if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)){
						}else{
							if((j>min)&&(j<max)){
							}else{
								//fprintf(out,"-------------> j=%d id=%d\n",j,prot[prt].ammino[j].id);fflush(out);
								energCA=Potential_CA(pr,i,prt,j,&add_ener);							
								if(old==OLDWATER){
									remove_old_water_bonds(pr,i,prt,j,energCA);
								}else{
									add_new_water_bonds(pr,i,prt,j,energCA);
								}
								
								#ifdef DIPOLE_SFC
								energ_ext+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
								/*energTUBE=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
								fprintf(out,"%llu i=%d j=%d energCA=%lf energTUBE=%lf prefact=%lf\n",icycl,i,j,energCA,energTUBE,M[prot[pr].ammino[i].residue][prot[prt].ammino[j].residue]);fflush(out);
								fprintf(out,"######################");fflush(out);*/
								add_ener_tot+=add_ener;
								///fprintf(out,"add_ener=%lf\n",add_ener);fflush(out);
								#else
								//exit(1);
								energDip=Potential_Dip(pr,i,prt,j,&touch);
								energ_ext+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
								
								#endif
								#ifdef TUBE
								op+=tube_cont;
                                                                #else
								op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                                #endif
							}
						}
						
					}else{
						energCA=Potential_CA(pr,i,prt,j,&add_ener);	
						if(old==OLDWATER){
							remove_old_water_bonds(pr,i,prt,j,energCA);
						}else{
							add_new_water_bonds(pr,i,prt,j,energCA);
						}
						
						#ifdef DIPOLE_SFC
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
						
						#else
						energDip=Potential_Dip(pr,i,prt,j,&touch);
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
						#endif
						#ifdef TUBE
						op+=tube_cont;
                                                #else
						op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                #endif
						
					}
					
					//numero_partners++;
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
			}		
			break;
			
			/*ATOMI H*/
			case ATOM_H:
			for(indice2=0;indice2<Neigh;indice2++){
				/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);*/
				
				icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				
				prt=hoc_H[indice]-1;
				j=hoc_H[indice+1]-1;
				
				//fprintf(out,"Energ Local H %d %d %d %d\n",pr,i,prt,j);fflush(out);
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					
					if(prt==pr){
						if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
						}else{
							if((j>min)&&(j<max)){
							}else{
								//fprintf(out,"-------------> j=%d id=%d\n",j,prot[prt].ammino[j].id);fflush(out);
								temp_touch=0;
								energCA=Potential_H(pr,i,prt,j,&temp_touch);
								touch+=temp_touch;
								energ_ext+=energCA;
								energ+=energCA;
							}
						}
						
					}else{
						temp_touch=0;
						energCA=Potential_H(pr,i,prt,j,&temp_touch);
						touch+=temp_touch;
						energ+=energCA;
					}
					
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"H pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
				
				
			}		
			break;
			
			/*ATOMI O*/
			
			
		}
	}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=floor(touch);
	sist->norder=op;
	sist->add_ener_tot=add_ener_tot;
	
	//if(icycl > 0) fprintf(out,"energy_local  %lu i=%d energ_int=%lf energ_ext=%lf\n",icycl,i,energ_int,energ_ext); fflush(out);
	return energ;
}

double energy_SP_fseq (int pr,int i,int old){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int j,prt,jj;
	double energ=0, add_ener_tot=0.;
	double op=0;
	int temp_touch=0;
	int touch=0;
	double energCA=0.0, add_ener=0.0,energDip=0.0;
	double temp_energ;
	//int numero_partners;
	//double opCA=0.0;
	
	
	
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			//op+=RMSD_CA_SP(pr,i);
			//energ+=Bond_Energy_SP(pr,i);
			for(indice2=0;indice2<Neigh;indice2++){
				//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
				/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);*/
				
				
				icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				
				prt=hoc_CA[indice]-1;
				j=hoc_CA[indice+1]-1;
				
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				
				//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
					if(prt==pr){
						if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)) {
						}else{
							energCA=Potential_CA(pr,i,prt,j,&add_ener);
							/*if(old==OLDWATER){
								remove_old_water_bonds(pr,i,prt,j,energCA);
							}else{
								add_new_water_bonds(pr,i,prt,j,energCA);
							}*/
							#ifdef DIPOLE_SFC
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;							
							add_ener_tot+=add_ener;
							#else
							energDip=Potential_Dip(pr,i,prt,j,&touch);
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
							#endif
							#ifdef TUBE
							op+=tube_cont;
                                                        #else
							op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                        #endif
							
							
						}					
					}else{
						energCA=Potential_CA(pr,i,prt,j,&add_ener);
						/*if(old==OLDWATER){
							remove_old_water_bonds(pr,i,prt,j,energCA);
						}else{
							add_new_water_bonds(pr,i,prt,j,energCA);
						}*/
						if(i==j){
							
							energCA/=2.0;
							#ifdef DIPOLE_SFC
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
							#else
							energDip=Potential_Dip(pr,i,prt,j,&touch);
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
							#endif
							#ifdef TUBE
							op+=tube_cont;
                                                        #else
							op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                        #endif
						}else{
							
							#ifdef DIPOLE_SFC
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
							#else
							energDip=Potential_Dip(pr,i,prt,j,&touch);
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
							#endif
							#ifdef TUBE
							op+=tube_cont;
                                                        #else
							op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                        #endif
							
						}
					}
					//numero_partners++;
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
			}		
			break;
			
			/*ATOMI H*/
			case ATOM_H:
			for(indice2=0;indice2<Neigh;indice2++){
				/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);*/
				
				icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				
				prt=hoc_H[indice]-1;
				j=hoc_H[indice+1]-1;
				
				//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					
					if(prt==pr){
						if((j >=i-NATOM*2)&&(j <=i+NATOM*2)) {
						}else{
							/*if((icycl>=1411)&&(icycl<=1411)){
								fprintf(out,"H pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);
								fflush(out);
							}*/
							temp_touch=0;
							temp_energ=Potential_H(pr,i,prt,j,&temp_touch);
							energ+=temp_energ;
							touch+=temp_touch;
							/*if((icycl>=1411)&&(icycl<=1411)){
								fprintf(out,"energy_SP %lu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,temp_energ,temp_touch);fflush(out);
							}*/
						}
						
					}else{
						if((int)(i/NATOM)==(int)(j/NATOM)){// This is to check if they are pathces from the particle with the same indice along the chains
							temp_touch=0;
							temp_energ=Potential_H(pr,i,prt,j,&temp_touch);
							energ+=temp_energ/2.0;
							touch+=temp_touch/2.0;
						}else{
							temp_touch=0;
							temp_energ=Potential_H(pr,i,prt,j,&temp_touch);
							energ+=temp_energ;
							touch+=temp_touch;
						}
						/*if((icycl>=1411)&&(icycl<=1411)){
							fprintf(out,"energy_SP %lu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,temp_energ,temp_touch);fflush(out);
						}*/
					}
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					
				}
				
				
				
			}		
			break;
			
			
			
		}
	}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;
	sist->add_ener_tot=add_ener_tot;
	
	//if(icycl > 0) fprintf(out,"energy_fw  %lu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
}

double energy_SP (int pr,int i,int old){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0, add_ener_tot=0.,dotprod,dotprod2,value,energh,energhor;
	double op=0;
	double tmp_energ;
	int tmp_touch;
	
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0, add_ener=0.0,energDip=0.0;
	int numero_partners;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			//op+=RMSD_CA_SP(pr,i);
			//energ+=Bond_Energy_SP(pr,i);
			for(indice2=0;indice2<Neigh;indice2++){
				//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
				/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);*/
				
				
				icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				
				prt=hoc_CA[indice]-1;
				j=hoc_CA[indice+1]-1;
				
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				
				//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					energCA=0;
					//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
					if(prt==pr){
						if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)) {
						}else{
							energCA=Potential_CA(pr,i,prt,j,&add_ener);							
							if(old==OLDWATER){
								remove_old_water_bonds(pr,i,prt,j,energCA);
							}else{
								add_new_water_bonds(pr,i,prt,j,energCA);
							}
							
							#ifdef DIPOLE_SFC
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
							add_ener_tot+=add_ener;
							///fprintf(out,"add_ener=%lf\n",add_ener);fflush(out);
							#else
							energDip=Potential_Dip(pr,i,prt,j,&touch);
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
							#endif
							#ifdef TUBE
							op+=tube_cont;
                                                        #else
							op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                        #endif
							
						}					
					}else{
						energCA=Potential_CA(pr,i,prt,j,&add_ener);						
						if(old==OLDWATER){
							remove_old_water_bonds(pr,i,prt,j,energCA);
						}else{
							add_new_water_bonds(pr,i,prt,j,energCA);
						}
						
						#ifdef DIPOLE_SFC
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
						#else
						energDip=Potential_Dip(pr,i,prt,j,&touch);
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
						#endif
						#ifdef TUBE
						op+=tube_cont;
                                                #else
						op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                #endif
					}
					//numero_partners++;
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
			}		
			break;
			
			/*ATOMI H*/
			case ATOM_H:
			for(indice2=0;indice2<Neigh;indice2++){
				/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);*/
				
				icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				
				prt=hoc_H[indice]-1;
				j=hoc_H[indice+1]-1;
				
				//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					
					if(prt==pr){
						if((j >=i-NATOM*2)&&(j <=i+NATOM*2)) {
						}else{
							/*if((icycl>=1411)&&(icycl<=1411)){
								fprintf(out,"H pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);
								fflush(out);
							}*/
							tmp_touch=0;
							tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch);
							energ+=tmp_energ;
							touch+=tmp_touch;
							/*if((icycl>=1411)&&(icycl<=1411)){
								fprintf(out,"energy_SP %lu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,tmp_energ,tmp_touch);fflush(out);
							}*/
						}
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
						/*if((icycl>=1411)&&(icycl<=1411)){
							fprintf(out,"energy_SP %lu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,tmp_energ,tmp_touch);fflush(out);
						}*/
					}
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					
				}
				
				
				
			}		
			break;
			
			
			
		}
	}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;
	sist->add_ener_tot=add_ener_tot;
	
	//if(icycl > 0) fprintf(out,"energy_fw  %lu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
}

double energy_SP_Pivot_fw (int pr,int i, int old){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0, add_ener_tot=0.,dotprod,dotprod2,value,energh,energhor;
	double op=0;
	
	
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0, add_ener=0.0,energDip=0.0;
	int numero_partners;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			//op+=RMSD_CA_fw(pr,i);
			for(indice2=0;indice2<Neigh;indice2++){
				//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
				/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);*/
				
				
				icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				
				prt=hoc_CA[indice]-1;
				j=hoc_CA[indice+1]-1;
				
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				
				//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
					if(prt==pr){
						if(j <i-NATOM*EXCLUDED_NEIGH) {
							energCA=Potential_CA(pr,i,prt,j,&add_ener);
							if(old==OLDWATER){
								remove_old_water_bonds(pr,i,prt,j,energCA);
							}else{
								add_new_water_bonds(pr,i,prt,j,energCA);
							}
							
							#ifdef DIPOLE_SFC
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
							add_ener_tot+=add_ener;
							#else
							energDip=Potential_Dip(pr,i,prt,j,&touch);
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
							#endif
							#ifdef TUBE
							op+=tube_cont;
                                                        #else
							op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                        #endif
						}				
					}else{
						energCA=Potential_CA(pr,i,prt,j,&add_ener);
						if(old==OLDWATER){
							remove_old_water_bonds(pr,i,prt,j,energCA);
						}else{
							add_new_water_bonds(pr,i,prt,j,energCA);
						}
						
						#ifdef DIPOLE_SFC
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
						#else
						energDip=Potential_Dip(pr,i,prt,j,&touch);
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
						#endif
						#ifdef TUBE
						op+=tube_cont;
                                                #else
						op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                #endif
						
					}
					//numero_partners++;
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
			}		
			break;
			
			/*ATOMI H*/
			case ATOM_H:
			for(indice2=0;indice2<Neigh;indice2++){
				/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);*/
				
				icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				
				prt=hoc_H[indice]-1;
				j=hoc_H[indice+1]-1;
				
				//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
				//numero_partners=0;
				while(j>=0){
					
					if(prt==pr){
						if(j <i-NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
					}
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"H pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
				
				
			}		
			break;
			
			/*ATOMI O*/
			
			
		}
	}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;
	sist->add_ener_tot=add_ener_tot;
	
	//if(icycl > 0) fprintf(out,"energy_fw  %lu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
} 

double energy_SP_Pivot_bw (int pr,int i, int old){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0.0,add_ener_tot=0.,dotprod,dotprod2,value,energh,energhor;
	double op=0.0;
	
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0, add_ener=0.0,energDip=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			//op+=RMSD_CA_bw(pr,i);
			for(indice2=0;indice2<Neigh;indice2++){
				//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
				/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);*/
				
				icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				
				//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				
				prt=hoc_CA[indice]-1;
				j=hoc_CA[indice+1]-1;
				
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				
				//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
				while(j>=0){
					//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
					if(prt==pr){
						if(j>i+NATOM*EXCLUDED_NEIGH) {
							energCA=Potential_CA(pr,i,prt,j,&add_ener);
							if(old==OLDWATER){
								remove_old_water_bonds(pr,i,prt,j,energCA);
							}else{
								add_new_water_bonds(pr,i,prt,j,energCA);
							}
							
							#ifdef DIPOLE_SFC
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
							add_ener_tot+=add_ener;
							#else
							energDip=Potential_Dip(pr,i,prt,j,&touch);
							energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
							#endif
							
							#ifdef TUBE
							op+=tube_cont;
                                                        #else
							op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                        #endif
							
						}
						
						
					}else{
						energCA=Potential_CA(pr,i,prt,j,&add_ener);
						if(old==OLDWATER){
							remove_old_water_bonds(pr,i,prt,j,energCA);
						}else{
							add_new_water_bonds(pr,i,prt,j,energCA);
						}
						
						#ifdef DIPOLE_SFC
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
						#else
						energDip=Potential_Dip(pr,i,prt,j,&touch);
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
						#endif
						#ifdef TUBE
						op+=tube_cont;
                                                #else
						op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                #endif
						
					}
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
			}		
			break;
			
			/*ATOMI H*/
			case ATOM_H:
			for(indice2=0;indice2<Neigh;indice2++){
				/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);*/
				
				icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				
				prt=hoc_H[indice]-1;
				j=hoc_H[indice+1]-1;
				
				//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
				//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
				//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
				while(j>=0){
					
					if(prt==pr){
						if(j>i+NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
					}
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				
			}		
			break;
			
			
			
			
		}
	}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;
	sist->add_ener_tot=add_ener_tot;
	
	//if(icycl > 0) fprintf(out,"energy_fw  %lu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
}

void order (void){
	
	int pr=0,i=0;
	int kk=0,k=0,j=0,prt=0;
	double r2=0,r6=0,r12=0,rh2=0;
	double r=0,LJ_repulsive=0,LJ_attractive=0,potential=0;
	double dx=0,dy=0,dz=0;
	double dx1=0,dy1=0,dz1=0;
	double energ=0.0,add_ener_tot=0.,dotprod=0,dotprod2=0,value=0,energh=0,energhor=0;
	double op=0.0;
	double contacts=0;
	int touch=0;
	double rCA2=0,energCA=0.0, add_ener=0.0,energDip=0.0;
	double energ_Water=0;
	sist->density=0;
	
	for(pr=0;pr<NPROT;pr++){
		for (i=0;i<Plength[pr];i++){
			
			if(prot[pr].ammino[i].spring_anchor!=1){
				
				switch(prot[pr].ammino[i].id){
					
					/*ATOMI CA*/
					case ATOM_CA:
					contacts=0;	
					//op+=RMSD_CA_fw(pr,i);
					for(prt=0;prt<NPROT;prt++){
						for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
							if(prot[prt].ammino[j].spring_anchor!=1){
								if(prt==pr){
									energCA=Potential_CA(pr,i,prt,j,&add_ener);
									if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)){
									}else{
										
										contacts+=energCA;
									}
									
									if(j >i+NATOM*EXCLUDED_NEIGH){
										
										#ifdef DIPOLE_SFC
										energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
										add_ener_tot+=add_ener;
										#else
										energDip=Potential_Dip(pr,i,prt,j,&touch);
										energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
										#endif
										#ifdef TUBE
										op+=tube_cont;
                                                                                #else
										op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                                                #endif
									       
									}
								}else{
									energCA=Potential_CA(pr,i,prt,j,&add_ener);
									contacts+=energCA/2.0;
									
									#ifdef DIPOLE_SFC
									energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
									#else
									energDip=Potential_Dip(pr,i,prt,j,&touch);
									energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
									#endif
									#ifdef TUBE
									op+=tube_cont;
                                                                        #else
									op+=energCA; /// without specific rescaling of the interaction matrix, to check if the polymer is 
                                                                        #endif
									
								}
							}
							
						}
					}
					
					if(contacts<=HOH_Burried_Threshold){
						if(M[prot[pr].ammino[i].residue][0]>=0) {
							energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
							energ+=energ_Water;
						}
						
					}
					if(contacts>=HOH_Burried_Threshold){
						if(M[prot[pr].ammino[i].residue][0]<=0) {
							energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
							energ+=energ_Water;
						}
						
					}		
					break;
					
					/*ATOMI H*/
					case ATOM_H:
					for(prt=0;prt<NPROT;prt++){
						for(k=ATOM_CA;k<Plength[prt];k+=NATOM){
							
							
							for(kk=0;kk<NSPOTS;kk++){
								
								j=k+kk+1;
								
								if(prot[prt].ammino[j].spring_anchor!=1){
									if(prt==pr){
										if(j >i+NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
										
									}else{
										energ+=Potential_H(pr,i,prt,j,&touch);
									}
								}
							}
							
						}
						
					}		
					break;
					
					
					
				}
				
				
				
				
				if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) sist->density+=1.0;
			}
		}
	}
	
	
	sist->touch=0;
	if(touch>0) sist->touch=1;
	
	sist->contact=touch;
	sist->order=op;
	
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	sist->add_ener_tot=add_ener_tot;
	
	return;
	
}

void order_test_water (double *E){
	
	int pr=0,i=0;
	int k=0,j=0,prt=0;
	double r2=0,r6=0,r12=0,rh2=0;
	double r=0,LJ_repulsive=0,LJ_attractive=0,potential=0;
	double dx=0,dy=0,dz=0;
	double dx1=0,dy1=0,dz1=0;
	double energ=0, add_ener_tot=0.,dotprod=0,dotprod2=0,value=0,energh=0,energhor=0,tmp_energ=0;
	double op=0.0,opCA=0.0;
	double op_Sub=0;
	int touch=0,oldtouch=0,tmp_touch=0;
	double rCA2,energCA=0.0, add_ener=0.0;
	double energ_Water=0,contacts=0,bond=0;
	FILE *fp=NULL;
	int flag=0;
	
	
	//if(icycl > 0) fprintf(out,"order 1626\n");
	for(pr=0;pr<NPROT;pr++){
		for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
			
			
			contacts=0;	
			//fprintf(out,"order_test op[%d][%d]=%lf\n",pr,i,opCA);fflush(out);
			for(prt=0;prt<NPROT;prt++){
				for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
					
					if(prt==pr){
						if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)){
						}else{
							bond=Potential_CA(pr,i,prt,j,&add_ener);
							contacts+=bond;
							
						}
						
						
					}else{
						bond=Potential_CA(pr,i,prt,j,&add_ener);
						op+=bond/2.0;
						contacts+=bond;
						
						
						
					}
					
				}
			}
			if(fabs(prot[pr].ammino[i].new_water_contacts-contacts)>1e-8){
				fprintf(out,"order_test_water Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf test_contacts=%20.15lf prot[%d].ammino[%d].old_water_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,contacts,pr,i,prot[pr].ammino[i].old_water_contacts);fflush(out);
				fsamp();
				fp=fopen("final-err.bin","w");
				writeBinary(fp);
				test_celllist_SAW(1);
				test_celllist_CA();
				test_celllist_H();
				flag=1;
				
			}	
			
			/* if(contacts<=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]>0) {
					energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
					energ+=energ_Water;
				}
				
			} */
			if(contacts<=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]>=0) {
					energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
					energ+=energ_Water;
				}
				
			}
			if(contacts>=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]<=0) {
					energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
					energ+=energ_Water;
				}
				
			}
			
			//fprintf(out,"order_test %lu E[%d][%d]=%lf \n",icycl,pr,i,tmp_energ);fflush(out);
			
		}
	}
	//fprintf(out,"Order Test final %lf\n",energ);fflush(out);	
	if(flag==1){
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	*E=energ;
	sist->add_ener_tot=add_ener_tot;
	
	
	return;
	
}

void order_test (int *C, double *O, double *E){
	
	int pr=0,i=0;
	int kk=0,k=0,j=0,prt=0;
	double r2=0,r6=0,r12=0,rh2=0;
	double r=0,LJ_repulsive=0,LJ_attractive=0,potential=0;
	double dx=0,dy=0,dz=0;
	double dx1=0,dy1=0,dz1=0;
	double energ=0, add_ener_tot=0.,dotprod=0,dotprod2=0,value=0,energh=0,energhor=0,tmp_energ=0;
	double op=0.0,opCA=0.0;
	int touch=0,oldtouch=0,tmp_touch=0,ttmp_touch=0;
	double rCA2=0,energCA=0.0, add_ener=0.0,energDip=0.0;
	double contacts=0;
	double energ_Water=0.0;
	sist->density=0;
	
	
	//if(icycl > 0) fprintf(out,"order 1626\n");
	for(pr=0;pr<NPROT;pr++){
		for (i=0;i<Plength[pr];i++){
			
			
			oldtouch=touch;
			switch(prot[pr].ammino[i].id){
				
				/*ATOMI CA*/
				case ATOM_CA:
				contacts=0;
				if(prot[pr].ammino[i].spring_anchor!=1){
					//opCA=RMSD_CA_fw(pr,i);
					//op+=opCA;
					//fprintf(out,"order_test op[%d][%d]=%lf\n",pr,i,opCA);fflush(out);
					
					for(prt=0;prt<NPROT;prt++){
						for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
							if(prot[prt].ammino[j].spring_anchor!=1){	
								tmp_energ=0;
								if(prt==pr){
									
									if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)){
									}else{
									  
									  contacts+=Potential_CA(pr,i,prt,j,&add_ener);
										
									}	
									
									if(j <i-NATOM*EXCLUDED_NEIGH){
										
									  tmp_energ=Potential_CA(pr,i,prt,j,&add_ener);
										
										#ifdef DIPOLE_SFC
										energ+=Potential_Seq(pr,i,prt,j,tmp_energ)*CA_Scale;
										add_ener_tot+=add_ener;
										#else
										energDip=Potential_Dip(pr,i,prt,j,&touch);
										energ+=Potential_Seq(pr,i,prt,j,tmp_energ)*CA_Scale+energDip*DIP_Scale;
										#endif
										#ifdef TUBE
										op+=tube_cont;
                                                                                #else
										op+=tmp_energ; /// without specific rescaling of the interaction matrix, to check if the polymer is compact
                                                                                #endif

							  
										
									}
								}else{
									tmp_energ=Potential_CA(pr,i,prt,j,&add_ener);
									contacts+=tmp_energ;
									tmp_energ/=2.0;
									#ifdef DIPOLE_SFC
									energ+=Potential_Seq(pr,i,prt,j,tmp_energ)*CA_Scale;
									//fprintf(out,"order_test Mossaid=%d %lu Potential_Seq(%d,%d,%d,%d,%lf)*CA_Scale=%lf\n",mossaid,icycl,pr,i,prt,j,tmp_energ,Potential_Seq(pr,i,prt,j,tmp_energ)*CA_Scale);fflush(out);
									#else
									energDip=Potential_Dip(pr,i,prt,j,&tmp_touch)/2.0;
									energ+=Potential_Seq(pr,i,prt,j,tmp_energ)*CA_Scale+energDip*DIP_Scale;
									#endif
									#ifdef TUBE
									op+=tube_cont;
                                                                        #else
									op+=tmp_energ; /// without specific rescaling of the interaction matrix, to check if the polymer is compact
                                                                        #endif

									
								}
							}
							
						}
					}
					if(fabs(prot[pr].ammino[i].new_water_contacts-contacts)>1e-8){
						fprintf(out,"order_test Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf test_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,contacts);fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					
					if(contacts<=HOH_Burried_Threshold){
						if(M[prot[pr].ammino[i].residue][0]>=0) {
							energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
							//fprintf(out,"order_test Mossaid=%d %lu H energ_Water pr=%d i=%d contacts=%lf energ_Water=%lf\n",mossaid,icycl,pr,i,contacts,energ_Water);fflush(out);
							energ+=energ_Water;
						}
						
					}
					
					if(contacts>=HOH_Burried_Threshold){
						if(M[prot[pr].ammino[i].residue][0]<=0) {
							energ_Water=(HOH_Burried_Threshold-contacts)*M[prot[pr].ammino[i].residue][0];	
							//fprintf(out,"order_test Mossaid=%d %lu P energ_Water pr=%d i=%d contacts=%lf energ_Water=%lf\n",mossaid,icycl,pr,i,contacts,energ_Water);fflush(out);
							energ+=energ_Water;
						}
						
					}
					
				}
				tmp_energ=Bond_Energy_fw(pr,i);
				energ+=tmp_energ;
				
				
				
				
				break;
				
				/*ATOMI H*/
				case ATOM_H:
				if(prot[pr].ammino[i].spring_anchor!=1){
					for(prt=0;prt<NPROT;prt++){
						for(k=ATOM_CA;k<Plength[prt];k+=NATOM){
							
							for(kk=0;kk<NSPOTS;kk++){
								
								j=k+kk+1;
								
								if(prot[prt].ammino[j].spring_anchor!=1){	
									if(prt==pr){
										if(j <i-NATOM*2) {
											ttmp_touch=0;
											tmp_energ=Potential_H(pr,i,prt,j,&ttmp_touch);
											//fprintf(out,"order_test Mossaid=%d %lu Potential_H(%d,%d,%d,%d)=%lf\n",mossaid,icycl,pr,i,prt,j,tmp_energ);fflush(out);
											touch+=ttmp_touch;
											energ+=tmp_energ;
											/*if((icycl>=1411)&&(icycl<=1411)){
												fprintf(out,"order_test %lu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,tmp_energ,ttmp_touch);fflush(out);
											}*/										
										}
										
									}else{
										tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch)/2.0;
										energ+=tmp_energ;
										//fprintf(out,"order_test %lu E_HO[%d][%d][%d][%d]=%lf \n",icycl,pr,i,prt,j,tmp_energ);fflush(out);
										//fprintf(out,"Order Test H %lf\n",energ);fflush(out);
									}
								}
								
							}
						}
						
					}
				}		
				break;
				
				
				
			}
			
			
			
			
			if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) sist->density+=1.0;
			
			//fprintf(out,"order_test %lu E[%d][%d]=%lf \n",icycl,pr,i,tmp_energ);fflush(out);
			
		}
	}
	//fprintf(out,"Order Test final %lf\n",energ);fflush(out);	
	if(tmp_touch%2 !=0){
		fprintf(out,"order_test %lu tmp_touch=%d not even\n",icycl,tmp_touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
		
	}
	
	
	
	touch+=tmp_touch/2;
	*C=touch;
	*O=op;
	*E=energ;
	//fprintf(out,"order_test %lu energ=%lf\n",icycl,energ);fflush(out);
	/*if(touch>=(NSPOTS-2)*ProtN){
		fprintf(out,"%lu [touch=%d] >[(NSPOTS-2)*ProtN=%d] \n",icycl,touch,(NSPOTS-2)*ProtN);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	if(touch<0){
		
		fprintf(out,"touch < 0  %lu %d\n",icycl,touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	return;
	
}

void fsamp (void){
	
	/************************************************
	* fsamp -- write the file with the result of    *
	* 	the simulation every nsamp steps			*
	*	and convert the sequence from number 		*
	* 	to letters 									*
	* 												*
	* Parameters: 									*
	*	prot -- is a proteina type structure		*
	*		where is stored the protein 			*
	*	fp -- the file where to write 				*
	*												*
	* Return:										*
	*	void										*
	*												*
	*************************************************/
	
	char sequenza[]="OACDEFGHIKLMNPQRSTVWY";
	
	int i=0,Z=0,j=0,k=0,pr=0,prt=0,locallinks=0,kk=0,pl=0;
	double lunbias=0,r2=0,dx=0,dy=0,dz=0,r6=0;
	double averagex=0,averagey=0,averagez=0;
	static int **indice=NULL,NTOT=0;
	static unsigned long int iter=0;
	double traslx=0,trasly=0,traslz=0;
	double x=0,y=0,z=0,module=0;
	char message[200];
	long int OALN=0,OEnd=0;
	///long int OTopo=0;
	FILE *fp=NULL,*fSeqvmd=NULL;

	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"betaindice=%d\n",betaindice);fflush(out);
	fprintf(out,"VMD/Seqvmd-T-%lf-I-%08lu.pdb\n",1/beta[betaindice],iter);fflush(out);}
	#endif
	pl=1;
	sprintf(message, "VMD/Seqvmd-T-%lf-I-%08lu.pdb",1/beta[betaindice],iter);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"%s\n",message);fflush(out);}
	
	#endif
	if(fSeqvmd!=NULL){
		printf ("fSeqvmd already allocated\n");fflush(NULL);
		fclose(fSeqvmd);
	}
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0ab\n");fflush(out);}
	#endif
	
	fSeqvmd=fopen(message,"w");
	
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0\n");fflush(out);}
	#endif
	
	if(fSeqvmd==NULL){
		printf("icycl= %lu mossaid= %d fSeqvmd for file %s could not be allocated\n",icycl,mossaid,message);fflush(NULL);
		exit(0);
	}else{	
	for(j=0;j<NPROT;j++){
		traslx=prot[j].ammino[0].x;
		trasly=prot[j].ammino[0].y;
		traslz=prot[j].ammino[0].z;
		traslx=P_Cd(traslx); 
		trasly=P_Cd(trasly);
		traslz=P_Cd(traslz);
		traslx-=prot[j].ammino[0].x;
		trasly-=prot[j].ammino[0].y;
		traslz-=prot[j].ammino[0].z;
		
		#ifdef PROGRESS_KNOT
		if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0a j=%d\n",j);fflush(out);}
		#endif
		for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			#ifdef PROGRESS_KNOT
			if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0b j=%d i=%d\n",j,i);fflush(out);}
			#endif
			for(kk=0;kk<NATOM;kk++){
				#ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0c j=%d i=%d kk=%d\n",j,i,kk);fflush(out);}
				#endif
				x=prot[j].ammino[i+kk].x+traslx; /// It`s recentering the whole chain in one box, just to print it in a fancy way in the pdb
				y=prot[j].ammino[i+kk].y+trasly;
				z=prot[j].ammino[i+kk].z+traslz;	
				fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,sist->Atoms[prot[j].ammino[i+kk].id],sist->Amminoacids[prot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
				///printing the pdb file
				pl++;		
			}
			x=prot[j].ammino[i].ndipole_x;
			y=prot[j].ammino[i].ndipole_y;
			z=prot[j].ammino[i].ndipole_z;		
			#ifdef PROGRESS_KNOT
			if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0d j=%d i=%d\n",j,i);fflush(out);}
			#endif
			
			if(E_Dip>0){
				module=sqrt(x*x+y*y+z*z); /// dipole module
				if((isnan(module))||(isnan(1./module))){
					fprintf(out,"Fsamp Mossaid=%d %lu module=%lf x=%lf y=%lf z=%lf\n",mossaid,icycl,module,x,y,z);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				#ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0e j=%d i=%d\n",j,i);fflush(out);}
				#endif		
				x*=Rmin[prot[j].ammino[i].residue]/module; 
				y*=Rmin[prot[j].ammino[i].residue]/module;
				z*=Rmin[prot[j].ammino[i].residue]/module;
				#ifdef PROGRESS_KNOT
				if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0f j=%d i=%d\n",j,i);fflush(out);}
				#endif
			}		
			x+=prot[j].ammino[i].x+traslx;
			y+=prot[j].ammino[i].y+trasly;
			z+=prot[j].ammino[i].z+traslz;

			#ifdef PROGRESS_KNOT
			if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 0g j=%d i=%d\n",j,i);fflush(out);}
			#endif
			fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,"O  ",sist->Amminoacids[prot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
			pl++;
		}
	
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
}
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 1\n");fflush(out);}
	#endif
	pl=1.;
	sprintf(message, "MinE-P-%d.pdb",my_rank);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		traslx=minprot[j].ammino[0].x;
		trasly=minprot[j].ammino[0].y;
		traslz=minprot[j].ammino[0].z;
		traslx=P_Cd(traslx);trasly=P_Cd(trasly);traslz=P_Cd(traslz);
		traslx-=minprot[j].ammino[0].x;
		trasly-=minprot[j].ammino[0].y;
		traslz-=minprot[j].ammino[0].z;
		for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			for(kk=0;kk<NATOM;kk++){
				x=minprot[j].ammino[i+kk].x+traslx;
				y=minprot[j].ammino[i+kk].y+trasly;
				z=minprot[j].ammino[i+kk].z+traslz;
				
				pl++;
				fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,sist->Atoms[minprot[j].ammino[i+kk].id],sist->Amminoacids[minprot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
				
			}
			x=minprot[j].ammino[i].ndipole_x;
			y=minprot[j].ammino[i].ndipole_y;
			z=minprot[j].ammino[i].ndipole_z;
			
			if(E_Dip>0){
				module=sqrt(x*x+y*y+z*z);
				if((isnan(module))||(isnan(1./module))){
					fprintf(out,"Fsamp Mossaid=%d %lu module=%lf x=%lf y=%lf z=%lf\n",mossaid,icycl,module,x,y,z);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				
				x*=Rmin[minprot[j].ammino[i].residue]/module;
				y*=Rmin[minprot[j].ammino[i].residue]/module;
				z*=Rmin[minprot[j].ammino[i].residue]/module;
			}		
			x+=minprot[j].ammino[i].x+traslx;
			y+=minprot[j].ammino[i].y+trasly;
			z+=minprot[j].ammino[i].z+traslz;
			
			
			pl++;
			fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,"O  ",sist->Amminoacids[minprot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
			
		}
		
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 2\n");fflush(out);}
	#endif
	pl=1.;
	sprintf(message, "BestH-P-%d.pdb",my_rank);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		
		traslx=bestOprot[j].ammino[0].x;
		trasly=bestOprot[j].ammino[0].y;
		traslz=bestOprot[j].ammino[0].z;
		traslx=P_Cd(traslx);trasly=P_Cd(trasly);traslz=P_Cd(traslz);
		traslx-=bestOprot[j].ammino[0].x;
		trasly-=bestOprot[j].ammino[0].y;
		traslz-=bestOprot[j].ammino[0].z;
		for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			for(kk=0;kk<NATOM;kk++){
				x=bestOprot[j].ammino[i+kk].x+traslx;
				y=bestOprot[j].ammino[i+kk].y+trasly;
				z=bestOprot[j].ammino[i+kk].z+traslz;
				
				pl++;
				fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,sist->Atoms[bestOprot[j].ammino[i+kk].id],sist->Amminoacids[bestOprot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
				
			}
			x=bestOprot[j].ammino[i].ndipole_x;
			y=bestOprot[j].ammino[i].ndipole_y;
			z=bestOprot[j].ammino[i].ndipole_z;
			
			if(E_Dip>0){
				module=sqrt(x*x+y*y+z*z);
				if((isnan(module))||(isnan(1./module))){
					fprintf(out,"Fsamp Mossaid=%d %lu module=%lf x=%lf y=%lf z=%lf\n",mossaid,icycl,module,x,y,z);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				
				x*=Rmin[bestOprot[j].ammino[i].residue]/module;
				y*=Rmin[bestOprot[j].ammino[i].residue]/module;
				z*=Rmin[bestOprot[j].ammino[i].residue]/module;
			}		
			x+=bestOprot[j].ammino[i].x+traslx;
			y+=bestOprot[j].ammino[i].y+trasly;
			z+=bestOprot[j].ammino[i].z+traslz;
			
			if(NPROT>1) {x=P_Cd(x);y=P_Cd(y);z=P_Cd(z);}
			module=sqrt(x*x+y*y+z*z)*Rmin[prot[j].ammino[i].residue];
			pl++;
			fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,"O  ",sist->Amminoacids[bestOprot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
			
		}
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 3\n");fflush(out);}
	#endif

	fprintf(fEner,"%lu ",iter);
	fflush(fEner);
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	OEnd=lround(OEND_bin*sist->End/DNPROT);
	///OTopo=lround(sist->Topo_indice/DNPROT);
	OALN=lround(sist->ALN_tot/DNPROT);
	if(OEnd>=sizeEnd) OEnd=sizeEnd-1;
	///if(OTopo>=sizeTopo) OTopo=sizeTopo-1;
	if(OALN>=sizeALN) OALN=sizeALN-1;
	
	/// from here it prints the order parameters 
	 
	///fprintf(fEner,"%10.5lf %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %d %10.5lf %10.5lf %10.5lf %10.5lf 			%lf\n",sist->E,MinE,BestH/Mean_H_Bonds_bin,MinEH/Mean_H_Bonds_bin,MinEO/Mean_CA_Bonds_bin,BestHE,BestHO/Mean_CA_Bonds_bin,sqrt(MINR),sist->contact,sist->Mean_H_Bonds/Mean_H_Bonds_bin,sist->Mean_CA_Bonds/Mean_CA_Bonds_bin,Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds],Wpot_Topo[betaindice][OEnd][OTopo],1/beta[betaindice]); 
	fprintf(fEner,"%10.5lf %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %d %10.5lf %10.5lf %10.5lf %10.5lf	       	%lf\n",sist->E,MinE,BestH/Mean_H_Bonds_bin,MinEH/Mean_H_Bonds_bin,MinEO/Mean_CA_Bonds_bin,BestHE,BestHO/Mean_CA_Bonds_bin,sqrt(MINR),sist->contact,sist->Mean_H_Bonds/Mean_H_Bonds_bin,sist->Mean_CA_Bonds/Mean_CA_Bonds_bin,Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds],Wpot_ALN[betaindice][OEnd][OALN],1/beta[betaindice]); 

	fflush(fEner);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 4\n");fflush(out);}
	#endif
	fprintf(Accepted,	"%lf %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",1/beta[betaindice],icycl,all_trasl_accepted,all_trasl_tried,all_rot_accepted,all_rot_tried,rot_accepted,rot_tried,trasl_fseq_accepted,trasl_fseq_tried,trasl_accepted,trasl_tried,fw_accepted,fw_tried,bw_accepted,bw_tried,fseq_accepted,fseq_tried);
	fprintf(Rejected,	"%lf %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",1/beta[betaindice],icycl,local_rejectedener,local_rejectedself,all_trasl_rejectedself,all_trasl_rejectedener,all_rot_rejectedself,all_rot_rejectedener,rot_rejectedener,trasl_fseq_rejectedself,trasl_fseq_rejectedener,trasl_rejectedself,trasl_rejectedener,fw_rejectedself,fw_rejectedener,bw_rejectedself,bw_rejectedener,fseq_rejectedener);
	
	fflush(Accepted);
	fflush(Rejected);
	
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 5\n");fflush(out);}
	#endif
	sprintf(message, "VMD/Seqvmd-T-%lf-I-%08lu.bin",1/beta[betaindice],iter);
	fp=fopen(message,"w");
	writeBinary(fp);
	fclose(fp);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 6\n");fflush(out);}
	#endif
	sprintf(message,"final-P-%d.out",my_rank);
	fp=fopen(message,"w");
	writeBinary(fp);
	fclose(fp);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 7\n");fflush(out);}
	#endif
	sprintf(message,"MinE-P-%d.bin",my_rank);
	fp=fopen(message,"w");
	writeBinaryMin(fp);
	fclose(fp);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 8\n");fflush(out);}
	#endif
	sprintf(message,"BestH-P-%d.bin",my_rank);
	fp=fopen(message,"w");
	writeBinaryMinO(fp);
	fclose(fp);
	#ifdef PROGRESS_KNOT
	if(icycl>PROGRESS_STEP_SKIP) {fprintf (out,"fsamp OK 9\n");fflush(out);}
	#endif
	iter++;

}

void native (void){
	
	FILE *fp=NULL;
	
	int i=0,j=0,k=0,kk=0,n1=0,p2=0,pr=0,prt=0,a=0,linea=0,native_test=0;
	double x=0,y=0,z=0;
	double r2=0,r6=0,r12=0;
	double dx=0,dy=0,dz=0;
	double E1=0.0,E2=0.0;
	char line[200];
	double r=0,LJ=0,LJ_repulsive=0,potential=0;
	long int indice=0,indicen=0,indicet=0;
	long int indicenn=0,indicett=0,indicex=0;	
	
	E1=0.0;
	E2=0.0;
	fprintf(out,"native 2230\n");
	fflush(out);
	fp=fopen("native1","r");
	if ( fp == NULL) {
		DNPROT=1.0;
		/*printf ("File native1 not found\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);*/
	}else{
		fgets_out=fgets(line,sizeof(line),fp);
		fgets_out=fgets(line,sizeof(line),fp);
		fgets_out=fgets(line,sizeof(line),fp);
		while(!feof(fp)){
			fgets_out=fgets(line,sizeof(line),fp);
			
			p2=sscanf (line,"%*d %*d %d %d %d %d %lf\n",&pr,&i,&prt,&j,&x);
			
			if(pr>-1){
				
				
				
				if(pr==prt){
					if(( j!= i+NATOM)&&(j != i)&&(j != i-NATOM)&&(j != i-2*NATOM)&&(j != i+2*NATOM)){
						
						r=sqrt(x);						
						E1+=Potential(r,pr,i,prt,j,prot[pr].ammino[i].residue,prot[prt].ammino[j].residue)/2.0;		
						if(x<sist->Rint2) {
							//fprintf(out,"pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);fflush(out);
							indicen=(i-ATOM_CA)/NATOM;
							
							indice=NEIGH_MAX*indicen+RMSD_Neigh[indicen];
							if(indice>=AmminoN*NEIGH_MAX){
								fprintf(out,"indice Big indice=%ld MAX=%d\n",indice,AmminoN*NEIGH_MAX);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
							//fprintf(out,"indice=%ld indicen=%ld\n",indice,indicen);fflush(out);
							
							RMSD_neigh[indice]=j;
							RMSD_Neigh[indicen]++;
							//fprintf(out,"Native 1 Neigh[%d][%ld]=%d dist=%lf limit=%lf\n",i,indice,j,x,sist->Rint2);fflush(out);
							DNPROT++;
							//fprintf(out,"RMSD_neigh[%ld]=%d RMSD_neigh[%ld]=%d RMSD_Neigh[%ld]=%d\n",indice,RMSD_neigh[indice],indice+1,RMSD_neigh[indice+1],indicen,RMSD_Neigh[indicen]);fflush(out);
							if(RMSD_Neigh[indicen]>=NEIGH_MAX){
								fprintf(out,"Too many neigh in the native1 structure Neigh=%d MAX=%d\n",RMSD_Neigh[indicen],NEIGH_MAX);
								fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
						}
						DistMap1[j][i]=DistMap1[i][j]=r;
					}
				}else{
					
					r=sqrt(x);						
					E1+=Potential(r,pr,i,prt,j,prot[pr].ammino[i].residue,prot[prt].ammino[j].residue)/2.0;	
					
					/*	if(x<sist->Rint2) {
						indicen=(i-ATOM_CA)/NATOM;
						
						indice=2*NEIGH_MAX*(indicen)+2*RMSD_Neigh[indicen];
						RMSD_neigh[indice]=prt;
						RMSD_neigh[indice+1]=j;
						RMSD_Neigh[indicen]++;
						if(RMSD_Neigh[indicen]>=NEIGH_MAX){
							fprintf(out,"Too many neigh in the native1 structure Neigh=%d MAX=%d\n",RMSD_Neigh[indicen],NEIGH_MAX);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
					}
					DistMap1[pr][i][prt][j]=r;*/
				}
			}
		}		
		fclose(fp);	
	}
	DNPROT*=NPROT;
	/******** Test Neighbour native list ****/
	for(i=ATOM_CA;i<Plength[0];i+=NATOM){
		indicen=(i-ATOM_CA)/NATOM;
		indicet=indicen*NEIGH_MAX;
		for(k=0;k<RMSD_Neigh[indicen];k++){
			indice=indicet+k;
			//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
			if(indice>=AmminoN*NEIGH_MAX){
				fprintf(out,"RMSD_CA_fw icycl=%lu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			j=RMSD_neigh[indice];
			
			indicenn=(j-ATOM_CA)/NATOM;
			indicett=indicenn*NEIGH_MAX;
			native_test=0;
			for(kk=0;kk<RMSD_Neigh[indicenn];kk++){
				indicex=indicett+kk;
				if(RMSD_neigh[indicex]==i) native_test++;
			}
			//fprintf(out,"Native neigh native_test=%d Neigh[%d][%ld]=%d\n",native_test,i,indice,j);fflush(out);
			if(native_test !=1){
				
				
				for(kk=0;kk<RMSD_Neigh[indicen];kk++){
					indicex=indicet+kk;
					fprintf(out,"----------> Neigh[%d][%ld]=%d\n",j,indicex,RMSD_neigh[indicex]);fflush(out);
				}
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
	}
	
	
	
	fprintf(out,"native 2329\n");
	fflush(out);
	fp=fopen("native2","r");
	linea=0;
	if ( fp == NULL) {
		/*printf ("File native2 not found\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);*/
	}else{
		fgets_out=fgets(line,sizeof(line),fp);
		fgets_out=fgets(line,sizeof(line),fp);
		fgets_out=fgets(line,sizeof(line),fp);
		while(!feof(fp)){
			fgets_out=fgets(line,sizeof(line),fp);
			p2=sscanf (line,"%*d%*d%d%d%d%d%lf\n",&pr,&i,&prt,&j,&x);
			
			/*fprintf(out,"native %d\n",linea);
			fflush(out);
			fprintf(out,"native %d %d %d %d %lf\n",pr,i,prt,j,x);
			fflush(out);
			if(pr>-1) fprintf(out,"native resA %d resB %d\n",prot[pr].ammino[i].residue,prot[prt].ammino[j].residue);			
			fflush(out);
			if(pr>-1) fprintf(out,"native contact %d \n",contact[pr][i][prt][j]);
			fflush(out);
			
			fprintf(out,"native %d %d\n",NPROT,ProtN);
			fflush(out);*/
			linea++;
			if(pr>-1){
				
				if(pr==prt){
					if(( j!= i+NATOM)&&(j != i)&&(j != i-NATOM)&&(j != i-2*NATOM)&&(j != i+2*NATOM)){
						r=sqrt(x);						
						E2+=Potential(r,pr,i,prt,j,prot[pr].ammino[i].residue,prot[prt].ammino[j].residue)/2.0;					
						
					}
				}else{
					
					r=sqrt(x);						
					E2+=Potential(r,pr,i,prt,j,prot[pr].ammino[i].residue,prot[prt].ammino[j].residue)/2.0;					
					
				}
			}
			/*fprintf(out,"OK3\n");
			fflush(out);*/
		}		
		fclose(fp);	
	}
	native2=0;
	
	fprintf(out,"native 2422\n");
	fflush(out);
	native1=0;
	
	for(i=0;i<ProtN;i++){
		
		for(j=0;j<ProtN;j++){
			
			if(( j!= i+NATOM)&&(j != i)&&(j != i-NATOM)&&(j != i-2*NATOM)&&(j != i+2*NATOM)){
				if(DistMap1[i][j]>0) native1++;
				
			}
			
			
		}
	}
	
	/*for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			for(prt=0;prt<NPROT;prt++){
				for(j=0;j<Plength[prt];j++){
					if(icycl > 0) fprintf(out,"%d ",contact[pr][i][prt][j]);
				}
			}
			if(icycl > 0) fprintf(out,"\n");
		}
	}*/
	
	fprintf(out,"native 2433\n");
	fflush(out);
	native1/=2;	
	native2/=2;	
	if(my_rank==0){
		fp=fopen("SimPar","a");
		fprintf(fp,"Native structure1\n");
		fprintf(fp,"Enative=%lf\n",E1);
		fprintf(fp,"Order=%d\n",native1);
		fprintf(fp,"Native structure 2\n");
		fprintf(fp,"Enative=%lf\n",E2);
		fprintf(fp,"Order=%d\n",native2);
		fclose(fp);
	}
	
	if(E1>=E2){
		Enative=E2;
	}else{
		Enative=E1;
	}
	
}

void fastadecoder (char **Dec, char **Enc){
	int pr=0,i=0;
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr]/NATOM;i++){
			switch (Enc[pr][i]) {
				case 'A':
				Dec[pr][i]=1;
				break;
				case 'C':
				Dec[pr][i]=2;
				break;
				case 'D':
				Dec[pr][i]=3;
				break;
				case 'E':
				Dec[pr][i]=4;
				break;
				case 'F':
				Dec[pr][i]=5;
				break;
				case 'G':
				Dec[pr][i]=6;
				break;
				case 'H':
				Dec[pr][i]=7;
				break;
				case 'I':
				Dec[pr][i]=8;
				break;
				case 'K':
				Dec[pr][i]=9;
				break;
				case 'L':
				Dec[pr][i]=10;
				break;
				case 'M':
				Dec[pr][i]=11;
				break;
				case 'N':
				Dec[pr][i]=12;
				break;
				case 'P':
				Dec[pr][i]=13;
				break;
				case 'Q':
				Dec[pr][i]=14;
				break;
				case 'R':
				Dec[pr][i]=15;
				break;
				case 'S':
				Dec[pr][i]=16;
				break;
				case 'T':
				Dec[pr][i]=17;
				break;
				case 'V':
				Dec[pr][i]=18;
				break;
				case 'W':
				Dec[pr][i]=19;
				break;
				case 'Y':
				Dec[pr][i]=20;
				break;
				case 'X':
				Dec[pr][i]=1;
				break;
				default:
				fprintf(out,"errore nel codice della Senquenza Residuo[%d]=%c unkwon\n",i,Enc[pr][i]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			} 
		}
	}
}

void fastadecoder2 (char *Dec, char *Enc){
	int i=0;
	
	for(i=0;i<N;i++){
		switch (Enc[i]) {
			case 'A':
			Dec[i]=1;
			break;
			case 'C':
			Dec[i]=2;
			break;
			case 'D':
			Dec[i]=3;
			break;
			case 'E':
			Dec[i]=4;
			break;
			case 'F':
			Dec[i]=5;
			break;
			case 'G':
			Dec[i]=6;
			break;
			case 'H':
			Dec[i]=7;
			break;
			case 'I':
			Dec[i]=8;
			break;
			case 'K':
			Dec[i]=9;
			break;
			case 'L':
			Dec[i]=10;
			break;
			case 'M':
			Dec[i]=11;
			break;
			case 'N':
			Dec[i]=12;
			break;
			case 'P':
			Dec[i]=13;
			break;
			case 'Q':
			Dec[i]=14;
			break;
			case 'R':
			Dec[i]=15;
			break;
			case 'S':
			Dec[i]=16;
			break;
			case 'T':
			Dec[i]=17;
			break;
			case 'V':
			Dec[i]=18;
			break;
			case 'W':
			Dec[i]=19;
			break;
			case 'Y':
			Dec[i]=20;
			break;
			case 'X':
			Dec[i]=1;
			break;
			default:
			if(icycl > 0) fprintf(out,"errore nel codice della Senquenza Residuo[%d]=%c unkwon\n",i,Enc[i]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
	}
}

void Tswap (void){
	
	/************************************************
	* Tswap --Swap the states at different 	        *
	*		temperatures		                    *
	* 						                        *
	* Parameters: 				                    *
	*	prot -- is a proteina type structure	    *
	*		where is stored the protein 	        *
	*						                        *
	* Return:					                    *
	*	void										*
	*												*
	*************************************************/
	
	int i=0,j=0,kk=0;
	static int kkt=0;
	int tag = 0 ;
	int position=0;
	int rr=0,a=0,b=0,my_indice=0,j_indice=0;
	int OEnd=0, OALN=0;
	///int OTopo=0;
	double acc=0.0,DW1=0.0,DW2=0.0,rrrr=0,espo=0,espo1=0;
	double DE=0.0;
	static double *sendord=NULL;
	static double *recvord=NULL;
	
	MPI_Status status;
	
	#define iCABonds 0
	#define iHBonds 1
	#define iEnd 2
	///#define iTopo 3
	#define iALN 3
	#define iTouch 4
	#define iE 5
	#define idensity 6
	#define ibetaindice 7
	#define imy_rank 8
	#define iRANDOM 9
	#define SWAP_SIZE 10
	
	if(sendord==NULL) sendord=(double *)calloc((p*SWAP_SIZE),sizeof(double));
	if(recvord==NULL) recvord=(double *)calloc((p*SWAP_SIZE),sizeof(double));
	
	for (i=0;i<p*(SWAP_SIZE);i++){		
		sendord[i]=0.0;
		recvord[i]=0.0;
	}
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	sendord[my_rank*SWAP_SIZE+iCABonds]=(double)(sist->Mean_CA_Bonds); /// my_rank is the node indice, in finit is initialized such as my_rank=betaindice
	sendord[my_rank*SWAP_SIZE+iHBonds]=(double)(sist->Mean_H_Bonds); /// Building of a vector which contain all the infos to be sent
	OEnd=lround(OEND_bin*sist->End/DNPROT);                          /// in all the nodes, it has size p*SWAP_SIZE (number of parameters per node)
	///OTopo=lround(sist->Topo_indice/DNPROT);
	OALN=lround(sist->ALN_tot/DNPROT);
	if(OEnd>=sizeEnd) OEnd=sizeEnd-1;
	///if(OTopo>=sizeTopo) OTopo=sizeTopo-1;
	if(OALN>=sizeALN) OALN=sizeALN-1;
	
	sendord[my_rank*SWAP_SIZE+iEnd]=(double)(OEnd);
	///sendord[my_rank*SWAP_SIZE+iTopo]=(double)(OTopo);
	sendord[my_rank*SWAP_SIZE+iALN]=(double)(OALN);
	
	if(sist->Mean_H_Bonds>0) sendord[my_rank*SWAP_SIZE+iTouch]=1;
	sendord[my_rank*SWAP_SIZE+iE]=sist->E;
	sendord[my_rank*SWAP_SIZE+idensity]=sist->density;	
	sendord[my_rank*SWAP_SIZE+ibetaindice]=(double)(betaindice);
	sendord[betaindice*SWAP_SIZE+imy_rank]=(double)(my_rank);
	sendord[betaindice*SWAP_SIZE+iRANDOM]=ran3(&seed);
		
	#ifdef PROGRESS
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"ALL Reduce try %lu energy %lf\n",icycl,sist->E);fflush(out);}
	#endif
	
	MPI_Allreduce(sendord,recvord,p*SWAP_SIZE,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); /// sum of all sendort vectors on different nodes in
																				/// recvord, and save recvord in all the nodes
	#ifdef PROGRESS
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Tswap Sampling try %lu energy %lf\n",icycl,sist->E);fflush(out);}
	#endif
	my_indice=my_rank*SWAP_SIZE;  /// indice of the starting point of the node my_rank in the big vector sendord
	
	for (j=0;j<p;j++){
		j_indice=j*SWAP_SIZE; /// running on all nodes
		if(j!=my_rank){ 
										/// those are OHBonds and OCABonds of the other node j
			DW1=		Wpot[betaindice][(int)(recvord[j_indice+iHBonds])][(int)(recvord[j_indice+iCABonds])]-
			  Wpot[betaindice][(int)(recvord[my_indice+iHBonds])][(int)(recvord[my_indice+iCABonds])];
			DW2= Wpot[(int)(recvord[j_indice+ibetaindice])][(int)(recvord[my_indice+iHBonds])][(int)(recvord[my_indice+iCABonds])]-
			        Wpot[(int)(recvord[j_indice+ibetaindice])][(int)(recvord[j_indice+iHBonds])][(int)(recvord[j_indice+iCABonds])];
/// DW= Wpot[beta_i][OHbond_j][OCABond_j] - Wpot[beta_i][OHbond_i][OCABond_i] + Wpot[beta_j][OHbond_i][OCABond_i] -Wpot[beta_j][OHbond_j][OCABond_j]
		
		/***	DW+=	Wpot_Topo[betaindice][(int)(recvord[j_indice+iEnd])][(int)(recvord[j_indice+iTopo])]-
			        Wpot_Topo[betaindice][(int)(recvord[my_indice+iEnd])][(int)(recvord[my_indice+iTopo])]+
			        Wpot_Topo[(int)(recvord[j_indice+ibetaindice])][(int)(recvord[my_indice+iEnd])][(int)(recvord[my_indice+iTopo])]-
			        Wpot_Topo[(int)(recvord[j_indice+ibetaindice])][(int)(recvord[j_indice+iEnd])][(int)(recvord[j_indice+iTopo])];***/
			
			/*DW+=	Wpot_ALN[betaindice][(int)(recvord[j_indice+iEnd])][(int)(recvord[j_indice+iALN])]-
			        Wpot_ALN[betaindice][(int)(recvord[my_indice+iEnd])][(int)(recvord[my_indice+iALN])]+
			        Wpot_ALN[(int)(recvord[j_indice+ibetaindice])][(int)(recvord[my_indice+iEnd])][(int)(recvord[my_indice+iALN])]-
			        Wpot_ALN[(int)(recvord[j_indice+ibetaindice])][(int)(recvord[j_indice+iEnd])][(int)(recvord[j_indice+iALN])];*/
			
			espo=(beta[betaindice]-beta[(int)(recvord[j_indice+ibetaindice])])*(recvord[my_indice+iE]-recvord[j_indice+iE])+beta[betaindice]*DW1+beta[(int)(recvord[j_indice+ibetaindice])]*DW2;
			
			if(espo<50.0){
				espo1=-log(1+exp(espo)); /// for the virtual move of parallel tempering
			}else{
				espo1=-espo;
			}
			
			if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
				if(icycl > 0) fprintf(out,"Cavolo TSWAP 1  %lu %lf %d %d espo=%lf\n",icycl,1/beta[betaindice],betaindice,(int)(recvord[j_indice+ibetaindice]),espo);
				fprintf(out,"%d %d %d\n",j_indice,(int)(recvord[j_indice+iCABonds]),(int)(recvord[j_indice+iHBonds]));
				fprintf(out,"Enew=%lf Eold=%lf DW1=%lf DW2=%lf \n",recvord[my_indice+iE],recvord[j_indice+iE],DW1,DW2);
				fprintf(out,"Simulation Time %lf (seconds)\n",MPI_Wtime()-starttime);
				
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			///sampling(recvord[j_indice+idensity],recvord[j_indice+iE],(int)(recvord[j_indice+iCABonds]),(int)(recvord[j_indice+iTouch]),(int)(recvord[j_indice+iHBonds]),(int)(recvord[j_indice+iEnd]),(int)(recvord[j_indice+iTopo]),espo+espo1);
			sampling(recvord[j_indice+idensity],recvord[j_indice+iE],(int)(recvord[j_indice+iCABonds]),(int)(recvord[j_indice+iTouch]),(int)(recvord[j_indice+iHBonds]),(int)(recvord[j_indice+iEnd]),(int)(recvord[j_indice+iALN]),espo+espo1);
			///sampling(recvord[my_indice+idensity],recvord[my_indice+iE],(int)(recvord[my_indice+iCABonds]),(int)(recvord[my_indice+iTouch]),(int)(recvord[my_indice+iHBonds]),(int)(recvord[my_indice+iEnd]),(int)(recvord[my_indice+iTopo]),espo1);	
			sampling(recvord[my_indice+idensity],recvord[my_indice+iE],(int)(recvord[my_indice+iCABonds]),(int)(recvord[my_indice+iTouch]),(int)(recvord[my_indice+iHBonds]),(int)(recvord[my_indice+iEnd]),(int)(recvord[my_indice+iALN]),espo1);	
		}
	}
		
	
	#ifdef PROGRESS
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Tswap Sampling ok %lu energy %lf\n",icycl,sist->E);fflush(out);}
	#endif
	
	rr=(int)(recvord[iRANDOM]*2);	/// this number is equal to 0 or 1
	rrrr=recvord[iRANDOM];
	
	for (i=rr;i<p-1;i+=2){	/// here I start swapping the temperatures from "rr"
		a=(int)(recvord[(i)*SWAP_SIZE+imy_rank]);
		b=(int)(recvord[(i+1)*SWAP_SIZE+imy_rank]);
		
		/// "a" and "b" are the indicees of the nodes simulating the temperature T_i and T_(i+1)
		/// I check if the actual node coincides with "a" or "b". In that case I try to swap the Temperatures.
		if (my_rank==a){
						
			DE=recvord[(b)*SWAP_SIZE+iE]-recvord[(a)*SWAP_SIZE+iE];
			
			DW1=		Wpot[i][(int)(recvord[b*SWAP_SIZE+iHBonds])][(int)(recvord[b*SWAP_SIZE+iCABonds])]-
			  Wpot[i][(int)(recvord[a*SWAP_SIZE+iHBonds])][(int)(recvord[a*SWAP_SIZE+iCABonds])];
			DW2=
			        Wpot[i+1][(int)(recvord[a*SWAP_SIZE+iHBonds])][(int)(recvord[a*SWAP_SIZE+iCABonds])]-
			        Wpot[i+1][(int)(recvord[b*SWAP_SIZE+iHBonds])][(int)(recvord[b*SWAP_SIZE+iCABonds])];
			
		/***	DW+=   Wpot_Topo[i][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iTopo])]-
			       Wpot_Topo[i][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iTopo])]+
			       Wpot_Topo[i+1][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iTopo])]-
			       Wpot_Topo[i+1][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iTopo])];***/
			/*DW+=   Wpot_ALN[i][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iALN])]-
			       Wpot_ALN[i][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iALN])]+
			       Wpot_ALN[i+1][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iALN])]-
			       Wpot_ALN[i+1][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iALN])];*/
			if(betaindice!=i){
				if(icycl > 0) fprintf(out,"Tswap Error A icycl= %lu betaindice= %d i= %d\n",icycl,betaindice,i);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			acc=exp((beta[i+1]-beta[i])*(DE)+DW1*beta[i]+DW2*beta[i+1]);
			ntempswap[betaindice]++;
			if (rrrr<acc){ /// accept the move. I fix T_a = T_b
				temthetasto[betaindice]++;
				betaindice=	(int)(recvord[b*SWAP_SIZE+ibetaindice]);
				
			}
		}
		
		
		if (my_rank==b){
			if(betaindice!=i+1){
				if(icycl > 0) fprintf(out,"Tswap Error B icycl= %lu betaindice= %d i+1= %d\n",icycl,betaindice,i+1);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			DE=recvord[(b)*SWAP_SIZE+iE]-recvord[(a)*SWAP_SIZE+iE];
			
			DW1=		Wpot[i][(int)(recvord[b*SWAP_SIZE+iHBonds])][(int)(recvord[b*SWAP_SIZE+iCABonds])]-
			  Wpot[i][(int)(recvord[a*SWAP_SIZE+iHBonds])][(int)(recvord[a*SWAP_SIZE+iCABonds])];
			DW2=
			        Wpot[i+1][(int)(recvord[a*SWAP_SIZE+iHBonds])][(int)(recvord[a*SWAP_SIZE+iCABonds])]-
			        Wpot[i+1][(int)(recvord[b*SWAP_SIZE+iHBonds])][(int)(recvord[b*SWAP_SIZE+iCABonds])];
			/***DW+=   Wpot_Topo[i][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iTopo])]-
			       Wpot_Topo[i][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iTopo])]+
			       Wpot_Topo[i+1][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iTopo])]-
			       Wpot_Topo[i+1][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iTopo])];***/
			/*DW+=   Wpot_ALN[i][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iALN])]-
			       Wpot_ALN[i][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iALN])]+
			       Wpot_ALN[i+1][(int)(recvord[a*SWAP_SIZE+iEnd])][(int)(recvord[a*SWAP_SIZE+iALN])]-
			       Wpot_ALN[i+1][(int)(recvord[b*SWAP_SIZE+iEnd])][(int)(recvord[b*SWAP_SIZE+iALN])];*/
			acc=exp((beta[i+1]-beta[i])*(DE)+DW1*beta[i]+DW2*beta[i+1]);
			ntempswap[betaindice]++;
			if (rrrr<acc){
				temthetasto[betaindice]++;
				betaindice=(int)(recvord[a*SWAP_SIZE+ibetaindice]);
				
			}	
		}	 
	}
}

void W (void){
	int i=0,j=0,k=0;
	char message[300];
	FILE *fp=NULL;
	
	static unsigned long int iter=0;
	
	static double *rWhisto=NULL,*minW=NULL,*minWmean=NULL;
	
	
	if(minW==NULL) minW=(double *)calloc(p,sizeof(double));
	if(minWmean==NULL) minWmean=(double *)calloc(p,sizeof(double));
	if(rWhisto==NULL) rWhisto=(double *)calloc((p*sizeC*sizex),sizeof(double));
	
	
	
	iter++;
	for(k=0;k<p;k++){
		minW[k]=1e10;
		minWmean[k]=1e10;
	}
	for(i=0;i<p*sizeC*sizex;i++){
		
		rWhisto[i]=-1000.0;
		
	}
	fprintf(out,"W MPI_Allreduce try\n");fflush(out);
	MPI_Allreduce(Whisto,rWhisto,p*sizeC*sizex,MPI_DOUBLE,myExpSum,MPI_COMM_WORLD);
	fprintf(out,"W MPI_Allreduce try OK\n");fflush(out);
	for(i=0;i<p*sizeC*sizex;i++){
		
		Whisto[i]=rWhisto[i];
		if(isnan(Whisto[i])!=0){
			if(icycl > 0) fprintf(out,"Cavolo %lu %lf Whisto[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto[i]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}
	
	for(k=0;k<p;k++){
		for(i=0;i<sizeC;i++){
			for(j=0;j<sizex;j++){
				Wpot[k][i][j]-=umbrella*Whisto[k*sizeC*sizex+i*sizex+j];
				if(Wpot[k][i][j]<minW[k]) minW[k]=Wpot[k][i][j];
				
			}
		}
		
		for(i=0;i<sizeC;i++){
			for(j=0;j<sizex;j++){
				Wpot[k][i][j]=Wpot[k][i][j]-minW[k];
				if(Wpot[k][i][j]>1e9){
					if(icycl > 0) fprintf(out,"Cavolo Wpot[%d][%d][%d]=%lf\n",k,i,j,Wpot[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				if(isnan(Wpot[k][i][j])!=0){
					if(icycl > 0) fprintf(out,"Cavolo %lu %lf Whisto[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto[i]);
					if(icycl > 0) fprintf(out,"Cavolo Wpot[%d][%d][%d]=%lf\n",k,i,j,Wpot[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				
			}
		}
	}
	
	if(my_rank==0){
		for(k=0;k<p;k++){
			sprintf(message,"Wpot-T-%2.3f.dat",1/beta[k]);
			fp=fopen(message,"w");
			fwrite(&sizeC,sizeof(int),1,fp);
			fwrite(&minH,sizeof(int),1,fp);
			fwrite(&Mean_H_Bonds_bin,sizeof(double),1,fp);
			fwrite(&sizex,sizeof(int),1,fp);
			fwrite(&minO,sizeof(int),1,fp);
			fwrite(&Mean_CA_Bonds_bin,sizeof(double),1,fp);
			
			for(i=0;i<sizeC;i++){
				for(j=0;j<sizex;j++){
					
					bin_x=Wpot[k][i][j];
					fwrite(&bin_x,sizeof(double),1,fp);
				}
			}
			fclose(fp);
		}
	}
}

void W_Topo (void){
	int i=0,j=0,k=0;
	char message[300];
	FILE *fp=NULL;
	
	static unsigned long int iter=0;
	
	///static double *rWhisto_Topo=NULL,*minW=NULL,*minWmean=NULL;
	static double *rWhisto_ALN=NULL,*minW=NULL,*minWmean=NULL;
		
	if(minW==NULL) minW=(double *)calloc(p,sizeof(double));
	if(minWmean==NULL) minWmean=(double *)calloc(p,sizeof(double));
	///if(rWhisto_Topo==NULL) rWhisto_Topo=(double *)calloc((p*sizeEnd*sizeTopo),sizeof(double));
	if(rWhisto_ALN==NULL) rWhisto_ALN=(double *)calloc((p*sizeEnd*sizeALN),sizeof(double));
		
	iter++;
	for(k=0;k<p;k++){
		minW[k]=1e10;
		minWmean[k]=1e10;
	}
	/**for(i=0;i<p*sizeEnd*sizeTopo;i++){
		rWhisto_Topo[i]=-1000.0;
	}**/
	for(i=0;i<p*sizeEnd*sizeALN;i++){
		rWhisto_ALN[i]=-1000.0;
	}
	
	fprintf(out,"W MPI_Allreduce try\n");fflush(out);
	///MPI_Allreduce(Whisto_Topo,rWhisto_Topo,p*sizeEnd*sizeTopo,MPI_DOUBLE,myExpSum,MPI_COMM_WORLD);
	MPI_Allreduce(Whisto_ALN,rWhisto_ALN,p*sizeEnd*sizeALN,MPI_DOUBLE,myExpSum,MPI_COMM_WORLD);
	fprintf(out,"W MPI_Allreduce try OK\n");fflush(out);
	
	/**for(i=0;i<p*sizeEnd*sizeTopo;i++){
		Whisto_Topo[i]=rWhisto_Topo[i];
		if(isnan(Whisto_Topo[i])!=0){
			if(icycl > 0) fprintf(out,"Cavolo %lu %lf Whisto_Topo[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto_Topo[i]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}**/
	
	for(i=0;i<p*sizeEnd*sizeALN;i++){
		Whisto_ALN[i]=rWhisto_ALN[i];
		if(isnan(Whisto_ALN[i])!=0){
			if(icycl > 0) fprintf(out,"Cavolo %lu %lf Whisto_ALN[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto_ALN[i]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}
	
	for(k=0;k<p;k++){	/// update the bias potential
		for(i=0;i<sizeEnd;i++){
			/**for(j=0;j<sizeTopo;j++){
				Wpot_Topo[k][i][j]-=umbrella*Whisto_Topo[k*sizeEnd*sizeTopo+i*sizeTopo+j];
				if(Wpot_Topo[k][i][j]<minW[k]) minW[k]=Wpot_Topo[k][i][j];
			}**/
			for(j=0;j<sizeALN;j++){	
				Wpot_ALN[k][i][j]-=umbrella*Whisto_ALN[k*sizeEnd*sizeALN+i*sizeALN+j];
				if(Wpot_ALN[k][i][j]<minW[k]) minW[k]=Wpot_ALN[k][i][j];
			}
		}
		
		/**for(i=0;i<sizeEnd;i++){
			for(j=0;j<sizeTopo;j++){
				Wpot_Topo[k][i][j]=Wpot_Topo[k][i][j]-minW[k];
				if(Wpot_Topo[k][i][j]>1e9){
					if(icycl > 0) fprintf(out,"Cavolo Wpot_Topo[%d][%d][%d]=%lf\n",k,i,j,Wpot_Topo[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				if(isnan(Wpot_Topo[k][i][j])!=0){
					if(icycl > 0) fprintf(out,"Cavolo %lu %lf Whisto_Topo[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto_Topo[i]);
					if(icycl > 0) fprintf(out,"Cavolo Wpot_Topo[%d][%d][%d]=%lf\n",k,i,j,Wpot_Topo[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
	}
	
	if(my_rank==0){
		for(k=0;k<p;k++){
			sprintf(message,"Wpot_Topo-T-%2.3f.dat",1/beta[k]);
			fp=fopen(message,"w");
			fwrite(&sizeEnd,sizeof(int),1,fp);
			fwrite(&sizeTopo,sizeof(int),1,fp);
			for(i=0;i<sizeEnd;i++){
				for(j=0;j<sizeTopo;j++){
					bin_x=Wpot_Topo[k][i][j];
					fwrite(&bin_x,sizeof(double),1,fp);
				}
			}
			fclose(fp);
		}
	}**/
		for(i=0;i<sizeEnd;i++){
			for(j=0;j<sizeALN;j++){
				Wpot_ALN[k][i][j]=Wpot_ALN[k][i][j]-minW[k];
				if(Wpot_ALN[k][i][j]>1e9){
					if(icycl > 0) fprintf(out,"Cavolo Wpot_ALN[%d][%d][%d]=%lf\n",k,i,j,Wpot_ALN[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				if(isnan(Wpot_ALN[k][i][j])!=0){
					if(icycl > 0) fprintf(out,"Cavolo %lu %lf Whisto_ALN[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto_ALN[i]);
					if(icycl > 0) fprintf(out,"Cavolo Wpot_ALN[%d][%d][%d]=%lf\n",k,i,j,Wpot_ALN[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
	}
	
	if(my_rank==0){
		for(k=0;k<p;k++){
			sprintf(message,"Wpot_ALN-%2.3f.dat",1/beta[k]);
			fp=fopen(message,"w");
			fwrite(&sizeEnd,sizeof(int),1,fp);
			fwrite(&OEND_bin,sizeof(double),1,fp);
			fwrite(&sizeALN,sizeof(int),1,fp);
			fwrite(&OALN_bin,sizeof(double),1,fp);
			for(i=0;i<sizeEnd;i++){
				for(j=0;j<sizeALN;j++){
					bin_x=Wpot_ALN[k][i][j];
					fwrite(&bin_x,sizeof(double),1,fp);
				}
			}
			fclose(fp);
		}
	}
}

void Minimum_Energy (double E,int order_p,int contact){
	int i=0,k=0;
	
	if(MinE>=E) {
		MinE=E;
		MinEO=order_p;
		MinEH=contact;
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				minprot[i].ammino[k].x=prot[i].ammino[k].x;
				minprot[i].ammino[k].y=prot[i].ammino[k].y;
				minprot[i].ammino[k].z=prot[i].ammino[k].z;
				minprot[i].ammino[k].id=prot[i].ammino[k].id;
				minprot[i].ammino[k].residue=prot[i].ammino[k].residue;
				if(prot[i].ammino[k].id==ATOM_CA){
					minprot[i].ammino[k].ndipole_x=prot[i].ammino[k].ndipole_x;
					minprot[i].ammino[k].ndipole_y=prot[i].ammino[k].ndipole_y;
					minprot[i].ammino[k].ndipole_z=prot[i].ammino[k].ndipole_z;
				}
			}
		}
	}
	
	if(BestH<=contact) {
		BestHE=E;
		BestH=contact;
		BestHO=order_p;
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				bestOprot[i].ammino[k].x=prot[i].ammino[k].x;
				bestOprot[i].ammino[k].y=prot[i].ammino[k].y;
				bestOprot[i].ammino[k].z=prot[i].ammino[k].z;
				bestOprot[i].ammino[k].id=prot[i].ammino[k].id;
				bestOprot[i].ammino[k].residue=prot[i].ammino[k].residue;
				if(prot[i].ammino[k].id==ATOM_CA){
					bestOprot[i].ammino[k].ndipole_x=prot[i].ammino[k].ndipole_x;
					bestOprot[i].ammino[k].ndipole_y=prot[i].ammino[k].ndipole_y;
					bestOprot[i].ammino[k].ndipole_z=prot[i].ammino[k].ndipole_z;
				}
			}
		}
	}
	
}

void Order_Conf_Sampling (double E,int order_p,int contact){
	int i=0,j=0,k=0,kk=0,pl=0;
	double indiceO=0,indiceH=0;
	double traslx=0,trasly=0,traslz=0;
	double x=0,y=0,z=0;
	double module=0;
	char message[200];
	FILE *fp=NULL;
	
	if((order_p<ImaxO)&&(order_p>=IminO)&&(contact<ImaxH)&&(contact>=IminH)){
		indiceO=order_p/Mean_CA_Bonds_bin+minO;
		indiceH=contact/Mean_H_Bonds_bin+minH;
		pl=0;
		if(MinEner[contact][order_p]>=E){
			MinEner[contact][order_p]=E;
			
			sprintf(message, "OP-%d/O-%06.1lf-C-%04.1lf.pdb",my_rank,indiceO,indiceH);
			fp=fopen(message,"w");
			fprintf(fp,"HEADER    E=%lf\n",E);
		/***	for(j=0;j<NPROT;j++){
				fprintf(fp,"HEADER    KNOT %s\n",prot[j].topology);
			}***/
			for(j=0;j<NPROT;j++){
				fprintf(fp,"HEADER    KNOT %lf\n",prot[j].ALN);
			}
			
			for(j=0;j<NPROT;j++){
				traslx=prot[j].ammino[0].x;
				trasly=prot[j].ammino[0].y;
				traslz=prot[j].ammino[0].z;
				traslx=P_Cd(traslx);trasly=P_Cd(trasly);traslz=P_Cd(traslz);
				traslx-=prot[j].ammino[0].x;
				trasly-=prot[j].ammino[0].y;
				traslz-=prot[j].ammino[0].z;
				for(i=ATOM_CA;i<Plength[j];i+=NATOM){
					for(kk=0;kk<NATOM;kk++){
						x=prot[j].ammino[i+kk].x+traslx;
						y=prot[j].ammino[i+kk].y+trasly;
						z=prot[j].ammino[i+kk].z+traslz;
						
						fprintf(fp,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,sist->Atoms[prot[j].ammino[i+kk].id],sist->Amminoacids[prot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
						
						pl++;
						
					}
					x=prot[j].ammino[i].ndipole_x;
					y=prot[j].ammino[i].ndipole_y;
					z=prot[j].ammino[i].ndipole_z;
					
					
					if(E_Dip>0){						
						module=sqrt(x*x+y*y+z*z);
						if((isnan(module))||(isnan(1./module))){
							fprintf(out,"Fsamp Mossaid=%d %lu module=%lf x=%lf y=%lf z=%lf\n",mossaid,icycl,module,x,y,z);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
							
						}
						
						x*=Rmin[prot[j].ammino[i].residue]/module;
						y*=Rmin[prot[j].ammino[i].residue]/module;
						z*=Rmin[prot[j].ammino[i].residue]/module;
					}				
					x+=prot[j].ammino[i].x+traslx;
					y+=prot[j].ammino[i].y+trasly;
					z+=prot[j].ammino[i].z+traslz;
					
					fprintf(fp,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",pl,"O  ",sist->Amminoacids[prot[j].ammino[i].residue],(int)(i/NATOM)+1,x,y,z);
					pl++;
				}
			}
			fprintf(fp,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
			fclose(fp);
			
			sprintf(message, "OP-%d/O-%06.1lf-C-%04.1lf.bin",my_rank,indiceO,indiceH);
			fp=fopen(message,"w");
			writeBinary(fp);
			fclose(fp);
		}
	}
}
	

void sampling (double density, double E,int order_p,int touch,int contact, int end, int topo, double espo){
	
	long int indice=0;
	double esposc=0;
	if((contact<0)){
		fprintf(out,"sampling non ok contact<0 icycl=%lu, contact=%d \n",icycl,contact);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}		
	
	/***********TEST***********/
	if((touch==0)||(touch==1)){
	}else{
		fprintf(out,"sampling non ok 5 icycl=%lu, touch=%d\n",icycl,touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((order_p<minO)){
		fprintf(out,"sampling non ok 6 icycl=%lu, order_p=%d %d %d\n",icycl,order_p,minO,maxO);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST***********/
	
	indice=betaindice*sizeC*sizex+(contact)*sizex+(order_p);
	
	/***********TEST***********/
	if(indice>=(unsigned int)(p*sizeC*sizex)){
		fprintf(out,"sampling non ok 1 indice=%lu p*sizeC*sizex=%d betaindice=%d sizex=%d sizeC=%d order_p=%d minO=%d minH=%d\n",indice,
		p*sizeC*sizex,betaindice,sizex,sizeC,order_p,minO,minH);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST***********/
	
	if(espo>=Whisto[indice]){
		Whisto[indice]=espo+log(1+exp(Whisto[indice]-espo));
         }else{
		Whisto[indice]=Whisto[indice]+log(1+exp(espo-Whisto[indice]));
       }
	if(isnan(Whisto[indice])!=0){
		if(icycl > 0) fprintf(out,"Cavolo sampling %lu %lf Whisto[%lu]=%lf\n",icycl,1/beta[betaindice],indice,Whisto[indice]);
		fprintf(out,"%lf %d %d %d %d\n",espo,order_p,contact,ImaxO,IminO);			
		fprintf(out,"Simulation Time %lf (seconds)\n",MPI_Wtime()-starttime);	
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	indice=betaindice*sizeEnd*sizeALN+(end)*sizeALN+(topo); 
	
	/***********TEST***********/
	if(indice>=(unsigned int)(p*sizeEnd*sizeALN)){
		fprintf(out,"sampling non ok 10 indice=%lu p*sizeC*sizex=%d betaindice=%d sizeALN=%d sizeEnd=%d end=%d topo=%d \n",indice,
		p*sizeEnd*sizeALN,betaindice,sizeALN,sizeEnd,end,topo);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
		}
	/***********TEST***********/
	
	/**if(espo>=Whisto_Topo[indice])
		Whisto_Topo[indice]=espo+log(1+exp(Whisto_Topo[indice]-espo));
	else
		Whisto_Topo[indice]=Whisto_Topo[indice]+log(1+exp(espo-Whisto_Topo[indice]));  
	
	if(isnan(Whisto_Topo[indice])!=0){
		if(icycl > 0) fprintf(out,"Cavolo sampling %lu %lf Whisto_Topo[%lu]=%lf\n",icycl,1/beta[betaindice],indice,Whisto_Topo[indice]);
		fprintf(out,"%lf %d %d\n",espo,end,topo);			
		fprintf(out,"Simulation Time %lf (seconds)\n",MPI_Wtime()-starttime);	
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}	C&V **/ 
	
	if(espo>=Whisto_ALN[indice])
		Whisto_ALN[indice]=espo+log(1+exp(Whisto_ALN[indice]-espo));
	else
		Whisto_ALN[indice]=Whisto_ALN[indice]+log(1+exp(espo-Whisto_ALN[indice]));
	if(isnan(Whisto_ALN[indice])!=0){
		if(icycl > 0) fprintf(out,"Cavolo sampling %lu %lf Whisto_ALN[%lu]=%lf\n",icycl,1/beta[betaindice],indice,Whisto_ALN[indice]);
		fprintf(out,"%lf %d %d\n",espo,end,topo);			
		fprintf(out,"Simulation Time %lf (seconds)\n",MPI_Wtime()-starttime);	
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}	
 	
	if((icycl<Equi1)||(icycl>10*(Equi2+Equi3)+Equi1)){	
		espo=espo-beta[betaindice]*(Wpot[betaindice][contact][(order_p)]+Wpot_ALN[betaindice][end][topo]);
		if(espo>=ndataen2[betaindice]){
			ndataen2[betaindice]=espo+log(1+exp(ndataen2[betaindice]-espo));
		}else{
			ndataen2[betaindice]=ndataen2[betaindice]+log(1+exp(espo-ndataen2[betaindice]));
		}
		
		if(density>0){
			
			if(espo>=Density[betaindice*3+1]){
				Density[betaindice*3+1]=espo+log(1+exp(Density[betaindice*3+1]-espo));
			}else{
				Density[betaindice*3+1]=Density[betaindice*3+1]+log(1+exp(espo-Density[betaindice*3+1]));
			} 
			
		}else{
			if(touch>0){	
				if(espo>=Density[betaindice*3]){
					Density[betaindice*3]=espo+log(1+exp(Density[betaindice*3]-espo));
				}else{
					Density[betaindice*3]=Density[betaindice*3]+log(1+exp(espo-Density[betaindice*3]));
				}
				
			}else{
				if(espo>=Density[betaindice*3+2]){
					Density[betaindice*3+2]=espo+log(1+exp(Density[betaindice*3+2]-espo));
				}else{
					Density[betaindice*3+2]=Density[betaindice*3+2]+log(1+exp(espo-Density[betaindice*3+2]));
				}
				
			}
		}
		
		///indice=betaindice*2*sizex+touch*sizex+(order_p);
		
		/***********TEST***********/
		/*if(indice>=(unsigned int)(p*2*sizex)){
			fprintf(out,"sampling non ok 2 indice=%lu p*2*sizex=%d betaindice=%d sizex=%d touch=%d order_p=%d minO=%d\n",indice,
			p*2*sizex,betaindice,sizex,touch,order_p,IminO);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		/***********TEST***********/
		
		/*if(espo>=histofill[indice]){
			histofill[indice]=espo+log(1+exp(histofill[indice]-espo));
		}else{
			histofill[indice]=histofill[indice]+log(1+exp(espo-histofill[indice]));
		}  C & V */ 
		
		/*if(icycl > 0) fprintf(out,"%lf %lf\n",histofill[0][0],ndataen);*/
		
		indice=(unsigned long int)( (E-Min_energy)*bin_energy );
				
		if (indice<0) indice=0;
		
		if (indice>=sizeEnergy) indice=sizeEnergy-1;
		
		indice=betaindice*sizeEnergy + indice;
		
		///printf("\nbetaindice = %d\t\t SizeEnergy = %d\t\tindice = %ld\n\n",betaindice,sizeEnergy,indice); fflush(NULL);
		
		/***********TEST***********/
		if(indice>=(unsigned int)(p*sizeEnergy)){
			fprintf(out,"sampling ENERGY non ok 3 indice=%lu p*sizeEnergy=%d betaindice=%d sizeEnergy=%d partint((E*bin_energy)-minE))=%d\n",
				indice,p*sizeEnergy,betaindice,sizeEnergy,partint( E*bin_energy-minE) );
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/***********TEST***********/
		
		if(espo>=histo_energy[indice]){
			histo_energy[indice]=espo+log(1+exp(histo_energy[indice]-espo));
		}else{
			histo_energy[indice]=histo_energy[indice]+log(1+exp(espo-histo_energy[indice]));
		}
		
		
		indice=betaindice*sizeC*sizex+contact*sizex+(order_p);
		
		/***********TEST***********/
		if(indice>=(unsigned int)(p*sizeC*sizex)){
			fprintf(out,"sampling non ok 4 indice=%lu p*sizeC*sizex=%d betaindice=%d sizex=%d sizeC=%d order_p=%d IminO=%d\n",indice,
			p*sizeC*sizex,betaindice,sizex,sizeC,order_p,IminO);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
				
		if(espo>=histoC[indice]){
			histoC[indice]=espo+log(1+exp(histoC[indice]-espo));
		}else{
			histoC[indice]=histoC[indice]+log(1+exp(espo-histoC[indice]));
		}
		/********************/
		esposc=espo+E*beta[betaindice];
		if(esposc>=histosc_norm[betaindice]){
		  histosc_norm[betaindice]=esposc+log(1+exp(histosc_norm[betaindice]-esposc));
		}else{
		  histosc_norm[betaindice]=histosc_norm[betaindice]+log(1+exp(esposc-histosc_norm[betaindice]));
		}

                if(esposc>=histoSC[indice]){
		  histoSC[indice]=esposc+log(1+exp(histoSC[indice]-esposc));
		}else{
		  histoSC[indice]=histoSC[indice]+log(1+exp(esposc-histoSC[indice]));
		}

		/*****************************/
		indice=betaindice*sizeEnd*sizeALN+end*sizeALN+(topo);
		
		if(indice>=(unsigned int)(p*sizeEnd*sizeALN)){
			fprintf(out,"sampling non ok 5 indice=%lu p*sizeEnd*sizeALN=%d betaindice=%d sizeALN=%d sizeEnd=%d end=%d topo=%d\n",indice,
			p*sizeEnd*sizeALN,betaindice,sizeALN,sizeEnd,end,topo);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			}
		/***********TEST***********/
		
		/**if(espo>=histo_Topo[indice])
			histo_Topo[indice]=espo+log(1+exp(histo_Topo[indice]-espo));
		else
			histo_Topo[indice]=histo_Topo[indice]+log(1+exp(espo-histo_Topo[indice])); **/
		/*if(espo>=histo_ALN[indice])
			histo_ALN[indice]=espo+log(1+exp(histo_ALN[indice]-espo));
		else
			histo_ALN[indice]=histo_ALN[indice]+log(1+exp(espo-histo_ALN[indice]));*/
			
	}
}


/*---------------------------*/ 


int MC_Crankshaft (int pr){
	 
	int self_flag=0;
	double dx=0,dy=0,dz=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX=0,RotY=0,RotZ=0,modulo=0;
	int i,k=0,MC_Crankshaft_test=0,kk=0,j=0,ii=0,jj=0,jjj=0,pprtv;
	unsigned int indice=0,indice2=0,indice3=0;
	int l=0,lll=0;
	int kkk=0,flag=1,kk_end=0;
	double angle1=0,angle2=0,random1=0,random2=0;
	double rr=0;
	int icel_x=0,icel_y=0,icel_z=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	///int OTopo_indice_New=0,OTopo_indice_Old=0;
	int OALN_New=0,OALN_Old=0;
	double End_New=0,aln_partial=0;
	///double Topo_indice_New=0;
	///int topo_New=0;
	double ALN_New=0, aln_new=0;
	double rCA2_end=0;
	int last=0;
	int test_flag=0;
	char info_ALN[20];
	FILE *fp=NULL;
	
	kk=(int)(ran3(&seed)*(Plength[pr]/NATOM)); /// Choose a bead
	
	
	if(kk>Plength[pr]/NATOM-4){      /// if it`s too close to the end perform Pivot_fw
		self_flag=MC_Pivot_fw(pr,kk);
		fw_tried++;
		
		#ifdef 	ORDER_TEST_WATER
		mossaid+=310;
			order_test_water(&testEWater);
		#endif
		return(self_flag);
	}
	if(kk<3){
		self_flag=MC_Pivot_bw(pr,kk); /// if it`s too close to the beginn perform Pivot_fw
		bw_tried++;
		#ifdef 	ORDER_TEST_WATER
		mossaid+=320;
			order_test_water(&testEWater);
		#endif
		return(self_flag);
	}
	
	if(ran3(&seed)<0.5){
		rr=ran3(&seed);
		kk_end=(int)(rr*(Plength[pr]/NATOM-kk-4))+3+kk;   /// choose second node according to the first one
		
		if(kk_end*NATOM>=(Plength[pr]-1)){
			fprintf(out,"Local kk_end kk_end=%u kk=%d rr=%lf Plength[pr]/NATOM-kk=%d\n",kk_end,kk,rr,Plength[pr]/NATOM-kk-4);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}else{
		kk_end=3+kk;
	}	
	
	
	kk=kk*NATOM;     
	kk_end=kk_end*NATOM;
	

	
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"CAZZOOOOOOO kk=%d %d %d\n",kk,prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(prot[pr].ammino[kk_end].id!=ATOM_CA){
		fprintf(out,"CAZZOOOOOOO kk_end=%d %d %d\n",kk_end,prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((kk>=Plength[pr]-1)||(kk<0)){
		fprintf(out,"KK out of range MC_Pivot_fw icycl %lu kk %d\n",icycl,kk);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/*strcpy(info_ALN,"crankshaft");	
	Linking_Number(prot,pr,ATOM_CA,kk,kk_end,Plength[pr],NATOM,info_ALN,&aln_partial); 
	aln_new=prot[pr].ALN-aln_partial;*/
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double)); /// I need to allocate only if I just initialized
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double)); /// so if it`s the first time that the function is called.
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));

	//Rotation is from last patch of Particle kk to the first patch of particle kk_end
	/// This is to avoid the change in the bond lenghts between the patches that connect the polymer
	if(SPRING_MODEL==1){
		dx=prot[pr].ammino[kk_end+1].x-prot[pr].ammino[kk+SPG_PATCH2].x;  /// SPG_PATCH2 =NSPOTS
		dy=prot[pr].ammino[kk_end+1].y-prot[pr].ammino[kk+SPG_PATCH2].y;
		dz=prot[pr].ammino[kk_end+1].z-prot[pr].ammino[kk+SPG_PATCH2].z;
	}else{
		dx=prot[pr].ammino[kk_end].x-prot[pr].ammino[kk].x;  /// SPG_PATCH2 =NSPOTS
		dy=prot[pr].ammino[kk_end].y-prot[pr].ammino[kk].y;
		dz=prot[pr].ammino[kk_end].z-prot[pr].ammino[kk].z;
	}
	
	//dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	RotX=dx;
	RotY=dy;
	RotZ=dz;
	
	/**/
	alpha=(PI*ran3(&seed)/5.0)-PI/10.0;
	
	/*************OLD STATE ENERGY****************/
	for(kkk=0;kkk<Plength[pr];kkk++){  /// Now I want to move so I remember the old coordinates
		oldX[kkk]=prot[pr].ammino[kkk].x;
		oldY[kkk]=prot[pr].ammino[kkk].y;
		oldZ[kkk]=prot[pr].ammino[kkk].z;
	}		
			
	for(kkk=kk+NATOM;kkk<kk_end;kkk++){
		
		indice=kkk;	
		if(indice>=(unsigned int)(Plength[pr]-1)){
			fprintf(out,"Local Eold indice=%u kk=%d kkk=%d\n",indice,kk,kkk);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		Eold+=energy_SP_brot_local(pr,indice,kk+SPG_PATCH2,kk_end,OLDWATER);
	
		Oold+=sist->norder;
		Cold+=sist->ncontact;	
	}		

	/********************************************************/
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	Eold+=Bond_Energy_bw(pr,kk+NATOM);
	Eold+=Bond_Energy_fw(pr,kk_end-NATOM);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
		
	/****** Torsion Start***********/	
	for(kkk=kk+NATOM;kkk<kk_end;kkk++){   
		
		indice=kkk;	
		prot[pr].ammino[indice].x-=prot[pr].ammino[kk].x;  /// Center the branch with respect to the perno
		prot[pr].ammino[indice].y-=prot[pr].ammino[kk].y;
		prot[pr].ammino[indice].z-=prot[pr].ammino[kk].z;	
		
		Rotation(pr,indice,alpha,RotX,RotY,RotZ,flag); /// Rotate with quaternions
		flag=0;
		
		prot[pr].ammino[indice].x+=prot[pr].ammino[kk].x; /// Recenter the branch with respect to the perno
		prot[pr].ammino[indice].y+=prot[pr].ammino[kk].y;
		prot[pr].ammino[indice].z+=prot[pr].ammino[kk].z;
		
		if(prot[pr].ammino[indice].id==ATOM_CA){
			updt_cell_list_SAW (pr,indice); /// Update cell list
			if ((energy_SAW_local(pr,indice,kk+SPG_PATCH2,kk_end)>000.0)){  /// reject
				
				for(lll=kk+NATOM;lll<kk_end;lll++){
					indice=lll;
								
					prot[pr].ammino[indice].x=oldX[indice];
					prot[pr].ammino[indice].y=oldY[indice]; 
					prot[pr].ammino[indice].z=oldZ[indice]; 
					
					updt_cell_list_SAW (pr,indice);
					updt_cell_list (pr,indice);				
				}
				Water_Update_Reject();
				
		#ifdef WATER_UP_TEST
		mossaid+=400;
		Water_Update_Test();
		mossaid-=400;
		#endif
				
		local_rejectedself++;
		#ifdef 	ORDER_TEST_WATER
		mossaid+=330;
		order_test_water(&testEWater);
		#endif
		return(REJECTSELF);
			}			
		}
	updt_cell_list (pr,indice);
	}
	
#ifdef ORDER_TEST
	for(lll=0;lll<Plength[pr];lll++){
			indice=lll;
			if((lll<kk+NATOM)||(lll>=kk_end)){
				if(fabs(prot[pr].ammino[indice].x-oldX[indice])>1e-8){
					fprintf(out,"After rotation check coordinate X should not have changed %lu\n prot[%d].ammino[%d].x= %lf oldX[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].x,indice,oldX[indice]);
					fflush(out);
					test_flag=1;
				}
				if(fabs(prot[pr].ammino[indice].y-oldY[indice])>1e-8){
					fprintf(out,"After rotation check coordinate Y should not have changed %lu\n prot[%d].ammino[%d].y= %lf oldY[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].y,indice,oldY[indice]);
					fflush(out);
					test_flag=1;
				}
				if(fabs(prot[pr].ammino[indice].z-oldZ[indice])>1e-8){
					fprintf(out,"After rotation check coordinate Z should not have changed %lu\n prot[%d].ammino[%d].z= %lf oldZ[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].z,indice,oldZ[indice]);
					fflush(out);
					test_flag=1;
				}
			}
		}
#endif

	/****** Torsion Over***********/
	
	#ifdef 	CLIST_TEST
	test_celllist_SAW(2);
	test_celllist_CA();
	test_celllist_H();
	
	#endif
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(kkk=kk+NATOM;kkk<kk_end;kkk++){
		
		indice=kkk;	
		if(indice>=(unsigned int)(Plength[pr]-1)){
			fprintf(out,"Local Enew indice=%u kk=%d kkk=%d\n",indice,kk,kkk);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		Enew+=energy_SP_brot_local(pr,indice,kk+SPG_PATCH2,kk_end,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;		
	}	
	
	Enew+=Bond_Energy_bw(pr,kk+NATOM);
	Enew+=Bond_Energy_fw(pr,kk_end-NATOM);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
	
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
		
	/***********Update Topology*************/
	
	
	/*last=Plength[pr]-NSPOTS;
	dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
	dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
	dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	rCA2_end=sqrt(dx*dx+ dy*dy + dz*dz);*/
	///if (rCA2_end<KNOT_END_COND*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue])){
		
		
#ifdef ORDER_TEST  
	  for(lll=0;lll<Plength[pr];lll++){
	    indice=lll;
	    if((lll<kk+NATOM)||(lll>=kk_end)){
	      if(fabs(prot[pr].ammino[indice].x-oldX[indice])>1e-8){
		fprintf(out,"Before whatknot check coordinate X should not have changed %lu\n prot[%d].ammino[%d].x= %lf oldX[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].x,indice,oldX[indice]);
		fflush(out);
		test_flag=1;
	      }
	      if(fabs(prot[pr].ammino[indice].y-oldY[indice])>1e-8){
		fprintf(out,"Before whatknot check coordinate Y should not have changed %lu\n prot[%d].ammino[%d].y= %lf oldY[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].y,indice,oldY[indice]);
		fflush(out);
		test_flag=1;
	      }
	      if(fabs(prot[pr].ammino[indice].z-oldZ[indice])>1e-8){
			fprintf(out,"Before whatknot check coordinate Z should not have changed %lu\n prot[%d].ammino[%d].z= %lf oldZ[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].z,indice,oldZ[indice]);
			fflush(out);
			test_flag=1;
	      }
	    }
	  }
	  if(test_flag==1){
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
#endif
		
		
	/*** C&V indice_knot=0;
	  for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
	    if(indice_knot>=Psize){
	      fprintf(out,"whatknot out of boudaries MC_Crankshaft %lu mossaid= %d indice_knot= %d Psize= %ldk= %d Plength[%d]= %d NATOM= %d\n",icycl,mossaid,indice_knot,Psize,k,pr,Plength[pr],NATOM);
	      fflush(out);
	      MPI_Abort(MPI_COMM_WORLD,err5);
	    }
	    prot_knot->bead[indice_knot].x=prot[pr].ammino[k].x;
	    prot_knot->bead[indice_knot].y=prot[pr].ammino[k].y;
	    prot_knot->bead[indice_knot].z=prot[pr].ammino[k].z;
	    indice_knot++;
	  }
	  whatknot(prot_knot, Psize, prot[pr].topology,&topo_New);
	  if(topo_New<0) topo_New=sizeTopo-1; 
		
		
		#ifdef ORDER_TEST
	for(lll=0;lll<Plength[pr];lll++){
			indice=lll;
			if((lll<kk+NATOM)||(lll>=kk_end)){
				if(fabs(prot[pr].ammino[indice].x-oldX[indice])>1e-8){
					fprintf(out,"After whatknot check coordinate X should not have changed %lu\n prot[%d].ammino[%d].x= %lf oldX[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].x,indice,oldX[indice]);
					fflush(out);
					test_flag=1;
				}
				if(fabs(prot[pr].ammino[indice].y-oldY[indice])>1e-8){
					fprintf(out,"After whatknot check coordinate Y should not have changed %lu\n prot[%d].ammino[%d].y= %lf oldY[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].y,indice,oldY[indice]);
					fflush(out);
					test_flag=1;
				}
				if(fabs(prot[pr].ammino[indice].z-oldZ[indice])>1e-8){
					fprintf(out,"After whatknot check coordinate Z should not have changed %lu\n prot[%d].ammino[%d].z= %lf oldZ[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].z,indice,oldZ[indice]);
					fflush(out);
					test_flag=1;
				}
			}
		}
		if(test_flag==1){
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
#endif
		
	}else{
	topo_New=0;
	}	***/
	
	///Linking_Number(prot,pr,ATOM_CA,kk,kk_end,Plength[pr],NATOM,info_ALN,&aln_partial);
	///aln_new+=aln_partial;
	
		
	
	#ifdef 	ORDER_TEST_WATER
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf min=%d max=%d\n",mossaid,icycl,Ewater,testEWater,kk,kk+kk_end+4);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;					
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;

	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;

	#ifdef 	ORDER_TEST
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	
	if(Cnew!=testC){
		fprintf(out,"CC a local %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b local %lu %15.10lf %15.10lf min=%d max=%d\n",icycl,Onew,testO,kk,kk_end);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	
	if(fabs(Enew-testE)>1e-8){
	  fprintf(out,"CC d local %lu %15.10lf %15.10lf min=%d max=%d\n",icycl,Enew,testE,kk,kk_end);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	/*strcpy(info_ALN,"initial");
	Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	if(fabs(aln_new-aln_partial)>1e-8){
	  fprintf(out,"CC aln local %lu pr=%d %15.10lf %15.10lf min=%d max=%d\n",icycl,pr,aln_new,aln_partial,kk,kk_end);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
	
	
	#endif			
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"MC_brot local non ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}

	if((Onew<0)){		
		fprintf(out,"MC_brot local non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST*************/
	
	
	
	End_New=sist->End-prot[pr].end+rCA2_end;
	///Topo_indice_New=sist->Topo_indice-prot[pr].topo_indice+topo_New;	
	ALN_New=sist->ALN_tot-prot[pr].ALN+aln_new;
	
	OEnd_New=lround(OEND_bin*End_New/DNPROT);
	OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	
	///OTopo_indice_New=lround(Topo_indice_New/DNPROT);
	///OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);		
	OALN_New=lround(fabs(OALN_bin*ALN_New/DNPROT));
	OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));
	
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot local New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_brot local New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d] Wpot_ALN[%d][%d][%d]-Wpot_ALN[%d][%d][%d]\n",
			mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds,betaindice,OEnd_New,OALN_New,betaindice,OEnd_Old,OALN_Old);
			fflush(out);
		}	
		
	#endif
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	///DW+=Wpot_Topo[betaindice][OEnd_New][OTopo_indice_New]-Wpot[betaindice][OEnd_Old][OTopo_indice_Old];	
	//DW+=Wpot_ALN[betaindice][OEnd_New][OALN_New]-Wpot[betaindice][OEnd_Old][OALN_Old];
	
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		fsamp();
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	#ifdef 	ORDER_TEST_WATER
		mossaid+=360;
			order_test_water(&testEWater);
		mossaid=7;
#endif
	
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		test_flag=0;
		for(lll=0;lll<Plength[pr];lll++){  
			indice=lll;
			if((lll<kk+NATOM)||(lll>=kk_end)){
				if(fabs(prot[pr].ammino[indice].x-oldX[indice])>1e-8){  /// QUESTO SERVE???
					fprintf(out,"Update reject coordinate X should not have changed %lu\n prot[%d].ammino[%d].x= %lf oldX[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].x,indice,oldX[indice]);
					fflush(out);
					test_flag=1;
				}
				if(fabs(prot[pr].ammino[indice].y-oldY[indice])>1e-8){
					fprintf(out,"Update reject coordinate Y should not have changed %lu\n prot[%d].ammino[%d].y= %lf oldY[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].y,indice,oldY[indice]);
					fflush(out);
					test_flag=1;
				}
				if(fabs(prot[pr].ammino[indice].z-oldZ[indice])>1e-8){
					fprintf(out,"Update reject coordinate Z should not have changed %lu\n prot[%d].ammino[%d].z= %lf oldZ[%d]= %lf  \n",icycl,pr,indice,prot[pr].ammino[indice].z,indice,oldZ[indice]);
					fflush(out);
					test_flag=1;
				}
			}
			prot[pr].ammino[indice].x=oldX[indice];
			prot[pr].ammino[indice].y=oldY[indice]; 
			prot[pr].ammino[indice].z=oldZ[indice]; 
			
			updt_cell_list_SAW (pr,indice);
			updt_cell_list (pr,indice);		
		}
		
		if(test_flag==1){ 
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=570;
		Water_Update_Test();
		mossaid-=570;
		#endif
		local_rejectedener++;
		
		
		#ifdef 	ORDER_TEST_WATER
		mossaid+=340;
			order_test_water(&testEWater);
#endif
		///strcpy(prot[pr].topology,prot[pr].Otopology);
		return(0);   
	}
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1000;
		Water_Update_Test();
		mossaid-=1000;
		#endif
	
	local_accepted++;
	sist->E=Enew;
	
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	sist->End=End_New;
	///sist->Topo_indice=Topo_indice_New;
	sist->ALN_tot=ALN_New;
	///prot[pr].topo_indice=topo_New;
	prot[pr].ALN=aln_new;
	prot[pr].end=rCA2_end;
	///strcpy(prot[pr].Otopology,prot[pr].topology);
	
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	#ifdef 	ORDER_TEST_WATER
		mossaid+=350;
		order_test_water(&testEWater);
	#endif
	return(0);

	
}

int Cluster_Builder (int **clust_indx, int *clust_size){
	
	int clust_ii,clust_jj;
	int clust_i,clust_j,clust_k,clust_m;
	int clust_lm,clust_ln,clust_kn;
	int clust_ip,clust_jp,clust_kp;
	int clust_iflag,clust_jflag;
	int clust_p=0; // cluster size
	int clust_nclust=0; // number of clusters
	int clust_halfN; // we take as reference to check the chain-chain distance the middle particle
	int **clust_nlist=NULL; //neighbour list
	int *clust_nneig=NULL; // number of neighbours
	int *clust_dum=NULL;
	int *clust_flag=NULL; // Flag =1 if a chain is already into a cluster
	double clust_r_cutoff; // cluster analysis cut off
	int cluster_test_flag_initial,cluster_test_flag_neighbour;
	double dx,dy,dz,r2;
	
	
	
	clust_r_cutoff=2*0.44*(sist->Rmin_CAsaw*1.1+1.0)*(pow(ProtN,0.588)); //the cutoff is the approximate radius of gyration of the chain multiplied by 2 for overalp check
	clust_r_cutoff=clust_r_cutoff*clust_r_cutoff; // we take the sqaure because we compare with the square of the distances 
	
	//Allocation
	clust_nlist=(int **) calloc(NPROT, sizeof(int *));
	clust_nneig=(int *) calloc(NPROT, sizeof(int));
	clust_flag=(int *) calloc(NPROT, sizeof(int));
	clust_dum=(int *) calloc(clust_maxsize, sizeof(int));
	
	
	for(clust_ii=0;clust_ii<NPROT;clust_ii++){    
		clust_nlist[clust_ii]=(int *) calloc(clust_MAXNEIGH, sizeof(int));
		clust_nneig[clust_ii]=0;
		clust_flag[clust_ii]=0;
	}
	for(clust_ii=0;clust_ii<clust_maxsize;clust_ii++){   
		clust_dum[clust_ii]=0;
	}
	#ifdef PROGRESS
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder A %lu\n",icycl);fflush(out);}
	#endif			
	
	//  build the neighbours list of the centers of the chains
	for(clust_ii=0;clust_ii<NPROT;clust_ii++){         
		for(clust_jj=clust_ii+1;clust_jj<NPROT;clust_jj++){  
			clust_halfN=lround(Plength[clust_ii]/2.);
			
			dx=prot[clust_ii].ammino[clust_halfN].x-prot[clust_jj].ammino[clust_halfN].x;
			dy=prot[clust_ii].ammino[clust_halfN].y-prot[clust_jj].ammino[clust_halfN].y;
			dz=prot[clust_ii].ammino[clust_halfN].z-prot[clust_jj].ammino[clust_halfN].z;
			
			r2=dx*dx+dy*dy+dz*dz;
			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
			
			if(r2<clust_r_cutoff) {
				
				clust_nlist[clust_ii][clust_nneig[clust_ii]]=clust_jj;
				clust_nlist[clust_jj][clust_nneig[clust_jj]]=clust_ii;
				clust_nneig[clust_ii]++;
				clust_nneig[clust_jj]++;
				if((clust_nneig[clust_ii]>clust_MAXNEIGH)||( clust_nneig[clust_jj]> clust_MAXNEIGH)) {
					fprintf(out,"Too many neighs nneig[%d]=%d nneig[%d]=%d  clust_MAXNEIGH=%d\n",clust_ii,clust_nneig[clust_ii],clust_jj,clust_nneig[clust_jj],clust_MAXNEIGH);
					fflush(out);
					exit(0);
				}
			}
		}
	} 
	#ifdef PROGRESS
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder B %lu\n",icycl);fflush(out);}
	#endif		
	// main cluster analysis loop
	for(clust_i=0;clust_i<NPROT;clust_i++){                  
		
		#ifdef PROGRESS
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder BA %lu %d\n",icycl,clust_i);fflush(out);}
		#endif
		
		
		if(clust_flag[clust_i] == 0) {      // if i has not been already assigned so we start a new cluster
			clust_p=0;
			clust_flag[clust_i]=1;
			
			
			
			
			clust_indx[clust_nclust][clust_p]=clust_i;
			clust_p++;  
			if (clust_p>clust_maxsize) {
				fprintf(out,"clust_p>clust_maxsize clust_p=%d clust_maxsize=%d\n",clust_p,clust_maxsize);
				fflush(out);
				exit(0);
			}
			#ifdef PROGRESS
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder BB %lu %d\n",icycl,clust_i);fflush(out);}
			#endif
			if (clust_nneig[clust_i] == 0) {   // single particle cluster
				
				
			}else{
				if (NPROT==1) {
					fprintf(out,"icycl=%lu mossaid=%d Impossible to have a non-single particle cluster with NPROT=1\n",icycl,mossaid);
					fflush(out);
					exit(0);
				}
				#ifdef PROGRESS
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder BC %lu %d\n",icycl,clust_i);fflush(out);}
				#endif
				
				for(clust_ln=0;clust_ln<clust_nneig[clust_i];clust_ln++){         // build a table with i, its neighbors and 
					clust_j=clust_nlist[clust_i][clust_ln];           // the second generation neighbors
					clust_flag[clust_j]=1;
					
					clust_indx[clust_nclust][clust_p]=clust_j;
					clust_p++;                  // add j to the cluster
					if (clust_p>clust_maxsize){
						fprintf(out,"clust_p>clust_maxsize B clust_p=%d mclust_axsize=%d\n",clust_p,clust_maxsize);
						fflush(out);
						exit(0);
					}
					for(clust_kn=0;clust_kn<clust_nneig[clust_j];clust_kn++){  // the third generation neighbors
						clust_k=clust_nlist[clust_j][clust_kn];
						if(clust_flag[clust_k] == 0){
							clust_flag[clust_k]=1;
							
							clust_indx[clust_nclust][clust_p]=clust_k;
							clust_p++;
							if (clust_p>clust_maxsize){
								fprintf(out,"clust_p>clust_maxsize C clust_p=%d clust_maxsize=%d\n",clust_p,clust_maxsize);
								fflush(out);
								exit(0);
							}
						}
					}
				}
				clust_iflag=1;
				#ifdef PROGRESS
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder BD %lu %d\n",icycl,clust_i);fflush(out);}
				#endif
				//fprintf(out,"Cluster %d Done \n Now Sorting\n",nclust);fflush(out);
				
				while (clust_iflag != 0) {
					clust_iflag=0;
					
					
					//Remove double counting
					clust_ln=0;
					clust_dum[clust_ln]=clust_indx[clust_nclust][0]; 
					for(clust_k=1;clust_k<clust_p;clust_k++){
						clust_jflag=0;
						for(clust_j=0;clust_j<clust_k;clust_j++){
							if(clust_indx[clust_nclust][clust_k] == clust_indx[clust_nclust][clust_j]) clust_jflag++;
						}
						if(clust_jflag == 0) {
							clust_ln++;  
							clust_dum[clust_ln]=clust_indx[clust_nclust][clust_k];
							
						}
					}
					//*********************
					
					clust_ln++;
					//Copy back cleaned list
					for(clust_k=0;clust_k<clust_ln;clust_k++){
						clust_indx[clust_nclust][clust_k]=clust_dum[clust_k];
					}
					//Emptying the rest of the vector clust_indx
					for(clust_k=clust_ln;clust_k<clust_p;clust_k++){
						clust_indx[clust_nclust][clust_k]=-1;
					}
					//Copy back cluster size
					clust_p=clust_ln;
					
					// add missing particles
					clust_m=clust_p;
					for(clust_lm=0;clust_lm<clust_m;clust_lm++){ 
						clust_ip=clust_indx[clust_nclust][clust_lm];
						for(clust_ln=1;clust_ln<clust_nneig[clust_ip];clust_ln++){
							clust_jp=clust_nlist[clust_ip][clust_ln];
							if (clust_flag[clust_jp] == 0) {
								clust_flag[clust_jp]=1;
								
								clust_indx[clust_nclust][clust_p]=clust_jp;
								clust_p++;
								if (clust_p>clust_maxsize){
									fprintf(out,"clust_p>clust_maxsize C clust_p=%d clust_maxsize=%d\n",clust_p,clust_maxsize);
									fflush(out);
									exit(0);
								}
								clust_iflag++;
							}
						}
					}
				}
				#ifdef PROGRESS
				if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder BE %lu %d\n",icycl,clust_i);fflush(out);}
				#endif
				
				
			}
			
			
			clust_size[clust_nclust]=clust_p; //record cluster size
			clust_nclust++;       // start a new cluster
			if(clust_nclust>NPROT){
				fprintf(out,"clust_nclust>NPROT clust_nclust=%d NPROT=%d\n",clust_nclust,NPROT);
				fflush(out);
				exit(0);
			}
			#ifdef PROGRESS
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder BF %lu %d\n",icycl,clust_i);fflush(out);}
			#endif
			
			
		}
	}
	#ifdef PROGRESS
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder C %lu clust_nclust= %d\n",icycl,clust_nclust);fflush(out);}
	#endif	
	
	//Record Center of mass of each cluster
	for(clust_i=0;clust_i<clust_nclust;clust_i++){  //loop over all clusters
		
		#ifdef PROGRESS
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"Cluster_Builder Ca %lu clust_size[%d]= %d\n",icycl,clust_i,clust_size[clust_i]);fflush(out);}
		#endif	
		
		for(clust_lm=0;clust_lm<clust_size[clust_i];clust_lm++){  // loop over all chain in the cluster
			clust_ip=clust_indx[clust_i][clust_lm];
			clust_halfN=lround(Plength[clust_ip]/2.);
		#ifdef PROGRESS
			if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"Cluster_Builder Caa %lu clust_indx[%d][%d]= %d  Plength[%d]/2.=%lf\n",icycl,clust_i,clust_lm,clust_indx[clust_i][clust_lm],clust_ip,Plength[clust_ip]/2.);fflush(out);
		fprintf(out,"sist->clust_CM_x[%d]= %lf prot[%d].ammino[%d].x= %lf\n",clust_i,sist->clust_CM_x[clust_i],clust_ip,clust_halfN,prot[clust_ip].ammino[clust_halfN].x);fflush(out);
		fprintf(out,"sist->clust_CM_y[%d]= %lf prot[%d].ammino[%d].y= %lf\n",clust_i,sist->clust_CM_y[clust_i],clust_ip,clust_halfN,prot[clust_ip].ammino[clust_halfN].y);fflush(out);
		fprintf(out,"sist->clust_CM_z[%d]= %lf prot[%d].ammino[%d].z= %lf\n",clust_i,sist->clust_CM_z[clust_i],clust_ip,clust_halfN,prot[clust_ip].ammino[clust_halfN].z);fflush(out);
	}
		#endif	
			
			sist->clust_CM_x[clust_i]=prot[clust_ip].ammino[clust_halfN].x;
			sist->clust_CM_y[clust_i]=prot[clust_ip].ammino[clust_halfN].y;
			sist->clust_CM_z[clust_i]=prot[clust_ip].ammino[clust_halfN].z;
		}
	}
	#ifdef TESTCLUST
	
	//Check if a chain in a cluster does belog there
	for(clust_i=0;clust_i<clust_nclust;clust_i++){  //loop over all clusters
		for(clust_lm=0;clust_lm<clust_size[clust_i];clust_lm++){  // loop over all chain in the cluster
			clust_ip=clust_indx[clust_i][clust_lm]; // chain in the cluster
			
			for(clust_jj=1;clust_jj<clust_nneig[clust_ip];clust_jj++){ // loop over all neighbours of the chain
				
				clust_jp=clust_nlist[clust_ip][clust_jj]; // neighbouring chain
				
				cluster_test_flag_neighbour=0;
				cluster_test_flag_initial=0;
				for(clust_ii=0;clust_ii<clust_nclust;clust_ii++){  //loop over all clusters to search where the neighbouring chain is and if is in mmore than one cluster
					for(clust_ln=0;clust_ln<clust_size[clust_ii];clust_ln++){  // loop over all chain in the cluster
						
						clust_kp=clust_indx[clust_ii][clust_ln]; // chain in the cluster
						
						if(clust_ip==clust_kp){ // check if the intial chain is in the correct cluster and only there
							
							if(clust_ii!=clust_i){ //check if the cluster is the correct one
								fprintf(out,"Cluster Test A Failed: intial chain %d in the wrong cluster %d instead of the cluster %d/%d\n",clust_ip,clust_ii,clust_i,clust_nclust);
								for(clust_ii=0;clust_ii<clust_nclust;clust_ii++){  
									for(clust_ln=0;clust_ln<clust_size[clust_ii];clust_ln++){
										fprintf(out,"clust_indx[%d][%d]=%d\n",clust_ii,clust_ln,clust_indx[clust_ii][clust_ln]);
									}
								}
								fflush(out);
								exit(0);
							}
							cluster_test_flag_initial++;
						}
						
						
						if(clust_jp==clust_kp){ // check if the nighbouring chain is in the correct cluster and only there
							
							if(clust_ii!=clust_i){ //check if the cluster is the correct one
								fprintf(out,"Cluster Test B Failed: nighbouring chain %d in the wrong cluster %d instead of the cluster %d/%d\n",clust_jp,clust_ii,clust_i,clust_nclust);
								fflush(out);
								exit(0);
							}
							cluster_test_flag_neighbour++;
						}
					}
				}
				
				if(cluster_test_flag_neighbour!=1){
					fprintf(out,"Cluster Test C Failed: neighbour chain %d is in more than one cluster and is in the clusters:\n",clust_jp);
					
					for(clust_ii=0;clust_ii<clust_nclust;clust_ii++){  //loop over all clusters to search where the neighbouring chain is and if is in mmore than one cluster
						for(clust_ln=0;clust_ln<clust_size[clust_ii];clust_ln++){  // loop over all chain in the cluster
							clust_kp=clust_indx[clust_ii][clust_ln]; // chain in the cluster
							
							
							if(clust_jp==clust_kp){ // check if the nighbouring chain is in the correct cluster and only there
								
								fprintf(out,"%d/%d\n",clust_jp,clust_nclust);
							}
						}
					}
					
					fflush(out);
					exit(0);
				}
				if(cluster_test_flag_initial!=1){
					fprintf(out,"Cluster Test D Failed: intial chain %d iis in more than one cluster and is in the clusters:\n",clust_ip);
					for(clust_ii=0;clust_ii<clust_nclust;clust_ii++){  //loop over all clusters to search where the neighbouring chain is and if is in mmore than one cluster
						for(clust_ln=0;clust_ln<clust_size[clust_ii];clust_ln++){  // loop over all chain in the cluster
							clust_kp=clust_indx[clust_ii][clust_ln]; // chain in the cluster
							
							
							if(clust_ip==clust_kp){ // check if the nighbouring chain is in the correct cluster and only there
								
								fprintf(out,"%d/%d\n",clust_ip,clust_nclust);
							}
						}
					}
					fflush(out);
					exit(0);
				}
				
				
			}
		}
	}
	
	#endif
	for(clust_ii=0;clust_ii<NPROT;clust_ii++){    
		free(clust_nlist[clust_ii]);
	}
	free(clust_nlist);
	free(clust_nneig);
	free(clust_flag);
	free(clust_dum);
	
	//retunrs the number of clusters
	return (clust_nclust);
}

int MC_Cluster_trasl (void ){
	double dx=0,dy=0,dz=0;
	int clust_kk=0,clust_i=0;
	int pr=0;
	
	//static int **vicini=NULL,*nvicini=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX=0,TY=0,TZ=0,dR=0;
	int i=0,k=0,MC_Cluster_trasl_test=0,j=0,ii=0,indice=0,jj=0;
	int l=0,lll=0;
	int kkk,flag=1;
	int selection=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int ORDER_TEST_WATER_FLAG=0;
	int OEnd_New=0,OEnd_Old=0;
	///OTopo_indice_New=0,OTopo_indice_Old=0;
	int OALN_New=0,OALN_Old=0;
	FILE *fp=NULL;
	
	
	
	
	
	sist->Cluster_N = Cluster_Builder(sist->Cluster_Index,sist->Cluster_Size);
	
	clust_kk=(int)(ran3(&seed)*(sist->Cluster_N));
	
	for(clust_i=0;clust_i<sist->Cluster_Size[clust_kk];clust_i++){ 
		pr=sist->Cluster_Index[clust_kk][clust_i];
		if(pr<0) {
			fprintf(out,"Something wrong in MC_Cluster_trasl at icycl=%lu pr=%d for clust_kk=%d clust_i=%d\n",icycl,pr,clust_kk,clust_i);
			fflush(out);
			exit(0);
		}
	}
	
	
	
	//fprintf(out,"MC_Cluster_trasl A OK %lu\n",icycl);fflush(out);
	
	
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	dR=ran3(&seed)*20.0;
	
	TX=sin(phi)*cos(theta)*dR;
	TY=sin(phi)*sin(theta)*dR;
	TZ=cos(phi)*dR;
	
	
	for(clust_i=0;clust_i<sist->Cluster_Size[clust_kk];clust_i++){ 
		pr=sist->Cluster_Index[clust_kk][clust_i];
		if(pr<0) {
			fprintf(out,"Something wrong in MC_Cluster_trasl at icycl=%lu pr=%d for clust_kk=%d clust_i=%d\n",icycl,pr,clust_kk,clust_i);
			fflush(out);
			exit(0);
		}
		for(k=0;k<Plength[pr];k++){ 
			
			Eold+=energy_SP_Pivot_fw(pr,k,OLDWATER);
			Oold+=sist->norder;
			Cold+=sist->ncontact;
			
		}
		#ifdef 	ORDER_TEST_WATER
			
				for (i=ATOM_CA;i<Plength[pr];i+=NATOM){


					if(fabs(prot[pr].ammino[i].new_water_contacts)>1e-8){
						fprintf(out,"MC_Cluster_trasl Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf prot[%d].ammino[%d].old_water_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,pr,i,prot[pr].ammino[i].old_water_contacts);fflush(out);
						
						ORDER_TEST_WATER_FLAG=1;

					}	
				}
			if(ORDER_TEST_WATER_FLAG==1){
				fsamp();
						fp=fopen("final-err.bin","w");
						writeBinary(fp);
						test_celllist_SAW(1);
						test_celllist_CA();
						test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
		#endif
		
	}
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	//fprintf(out,"MC_Cluster_trasl %lu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	//fprintf(out,"MC_Cluster_trasl A OK %lu\n",icycl);fflush(out);
	for(clust_i=0;clust_i<sist->Cluster_Size[clust_kk];clust_i++){ 
		pr=sist->Cluster_Index[clust_kk][clust_i];
		if(pr<0) {
			fprintf(out,"Something wrong in MC_Cluster_trasl at icycl=%lu pr=%d for clust_kk=%d clust_i=%d\n",icycl,pr,clust_kk,clust_i);
			fflush(out);
			exit(0);
		}
		for(k=0;k<Plength[pr];k++){ 
			
			old_prot[pr].ammino[k].x=prot[pr].ammino[k].x;
			old_prot[pr].ammino[k].y=prot[pr].ammino[k].y;
			old_prot[pr].ammino[k].z=prot[pr].ammino[k].z;
			
		}
		Dold=0;
		if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;	
	}
	
	//fprintf(out,"MC_Cluster_trasl B OK %lu\n",icycl);fflush(out);
	for(clust_i=0;clust_i<sist->Cluster_Size[clust_kk];clust_i++){ 
		pr=sist->Cluster_Index[clust_kk][clust_i];
		if(pr<0) {
			fprintf(out,"Something wrong in MC_Cluster_trasl at icycl=%lu pr=%d for clust_kk=%d clust_i=%d\n",icycl,pr,clust_kk,clust_i);
			fflush(out);
			exit(0);
		}
		for(k=0;k<Plength[pr];k++){ 
			
			
			
			//if(icycl>0){fprintf(out,"Rotation %lu %d %d\n",icycl,kk,indice);fflush(out);}
			prot[pr].ammino[k].x+=TX;
			prot[pr].ammino[k].y+=TY;
			prot[pr].ammino[k].z+=TZ;
			
			
			
			
			
			
			if(prot[pr].ammino[k].id==ATOM_CA){
				//if(icycl>0){fprintf(out,"Update cell SAW %lu %d %d\n",icycl,kk,indice);fflush(out);}
				updt_cell_list_SAW (pr,k);
				if ((energy_SAW_All_rot_trasl(pr,k)>000.0)){  // reject
					for(l=0;l<=k;l++){ 
						
						
						prot[pr].ammino[l].x=old_prot[pr].ammino[l].x;
						prot[pr].ammino[l].y=old_prot[pr].ammino[l].y; 
						prot[pr].ammino[l].z=old_prot[pr].ammino[l].z; 
						updt_cell_list_SAW (pr,l);
						
						updt_cell_list (pr,l);
						
						
						
						
					}
					//if(icycl>0){fprintf(out,"Test cell before rejection SAW\n");fflush(out);}
					//test_celllist_SAW(4);
					//if(icycl>0){fprintf(out,"Test cell before rejection CA\n");fflush(out);}
					//test_celllist_CA();
					//if(icycl>0){fprintf(out,"Test cell before rejection H\n");fflush(out);}
					//test_celllist_H();
					//if(icycl>0){fprintf(out,"Test cell before rejection O\n");fflush(out);}
					//
					//if(icycl>0){fprintf(out,"Test cell before rejection OK\n");fflush(out);}
					Water_Update_Reject();
					#ifdef WATER_UP_TEST
		mossaid+=410;
		Water_Update_Test();
		mossaid-=410;
		#endif
					cluster_trasl_rejectedself++;
					#ifdef 	CLIST_TEST			
					test_celllist_SAW(5);
					test_celllist_CA();
					test_celllist_H();
					
					#endif	
					return(REJECTSELF);
				}
			}
			//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
			updt_cell_list (pr,k);
			//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
			//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
			
			
			
			
			
		}
	}
	//fprintf(out,"MC_Cluster_trasl C OK %lu\n",icycl);fflush(out);
	
	#ifdef 	CLIST_TEST			
	test_celllist_SAW(6);
	test_celllist_CA();
	test_celllist_H();
	
	#endif			
	/*if ((energy_SAW_test ()>0.0)){
		fprintf(out,"SAW  fw %lu %d %d\n",icycl);
		
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	#ifdef DIPOLE_SFC
	if(E_Dip>0){		
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(clust_i=0;clust_i<sist->Cluster_Size[clust_kk];clust_i++){ 
		pr=sist->Cluster_Index[clust_kk][clust_i];
		if(pr<0) {
			fprintf(out,"Something wrong in MC_Cluster_trasl at icycl=%lu pr=%d for clust_kk=%d clust_i=%d\n",icycl,pr,clust_kk,clust_i);
			fflush(out);
			exit(0);
		}
		for(k=0;k<Plength[pr];k++){ 
			
			Enew+=energy_SP_Pivot_fw(pr,k,NEWWATER);
			Onew+=sist->norder;
			Cnew+=sist->ncontact;
			
			
			
		}
		Dnew=Dold;
		if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	}
	//	fprintf(out,"MC_brot_f %lu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
	//fprintf(out,"MC_Cluster_trasl C OK %lu\n",icycl);fflush(out);
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
	
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	
	#ifdef 	ORDER_TEST_WATER
	
	
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(7);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;	
	
	
	
	//fprintf(out,"ok1\n");
	
	
	
	//fprintf(out,"ok2\n");
	
	
	
	
	
	
	if(Cnew>0) Znew=1;
	
	
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %lu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %lu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//fprintf(out,"MC_Cluster_trasl D OK %lu\n",icycl);fflush(out);
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"CC a trasl  %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b trasl  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"CC d trasl  %lu %lf %lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/*strcpy(info_ALN,"initial");
	Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	if(fabs(aln_new-aln_partial)>1e-8){
	  fprintf(out,"CC aln trasl %lu pr=%d %15.10lf %15.10lf min=%d max=%d\n",icycl,pr,aln_new,aln_partial,kk,kk_end);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
	#endif	
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %lu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP_Pivot_fw(pr,k);
		
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %lu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/
	//fprintf(out,"MC_Cluster_trasl D OK %lu\n",icycl);fflush(out);
	/*if((Onew<IminO)){		
		fprintf(out,"MC_brot fw non ok icycl=%lu, order=%d %d %d %d %d\n",icycl,Onew,sist->order,Oold,minO,maxO);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	if((Cnew<0)){		
	  fprintf(out,"Cluster_traslnon ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
	  fflush(out);
	  MPI_Abort(MPI_COMM_WORLD,err5);
	}

	if((Onew<0)){		
		fprintf(out,"Cluster_trasl non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST*************/
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	//if((New_Mean_H_Bonds<=minH)) New_Mean_H_Bonds=0;
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot trasl New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_brot trasl New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);
				fflush(out);
				}	
		
	#endif
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
	OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));	
			
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
		
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		for(clust_i=0;clust_i<sist->Cluster_Size[clust_kk];clust_i++){ 
			pr=sist->Cluster_Index[clust_kk][clust_i];
			if(pr<0) {
				fprintf(out,"Something wrong in MC_Cluster_trasl at icycl=%lu pr=%d for clust_kk=%d clust_i=%d\n",icycl,pr,clust_kk,clust_i);
				fflush(out);
				exit(0);
			}
			for(l=0;l<Plength[pr];l++){ 							
				prot[pr].ammino[l].x=old_prot[pr].ammino[l].x;
				prot[pr].ammino[l].y=old_prot[pr].ammino[l].y; 
				prot[pr].ammino[l].z=old_prot[pr].ammino[l].z; 
				//fprintf(out,"MC_Cluster_trasl reject %lu l=%d\n",icycl,l);fflush(out);
				updt_cell_list_SAW (pr,l);
				updt_cell_list (pr,l);
			}
		}
		
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=420;
		Water_Update_Test();
		mossaid-=420;
		#endif
		cluster_trasl_rejectedener++;
		return(0);   
	}
	
	
	
	
	
	
	
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1100;
		Water_Update_Test();
		mossaid-=1100;
		#endif
	//fprintf(out,"MC_Cluster_trasl G OK %lu\n",icycl);fflush(out);
	cluster_trasl_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %lu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	//fprintf(out,"MC_Cluster_trasl H OK %lu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}

int MC_All_trasl (int pr){
	double dx,dy,dz;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	
	//static int **vicini=NULL,*nvicini=NULL;
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX,TY,TZ,dR;
	int i,k,MC_All_trasl_test=0,j,ii,indice,jj;
	int l,lll;
	int kkk,flag=1;
	int selection;
	double testO,testOP;
	int testC;
	double testE;
	int New_Mean_CA_Bonds,New_Mean_H_Bonds;
	double Ewater=0,testEWater=0;
	int ORDER_TEST_WATER_FLAG=0;
	int OEnd_New,OEnd_Old;
	int OALN_New=0,OALN_Old=0;
	///OTopo_indice_New,OTopo_indice_Old;
	FILE *fp=NULL;
	MC_All_trasl_test=0;
	
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	dR=ran3(&seed)*20.0;
	
	TX=sin(phi)*cos(theta)*dR;
	TY=sin(phi)*sin(theta)*dR;
	TZ=cos(phi)*dR;
		
	for(k=0;k<Plength[pr];k++){ 
		
		Eold+=energy_SP_Pivot_fw(pr,k,OLDWATER);
		Oold+=sist->norder;
		Cold+=sist->ncontact;
		
	}
	#ifdef 	ORDER_TEST_WATER
			
				for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
					if(fabs(prot[pr].ammino[i].new_water_contacts)>1e-8){
						fprintf(out,"MC_All_trasl Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf prot[%d].ammino[%d].old_water_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,pr,i,prot[pr].ammino[i].old_water_contacts);fflush(out);
						fsamp();
						fp=fopen("final-err.bin","w");
						writeBinary(fp);
						test_celllist_SAW(1);
						test_celllist_CA();
						test_celllist_H();
						ORDER_TEST_WATER_FLAG=1;

					}	
				}
				if(ORDER_TEST_WATER_FLAG==1){
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
			
		#endif
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	//fprintf(out,"MC_All_trasl %lu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	//fprintf(out,"MC_All_trasl A OK %lu\n",icycl);fflush(out);
	for(k=0;k<Plength[pr];k++){ 
		
		oldX[k]=prot[pr].ammino[k].x;
		oldY[k]=prot[pr].ammino[k].y;
		oldZ[k]=prot[pr].ammino[k].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"MC_All_trasl B OK %lu\n",icycl);fflush(out);
	for(k=0;k<Plength[pr];k++){ 
		prot[pr].ammino[k].x+=TX;
		prot[pr].ammino[k].y+=TY;
		prot[pr].ammino[k].z+=TZ;
		
		
		
		if(prot[pr].ammino[k].id==ATOM_CA){
			//if(icycl>0){fprintf(out,"Update cell SAW %lu %d %d\n",icycl,kk,indice);fflush(out);}
			updt_cell_list_SAW (pr,k);
			if ((energy_SAW_All_rot_trasl(pr,k)>000.0)){  // reject
				for(l=0;l<=k;l++){ 
					
					
					prot[pr].ammino[l].x=oldX[l];
					prot[pr].ammino[l].y=oldY[l]; 
					prot[pr].ammino[l].z=oldZ[l]; 
					updt_cell_list_SAW (pr,l);
					
					updt_cell_list (pr,l);
					
					
					
					
				}
				Water_Update_Reject();
				#ifdef WATER_UP_TEST
		mossaid+=430;
		Water_Update_Test();
		mossaid-=430;
		#endif
				all_trasl_rejectedself++;
				#ifdef 	CLIST_TEST			
				test_celllist_SAW(9);
				test_celllist_CA();
				test_celllist_H();
				
				#endif	
				return(REJECTSELF);
			}
		}
		//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
		updt_cell_list (pr,k);
		
	}
	//fprintf(out,"MC_All_trasl C OK %lu\n",icycl);fflush(out);
	
	#ifdef 	CLIST_TEST			
	test_celllist_SAW(10);
	test_celllist_CA();
	test_celllist_H();
	
	#endif			
	/*if ((energy_SAW_test ()>0.0)){
		fprintf(out,"SAW  fw %lu %d %d\n",icycl);
		
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	#ifdef DIPOLE_SFC
	if(E_Dip>0){		
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(k=0;k<Plength[pr];k++){ 
		
		Enew+=energy_SP_Pivot_fw(pr,k,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;
		
	}
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
	
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	
	#ifdef 	ORDER_TEST_WATER
	
	
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(11);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//fprintf(out,"MC_All_trasl D OK %lu\n",icycl);fflush(out);
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"CC a trasl  %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b trasl  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"CC d trasl  %lu %lf %lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	#endif	
	
	
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"all_trasl non ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"all_trasl non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********END TEST*************/
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	//if((New_Mean_H_Bonds<=minH)) New_Mean_H_Bonds=0;
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot trasl New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_brot trasl New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);
				fflush(out);
				}	
		
	#endif
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
	OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));	
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		for(l=0;l<Plength[pr];l++){ 							
			prot[pr].ammino[l].x=oldX[l];
			prot[pr].ammino[l].y=oldY[l]; 
			prot[pr].ammino[l].z=oldZ[l]; 
			updt_cell_list_SAW (pr,l);
			updt_cell_list (pr,l);
		}
		
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=440;
		Water_Update_Test();
		mossaid-=440;
		#endif
		all_trasl_rejectedener++;
		return(0);   
	}
	
	
	
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1200;
		Water_Update_Test();
		mossaid-=1200;
		#endif
	//fprintf(out,"MC_All_trasl G OK %lu\n",icycl);fflush(out);
	all_trasl_accepted++;
	sist->E=Enew;
	
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	//fprintf(out,"MC_All_trasl H OK %lu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}

int MC_All_rot (int pr){
	double dx=0,dy=0,dz=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	
	//static int **vicini=NULL,*nvicini=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX,RotY,RotZ;
	double CM_x=0.0,CM_y=0.0,CM_z=0.0;
	int i=0,k=0,MC_All_rot_test=0,j=0,ii=0,indice=0,jj=0;
	int l=0,lll=0;
	int kkk=0,flag=1;
	int ORDER_TEST_WATER_FLAG=0;
	int selection=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	///OTopo_indice_New=0,OTopo_indice_Old=0;
	int OALN_New=0,OALN_Old=0;
	FILE *fp=NULL;
	
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	alpha=(PI*ran3(&seed)/10.0)-PI/20.0;
	
	
	
	for(k=0;k<Plength[pr];k++){ 
		
		Eold+=energy_SP_Pivot_fw(pr,k,OLDWATER);
		Oold+=sist->norder;
		Cold+=sist->ncontact;
		
	}
	#ifdef 	ORDER_TEST_WATER
	
		for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
			
			if(fabs(prot[pr].ammino[i].new_water_contacts)>1e-8){
				fprintf(out,"MC_All_rot Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf prot[%d].ammino[%d].old_water_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,pr,i,prot[pr].ammino[i].old_water_contacts);fflush(out);
				fsamp();
				fp=fopen("final-err.bin","w");
				writeBinary(fp);
				test_celllist_SAW(1);
				test_celllist_CA();
				test_celllist_H();
				ORDER_TEST_WATER_FLAG=1;
				
			}	
		}
		if(ORDER_TEST_WATER_FLAG==1){
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
#endif
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	//fprintf(out,"MC_All_rot %lu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	//fprintf(out,"MC_All_rot A OK %lu\n",icycl);fflush(out);
	for(k=0;k<Plength[pr];k++){ 
		
		oldX[k]=prot[pr].ammino[k].x;
		oldY[k]=prot[pr].ammino[k].y;
		oldZ[k]=prot[pr].ammino[k].z;
		
		CM_x+=prot[pr].ammino[k].x;
		CM_y+=prot[pr].ammino[k].y;
		CM_z+=prot[pr].ammino[k].z;
		
		
	}	
	CM_x/=Plength[pr];
	CM_y/=Plength[pr];
	CM_z/=Plength[pr];	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"MC_All_rot B OK %lu\n",icycl);fflush(out);
	for(k=0;k<Plength[pr];k++){ 
		
		
		
		//if(icycl>0){fprintf(out,"Rotation %lu %d %d\n",icycl,kk,indice);fflush(out);}
		
		
		
		prot[pr].ammino[k].x-=CM_x;
		prot[pr].ammino[k].y-=CM_y;
		prot[pr].ammino[k].z-=CM_z;
		
		
		
		
		Rotation(pr,k,alpha,RotX,RotY,RotZ,flag);
		flag=0;
		
		
		prot[pr].ammino[k].x+=CM_x;
		prot[pr].ammino[k].y+=CM_y;
		prot[pr].ammino[k].z+=CM_z;
		
		
		
		if(prot[pr].ammino[k].id==ATOM_CA){
			//if(icycl>0){fprintf(out,"Update cell SAW %lu %d %d\n",icycl,kk,indice);fflush(out);}
			updt_cell_list_SAW (pr,k);
			if ((energy_SAW_All_rot_trasl(pr,k)>000.0)){  // reject
				for(l=0;l<=k;l++){ 
					
					
					prot[pr].ammino[l].x=oldX[l];
					prot[pr].ammino[l].y=oldY[l]; 
					prot[pr].ammino[l].z=oldZ[l]; 
					updt_cell_list_SAW (pr,l);
					
					updt_cell_list (pr,l);
					
					
					
					
				}
				
				Water_Update_Reject();
				#ifdef WATER_UP_TEST
		mossaid+=450;
		Water_Update_Test();
		mossaid-=450;
		#endif
				all_rot_rejectedself++;
				#ifdef 	CLIST_TEST			
				test_celllist_SAW(12);
				test_celllist_CA();
				test_celllist_H();
				
				#endif	
				return(REJECTSELF);
			}
		}
		//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
		updt_cell_list (pr,k);
		//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
		//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
		
		
		
		
		
	}
	//fprintf(out,"MC_All_rot C OK %lu\n",icycl);fflush(out);
	
	#ifdef 	CLIST_TEST			
	test_celllist_SAW(13);
	test_celllist_CA();
	test_celllist_H();
	
	#endif			
	/*if ((energy_SAW_test ()>0.0)){
		fprintf(out,"SAW  fw %lu %d %d\n",icycl);
		
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	#ifdef DIPOLE_SFC
	if(E_Dip>0){	
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(k=0;k<Plength[pr];k++){ 
		
		Enew+=energy_SP_Pivot_fw(pr,k,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;
		
		
		
	}
	
	//	fprintf(out,"MC_brot_f %lu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
	//fprintf(out,"MC_All_rot C OK %lu\n",icycl);fflush(out);
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
	
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	
	#ifdef 	ORDER_TEST_WATER
	
	
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lfn",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(14);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;	
	
	
	
	//fprintf(out,"ok1\n");
	
	
	
	//fprintf(out,"ok2\n");
	
	
	
	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %lu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %lu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//fprintf(out,"MC_All_rot D OK %lu\n",icycl);fflush(out);
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"CC a all_rot  %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b all_rot  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"CC d all_rot  %lu %lf %lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/*strcpy(info_ALN,"initial");
	Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	if(fabs(aln_new-aln_partial)>1e-8){
	  fprintf(out,"MC_all_rot aln test %lu pr=%d %15.10lf %15.10lf min=%d max=%d\n",icycl,pr,aln_new,aln_partial,kk,kk_end);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
	#endif	
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %lu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP_Pivot_fw(pr,k);
		
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %lu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/

	if((Cnew<0)) {
  fprintf(out,"all_rot non ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
  fflush(out);
  MPI_Abort(MPI_COMM_WORLD,err5);
}
	if((Onew<0)){		
		fprintf(out,"all_rot non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
}
	/***********TEST*************/
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	//if((New_Mean_H_Bonds<=minH)) New_Mean_H_Bonds=0;
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot all_rot New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_brot all_rot New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);
				fflush(out);
				}	
		
	#endif
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);
	OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));		
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	
	//fprintf(out,"MC_All_rot F OK %lu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		for(l=0;l<Plength[pr];l++){ 							
			prot[pr].ammino[l].x=oldX[l];
			prot[pr].ammino[l].y=oldY[l]; 
			prot[pr].ammino[l].z=oldZ[l]; 
			//fprintf(out,"MC_All_rot reject %lu l=%d\n",icycl,l);fflush(out);
			updt_cell_list_SAW (pr,l);
			updt_cell_list (pr,l);
		}
		
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=460;
		Water_Update_Test();
		mossaid-=460;
		#endif
		all_rot_rejectedener++;
		return(0);   
	}
	

	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1300;
		Water_Update_Test();
		mossaid-=1300;
		#endif
	//fprintf(out,"MC_All_rot G OK %lu\n",icycl);fflush(out);
	all_rot_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %lu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	//fprintf(out,"MC_All_rot H OK %lu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}

int MC_SP_rot (int pr){
	int kk=0;
	double dx=0,dy=0,dz=0;
	double RotX=0,RotY=0,RotZ=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX=0,TY=0,TZ=0,dR=0;
	int i=0,k=0,MC_SP_rot_test=0,j=0,ii=0,indice=0,jj=0;
	int l=0,lll=0;
	int kkk=0,flag=1;
	int selection=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	///OTopo_indice_New=0,OTopo_indice_Old=0;
	int OALN_New=0,OALN_Old=0;
	FILE *fp=NULL;
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	kk=(int)(ran3(&seed)*((Plength[pr]/NATOM)));
	kk*=NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"MC_SP_rot ref non CA %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/**Random Axis**/
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	alpha=(PI*ran3(&seed)/10.0)-PI/20.0;
	
	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
		Eold+=energy_SP(pr,indice,OLDWATER);
		Oold+=sist->norder;
		Cold+=sist->ncontact;
	}
	Eold+=Bond_Energy_SP(pr,kk);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif

	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
		oldX[indice]=prot[pr].ammino[indice].x;
		oldY[indice]=prot[pr].ammino[indice].y;
		oldZ[indice]=prot[pr].ammino[indice].z;
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	
	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
		prot[pr].ammino[indice].x-=prot[pr].ammino[kk].x;
		prot[pr].ammino[indice].y-=prot[pr].ammino[kk].y;
		prot[pr].ammino[indice].z-=prot[pr].ammino[kk].z;
		Rotation(pr,indice,alpha,RotX,RotY,RotZ,flag);
		flag=0;
		prot[pr].ammino[indice].x+=prot[pr].ammino[kk].x;
		prot[pr].ammino[indice].y+=prot[pr].ammino[kk].y;
		prot[pr].ammino[indice].z+=prot[pr].ammino[kk].z;
		updt_cell_list (pr,indice);
	}
	
	#ifdef 	CLIST_TEST			
	test_celllist_SAW(15);
	test_celllist_CA();
	test_celllist_H();
	#endif			
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
		Enew+=energy_SP(pr,indice,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;
	}
	Enew+=Bond_Energy_SP(pr,kk);
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	
	Ewater=E_Water_New();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	#ifdef 	ORDER_TEST_WATER
		
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(16);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"testC SP_rotl  %lu Cnew=%d testC=%d Cold=%d sist->contact=%d\n",icycl,Cnew,testC,Cold,sist->contact);
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"testO SP_rotl  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"testE SP_rotl  %lu %lf %lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	#endif	
	
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"MC_brot local non ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"MC_brot local non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/***********TEST*************/
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
		
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);fflush(out);}	
	#endif
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
		
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);
	OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));	
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		for(k=0;k<NSPOTS;k++){ 
			indice=kk+k+1;							
			prot[pr].ammino[indice].x=oldX[indice];
			prot[pr].ammino[indice].y=oldY[indice]; 
			prot[pr].ammino[indice].z=oldZ[indice]; 
			updt_cell_list_SAW (pr,indice);
			updt_cell_list (pr,indice);
		}
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){	
			Dipole_Reject();
		}
		#endif
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=470;
		Water_Update_Test();
		mossaid-=470;
		#endif
		#ifdef 	ORDER_TEST_WATER
		mossaid+=800;
		order_test_water(&testEWater);
		#endif
		rot_rejectedener++;
		return(0);   
	}
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0)
		Dipole_Accept();
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
	mossaid+=1400;
	Water_Update_Test();
	mossaid-=1400;
	#endif
	rot_accepted++;
	sist->E=Enew;
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	#ifdef 	ORDER_TEST_WATER
	mossaid+=810;
	order_test_water(&testEWater);
	#endif
	return(0);
}

int MC_SP_trasl_fseq (int pr){
	double dx=0,dy=0,dz=0;
	int kk=0,kkk=0,res=0,old_res=0,fseq_flag=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0,deltaP=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX=0,TY=0,TZ=0,dR=0;
	int i=0,k=0,MC_SP_trasl_fseq_test=0,j,ii,indice,jj;
	int l=0,lll=0;
	int flag=1;
	int selection=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0,Otest=0.0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	double ALN_New=0, aln_new=0;
	int OALN_New=0, OALN_Old=0;
	///OTopo_indice_New=0,OTopo_indice_Old=0;
	FILE *fp=NULL;
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	kk=(int)(ran3(&seed)*((Plength[pr]/NATOM)));
	res=(int)(ran3(&seed)*(S-1))+1;
	
	kk*=NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"MC_SP_trasl_fseq ref non CA %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(prot[pr].ammino[kk].residue<=0){
		fprintf(out,"MC_SP_trasl_fseq prot[%d].ammino[%d].residue=%d less equal than 0 !!!\n",pr,kk,prot[pr].ammino[kk].residue);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	dR=ran3(&seed)*2.0;
	
	TX=sin(phi)*cos(theta)*dR;
	TY=sin(phi)*sin(theta)*dR;
	TZ=cos(phi)*dR;
	//fprintf(out,"MC_SP_trasl_fseq B pr=%d kk=%d res=%d S=%d NPROT=%d\n",pr,kk,res,S,NPROT);
	
	
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;		
		Eold+=energy_SP(pr,indice,OLDWATER);
		Oold+=sist->norder;
		Cold+=sist->ncontact;
		
	}
	Eold+=Bond_Energy_SP(pr,kk);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	//fprintf(out,"MC_SP_trasl_fseq %lu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq A OK %lu\n",icycl);fflush(out);
	#endif
	//fprintf(out,"MC_SP_trasl_fseq C pr=%d kk=%d res=%d S=%d NPROT=%d\n",pr,kk,res,S,NPROT);
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
		
		oldX[indice]=prot[pr].ammino[indice].x;
		oldY[indice]=prot[pr].ammino[indice].y;
		oldZ[indice]=prot[pr].ammino[indice].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq B OK %lu\n",icycl);fflush(out);
	#endif
	//fprintf(out,"MC_SP_trasl_fseq D pr=%d kk=%d res=%d S=%d NPROT=%d\n",pr,kk,res,S,NPROT);
	fseq_flag=0;
	if(res!=prot[pr].ammino[kk].residue){
		
		//fprintf(out,"MC_SP_trasl_fseq E pr=%d kk=%d res=%d S=%d NPROT=%d\n",pr,kk,res,S,NPROT);
		old_res=prot[pr].ammino[kk].residue;
		//fprintf(out,"MC_SP_trasl_fseq F pr=%d kk=%d res=%d S=%d NPROT=%d prot[pr].ammino[kk].residue=%d old_res=%d\n",pr,kk,res,S,NPROT,prot[pr].ammino[kk].residue,old_res);
		
		if(sist->np[old_res]<=1){ 
			fprintf(out,"SP_trasl_fseq %lu cazzo sist->np[old_res] e' negativo sist->np[%d]=%d prot[%d].ammino[%d].residue=%d NPROT=%d\n",icycl,old_res,sist->np[old_res],pr,kk,prot[pr].ammino[kk].residue,NPROT);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		sist->np[old_res]--;
		
		prot[pr].ammino[kk].residue=res;
		
		sist->np[res]++;
		if(sist->np[res]<=0){ 
			fprintf(out,"SP_trasl_fseq %lu cazzo sist->np[res] e' negativo sist->np[%d]=%d prot[%d].ammino[%d].residue=%d NPROT=%d\n",icycl,res,sist->np[res],pr,kk,prot[pr].ammino[kk].residue,NPROT);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		deltaP=((double)(sist->p[old_res])/(double)(sist->np[res]));		
		
		deltaP=log(deltaP);
		if(partint(sist->NP+deltaP)<0){ 
			fprintf(out,"SP_trasl_fseq %lu cazzo order e' negativo %f %f %ld beta2=%f\n",icycl,sist->NP,deltaP,lround(sist->NP+deltaP),beta2);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		fseq_flag=1;
		
		
		if((isnan(Rmin[res]/Rmin[old_res]))||(isnan(Rmin[old_res]/Rmin[res]))){
			fprintf(out,"MC_SP_trasl_fseq %lu NAN Rmin[res]=%lf Rmin[old_res]=%lf res=%d old_res=%d\n",icycl,Rmin[res],Rmin[old_res],res,old_res);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			
		}
		/*---------------Nex we shrink the particle-------*/
		for(k=1;k<=NSPOTS;k++){ 
			indice=kk+k;
			
			if(prot[pr].ammino[indice].id==ATOM_H){
				prot[pr].ammino[indice].x-=prot[pr].ammino[kk].x;
				prot[pr].ammino[indice].y-=prot[pr].ammino[kk].y;
				prot[pr].ammino[indice].z-=prot[pr].ammino[kk].z;
				
				if((isnan(prot[pr].ammino[indice].x))||(isnan(1./prot[pr].ammino[indice].x))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq A prot[%d].ammino[%d].x=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].x);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				if((isnan(prot[pr].ammino[indice].y))||(isnan(1./prot[pr].ammino[indice].y))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq A prot[%d].ammino[%d].y=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].y);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				if((isnan(prot[pr].ammino[indice].z))||(isnan(1./prot[pr].ammino[indice].z))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq A prot[%d].ammino[%d].z=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].z);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}	
				
				
				prot[pr].ammino[indice].x*=Rmin[res]/Rmin[old_res];
				prot[pr].ammino[indice].y*=Rmin[res]/Rmin[old_res];
				prot[pr].ammino[indice].z*=Rmin[res]/Rmin[old_res];
				
				
				if((isnan(prot[pr].ammino[indice].x))||(isnan(1./prot[pr].ammino[indice].x))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq B prot[%d].ammino[%d].x=%lf Rmin[%d]=%lf Rmin[%d]=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].x,res,Rmin[res],old_res,Rmin[old_res]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				if((isnan(prot[pr].ammino[indice].y))||(isnan(1./prot[pr].ammino[indice].y))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq B prot[%d].ammino[%d].y=%lf Rmin[%d]=%lf Rmin[%d]=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].y,res,Rmin[res],old_res,Rmin[old_res]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				if((isnan(prot[pr].ammino[indice].z))||(isnan(1./prot[pr].ammino[indice].z))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq B prot[%d].ammino[%d].z=%lf Rmin[%d]=%lf Rmin[%d]=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].z,res,Rmin[res],old_res,Rmin[old_res]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}	
				
				prot[pr].ammino[indice].x+=prot[pr].ammino[kk].x;
				prot[pr].ammino[indice].y+=prot[pr].ammino[kk].y;
				prot[pr].ammino[indice].z+=prot[pr].ammino[kk].z;
				
				
				if((isnan(prot[pr].ammino[indice].x))||(isnan(1./prot[pr].ammino[indice].x))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq C prot[%d].ammino[%d].x=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].x);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				if((isnan(prot[pr].ammino[indice].y))||(isnan(1./prot[pr].ammino[indice].y))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq C prot[%d].ammino[%d].y=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].y);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}
				if((isnan(prot[pr].ammino[indice].z))||(isnan(1./prot[pr].ammino[indice].z))){
					fprintf(out,"Coord NAN %lu MC_SP_trasl_fseq C prot[%d].ammino[%d].z=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].z);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
					
				}	
			}
		}
		
		
	}
	
	
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
		
		
		
		//if(icycl>0){fprintf(out,"Rotation %lu %d %d\n",icycl,kk,indice);fflush(out);}
		prot[pr].ammino[indice].x+=TX;
		prot[pr].ammino[indice].y+=TY;
		prot[pr].ammino[indice].z+=TZ;
		
		
		
		
		
		if(prot[pr].ammino[indice].id==ATOM_CA){
			//if(icycl>0){fprintf(out,"Update cell SAW %lu %d %d\n",icycl,kk,indice);fflush(out);}
			updt_cell_list_SAW (pr,kk);
			if ((energy_SAW_SP(pr,kk)>000.0)){  // reject
				
				for(kkk=0;kkk<=NSPOTS;kkk++){ 
					indice=kk+kkk;
					prot[pr].ammino[indice].x=oldX[indice]; //It is important that the SAW is the first in the list otherwise it will not update the position of the patches
					prot[pr].ammino[indice].y=oldY[indice]; 
					prot[pr].ammino[indice].z=oldZ[indice]; 
					updt_cell_list_SAW (pr,indice);
					
					updt_cell_list (pr,indice);
				}
				if(fseq_flag==1){
					prot[pr].ammino[kk].residue=old_res;
					
					sist->np[res]=sist->p[res];
					sist->np[old_res]=sist->p[old_res];
				}
				
				
				
				//if(icycl>0){fprintf(out,"Test cell before rejection SAW\n");fflush(out);}
				//test_celllist_SAW(17);
				//if(icycl>0){fprintf(out,"Test cell before rejection CA\n");fflush(out);}
				//test_celllist_CA();
				//if(icycl>0){fprintf(out,"Test cell before rejection H\n");fflush(out);}
				//test_celllist_H();
				//if(icycl>0){fprintf(out,"Test cell before rejection O\n");fflush(out);}
				//
				//if(icycl>0){fprintf(out,"Test cell before rejection OK\n");fflush(out);}
				Water_Update_Reject();
				#ifdef WATER_UP_TEST
		mossaid+=480;
		Water_Update_Test();
		mossaid-=480;
		#endif
				trasl_fseq_rejectedself++;
				return(REJECTSELF);
			}
		}
		//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
		updt_cell_list (pr,indice);
		//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
		//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
		
		
		
		
		
	}
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq C OK %lu\n",icycl);fflush(out);
	#endif	
	#ifdef 	CLIST_TEST			
	test_celllist_SAW(18);
	test_celllist_CA();
	test_celllist_H();
	
	#endif			
	/*if ((energy_SAW_test ()>0.0)){
		fprintf(out,"SAW  fw %lu %d %d\n",icycl);
		
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
		
		Enew+=energy_SP(pr,indice,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;
		
		
		
	}
	Enew+=Bond_Energy_SP(pr,kk);
	//	fprintf(out,"MC_brot_f %lu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
	
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq D OK %lu\n",icycl);fflush(out);
	#endif
	
	
	
	//fprintf(out,"ok1\n");
	
	
	
	//fprintf(out,"ok2\n");
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
	
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	
	#ifdef 	ORDER_TEST_WATER
	
	
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(19);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;	
	
	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %lu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %lu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq E OK %lu\n",icycl);fflush(out);
	#endif
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"testC SP_trasl_fseq  %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"testO SP_trasl_fseq  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"testE SP_trasl_fseq  %lu %lf %lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}

	
	N=(int)(Plength[0]/NATOM)+1;
/********test for factorial does not work due to the stirling approx*************
	if (N>50){
	 Otest=N*log(N)-N;}
	else{
	Otest=log(factorial(N));
	}
	
	for(jj=0;jj<S;jj++){

	  if (sist->np[jj]>50) Otest-=(sist->np[jj]*log(sist->np[jj]))-sist->np[jj];
	  else{
	    Otest-=log(factorial(sist->np[jj]));
	  }
	}
	if(fabs(Otest-(sist->NP+deltaP))>1e-5){
		fprintf(out,"NP SP_trasl_fseq %lu Otest failed Otest=%lf sist->NP=%lf deltaP=%lf factorial(N)=%g N=%d\n",icycl,Otest,sist->NP,deltaP,factorial(N),N);fflush(out);
		
		for(jj=0;jj<S;jj++){
			fprintf(out,"factorial(sist->np[%d])=%g sist->np[%d]=%d\n",jj,factorial(sist->np[jj]),jj,sist->np[jj]);   
		}
		for(jj=0;jj<S;jj++){
			fprintf(out,"factorial(sist->p[%d])=%g sist->p[%d]=%d\n",jj,factorial(sist->p[jj]),jj,sist->p[jj]);   
		}
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	/***/
	#endif	

	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %lu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP(pr,k);
		
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %lu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/
	//fprintf(out,"MC_SP_trasl_fseq D OK %lu\n",icycl);fflush(out);
	/*if((Onew<IminO)){		
		fprintf(out,"MC_brot fw non ok icycl=%lu, order=%d %d %d %d %d\n",icycl,Onew,sist->order,Oold,minO,maxO);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	if((Cnew<0)){		
		fprintf(out,"MC_SP_trasl_fseq non ok Cnew<0 C icycl=%lu, Cnew=%d sist->contact=%d Cold=%d sist->ndip_contact=%d sist->dip_contact=%d\n",icycl,Cnew,sist->contact,Cold,sist->ndip_contact,sist->dip_contact);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"MC_SP_trasl_fseq non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}

	/***********TEST*************/
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	//if((New_Mean_H_Bonds<=minH)) New_Mean_H_Bonds=0;
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq F OK %lu\n",icycl);fflush(out);
	#endif
	
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);fflush(out);}	
	#endif
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	//DW+=Wpot_Topo[betaindice][OEnd_New][OTopo_indice_New]-Wpot[betaindice][OEnd_Old][OTopo_indice_Old];	

	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq G OK %lu\n",icycl);fflush(out);
	#endif
	
	espo=-beta[betaindice]*(Enew-Eold-beta2*deltaP)+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
	OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));	
	
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq H OK %lu\n",icycl);fflush(out);
	#endif
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq I OK %lu\n",icycl);fflush(out);
	#endif
	
	//fprintf(out,"MC_SP_trasl_fseq F OK %lu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		//if(icycl>3576610){fprintf(out,"Orders %lu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
		//fprintf(out,"MC_SP_trasl_fseq Fbis OK %lu\n",icycl);fflush(out);
		for(k=0;k<=NSPOTS;k++){ 
			indice=kk+k;							
			prot[pr].ammino[indice].x=oldX[indice];
			prot[pr].ammino[indice].y=oldY[indice]; 
			prot[pr].ammino[indice].z=oldZ[indice]; 
			//fprintf(out,"MC_SP_trasl_fseq reject %lu l=%d\n",icycl,l);fflush(out);
			updt_cell_list_SAW (pr,indice);
			updt_cell_list (pr,indice);
			
			
			
		}
		if(fseq_flag==1){
			prot[pr].ammino[kk].residue=old_res;
			
			sist->np[res]=sist->p[res];
			sist->np[old_res]=sist->p[old_res];
		}
		
		/*#ifdef DIPOLE_SFC
		Dipole_Reject();
		#endif
		*/
		/*for(ii=0;ii<Plength[pr];ii++){
			
			if(fabs(oldX1[ii]-prot[pr].ammino[ii].x)>1e-10){ fprintf(out,"Update fw X %lu %d %d %lf %lf\n",icycl,kk,ii,oldX1[ii],prot[pr].ammino[ii].x);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldY1[ii]-prot[pr].ammino[ii].y)>1e-10){ fprintf(out,"Update fw Y %lu %d %d %lf %lf\n",icycl,kk,ii,oldY1[ii],prot[pr].ammino[ii].y);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldZ1[ii]-prot[pr].ammino[ii].z)>1e-10){ fprintf(out,"Update fw Z %lu %d %d %lf %lf\n",icycl,kk,ii,oldZ1[ii],prot[pr].ammino[ii].z);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
		}*/
		
		/*for(k=0;k<Plength[pr];k++){
			MC_SP_trasl_fseq_test=0;
			if(prot[pr].ammino[k].Nverl!=nvicini[k]){fprintf(out,"Wrong number of neigh %lu %d %d %d\n",icycl,k,prot[pr].ammino[k].Nverl,nvicini[k]);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			for(j=0;j<prot[pr].ammino[k].Nverl;j++){
				
				for(jj=0;jj<nvicini[k];jj++){
					if(vicini[k][jj]==prot[pr].ammino[k].verli[j]) MC_SP_trasl_fseq_test++;
				}
				
			}		
			
			if(MC_SP_trasl_fseq_test!=prot[pr].ammino[k].Nverl){
				fprintf(out,"Update fw Verl %lu %d %d\n",icycl,MC_SP_trasl_fseq_test,prot[pr].ammino[k].Nverl);
				for(j=0;j<prot[pr].ammino[k].Nverl;j++){
					fprintf(out,"vicini[%d][%d]=%d %d\n",k,j,vicini[k][j],prot[pr].ammino[k].verli[j]);
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
		}*/
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		#ifdef PROG_TRASL_FSEQ
		fprintf(out,"MC_SP_trasl_fseq L OK %lu\n",icycl);fflush(out);
		#endif
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=490;
		Water_Update_Test();
		mossaid-=490;
		#endif
		trasl_fseq_rejectedener++;
		return(0);   
	}
	
	
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1600;
		Water_Update_Test();
		mossaid-=1600;
		#endif
	if(fseq_flag==1){
		sist->p[res]=sist->np[res];
		sist->p[old_res]=sist->np[old_res];
		
		sist->NP=sist->NP+deltaP;
	}
	
	
	
	//fprintf(out,"MC_SP_trasl_fseq G OK %lu\n",icycl);fflush(out);
	trasl_fseq_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %lu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	//fprintf(out,"MC_SP_trasl_fseq H OK %lu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->order,sist->contact);
	#ifdef PROG_TRASL_FSEQ
	fprintf(out,"MC_SP_trasl_fseq M OK %lu\n",icycl);fflush(out);
	#endif
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}

int MC_SP_trasl (int pr){
	double dx=0,dy=0,dz=0;
	int kk=0,kkk=0,res=0,old_res=0,fseq_flag=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0,deltaP=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX=0,TY=0,TZ=0,dR=0;
	int i=0,k=0,MC_SP_trasl_test=0,j,ii,indice,jj;
	int l=0,lll=0;
	int flag=1;
	int selection=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0,Otest=0.0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	///int OTopo_indice_New=0,OTopo_indice_Old=0;
	double End_New=0;
	int node1=0, node2;
	int OALN_New=0,OALN_Old=0;
	double ALN_New=0, aln_new=0,aln_partial=0;
	double rCA2_end=0;
	int last=0;
	char info_ALN[20];
	FILE *fp=0;
		
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	kk=(int)(ran3(&seed)*((Plength[pr]/NATOM)));
	kk*=NATOM;
	
	node1=kk-NATOM;
	node2=kk+NATOM;
	/*strcpy(info_ALN,"single_move");	
	Linking_Number(prot,pr,ATOM_CA,node1,node2,Plength[pr],NATOM,info_ALN,&aln_partial); 
	aln_new=prot[pr].ALN-aln_partial;*/
	
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"MC_SP_trasl ref non CA %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(prot[pr].ammino[kk].residue<=0){
		fprintf(out,"MC_SP_trasl prot[%d].ammino[%d].residue=%d less equal than 0 !!!\n",pr,kk,prot[pr].ammino[kk].residue);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	theta=2*PI*ran3(&seed);
	///phi=PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	dR=ran3(&seed)*2.0;
	
	TX=sin(phi)*cos(theta)*dR;
	TY=sin(phi)*sin(theta)*dR;
	TZ=cos(phi)*dR;
	
	for(k=0;k<=NSPOTS;k++){ /// change the energy of bead kk and its patches
		indice=kk+k;		
		Eold+=energy_SP(pr,indice,OLDWATER);
		Oold+=sist->norder;
		Cold+=sist->ncontact;
		
	}
	Eold+=Bond_Energy_SP(pr,kk);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl A OK %lu\n",icycl);fflush(out);
	#endif
	for(k=0;k<=NSPOTS;k++){ /// save old coordinates of bead kk and its patches
		indice=kk+k;
		oldX[indice]=prot[pr].ammino[indice].x;
		oldY[indice]=prot[pr].ammino[indice].y;
		oldZ[indice]=prot[pr].ammino[indice].z;
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl B OK %lu\n",icycl);fflush(out);
	#endif
	
	
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;

		prot[pr].ammino[indice].x+=TX;   /// translation of bead kk and its patches
		prot[pr].ammino[indice].y+=TY;
		prot[pr].ammino[indice].z+=TZ;
	
		if(prot[pr].ammino[indice].id==ATOM_CA){
			updt_cell_list_SAW (pr,kk);
			/** Check if the moved bead overlaps with the others **/
			if ((energy_SAW_SP(pr,kk)>000.0)){  /// reject
				
				for(kkk=0;kkk<=NSPOTS;kkk++){ 
					indice=kk+kkk;
					prot[pr].ammino[indice].x=oldX[indice]; /// It is important that the SAW is the first in 
					prot[pr].ammino[indice].y=oldY[indice]; /// the list otherwise it will not update the position of the patches
					prot[pr].ammino[indice].z=oldZ[indice]; 
					updt_cell_list_SAW (pr,indice);
					updt_cell_list (pr,indice);
				}
	
				#ifdef 	CLIST_TEST			
				test_celllist_SAW(21);
				test_celllist_CA();
				test_celllist_H();
				#endif	
				
				trasl_rejectedself++;
				Water_Update_Reject();
				
				#ifdef WATER_UP_TEST
				mossaid=158;
				Water_Update_Test();
				#endif
				return(REJECTSELF); /// if rejected than return
			}
		}
		updt_cell_list (pr,indice);
	
	}
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl C OK %lu\n",icycl);fflush(out);
	#endif	
	#ifdef 	CLIST_TEST			
	test_celllist_SAW(22);
	test_celllist_CA();
	test_celllist_H();
	#endif			

	#ifdef DIPOLE_SFC
	if(E_Dip>0){		
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	
	for(k=0;k<=NSPOTS;k++){ /// if accepted I calculate new order parameters
		indice=kk+k;
		Enew+=energy_SP(pr,indice,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;
	}
	Enew+=Bond_Energy_SP(pr,kk);

	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl D OK %lu\n",icycl);fflush(out);
	#endif

	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
		
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
		
	#ifdef 	ORDER_TEST_WATER
	
	
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(23);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	Enew+=Ewater;	

	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;

	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	
	//Linking_Number(prot,pr,ATOM_CA,node1,node2,Plength[pr],NATOM,info_ALN,&aln_partial);
	//aln_new+=aln_partial;

	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl E OK %lu\n",icycl);fflush(out);
	#endif
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"testC SP_trasl  %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"testO SP_trasl  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"testE SP_trasl  %lu %lf %lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/*strcpy(info_ALN,"initial");
	Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	if(fabs(aln_new-aln_partial)>1e-8){
	  fprintf(out,"SP_trasl aln test %lu pr=%d %15.10lf %15.10lf bead_kk=%d \n",icycl,pr,aln_new,aln_partial,kk);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
	
	
	#endif	

	/***********TEST*******************/
	if((Cnew<0)){		
		fprintf(out,"SP_trasl non ok icycl=%lu, Cnew=%d sist->contact=%d Cold=%d sist->ndip_contact=%d sist->dip_contact=%d\n",icycl,Cnew,sist->contact,Cold,sist->ndip_contact,sist->dip_contact);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"SP_trasl non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********END TEST*************/
	
	ALN_New=sist->ALN_tot-prot[pr].ALN+aln_new;
	OALN_New=lround(fabs(OALN_bin*ALN_New/DNPROT));
	OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 	
	
	/*if ((kk>ATOM_CA) && (kk<Plength[pr]-NATOM)){
		OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
		rCA2_end=prot[pr].end;
		End_New=sist->End;
	}else	{
	  last=Plength[pr]-NSPOTS;  
		dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
		dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
		dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
		dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		rCA2_end=sqrt(dx*dx+ dy*dy + dz*dz);
		End_New=sist->End-prot[pr].end+rCA2_end;
		OEnd_New=lround(OEND_bin*End_New/DNPROT);
		OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
		}*/
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;  
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	//if((New_Mean_H_Bonds<=minH)) New_Mean_H_Bonds=0;
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl F OK %lu\n",icycl);fflush(out);
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);fflush(out);}	
	#endif
		
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	//DW+=Wpot_ALN[betaindice][OEnd_New][OALN_New]-Wpot[betaindice][OEnd_Old][OALN_Old];	

	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl G OK %lu\n",icycl);fflush(out);
	#endif
	
	espo=-beta[betaindice]*(Enew-Eold)+DW*beta[betaindice];
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}

	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}

	///OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
	
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl H OK %lu\n",icycl);fflush(out);
	#endif
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl I OK %lu\n",icycl);fflush(out);
	#endif

	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		for(k=0;k<=NSPOTS;k++){ 
			indice=kk+k;							
			prot[pr].ammino[indice].x=oldX[indice];
			prot[pr].ammino[indice].y=oldY[indice]; 
			prot[pr].ammino[indice].z=oldZ[indice]; 
			updt_cell_list_SAW (pr,indice);
			updt_cell_list (pr,indice);
		}
	
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		#ifdef PROG_trasl
		fprintf(out,"MC_SP_trasl L OK %lu\n",icycl);fflush(out);
		#endif
		
		Water_Update_Reject();
		
		#ifdef WATER_UP_TEST
		mossaid=155;
		Water_Update_Test();
		#endif
		
		trasl_rejectedener++;
		return(0);   
	}
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
	mossaid=156;
	Water_Update_Test();
	#endif

	trasl_accepted++;
	sist->E=Enew;
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	sist->End=End_New;
	prot[pr].end=rCA2_end;
	sist->ALN_tot=ALN_New;
	prot[pr].ALN=aln_new;
	
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	#ifdef PROG_trasl
	fprintf(out,"MC_SP_trasl M OK %lu\n",icycl);fflush(out);
	#endif
	return(0);	
}

int MC_Pivot_fw (int pr,int kk){
	double dx=0,dy=0,dz=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX=0,RotY=0,RotZ=0,modulo=0;
	int i=0,k=0,MC_Pivot_fw_test=0,j=0,ii=0,jj=0;
	int l=0,lll=0;
	int kkk,flag=1;
	double testO=0,testOP=0,aln_partial=0;
	int testC=0;
	double testE=0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	///int OTopo_indice_New=0,OTopo_indice_Old=0;
	int OALN_New=0,OALN_Old=0;
	double End_New=0;
	///double Topo_indice_New=0;
	///int topo_New=0;
	double ALN_New=0, aln_new=0;
	double rCA2_end=0;
	int last=0;
	char info_ALN[20];
	FILE *fp=NULL;
	
	kk=kk*NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"CAZZOOOOOOO %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((kk>=Plength[pr]-1)||(kk<0)){
		fprintf(out,"KK out of range MC_Pivot_fw icycl %lu kk %d\n",icycl,kk);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
       
	/*	strcpy(info_ALN,"pivot");       /// Because here the perno is kk-NATOM and kk is moving!!!!!!!!!!!!!!!!
	Linking_Number(prot,pr,ATOM_CA,kk-NATOM,0,Plength[pr],NATOM,info_ALN,&aln_partial); 
	aln_new=prot[pr].ALN-aln_partial;*/

	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));

	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	
	/**/
	alpha=(PI*ran3(&seed)/10.0)-PI/20.0;
	
	for(k=kk;k<Plength[pr];k++){ 						   
	
		
		Eold+=energy_SP_Pivot_fw(pr,k,OLDWATER);

		Oold+=sist->norder;
		Cold+=sist->ncontact;
	
		
	}	
	Eold+=Bond_Energy_bw(pr,kk);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	for(k=kk;k<Plength[pr];k++){ 
		oldX[k]=prot[pr].ammino[k].x;
		oldY[k]=prot[pr].ammino[k].y;
		oldZ[k]=prot[pr].ammino[k].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	for(k=kk;k<Plength[pr];k++){ 
		prot[pr].ammino[k].x-=prot[pr].ammino[kk-NATOM].x;
		prot[pr].ammino[k].y-=prot[pr].ammino[kk-NATOM].y;
		prot[pr].ammino[k].z-=prot[pr].ammino[kk-NATOM].z;
		
		
		
		
		Rotation(pr,k,alpha,RotX,RotY,RotZ,flag);
		flag=0;
		
		
		prot[pr].ammino[k].x+=prot[pr].ammino[kk-NATOM].x;
		prot[pr].ammino[k].y+=prot[pr].ammino[kk-NATOM].y;
		prot[pr].ammino[k].z+=prot[pr].ammino[kk-NATOM].z;
		
		
		
		
		
		if(prot[pr].ammino[k].id==ATOM_CA){
			updt_cell_list_SAW (pr,k);
			if ((energy_SAW(pr,k)>000.0)){  // reject
				for(l=kk;l<=k;l++){ 
					
					
					
					prot[pr].ammino[l].x=oldX[l];
					prot[pr].ammino[l].y=oldY[l]; 
					prot[pr].ammino[l].z=oldZ[l]; 
					
					updt_cell_list_SAW (pr,l);
					updt_cell_list (pr,l);
					
					
					
					
				}
				Water_Update_Reject();
				#ifdef WATER_UP_TEST
		mossaid+=500;
		Water_Update_Test();
		mossaid-=500;
		#endif
				fw_rejectedself++;
				#ifdef 	CLIST_TEST
				test_celllist_SAW(25);
				test_celllist_CA();
				test_celllist_H();
				
				#endif	
				return(REJECTSELF);
			}
		}
		

		updt_cell_list (pr,k);
	}
	
	#ifdef 	CLIST_TEST
	fprintf(out,"MC_Pivot_fw Range kk=%d Plength[%d]=%d\n",kk,pr,Plength[pr]);fflush(out);
	test_celllist_SAW(26);
	test_celllist_CA();
	test_celllist_H();
	
	#endif	
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){		
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	for(k=kk;k<Plength[pr];k++){ 						   
		
		
		
		
		
		Enew+=energy_SP_Pivot_fw(pr,k,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;
		
		
		
	}
	Enew+=Bond_Energy_bw(pr,kk);
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
	
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	/***********Update Topology*************/
	#ifdef OTEST
						Overalp_Test(41,0);
						#endif
	
						/*last=Plength[pr]-NSPOTS;
	dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
	dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
	dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	rCA2_end=sqrt(dx*dx+ dy*dy + dz*dz);*/
	/*** C&V
	 if (rCA2_end<KNOT_END_COND*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue])){
		indice_knot=0;
		for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
			if(indice_knot>=Psize){
							fprintf(out,"whatknot out of boudaries MC_pivot_fw %lu mossaid= %d kk= %d Psize= %ldk= %d Plength[%d]= %d NATOM= %d\n",icycl,mossaid,indice_knot,Psize,k,pr,Plength[pr],NATOM);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
						}
			prot_knot->bead[indice_knot].x=prot[pr].ammino[k].x;
			prot_knot->bead[indice_knot].y=prot[pr].ammino[k].y;
			prot_knot->bead[indice_knot].z=prot[pr].ammino[k].z;
			indice_knot++;
		}
		whatknot(prot_knot, Psize, prot[pr].topology,&topo_New);
		if(topo_New<0) topo_New=sizeTopo-1;
		
		#ifdef OTEST
			Overalp_Test(42,0);
		#endif
		
	}else{
		topo_New=0;
	} **/
	
	
	//Linking_Number(prot,pr,ATOM_CA,kk-NATOM,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	//aln_new+=aln_partial;
			
	
	
	
		
	#ifdef 	ORDER_TEST_WATER
	
	
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(27);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	Enew+=Ewater;	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	Dnew=sist->density+Dnew-Dold;
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	if(Cnew!=testC){
		fprintf(out,"CC a fw  %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b fw  %lu %lf %lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-8){
		fprintf(out,"CC d fw  %lu %15.10lf %15.10lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/*strcpy(info_ALN,"initial");
	Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	if(fabs(aln_new-aln_partial)>1e-8){
	  fprintf(out,"fw aln test %lu pr=%d %15.10lf %15.10lf bead_kk=%d\n",icycl,pr,aln_new,aln_partial,kk);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
	
	#endif	
	
	/***********TEST*************/
	//fprintf(out,"MC_Pivot_fw D OK %lu\n",icycl);fflush(out);
	/*if((Onew<IminO)){		
		fprintf(out,"MC_brot fw non ok icycl=%lu, order=%d %d %d %d %d\n",icycl,Onew,sist->order,Oold,minO,maxO);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	
	if((Cnew<0)){		
		fprintf(out,"fw non ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"fw non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/***********TEST*************/
	
	
	
	End_New=sist->End-prot[pr].end+rCA2_end;
	///Topo_indice_New=sist->Topo_indice-prot[pr].topo_indice+topo_New;	
	ALN_New=sist->ALN_tot-prot[pr].ALN+aln_new;
	
	OEnd_New=lround(OEND_bin*End_New/DNPROT);
	OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	
	///OTopo_indice_New=lround(Topo_indice_New/DNPROT);
	///OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
	OALN_New=lround(fabs(OALN_bin*ALN_New/DNPROT));
	OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));
		
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	
	if((New_Mean_CA_Bonds<0)){		
		fprintf(out,"MC_brot fw non ok icycl=%lu, New_Mean_CA_Bonds=%d %lf %lf \n",icycl,New_Mean_CA_Bonds,Onew,DNPROT);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot fw New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=sizeC)){		
		fprintf(out,"MC_brot fw New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d] Wpot_ALN[%d][%d][%d]-Wpot_ALN[%d][%d][%d]\n",
			mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds,betaindice,OEnd_New,OALN_New,betaindice,OEnd_Old,OALN_Old);
			fflush(out);
		}	
		
	#endif
	
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
	///DW+=Wpot_Topo[betaindice][OEnd_New][OTopo_indice_New]-Wpot[betaindice][OEnd_Old][OTopo_indice_Old];	
	//DW+=Wpot_ALN[betaindice][OEnd_New][OALN_New]-Wpot[betaindice][OEnd_Old][OALN_Old];	
	
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice];
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	
	///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	
	acc=exp(espo);
	
	if(ran3(&seed)>acc){ // reject
		for(k=kk;k<Plength[pr];k++){ 
			prot[pr].ammino[k].x=oldX[k];
			prot[pr].ammino[k].y=oldY[k]; 
			prot[pr].ammino[k].z=oldZ[k]; 
			updt_cell_list_SAW (pr,k);
			updt_cell_list (pr,k);	
		}

		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
	///	strcpy(prot[pr].topology,prot[pr].Otopology);
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=510;
		Water_Update_Test();
		mossaid-=510;
		#endif
		fw_rejectedener++;
		
		#ifdef OTEST
						Overalp_Test(43,0);
						#endif
		return(0);   
	}
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1800;
		Water_Update_Test();
		mossaid-=1800;
		#endif
	
	fw_accepted++;
	sist->E=Enew;
	
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	sist->End=End_New;
	///sist->Topo_indice=Topo_indice_New;
	sist->ALN_tot=ALN_New;
	///prot[pr].topo_indice=topo_New;
	prot[pr].ALN=aln_new;
	prot[pr].end=rCA2_end;
	///strcpy(prot[pr].Otopology,prot[pr].topology);
	
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	//Order_Conf_Sampling(sist->E,sist->order,sist->contact);
	#ifdef OTEST
						Overalp_Test(44,0);
						#endif
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
} 

int MC_Pivot_bw (int pr,int kk){
	double dx=0,dy=0,dz=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL; /// static means that after the first time that the funcion has been called,
	                                                /// when the funcion is called again the variable is NOT reinitialized or changed,
	                                                /// so save time if I don`t need this but I don`t need a global variable ether.	
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2=0.0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX=0,RotY=0,RotZ=0,modulo=0;
	int i=0,k=0,MC_Pivot_bw_test=0,j,ii=0;
	int kkk,flag=1;
	int l=0,lll=0;
	int prt=0;
	double testO=0,testOP=0, aln_partial=0;
	int testC=0;
	double testE=0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	///int OTopo_indice_New=0,OTopo_indice_Old=0;
	int OALN_New=0,OALN_Old=0;
	double End_New=0;
	///double Topo_indice_New=0;
	///int topo_New=0;
	double ALN_New=0, aln_new=0;
	double rCA2_end=0;
	int last=0;
	char info_ALN[20];
	FILE *fp=NULL;
	
	kk=kk*NATOM; /// kk was the BEAD number of perno
	if(prot[pr].ammino[kk].id!=ATOM_CA){ /// check that kk is a bead and not a patch
		fprintf(out,"CAZZOOOOOOO %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((kk>=Plength[pr]-1)||(kk<0)){
		fprintf(out,"KK out of range MC_Pivot_bw icycl %lu kk %d\n",icycl,kk);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/*strcpy(info_ALN,"pivot");	
	Linking_Number(prot,pr,ATOM_CA,kk,0,Plength[pr],NATOM,info_ALN,&aln_partial); 
	aln_new=prot[pr].ALN-aln_partial;*/
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));  /// I need to allocate only if I just initialized, so if it`s the 
	if(oldZ==NULL) oldY=(double *)calloc(ProtN,sizeof(double));  /// first time that the function is called.
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double)); 
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	
	alpha=(PI*ran3(&seed)/5.0)-PI/10.0;
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 1 %lu\n",icycl);fflush(out);}	
	#endif
	for(k=kk-1;k>=0;k--){	/// Calculate the energy of the moving branch (before kk), use global coordinates
		Eold+=energy_SP_Pivot_bw(pr,k,OLDWATER);
		Oold+=sist->norder;
		Cold+=sist->ncontact;
	}
	Eold+=Bond_Energy_bw(pr,kk);
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Eold+=E_Water_Old();
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 2 %lu\n",icycl);fflush(out);}	
	#endif
	
	for(k=kk-1;k>=0;k--){	/// Now I want to move so I remember the old coordinates
		oldX[k]=prot[pr].ammino[k].x;
		oldY[k]=prot[pr].ammino[k].y;
		oldZ[k]=prot[pr].ammino[k].z;
	}
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 3 %lu\n",icycl);fflush(out);}	
	#endif
	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	
	MC_Pivot_bw_test=0;
	
	for(k=kk-1;k>=0;k--){	
		prot[pr].ammino[k].x-=prot[pr].ammino[kk].x;  /// Center the branch with respect to the perno
		prot[pr].ammino[k].y-=prot[pr].ammino[kk].y;
		prot[pr].ammino[k].z-=prot[pr].ammino[kk].z;
		
		Rotation(pr,k,alpha,RotX,RotY,RotZ,flag); /// Rotate with quaternions
		flag=0;
		prot[pr].ammino[k].x+=prot[pr].ammino[kk].x;  /// Recenter the branch with respect to the perno
		prot[pr].ammino[k].y+=prot[pr].ammino[kk].y;
		prot[pr].ammino[k].z+=prot[pr].ammino[kk].z;
		
		if(prot[pr].ammino[k].id==ATOM_CA){
			updt_cell_list_SAW (pr,k);   /// Update cell list
			if ((energy_SAW_bw(pr,k)>0.0)){  // reject
				for(l=kk-1;l>=k;l--){	
					prot[pr].ammino[l].x=oldX[l];
					prot[pr].ammino[l].y=oldY[l]; 
					prot[pr].ammino[l].z=oldZ[l];
					updt_cell_list_SAW (pr,l);
					updt_cell_list (pr,l);
				}
								
				#ifdef 	CLIST_TEST
				test_celllist_SAW(28);
				test_celllist_CA();
				test_celllist_H();
				
				#endif			
				Water_Update_Reject();
				#ifdef WATER_UP_TEST
		mossaid+=520;
		Water_Update_Test();
		mossaid-=520;
		#endif
				bw_rejectedself++;
				return(REJECTSELF);
			}
		}
		updt_cell_list (pr,k);
	}
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 4 %lu\n",icycl);fflush(out);}	
	#endif
	
	#ifdef 	CLIST_TEST
	test_celllist_SAW(29);
	test_celllist_CA();
	test_celllist_H();
	
	#endif			
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		sist->EDip_New=Dipole_Self_Cons();
		Enew=sist->EDip_New-sist->EDip;
		Cnew=sist->ndip_contact-sist->dip_contact;
	}
	#endif
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 6 %lu\n",icycl);fflush(out);}	
	#endif
	for(k=kk-1;k>=0;k--){
		Enew+=energy_SP_Pivot_bw(pr,k,NEWWATER);
		Onew+=sist->norder;
		Cnew+=sist->ncontact;	
	}
	Enew+=Bond_Energy_bw(pr,kk);
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 7 %lu\n",icycl);fflush(out);}	
	#endif	
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
		
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	Dnew=sist->density+Dnew-Dold;
	
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
	#endif
	Ewater=E_Water_New();
		
	#ifdef PROGRESS_WATER			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	/*last=Plength[pr]-NSPOTS;  
	dx=prot[pr].ammino[ATOM_CA].x-prot[pr].ammino[last].x;
	dy=prot[pr].ammino[ATOM_CA].y-prot[pr].ammino[last].y;
	dz=prot[pr].ammino[ATOM_CA].z-prot[pr].ammino[last].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	rCA2_end=sqrt(dx*dx+ dy*dy + dz*dz);*/
	 /*** COMMENTATO DA VALO E CHIARA : NON CANCELLARE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 		
	if (rCA2_end<KNOT_END_COND*(Rmin[prot[pr].ammino[ATOM_CA].residue]+Rmin[prot[pr].ammino[last].residue])){
		indice_knot=0;
		for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
			if(indice_knot>=Psize){
				fprintf(out,"whatknot out of boudaries MC_pivot_bw %lu mossaid= %d indice_knot= %d Psize= %ldk= %d Plength[%d]= %d NATOM= %d\n",icycl,mossaid,indice_knot,Psize,k,pr,Plength[pr],NATOM);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			prot_knot->bead[indice_knot].x=prot[pr].ammino[k].x;
			prot_knot->bead[indice_knot].y=prot[pr].ammino[k].y;
			prot_knot->bead[indice_knot].z=prot[pr].ammino[k].z;
			indice_knot++;
		}
		whatknot(prot_knot, Psize, prot[pr].topology,&topo_New);
		if(topo_New<0) topo_New=sizeTopo-1;
	}else{
		topo_New=0;
	}	**/	
		
	//Linking_Number(prot,pr,ATOM_CA,kk,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	//aln_new+=aln_partial;
	
		
	#ifdef 	ORDER_TEST_WATER
	
	order_test_water(&testEWater);
	if(fabs(Ewater-testEWater)>1e-8){
		
		fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(30);
		test_celllist_CA();
		test_celllist_H();
		
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
	#endif
	
	Enew+=Ewater;	
	/***********TEST*************/
	
	if((Cnew<0)){		
		fprintf(out,"bw non ok icycl=%lu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"bw non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 8 %lu\n",icycl);fflush(out);}	
	#endif
	
	/***********Update Topology*************/
	
	End_New=sist->End-prot[pr].end+rCA2_end;
	///Topo_indice_New=sist->Topo_indice-prot[pr].topo_indice+topo_New;	
	ALN_New=sist->ALN_tot-prot[pr].ALN+aln_new;
		
	OEnd_New=lround(OEND_bin*End_New/DNPROT);
	OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
	
	/// OTopo_indice_New=lround(Topo_indice_New/DNPROT);  /// Average Topo_indice global per chain
	/// OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);
	OALN_New=lround(fabs(OALN_bin*ALN_New/DNPROT));
	OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));
	
	if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	/// if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;  /// If it goes out from istogram boundaries, it`s fixed at the boundary-1
	/// if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
	if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
	New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT));
	if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
	
	sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT));
	if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
	if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 9 %lu\n",icycl);fflush(out);}	
	#endif
	#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d] Wpot_ALN[%d][%d][%d]-Wpot_ALN[%d][%d][%d]\n",
			mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds,betaindice,OEnd_New,OALN_New,betaindice,OEnd_Old,OALN_Old);
		fflush(out);
	}	
	#endif
	
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds]; /// DeltaW bias from new and old
	//DW+=Wpot_ALN[betaindice][OEnd_New][OALN_New]-Wpot[betaindice][OEnd_Old][OALN_Old];	
	/// DW+=Wpot_Topo[betaindice][OEnd_New][OTopo_New]-Wpot[betaindice][OEnd_Old][OTopo_Old];	C&V
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 10 %lu\n",icycl);fflush(out);}	
	#endif
	espo=-(Enew-Eold)*beta[betaindice]+DW*beta[betaindice]; /// Exponential in the acceptance probability, Enew= Ewater+Ebond+Epivot without the part that is not changing
	if(espo<50.0){                        /// Eold is the same contribution before the move
		espo1=-log(1+exp(espo)); /// Denominator of the virtual move weights
	}else{
		espo1=-espo;  /// Neglect 1 if espo is big
	}
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot bw non ok icycl=%lu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	Enew=sist->E+Enew-Eold; /// Total new energy
	Eold=sist->E; /// Total old energy
	
	/***********TEST*************/
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 11 %lu\n",icycl);fflush(out);}	
	#endif
	#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		testE+=sist->EDip_New;
		
		testC+=sist->ndip_contact;
	}
	#endif
	
	if(Cnew!=testC){
		fprintf(out,"CC a bw %lu %d %d\n",icycl,Cnew,testC);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b bw %lu %.20lf %.20lf\n",icycl,Onew,testO);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-8){
		fprintf(out,"CC d bw %lu %15.10lf %15.10lf\n",icycl,Enew,testE);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/*strcpy(info_ALN,"initial");
	Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
	if(fabs(aln_new-aln_partial)>1e-8){
	  fprintf(out,"bw aln test %lu pr=%d %15.10lf %15.10lf bead_kk=%d\n",icycl,pr,aln_new,aln_partial,kk);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
	#endif
		
	/***********SAMPLING*************/
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 12 %lu\n",icycl);fflush(out);}	
	#endif
	
	/** espo+espo1 is the logarithm of the new configuration weigth of the virtual move **/
	sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 13 %lu\n",icycl);fflush(out);}	
	#endif
	sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 14 %lu\n",icycl);fflush(out);}	
	#endif
	
	acc=exp(espo); /// Acceptance probability
	
	if (ran3(&seed)>acc){ // reject
		for(k=kk-1;k>=0;k--){	
			prot[pr].ammino[k].x=oldX[k];
			prot[pr].ammino[k].y=oldY[k]; 
			prot[pr].ammino[k].z=oldZ[k]; 
			updt_cell_list_SAW (pr,k);
			updt_cell_list (pr,k);
		}
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Reject();
		}
		#endif
		
		///strcpy(prot[pr].topology,prot[pr].Otopology);
		Water_Update_Reject();
		#ifdef WATER_UP_TEST
		mossaid+=530;
		Water_Update_Test();
		mossaid-=530;
		#endif
		bw_rejectedener++;
		return(0);   
	}
	
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 15 %lu\n",icycl);fflush(out);}	
	#endif
	
	#ifdef DIPOLE_SFC
	if(E_Dip>0){
		Dipole_Accept();
	}
	#endif
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 16 %lu\n",icycl);fflush(out);}	
	#endif
	Water_Update_Accept();
	#ifdef WATER_UP_TEST
		mossaid+=1900;
		Water_Update_Test();
		mossaid-=1900;
		#endif
	
	bw_accepted++;
	sist->E=Enew;    /// If accepted overwrite new energy
	  
	sist->order=Onew;   /// If accepted overwrite all the new order parameters
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	sist->End=End_New;
	///sist->Topo_indice=Topo_indice_New;
	sist->ALN_tot=ALN_New;
	///prot[pr].topo_indice=topo_New;
	prot[pr].ALN=aln_new;
	prot[pr].end=rCA2_end;
	///strcpy(prot[pr].Otopology,prot[pr].topology);
	
	Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
	#ifdef PROG_BW
	if(icycl>0) {fprintf(out,"PIvot BW ok 17 %lu\n",icycl);fflush(out);}	
	#endif
	
	return(0);
}

void ExpSum (void *ina,void *output, int *len, MPI_Datatype *dptr){
	
	int i=0;
	double c=0;
	double *in,*inout;
	in=(double *) ina;
	inout=(double *) output;
	for(i=0;i<*len;++i){
		if(in[i]>inout[i])
			inout[i]=in[i]+log(1+exp(inout[i]-in[i]));
		else
			inout[i]+=log(1+exp(in[i]-inout[i]));
	}
	return;
}

double gasdev(long *idum){
	
	static int iset=0;
	static double gset;
	double fac,rsq,v1,v2;
	
	if  (iset == 0) {
		do {
			v1=2.0*ran3(idum)-1.0;
			v2=2.0*ran3(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} else {
		iset=0;
		return gset;
	}
}

/*********************************************************************************************
 * In this routine I rescale the distances of the patches in the right position with respect
 * the CA atom, because rotation routine introduces a small numerical error that propagates.  
 * *******************************************************************************************/

void Rescaler (void){
	
	int pr=0,i=0,prt=0,j=0,k=0;
	double dx=0,dy=0,dz=0,r2=0,r=0;
	double Etouch=0,EtouchO=0;
	int indice_kCA=0;
	
	fprintf(out,"################## Rescaling %lu\n",icycl);fflush(out);
		
	for(pr=0;pr<NPROT;pr++){
		for(k=0;k<Plength[pr];k++){
			if(prot[pr].ammino[k].id==ATOM_H){
				indice_kCA=floor(k/NATOM)*NATOM;
				
				dx=(prot[pr].ammino[indice_kCA].x-prot[pr].ammino[k].x);
				dy=(prot[pr].ammino[indice_kCA].y-prot[pr].ammino[k].y);
				dz=(prot[pr].ammino[indice_kCA].z-prot[pr].ammino[k].z);
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				r2=(dx*dx+ dy*dy + dz*dz);
				r=sqrt(r2);
				if(fabs(r-Rmin[prot[pr].ammino[indice_kCA].residue])>1e-10){
					fprintf(out,"H-CA rescale %lu %d %30.20lf \n",icycl,k,r-Rmin[prot[pr].ammino[indice_kCA].residue]);fflush(out);
					prot[pr].ammino[k].x-=prot[pr].ammino[indice_kCA].x;
					prot[pr].ammino[k].y-=prot[pr].ammino[indice_kCA].y;
					prot[pr].ammino[k].z-=prot[pr].ammino[indice_kCA].z;
					
					prot[pr].ammino[k].x*=Rmin[prot[pr].ammino[indice_kCA].residue]/r;
					prot[pr].ammino[k].y*=Rmin[prot[pr].ammino[indice_kCA].residue]/r;
					prot[pr].ammino[k].z*=Rmin[prot[pr].ammino[indice_kCA].residue]/r;
					
					prot[pr].ammino[k].x+=prot[pr].ammino[indice_kCA].x;
					prot[pr].ammino[k].y+=prot[pr].ammino[indice_kCA].y;
					prot[pr].ammino[k].z+=prot[pr].ammino[indice_kCA].z;
					
					dx=(prot[pr].ammino[indice_kCA].x-prot[pr].ammino[k].x);
					dy=(prot[pr].ammino[indice_kCA].y-prot[pr].ammino[k].y);
					dz=(prot[pr].ammino[indice_kCA].z-prot[pr].ammino[k].z);
					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					r2=(dx*dx+ dy*dy + dz*dz);
					r=sqrt(r2);
					if(fabs(r-Rmin[prot[pr].ammino[indice_kCA].residue])>1e-10){
						fprintf(out,"H-CA wrong %lu %d %30.20lf \n",icycl,k,r-sist->Bbond[1]);fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
					}
				}
			}
			
			if((isnan(prot[pr].ammino[k].x))||(isnan(1./prot[pr].ammino[k].x))){
				fprintf(out,"Coord NAN %lu after Rescaler prot[%d].ammino[%d].x=%lf\n",icycl,pr,k,prot[pr].ammino[k].x);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			if((isnan(prot[pr].ammino[k].y))||(isnan(1./prot[pr].ammino[k].y))){
				fprintf(out,"Coord NAN %lu after Rescaler prot[%d].ammino[%d].y=%lf\n",icycl,pr,k,prot[pr].ammino[k].y);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			if((isnan(prot[pr].ammino[k].z))||(isnan(1./prot[pr].ammino[k].z))){
				fprintf(out,"Coord NAN %lu after Rescaler prot[%d].ammino[%d].z=%lf\n",icycl,pr,k,prot[pr].ammino[k].z);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
		}
	}
	fprintf(out,"################## Rescaling ok %lu\n",icycl);fflush(out);
	
	return;
}

void Inflate (void){
	
	int pr=0,i=0,prt=0,j=0,k=0;
	double dx=0,dy=0,dz=0,r2=0,r=0;
	if(Rmin_flag==1){
		MINR=1000.0;
		fprintf(out,"INFLATE %d %lf %lf\n",Rmin_flag,MINR,sist->Rmin);
		fflush(out);
		for(pr=0;pr<NPROT;pr++){
			for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
				for(prt=0;prt<NPROT;prt++){
					for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
						//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
						
						//if(icycl > 0) fprintf(out,"##%d###%d %d %d %d\n",k,pr,i,prt,j);
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
						
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
						
						r2=(dx*dx+ dy*dy + dz*dz);
						if((prot[pr].ammino[i].id==ATOM_CA)&&(prot[prt].ammino[j].id==ATOM_CA)){
							if(pr==prt){
								if((j != i+NATOM)&&(j != i)&&(j != i-NATOM)&&(MINR>r2)) MINR=r2; 
							}else{
								if(MINR>r2) MINR=r2; 
							}
							
						}
					}
				}
			}
			
		}
		sist->Rmin=sqrt(MINR);
		fprintf(out,"INFLATE %d %lf %lf ",Rmin_flag,MINR,sist->Rmin);
		fflush(out);
		//printf("INFLATE %d %lf %lf\n",Rmin_flag,MINR,sist->Rmin);
		//fflush(NULL);
		if(sist->Rmin>=sist->Rmin_CAsaw){
			sist->Rmin=sist->Rmin_CAsaw;
			Rmin_flag=0;
		}
		fprintf(out," final choice %lf\n",sist->Rmin);
		fflush(out);
		//if(ECA_Range+5.0>12.0){
			
			/*12=7(H-bond range) + 2(1+1 max distance O-Ca N-CA) + 3(safety region) is to make shure that even for 
			short range interactions the Hydrogen bonds are still taken into account*/
			
			sist->Rint=ECA_Range+5.0;
			sist->Rmax=sist->Rmin;
			sist->RintD=20.0;
			sist->RintD2=20.0*20.0;
			//}else{
			//sist->Rint=12.0;
			//sist->Rmax=sist->Rmin;
			//}
		
		//sist->Rint=ECA_Range;
		/*is divided by 2.0 because the maximum variation for a move without update is 2*Rvgap (one per particle) and this should 
		not bring particle close enough to interact so 2*Rvgap=Rver-Rint*/
		//fprintf(out,"%lf %lf %lf %lf\n",sist->Rint,sist->Rver,sist->Rmax,sist->Rvgap);
		
		
		
		sist->Rmin2=sist->Rmin*sist->Rmin;
		sist->Rmin_CAsaw2=sist->Rmin_CAsaw*sist->Rmin_CAsaw;
		sist->Rint2=sist->Rint*sist->Rint;
		
		
		sist->Rmax2=sist->Rmax*sist->Rmax;
		
		
		LJ2_a=sist->Rmin;
		LJ2_c=ECA_Range;
		
		//LJ2_b=(exp((LJ2_a-LJ2_c)/2.0)+1.0);
		//LJ2_b*=LJ2_b;
		//printf("exp(%lf-x) + %lf/(1.0 + exp((%lf - x))) - %lf\n",LJ2_a,LJ2_b,LJ2_c,LJ2_b);
		
		
		LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));
		fprintf(out,"%lf/(1.0 + exp((%lf - x))) - %lf\n",LJ2_b,LJ2_c,LJ2_b);
		
		fflush(out);
		/*sist->Rmin6=pow(sist->Rmin2,LJpower);
		sist->Rmin12=sist->Rmin6*sist->Rmin6;*/
		
		sigma6=pow(sist->Rmin,6.0);
		
		
		MINR=1000.0;
	}
}

double Bond_Energy_bw (int pr,int input_k){
	
	
	/************
	Uses The CA as the reference for and always computes the Bending energy with respec to the
	previous residue.
	
	**********/
	
	double dx,dy,dz;
	
	double r;
	int indice_a,indice_b,indice;
	
	double ener=0.0;
	
	
	#ifdef TUBE
	if(input_k-NATOM>=0){
	  indice_b=input_k-NATOM;
	  dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
	  dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
	  dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
	  	  
	  r=sqrt(dx*dx+dy*dy+dz*dz);
	  
	  if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
	    
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
	    fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	  
	  sist->Bbond[5]=3.8;
	  ener=(r-sist->Bbond[5])*(r-sist->Bbond[5])+100*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	}
	return(ener);
	#endif
	
	
	if(SPRING_MODEL==1) {
		
		if(input_k-NATOM>=0){
			indice_a=input_k+1;
			indice_b=input_k-NATOM+SPG_PATCH2;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;
			
			
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			if(r>Bond_extension) ener=Bond_factor*(r-Bond_extension)*(r-Bond_extension);
			
			if((isnan(ener))||(isnan(1./ener))){
				fprintf(out,"Bond_Energy_bw non ok icycl=%lu, ener=%lf r=%lf, indice_a=%d indice_b=%d Rmin[res[0]]=%lf Rmin[res[1]]=%lf res[0]=%d res[1]=%d\n",icycl,ener,r,indice_a,indice_b,Rmin[prot[pr].ammino[input_k].residue],Rmin[prot[pr].ammino[input_k-NATOM].residue],prot[pr].ammino[input_k].residue,prot[pr].ammino[input_k-NATOM].residue);
				
				
				
				fprintf(out,"A_x=%lf B_x=%lf\n",prot[pr].ammino[indice_a].x,prot[pr].ammino[indice_b].x);
				fprintf(out,"A_y=%lf B_y=%lf\n",prot[pr].ammino[indice_a].y,prot[pr].ammino[indice_b].y);
				fprintf(out,"A_z=%lf B_z=%lf\n",prot[pr].ammino[indice_a].z,prot[pr].ammino[indice_b].z);
				
				
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
		}
		
		
		
		
	}else{
		if(input_k-NATOM>=0){
			indice_b=input_k-NATOM;
			dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
			
			
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			
			if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
				
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
			
			ener=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
		}
	}
	
	return(ener);
}

double Bond_Energy_fw (int pr,int input_k){
	
	
	/************
	Uses The CA as the reference for and always computes the Bending energy with respec to the
	next residue.
	
	**********/
	
	double dx,dy,dz;
	
	double r;
	int indice_a,indice_b;
	
	double ener=0.0;
	
	
	#ifdef TUBE
	if(input_k+NATOM<Plength[pr]){
	  indice_b=input_k+NATOM;
	  dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
	  dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
	  dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
	  	  
	  r=sqrt(dx*dx+dy*dy+dz*dz);
	  
	  if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
	    
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
	    fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	  
	  sist->Bbond[5]=3.8;
	  ener=(r-sist->Bbond[5])*(r-sist->Bbond[5])+100*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	}
	return(ener);
	#endif
	
	
	if(SPRING_MODEL==1) {
		
		if(input_k+NATOM<Plength[pr]){
		  indice_a=input_k+SPG_PATCH2; /// from the last patch of the input_k bead to the first patch of the next bead
			indice_b=input_k+NATOM+1;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;
			
			
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			//fprintf(out,"pr=%d indice_a=%d indice_b=%d r=%lf\n",pr,indice_a,indice_b,r);fflush(out);
			if(r>Bond_extension) ener=Bond_factor*(r-Bond_extension)*(r-Bond_extension);
			
		}
		
		
		
		
	}else{
		if(input_k+NATOM<Plength[pr]){
			indice_b=input_k+NATOM;
			dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
			
			
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			
			
			if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
				
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
			ener=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
		}
	}
	
	return(ener);
}

double Bond_Energy_SP (int pr,int input_k){
	
	
	/************
	Uses The CA as the reference for and always computes the Bending energy with respec to the
	next residue.
	
	**********/
	
	double dx,dy,dz;
	
	double r;
	int indice_a,indice_b;
	
	double ener=0.0;
	
	
	#ifdef TUBE
	if(input_k+NATOM<Plength[pr]){
	  indice_b=input_k+NATOM;
	  dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
	  dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
	  dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
	  	  
	  r=sqrt(dx*dx+dy*dy+dz*dz);
	  
	  if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
	    
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
	    fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	  
	  sist->Bbond[5]=3.8;
	  ener+=(r-sist->Bbond[5])*(r-sist->Bbond[5])+100*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	}
	if(input_k-NATOM>=0){
	  indice_b=input_k-NATOM;
	  dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
	  dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
	  dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
	  	  
	  r=sqrt(dx*dx+dy*dy+dz*dz);
	  
	  if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
	    fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
	    
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
	    fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
	    fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	  
	  sist->Bbond[5]=3.8;
	  ener+=(r-sist->Bbond[5])*(r-sist->Bbond[5])+100*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	}
	return(ener);
	#endif
	
	
	if(SPRING_MODEL==1) {
		
		if(input_k+NATOM<Plength[pr]){
			indice_a=input_k+SPG_PATCH2;
			indice_b=input_k+NATOM+1;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			if(r>Bond_extension) ener+=Bond_factor*(r-Bond_extension)*(r-Bond_extension);
		}
		
		if(input_k-NATOM>=0){
			indice_a=input_k+1;
			indice_b=input_k-NATOM+SPG_PATCH2;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			if(r>Bond_extension) ener+=Bond_factor*(r-Bond_extension)*(r-Bond_extension);
		}
		
		
		
		
	}else{
		if(input_k+NATOM<Plength[pr]){
			indice_b=input_k+NATOM;
			dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			
			if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
				
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
			ener+=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
		}
		
		if(input_k-NATOM>=0){
			indice_b=input_k-NATOM;
			dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
			
			r=sqrt(dx*dx+dy*dy+dz*dz);
			
			if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
				fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
				
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,input_k,Rmin[prot[pr].ammino[input_k].residue]);
				fprintf(out,"Rmin[prot[%d].ammino[%d].residue]=%lf\n",pr,indice_b,Rmin[prot[pr].ammino[indice_b].residue]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
			ener+=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
		}
	}
	
	return(ener);
}

void new_cell_list (void){
	
	
	unsigned long int indice=0;
	int i=0,j=0,k=0;
	int icel_x=0,icel_y=0,icel_z=0,pr=0;
	int ppr=0,ii=0;
	
	fprintf(out,"new_cell_list N_CA_cells==%d cell_CA_size=%lf BoxSize=%lf\nperiod_factor_CA=%lf`n",N_CA_cells,cell_CA_size,BoxSize,period_factor_CA);fflush(out);
	fprintf(out,"new_cell_list N_H_cells==%d cell_H_size=%lf\n",N_H_cells,cell_H_size);fflush(out);
	if(hoc_CA==NULL) hoc_CA=(unsigned int*) calloc (2*N_CA_cells*N_CA_cells*N_CA_cells,sizeof(unsigned int));
	if(hoc_H==NULL) hoc_H=(unsigned int*) calloc (2*N_H_cells*N_H_cells*N_H_cells,sizeof(unsigned int));
	
	
	fprintf(out,"new_cell_list ok 1\n");fflush(out);
	for(indice=0;indice<(unsigned int)(2*N_CA_cells*N_CA_cells*N_CA_cells);indice++){
		hoc_CA[indice]=0;
	}
	fprintf(out,"new_cell_list ok 2\n");fflush(out);
	for(indice=0;indice<(unsigned int)(2*N_H_cells*N_H_cells*N_H_cells);indice++){
		hoc_H[indice]=0;
	}
	fprintf(out,"new_cell_list ok 3\n");fflush(out);
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			prot[pr].ammino[i].lfw_pr=prot[pr].ammino[i].lfw_i=-1;
		}
	}
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			if(prot[pr].ammino[i].spring_anchor!=1){
				
				switch(prot[pr].ammino[i].id){ /// CA or H
					
					/*ATOMI CA*/
					case ATOM_CA:
					icel_x=floor(prot[pr].ammino[i].x/cell_CA_size);
					icel_y=floor(prot[pr].ammino[i].y/cell_CA_size);
					icel_z=floor(prot[pr].ammino[i].z/cell_CA_size);
					
					icel_x=P_Cell(icel_x,N_CA_cells);
					icel_y=P_Cell(icel_y,N_CA_cells);
					icel_z=P_Cell(icel_z,N_CA_cells);
					
					indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
					ppr=hoc_CA[indice]-1;ii=hoc_CA[indice+1]-1;
					fprintf(out,"New list CA pr=%d i=%d\nppr=%d ii=%d\nicel_x=%d icel_y=%d icel_z=%d\n",pr,i,ppr,ii,icel_x,icel_y,icel_z);fflush(out);
					
					prot[pr].ammino[i].lbw_pr=ppr;
					prot[pr].ammino[i].lbw_i=ii;
					if((ppr!=-1)&&(ii!=-1)){
						prot[ppr].ammino[ii].lfw_pr=pr;
						prot[ppr].ammino[ii].lfw_i=i;
					}
					hoc_CA[indice]=pr+1;
					hoc_CA[indice+1]=i+1;
					prot[pr].ammino[i].indice=indice;
					break;
					/*ATOMI H*/
					case ATOM_H:
		
					icel_x=floor(prot[pr].ammino[i].x/cell_H_size);
					icel_y=floor(prot[pr].ammino[i].y/cell_H_size);
					icel_z=floor(prot[pr].ammino[i].z/cell_H_size);
					
					icel_x=P_Cell(icel_x,N_H_cells);
					icel_y=P_Cell(icel_y,N_H_cells);
					icel_z=P_Cell(icel_z,N_H_cells);
					
					indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
					ppr=hoc_H[indice]-1;ii=hoc_H[indice+1]-1;
					fprintf(out,"New list H pr=%d i=%d\nppr=%d ii=%d\nicel_x=%d icel_y=%d icel_z=%d\n",pr,i,ppr,ii,icel_x,icel_y,icel_z);fflush(out);
					prot[pr].ammino[i].lbw_pr=ppr;
					prot[pr].ammino[i].lbw_i=ii;
					if((ppr!=-1)&&(ii!=-1)){
						prot[ppr].ammino[ii].lfw_pr=pr;
						prot[ppr].ammino[ii].lfw_i=i;
					}
					hoc_H[indice]=pr+1;
					hoc_H[indice+1]=i+1;
					prot[pr].ammino[i].indice=indice;
					break;
					
					
				}
			}	
		}
	}
	fprintf(out,"new_cell_list ok 5\n");fflush(out);
	//	MPI_Abort(MPI_COMM_WORLD,err5);
	return;
}

void new_cell_list_SAW (void){
	
	/// Cell list of the hard core interaction of the Carbon atoms
	
	unsigned long int indice=0;
	int icel_x=0,icel_y=0,icel_z=0;
	int ppr=0,ii=0,i=0,j=0,k=0,pr=0;
	
	if(hoc_SAW==NULL){
		hoc_SAW=(unsigned int*) calloc (2*N_SAW_cells3,sizeof(unsigned int));
	}
	fprintf(out,"new_cell_list_SAW ok 1\n");fflush(out);
	for(indice=0;indice<(unsigned int)(2*N_SAW_cells3);indice++){
		hoc_SAW[indice]=0;
	}
	fprintf(out,"new_cell_list_SAW ok 2\n");fflush(out);
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].lfw_SAW_pr=prot[pr].ammino[i].lfw_SAW_i=-1;
		}
	}
	fprintf(out,"new_cell_list_SAW ok 3\n");fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size);
			icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size);
			icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size);
			
			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
			ppr=hoc_SAW[indice]-1;
			ii=hoc_SAW[indice+1]-1;
			prot[pr].ammino[i].lbw_SAW_pr=ppr;
			prot[pr].ammino[i].lbw_SAW_i=ii;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lfw_SAW_pr=pr;
				prot[ppr].ammino[ii].lfw_SAW_i=i;
			}
			hoc_SAW[indice]=pr+1;
			hoc_SAW[indice+1]=i+1;
			prot[pr].ammino[i].indice_SAW=indice;	
		}
	}
	fprintf(out,"new_cell_list_SAW ok 4\n");fflush(out);
	return;
	
}

void updt_cell_list (int pr, int i){
	
	
	
	unsigned long int indice=0,indice_old=0;
	int j=0,k=0;
	int icel_x=0,icel_y=0,icel_z=0;
	int ppr=0,ii=0;
	
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			
			
			//if(icycl>0){fprintf(out,"Update cell particella CA precedente\n");fflush(out);}
			
			/*Aggiorna la catena della particella precedente*/
			indice_old=prot[pr].ammino[i].indice;	
			ppr=prot[pr].ammino[i].lbw_pr; //pallina precedente
			ii=prot[pr].ammino[i].lbw_i;
			//if(icycl>0){fprintf(out,"Update cell particella CA precedente ppr=%d ii=%d\n",ppr,ii);fflush(out);}
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lfw_pr=prot[pr].ammino[i].lfw_pr;
				prot[ppr].ammino[ii].lfw_i= prot[pr].ammino[i].lfw_i; // se la pallina non e' la prima
			}else{
				if((prot[pr].ammino[i].lfw_pr==-1)&&(prot[pr].ammino[i].lfw_i==-1)){
					hoc_CA[indice_old]=hoc_CA[indice_old+1]=0; // se la pallina e' l'unica nella cella
				}else{
					ppr=prot[pr].ammino[i].lfw_pr; // se la pallina e' la prima nella lista nella cella, ma non l'unica, allora 
					ii=prot[pr].ammino[i].lfw_i;   // inizializza il link posteriore (lbw) della pallina successiva a -1
					// in modo che l'inzio della lista si aggiornato
					prot[ppr].ammino[ii].lbw_pr=-1;
					prot[ppr].ammino[ii].lbw_i=-1;
				}
			}
			//if(icycl>0){fprintf(out,"Update cell particella CA successiva\n");fflush(out);}
			ppr=prot[pr].ammino[i].lfw_pr; //pallina successiva
			ii=prot[pr].ammino[i].lfw_i;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lbw_pr=prot[pr].ammino[i].lbw_pr;
				prot[ppr].ammino[ii].lbw_i= prot[pr].ammino[i].lbw_i; // se la pallina  non e' l'ultima
			}else{
				if((prot[pr].ammino[i].lbw_pr==-1)&&(prot[pr].ammino[i].lbw_i==-1)){
					hoc_CA[indice_old]=hoc_CA[indice_old+1]=0; // se la pallina e' l'unica nella cella
				}else{
					hoc_CA[indice_old]=prot[pr].ammino[i].lbw_pr+1;
					hoc_CA[indice_old+1]=prot[pr].ammino[i].lbw_i+1;
					
					ppr=prot[pr].ammino[i].lbw_pr; // se la pallina e' l'ultima nella lista nella cella, ma non l'unica, allora 
					ii=prot[pr].ammino[i].lbw_i;   // inizializza il link successivo (lfw) della pallina precedente a -1
					// in modo che la lista sia aggiornata
					
					prot[ppr].ammino[ii].lfw_pr=-1;
					prot[ppr].ammino[ii].lfw_i=-1;
				}
			}
			
			// aggiungiamo la pallina lla nuova lista
			
			//if(icycl>0){fprintf(out,"Update cell particella CA NEW cel\n");fflush(out);}
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size);
			icel_y=(prot[pr].ammino[i].y/cell_CA_size);
			icel_z=(prot[pr].ammino[i].z/cell_CA_size);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
			
			icel_x=floor(prot[pr].ammino[i].x/cell_CA_size);
			icel_y=floor(prot[pr].ammino[i].y/cell_CA_size);
			icel_z=floor(prot[pr].ammino[i].z/cell_CA_size);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			
			indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
			ppr=hoc_CA[indice]-1;ii=hoc_CA[indice+1]-1;
			prot[pr].ammino[i].lbw_pr=ppr;
			prot[pr].ammino[i].lbw_i=ii;
			prot[pr].ammino[i].lfw_pr=-1;
			prot[pr].ammino[i].lfw_i=-1;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lfw_pr=pr;
				prot[ppr].ammino[ii].lfw_i=i;
			}
			hoc_CA[indice]=pr+1;
			hoc_CA[indice+1]=i+1;
			prot[pr].ammino[i].indice=indice;	
			//if(icycl>0){fprintf(out,"Update cell particella CA OK\n");fflush(out);}
			break;
			
			/*ATOMI H*/
			
			
			
			case ATOM_H:
			/*Aggiorna la catena della particella precedente*/
			//	if(icycl>0){fprintf(out,"Update cell particella H precedente\n");fflush(out);}
			indice_old=prot[pr].ammino[i].indice;	
			ppr=prot[pr].ammino[i].lbw_pr; //pallina precedente
			ii=prot[pr].ammino[i].lbw_i;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lfw_pr=prot[pr].ammino[i].lfw_pr;
				prot[ppr].ammino[ii].lfw_i= prot[pr].ammino[i].lfw_i; // se la pallina non e' la prima
			}else{
				if((prot[pr].ammino[i].lfw_pr==-1)&&(prot[pr].ammino[i].lfw_i==-1)){
					hoc_H[indice_old]=hoc_H[indice_old+1]=0; // se la pallina e' l'unica nella cella
				}else{
					ppr=prot[pr].ammino[i].lfw_pr; // se la pallina e' la prima nella lista nella cella, ma non l'unica, allora 
					ii=prot[pr].ammino[i].lfw_i;   // inizializza il link posteriore (lbw) della pallina successiva a -1
					// in modo che l'inzio della lista si aggiornato
					prot[ppr].ammino[ii].lbw_pr=-1;
					prot[ppr].ammino[ii].lbw_i=-1;
				}
			}
			//if(icycl>0){fprintf(out,"Update cell particella H successiva\n");fflush(out);}
			ppr=prot[pr].ammino[i].lfw_pr; //pallina successiva
			ii=prot[pr].ammino[i].lfw_i;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lbw_pr=prot[pr].ammino[i].lbw_pr;
				prot[ppr].ammino[ii].lbw_i= prot[pr].ammino[i].lbw_i; // se la pallina  non e' l'ultima
			}else{
				if((prot[pr].ammino[i].lbw_pr==-1)&&(prot[pr].ammino[i].lbw_i==-1)){
					hoc_H[indice_old]=hoc_H[indice_old+1]=0; // se la pallina e' l'unica nella cella
				}else{
					hoc_H[indice_old]=prot[pr].ammino[i].lbw_pr+1;
					hoc_H[indice_old+1]=prot[pr].ammino[i].lbw_i+1;
					
					ppr=prot[pr].ammino[i].lbw_pr; // se la pallina e' l'ultima nella lista nella cella, ma non l'unica, allora 
					ii=prot[pr].ammino[i].lbw_i;   // inizializza il link successivo (lfw) della pallina precedente a -1
					// in modo che la lista sia aggiornata
					
					prot[ppr].ammino[ii].lfw_pr=-1;
					prot[ppr].ammino[ii].lfw_i=-1;
				}
			}
			
			// aggiungiamo la pallina lla nuova lista
			
			//if(icycl>0){fprintf(out,"Update cell particella H new\n");fflush(out);}
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size);
			icel_y=(prot[pr].ammino[i].y/cell_H_size);
			icel_z=(prot[pr].ammino[i].z/cell_H_size);
			
			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=floor(prot[pr].ammino[i].x/cell_H_size);
			icel_y=floor(prot[pr].ammino[i].y/cell_H_size);
			icel_z=floor(prot[pr].ammino[i].z/cell_H_size);
			
			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
			
			indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
			ppr=hoc_H[indice]-1;ii=hoc_H[indice+1]-1;
			prot[pr].ammino[i].lbw_pr=ppr;
			prot[pr].ammino[i].lbw_i=ii;
			prot[pr].ammino[i].lfw_pr=-1;
			prot[pr].ammino[i].lfw_i=-1;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lfw_pr=pr;
				prot[ppr].ammino[ii].lfw_i=i;
			}
			hoc_H[indice]=pr+1;
			hoc_H[indice+1]=i+1;
			prot[pr].ammino[i].indice=indice;	
			//if(icycl>0){fprintf(out,"Update cell particella H Ok\n");fflush(out);}
		}
		
		
	}
	return;
	
}

void updt_cell_list_SAW (int pr, int i){
	
	
	
	unsigned long int indice=0,indice_old=0;
	int j=0,k=0;
	int icel_x=0,icel_y=0,icel_z=0;
	int ppr=0,ii=0,prt=0,jj=0;
	
	
	if(prot[pr].ammino[i].id==ATOM_CA){
		
		//if(icycl>0){fprintf(out,"Update cell particella precedente\n");fflush(out);}
		
		/*Aggiorna la catena della particella precedente*/
		indice_old=prot[pr].ammino[i].indice_SAW;
		ppr=prot[pr].ammino[i].lbw_SAW_pr; //pallina precedente
		ii=prot[pr].ammino[i].lbw_SAW_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_SAW_pr=prot[pr].ammino[i].lfw_SAW_pr;
			prot[ppr].ammino[ii].lfw_SAW_i= prot[pr].ammino[i].lfw_SAW_i; // se la pallina non e' la prima
		}else{
			if((prot[pr].ammino[i].lfw_SAW_pr==-1)&&(prot[pr].ammino[i].lfw_SAW_i==-1)){
				hoc_SAW[indice_old]=hoc_SAW[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				ppr=prot[pr].ammino[i].lfw_SAW_pr; // se la pallina e' la prima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lfw_SAW_i;   // inizializza il link posteriore (lbw) della pallina successiva a -1
				// in modo che l'inzio della lista si aggiornato
				prot[ppr].ammino[ii].lbw_SAW_pr=-1;
				prot[ppr].ammino[ii].lbw_SAW_i=-1;
			}
		}
		//if(icycl>0){fprintf(out,"Update cell particella precedente OK \n");fflush(out);}
		//if(icycl>0){fprintf(out,"Update cell particella successiva\n");fflush(out);}
		ppr=prot[pr].ammino[i].lfw_SAW_pr; //pallina successiva
		ii=prot[pr].ammino[i].lfw_SAW_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lbw_SAW_pr=prot[pr].ammino[i].lbw_SAW_pr;
			prot[ppr].ammino[ii].lbw_SAW_i= prot[pr].ammino[i].lbw_SAW_i; // se la pallina  non e' l'ultima
		}else{
			if((prot[pr].ammino[i].lbw_SAW_pr==-1)&&(prot[pr].ammino[i].lbw_SAW_i==-1)){
				hoc_SAW[indice_old]=hoc_SAW[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				hoc_SAW[indice_old]=prot[pr].ammino[i].lbw_SAW_pr+1;
				hoc_SAW[indice_old+1]=prot[pr].ammino[i].lbw_SAW_i+1;
				
				ppr=prot[pr].ammino[i].lbw_SAW_pr; // se la pallina e' l'ultima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lbw_SAW_i;   // inizializza il link successivo (lfw) della pallina precedente a -1
				// in modo che la lista sia aggiornata
				
				prot[ppr].ammino[ii].lfw_SAW_pr=-1;
				prot[ppr].ammino[ii].lfw_SAW_i=-1;
			}
		}
		//if(icycl>0){fprintf(out,"Update cell particella successiva OK\n");fflush(out);}
		// aggiungiamo la pallina lla nuova lista
		//if(icycl>0){fprintf(out,"Update cell New cell\n");fflush(out);}
		icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size);
		icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size);
		icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size);
		
		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		indice=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
		ppr=hoc_SAW[indice]-1;ii=hoc_SAW[indice+1]-1;
		//if(icycl>0){fprintf(out,"Update cell New cell OK1\n");fflush(out);}
		prot[pr].ammino[i].lbw_SAW_pr=ppr;
		prot[pr].ammino[i].lbw_SAW_i=ii;
		prot[pr].ammino[i].lfw_SAW_pr=-1;
		prot[pr].ammino[i].lfw_SAW_i=-1;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_SAW_pr=pr;
			prot[ppr].ammino[ii].lfw_SAW_i=i;
		}
		hoc_SAW[indice]=pr+1;
		hoc_SAW[indice+1]=i+1;
		prot[pr].ammino[i].indice_SAW=indice;	
		//if(icycl>0){fprintf(out,"Update cell New cell OK2\n");fflush(out);}
	}
	
	
	#ifdef 	CLIST_TEST
	
	for(indice=0;indice<(unsigned int)(N_SAW_cells3);indice++){
		
		prt=hoc_SAW[2*indice]-1;
		j=hoc_SAW[2*indice+1]-1;
		
		//fprintf(out,"TEST 2  pr=%d i=%d\n",pr,i);fflush(out);
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(j>=0){
			
			if((prot[pr].ammino[i].indice_SAW!=2*indice)&&(prt==pr)&&(j==i)){
				fprintf(out,"TEST updt_cell_list_SAW at %d failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",mossaid,pr,i,prot[pr].ammino[i].indice_SAW,2*indice);fflush(out);
				fprintf(out,"x=%lf y=%lf z=%fl\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
				icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size);
				icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size);
				icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size);
				fprintf(out,"icel_x=%d icel_y=%d icel_z=%d\n",icel_x,icel_y,icel_z);
				icel_x=P_Cell(icel_x,N_SAW_cells);
				icel_y=P_Cell(icel_y,N_SAW_cells);
				icel_z=P_Cell(icel_z,N_SAW_cells);
				fprintf(out,"P icel_x=%d icel_y=%d icel_z=%d\n",icel_x,icel_y,icel_z);
				fflush(out);
				fsamp();
				MPI_Abort(MPI_COMM_WORLD,err5);
			}	
			
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	#endif
	return;
	
}

void test_celllist_H (void){
	
	
	unsigned long int indice=0,indice_old=0,indice2=0;
	int j=0,k=0,pprt=0,jjj=0;
	int icel_x=0,icel_y=0,icel_z=0;
	int ppr=0,ii=0;
	int celllist_H_test=0,pr=0,prt=0,i=0,jj=0,test5=0;
	double dx=0,dy=0,dz=0,rCA2=0;
	
	// TEST_H 1 connected chains
	//fprintf(out,"TEST_H 1\n");fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_H;i<Plength[pr];i++){
			if(prot[pr].ammino[i].spring_anchor!=1){
				if(prot[pr].ammino[i].id==ATOM_H){
					//fprintf(out,"TEST_H 1  pr=%d i=%d\n",pr,i);fflush(out);
					ppr=prot[pr].ammino[i].lbw_pr;
					ii=prot[pr].ammino[i].lbw_i;
					//fprintf(out,"TEST_H 1  ppr=%d ii=%d\n",ppr,ii);fflush(out);
					if((ppr!=-1)&&(ii!=-1)){
						if((prot[ppr].ammino[ii].lfw_pr!=pr)||(prot[ppr].ammino[ii].lfw_i!=i)){
							fprintf(out,"TEST_H 1 celllist SAW failed pr=%d i=%d ppr=%d ii=%d\n prot[ppr].ammino[ii].lfw_pr=%d prot[ppr].ammino[ii].lfw_i=%d\n",pr,i,ppr,ii,prot[ppr].ammino[ii].lfw_pr,prot[ppr].ammino[ii].lfw_i);fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
						
						
					}
				}
			}
		}
		
	}
	//fprintf(out,"TEST_H 2 and 3\n");fflush(out);
	//TEST_H 2 all particles are in a cell
	//TEST_H 3 all particles are in the cocrrect cell
	for(indice=0;indice<(unsigned int)(N_H_cells*N_H_cells*N_H_cells);indice++){
		
		pr=hoc_H[2*indice]-1;
		i=hoc_H[2*indice+1]-1;
		
		//fprintf(out,"TEST_H 2  pr=%d i=%d\n",pr,i);fflush(out);
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(i>=0){
			
			if(prot[pr].ammino[i].indice!=2*indice){
				fprintf(out,"TEST_H 3a failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",pr,i,prot[pr].ammino[i].indice,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size);
			icel_y=(prot[pr].ammino[i].y/cell_H_size);
			icel_z=(prot[pr].ammino[i].z/cell_H_size);
			
			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=floor(prot[pr].ammino[i].x/cell_H_size);
			icel_y=floor(prot[pr].ammino[i].y/cell_H_size);
			icel_z=floor(prot[pr].ammino[i].z/cell_H_size);
			
			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
			
			indice2=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
			
			if(indice2!=2*indice){
				fprintf(out,"TEST_H 3b failed misplaced particle pr=%d i=%d\n indice2=%lu indice=%lu\n",pr,i,2*indice2,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}		
			
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	//fprintf(out,"TEST_H 4\n");fflush(out);
	// TEST_H4 no duplicated particles
	for(indice=0;indice<(unsigned int)(N_H_cells*N_H_cells*N_H_cells);indice++){
		
		pr=hoc_H[2*indice]-1;
		i=hoc_H[2*indice+1]-1;
		
		
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(i>=0){
			celllist_H_test=0;
			for(indice2=0;indice2<(unsigned int)(N_H_cells*N_H_cells*N_H_cells);indice2++){
				prt=hoc_H[2*indice2]-1;
				j=hoc_H[2*indice2+1]-1;
				while(j>=0){
					
					if((prt==pr)&&(i==j)) {
						celllist_H_test++;
						if(celllist_H_test>1) fprintf(out,"TEST_H 4 failed particle pr=%d i=%d duplicated %d times prt=%d j=%d\n",pr,i,celllist_H_test,prt,j);
					}
					
					
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					/*if(rCA2<sist->Rint2){
						prot[pr].ammino[i].verlpr[Nver]=prt;
						prot[pr].ammino[i].verli[Nver]=j;
						prot[pr].ammino[i].Nverl=Nver;
						Nver++;
					}*/
				}
			}	
			if(celllist_H_test>1) {
				fprintf(out,"TEST_H 4 failed particle pr=%d i=%d duplicated %d times\n",pr,i,celllist_H_test);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	// TEST_H5 no duplicated particles
	for(pr=0;pr<NPROT;pr++){
		for (i=0;i<Plength[pr];i++){
			if((prot[pr].ammino[i].id==ATOM_H)&&(prot[pr].ammino[i].spring_anchor!=1)){
				for(prt=0;prt<NPROT;prt++){
					for (j=0;j<Plength[prt];j++){
						if((prot[prt].ammino[j].id==ATOM_H)&&(prot[prt].ammino[j].spring_anchor!=1)){
							
							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
							
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							
							rCA2=(dx*dx+ dy*dy + dz*dz);
							
							if (rCA2<sist->RintH2){
								
								test5=0;		
								for(indice2=0;indice2<Neigh;indice2++){
									//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
									/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
									icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
									icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
									
									//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
									
									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);*/
									
									icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
									icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
									icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
									
									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);
									
									if((icel_x>=N_H_cells)||(icel_y>=N_H_cells)||(icel_z>=N_H_cells)){
										fprintf(out,"TEST_H 5 icel too big\n");
										fprintf(out,"TEST_H 5 N_H_cells=%d x=%lf y=%lf z=%lf\n",N_H_cells,(prot[pr].ammino[i].x),(prot[pr].ammino[i].y),(prot[pr].ammino[i].z));fflush(out);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										icel_x=floor(prot[pr].ammino[i].x/cell_H_size);
										icel_y=floor(prot[pr].ammino[i].y/cell_H_size);
										icel_z=floor(prot[pr].ammino[i].z/cell_H_size);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										
										icel_x=P_Cell(icel_x,N_H_cells);
										icel_y=P_Cell(icel_y,N_H_cells);
										icel_z=P_Cell(icel_z,N_H_cells);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
										icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
										icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										icel_x=P_Cell(icel_x,N_H_cells);
										icel_y=P_Cell(icel_y,N_H_cells);
										icel_z=P_Cell(icel_z,N_H_cells);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										MPI_Abort(MPI_COMM_WORLD,err5);
										
									}
									//fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
									indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
									//if(indice==prot[prt].ammino[j].indice) test5++;
									pprt=hoc_H[indice]-1;
									jj=hoc_H[indice+1]-1;
									//if(indice==prot[prt].ammino[j].indice) test5++;
									while(jj>=0){
										if((jj==j)&&(pprt==prt))    test5++;
										//fprintf(out,"--->pprt=%d jj=%d\n",pprt,jj);fflush(out);
										jjj=prot[pprt].ammino[jj].lbw_i;
										pprt=prot[pprt].ammino[jj].lbw_pr;
										jj=jjj;
									}
								}
								if(test5!=1){
									fprintf(out,"TEST_H 5 failed particle prt=%d j=%d wrong neigh_cell of pr=%d i=%d\n  test5=%d Rinit=%lf N_H_cells=%d cell_H_size=%lf cell_H_factor=%lf\n",prt,j,pr,i,test5,sist->RintH,N_H_cells,cell_H_size,cell_H_factor);
									icel_x=floor(prot[pr].ammino[i].x/cell_H_size);
									icel_y=floor(prot[pr].ammino[i].y/cell_H_size);
									icel_z=floor(prot[pr].ammino[i].z/cell_H_size);
									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);
									fprintf(out,"I icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
									fprintf(out,"I %lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									icel_x=floor(prot[prt].ammino[j].x/cell_H_size);
									icel_y=floor(prot[prt].ammino[j].y/cell_H_size);
									icel_z=floor(prot[prt].ammino[j].z/cell_H_size);
									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);
									fprintf(out,"J icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
									fprintf(out,"J %lf %lf %lf\n",prot[pr].ammino[j].x,prot[pr].ammino[j].y,prot[pr].ammino[j].z);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
							}
							
						}
					}
				}
			}
		}
	}
	
	return;
	
}

void test_celllist_CA(void){
	
	
	unsigned long int indice=0,indice_old=0,indice2=0;
	int j=0,k=0,pprt=0,jjj=0;
	int icel_x=0,icel_y=0,icel_z=0;
	int ppr=0,ii=0;
	int celllist_CA_test=0,pr=0,prt=0,i=0,jj=0;
	int test5=0;
	double dx=0,dy=0,dz=0,rCA2=0;
	
	// TEST_CA 1 connected chains
	//fprintf(out,"TEST_CA 1\n");fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			//fprintf(out,"TEST_CA 1  pr=%d i=%d\n",pr,i);fflush(out);
			ppr=prot[pr].ammino[i].lbw_pr;
			ii=prot[pr].ammino[i].lbw_i;
			//fprintf(out,"TEST_CA 1  ppr=%d ii=%d\n",ppr,ii);fflush(out);
			if((ppr!=-1)&&(ii!=-1)){
				if((prot[ppr].ammino[ii].lfw_pr!=pr)||(prot[ppr].ammino[ii].lfw_i!=i)){
					fprintf(out,"TEST_CA 1 celllist SAW failed pr=%d i=%d ppr=%d ii=%d\n prot[ppr].ammino[ii].lfw_pr=%d prot[ppr].ammino[ii].lfw_i=%d\n",pr,i,ppr,ii,prot[ppr].ammino[ii].lfw_pr,prot[ppr].ammino[ii].lfw_i);fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				
				
			}
		}
		
	}
	//fprintf(out,"TEST_CA 2 and 3\n");fflush(out);
	//TEST_CA 2 all particles are in a cell
	//TEST_CA 3 all particles are in the cocrrect cell
	for(indice=0;indice<(unsigned int)(N_CA_cells*N_CA_cells*N_CA_cells);indice++){
		
		pr=hoc_CA[2*indice]-1;
		i=hoc_CA[2*indice+1]-1;
		
		//fprintf(out,"TEST_CA 2  pr=%d i=%d\n",pr,i);fflush(out);
		
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
		while(i>=0){
			
			if(prot[pr].ammino[i].indice!=2*indice){
				fprintf(out,"TEST_CA 3a failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",pr,i,prot[pr].ammino[i].indice,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size);
			icel_y=(prot[pr].ammino[i].y/cell_CA_size);
			icel_z=(prot[pr].ammino[i].z/cell_CA_size);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
			
			icel_x=floor(prot[pr].ammino[i].x/cell_CA_size);
			icel_y=floor(prot[pr].ammino[i].y/cell_CA_size);
			icel_z=floor(prot[pr].ammino[i].z/cell_CA_size);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			
			indice2=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
			
			if(indice2!=2*indice){
				fprintf(out,"TEST_CA 3b failed misplaced particle pr=%d i=%d\n indice2=%lu indice=%lu\n",pr,i,2*indice2,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}		
			
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	//fprintf(out,"TEST_CA 4\n");fflush(out);
	// TEST_CA4 no duplicated particles
	for(indice=0;indice<(unsigned int)(N_CA_cells*N_CA_cells*N_CA_cells);indice++){
		
		pr=hoc_CA[2*indice]-1;
		i=hoc_CA[2*indice+1]-1;
		while(i>=0){
			celllist_CA_test=0;
			for(indice2=0;indice2<(unsigned int)(N_CA_cells*N_CA_cells*N_CA_cells);indice2++){
				prt=hoc_CA[2*indice2]-1;
				j=hoc_CA[2*indice2+1]-1;
				while(j>=0){
					
					if((prt==pr)&&(i==j)) {
						celllist_CA_test++;
						if(celllist_CA_test>1) fprintf(out,"TEST_CA 4 failed particle pr=%d i=%d duplicated %d times prt=%d j=%d\n",pr,i,celllist_CA_test,prt,j);
					}
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
			}	
			if(celllist_CA_test>1) {
				fprintf(out,"TEST_CA 4 failed particle pr=%d i=%d duplicated %d times\n",pr,i,celllist_CA_test);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
		}
	}
	
	for(pr=0;pr<NPROT;pr++){
		for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
			for(prt=0;prt<NPROT;prt++){
				for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
					dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
					dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
					dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
					
					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					
					rCA2=(dx*dx+ dy*dy + dz*dz);
					
					if (rCA2<sist->Rint2){
						test5=0;	
						for(indice2=0;indice2<Neigh;indice2++){
							icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
							icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
							icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
							
							icel_x=P_Cell(icel_x,N_CA_cells);
							icel_y=P_Cell(icel_y,N_CA_cells);
							icel_z=P_Cell(icel_z,N_CA_cells);
							
							indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
							pprt=hoc_CA[indice]-1;
							jj=hoc_CA[indice+1]-1;
							
							while(jj>=0){
								if((jj==j)&&(pprt==prt))  {
									test5++;
								}
								jjj=prot[pprt].ammino[jj].lbw_i;
								pprt=prot[pprt].ammino[jj].lbw_pr;
								jj=jjj;
							}
						}
						if(test5!=1){
							fprintf(out,"TEST_CA 5 test5=%d failed particle prt=%d j=%d wrong neigh_cell of pr=%d i=%d\n  test5=%d Rinit=%lf %lf\n",test5,prt,j,pr,i,test5,sist->Rint,cell_CA_size);
							icel_x=floor(prot[pr].ammino[i].x/cell_CA_size);
							icel_y=floor(prot[pr].ammino[i].y/cell_CA_size);
							icel_z=floor(prot[pr].ammino[i].z/cell_CA_size);
							icel_x=P_Cell(icel_x,N_CA_cells);
							icel_y=P_Cell(icel_y,N_CA_cells);
							icel_z=P_Cell(icel_z,N_CA_cells);
							fprintf(out,"I icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
							fprintf(out,"I %lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
							icel_x=floor(prot[prt].ammino[j].x/cell_CA_size);
							icel_y=floor(prot[prt].ammino[j].y/cell_CA_size);
							icel_z=floor(prot[prt].ammino[j].z/cell_CA_size);
							icel_x=P_Cell(icel_x,N_CA_cells);
							icel_y=P_Cell(icel_y,N_CA_cells);
							icel_z=P_Cell(icel_z,N_CA_cells);
							fprintf(out,"J icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
							fprintf(out,"J %lf %lf %lf\n",prot[pr].ammino[j].x,prot[pr].ammino[j].y,prot[pr].ammino[j].z);
							fflush(out);
							MPI_Abort(MPI_COMM_WORLD,err5);
						}
					}
					
					
				}
			}
		}
	}
	
	
	
	
	
	
	return;
	
}

void test_celllist_SAW(int dove ){
	
	
	unsigned long int indice=0,indice_old=0,indice2=0;
	int j=0,k=0;
	int icel_x=0,icel_y=0,icel_z=0;
	int ppr=0,ii=0;
	int celllist_SAW_test=0,pr=0,prt=0,i=0,jj=0;
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			ppr=prot[pr].ammino[i].lbw_SAW_pr;
			ii=prot[pr].ammino[i].lbw_SAW_i;
			if((ppr!=-1)&&(ii!=-1)){
				if((prot[ppr].ammino[ii].lfw_SAW_pr!=pr)||(prot[ppr].ammino[ii].lfw_SAW_i!=i)){
					fprintf(out,"TEST 1 at %d celllist SAW failed pr=%d i=%d ppr=%d ii=%d\n prot[ppr].ammino[ii].lfw_SAW_pr=%d prot[ppr].ammino[ii].lfw_SAW_i=%d\n",dove,pr,i,ppr,ii,prot[ppr].ammino[ii].lfw_SAW_pr,prot[ppr].ammino[ii].lfw_SAW_i);fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
	}
	
	for(indice=0;indice<(unsigned int)(N_SAW_cells3);indice++){
		
		pr=hoc_SAW[2*indice]-1;
		i=hoc_SAW[2*indice+1]-1;
		
		while(i>=0){
			
			if(prot[pr].ammino[i].indice_SAW!=2*indice){
				fprintf(out,"TEST 3a at %d failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",dove,pr,i,prot[pr].ammino[i].indice_SAW,2*indice);fflush(out);
				fsamp();
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size);
			icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size);
			icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size);
			
			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			
			indice2=icel_x*2*N_SAW_cells2+icel_y*2*N_SAW_cells+icel_z*2;
			
			if(indice2!=2*indice){
				fprintf(out,"TEST 3b at %d failed misplaced particle pr=%d i=%d prot[%d].ammino[%d].id=%d\n indice2=%lu indice=%lu cell_SAW_size=%lf N_SAW_cells=%d\n",dove,pr,i,pr,i,prot[pr].ammino[i].id,indice2,2*indice,cell_SAW_size,N_SAW_cells);
				
				fprintf(out,"x=%lf y=%lf z=%fl\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
				icel_x=floor(prot[pr].ammino[i].x/cell_SAW_size);
				icel_y=floor(prot[pr].ammino[i].y/cell_SAW_size);
				icel_z=floor(prot[pr].ammino[i].z/cell_SAW_size);
				fprintf(out,"icel_x=%d icel_y=%d icel_z=%d\n",icel_x,icel_y,icel_z);
				icel_x=P_Cell(icel_x,N_SAW_cells);
				icel_y=P_Cell(icel_y,N_SAW_cells);
				icel_z=P_Cell(icel_z,N_SAW_cells);
				fprintf(out,"P icel_x=%d icel_y=%d icel_z=%d\n",icel_x,icel_y,icel_z);
				fflush(out);
				fsamp();
				MPI_Abort(MPI_COMM_WORLD,err5);
			}		
			
			
			jj=prot[pr].ammino[i].lbw_SAW_i;
			pr=prot[pr].ammino[i].lbw_SAW_pr;
			i=jj;
		}
	}
	
	for(indice=0;indice<(unsigned int)(N_SAW_cells3);indice++){
		
		pr=hoc_SAW[2*indice]-1;
		i=hoc_SAW[2*indice+1]-1;
		
		while(i>=0){
			celllist_SAW_test=0;
			for(indice2=0;indice2<(unsigned int)(N_SAW_cells3);indice2++){
				prt=hoc_SAW[2*indice2]-1;
				j=hoc_SAW[2*indice2+1]-1;
				while(j>=0){
					
					if((prt==pr)&&(i==j)) {
						celllist_SAW_test++;
						if(celllist_SAW_test>1) fprintf(out,"TEST 4 at %d failed particle pr=%d i=%d duplicated %d times prt=%d j=%d\n",dove,pr,i,celllist_SAW_test,prt,j);
					}
					
					
					
					jj=prot[prt].ammino[j].lbw_SAW_i;
					prt=prot[prt].ammino[j].lbw_SAW_pr;
					j=jj;
					/*if(rCA2<sist->Rint2){
						prot[pr].ammino[i].verlpr[Nver]=prt;
						prot[pr].ammino[i].verli[Nver]=j;
						prot[pr].ammino[i].Nverl=Nver;
						Nver++;
					}*/
				}
			}	
			if(celllist_SAW_test>1) {
				fprintf(out,"TEST 4 at %d failed particle pr=%d i=%d duplicated %d times\n",dove,pr,i,celllist_SAW_test);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			jj=prot[pr].ammino[i].lbw_SAW_i;
			pr=prot[pr].ammino[i].lbw_SAW_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	
	
	return;
	
}

/// Here we apply the periodic boundary conditions on the cell list
int  P_Cell (int x, int N_cels){
	int x1;
	x1=x;
	if (x>=N_cels) x1=x-floor(x/N_cels)*N_cels;
	
	if (x<0)	x1=x+(1-floor((x+1)/N_cels))*N_cels;  
	
	return (x1);
}

double  P_Cd (double x){
	double x1;
	x1=x;
	if (x>=BoxSize) x1=x-(int)(x/BoxSize)*BoxSize;
	
	if (x<0)	x1=x+(1-(int)(x/BoxSize))*BoxSize;  
	
	return (x1);
}
/* Function for writing the psf file */

int  write_psf(FILE *fPSFvmd){
	
	int k,l, j, i, nbonds, nf, pl, tot_atoms=0;
	
	fprintf(fPSFvmd,"PSF\n\n");
	fprintf(fPSFvmd,"%8d !TITLE\n\n\n",NPROT);
	for(j=0;j<NPROT;j++){
		tot_atoms += Plength[j]+Plength[j]/NATOM;
	}
	fprintf(fPSFvmd,"%8d !NATOM\n",tot_atoms);
	l=1;
	
	for(j=0;j<NPROT;j++){
		for(i=0;i< Plength[j];i=i+NATOM){
			fprintf(fPSFvmd,"%8d PROT %-3d  %3s  CA   CA     %7.6f        %6.4f           0\n", l,j+1,sist->Amminoacids[prot[j].ammino[i].residue],0.0,12.011);
			for(k=1;k<NATOM;k++){
				fprintf(fPSFvmd,"%8d PROT %-3d  %3s  H    H      %7.6f        %6.4f           0\n", l+k,j+1,sist->Amminoacids[prot[j].ammino[i].residue],0.0,1.008);
			}
			fprintf(fPSFvmd,"%8d PROT %-3d  %3s  O    O      %7.6f        %6.4f           0\n", l+NATOM,j+1,sist->Amminoacids[prot[j].ammino[i].residue],0.0,6.000);
			l+=NATOM+1;
		}
	}
	fprintf(fPSFvmd,"\n");
	nbonds = 0;
	for (j=0;j<NPROT;j++){
		nbonds += Plength[j]+Plength[j]/NATOM-1;
	}
	fprintf(fPSFvmd,"%9d !NBOND\n",nbonds);
	l=1;
	nf=0;
	pl=0;
	for (j=0;j<NPROT;j++){
		pl += Plength[j]+Plength[j]/NATOM;
		for(i=0;i<Plength[j];i=i+NATOM){
			for(k=1;k<NATOM;k++){
				fprintf(fPSFvmd," %7d %7d",l,l+k); nf = nf+1; // HCA
				if (nf == 4) {
					fprintf(fPSFvmd,"\n"); nf=0;
				}
			}	
			fprintf(fPSFvmd," %7d %7d",l,l+NATOM); nf = nf+1; // CADipole
			if (nf == 4) {
				fprintf(fPSFvmd,"\n"); nf=0;
			}   
			if (l<pl-NATOM-1){
				fprintf(fPSFvmd," %7d %7d",l,l+NATOM+1); nf = nf+1; // CACA
				if (nf == 4) {
					fprintf(fPSFvmd,"\n"); nf=0;
				}
			} 
			l+=NATOM+1;
		}
	}
	return(0);
}

double E_Water_Old (void){
	
	int indice,i,pr;
	double E=0;
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			#ifdef PROGRESS_WATER			
			if(icycl>PROGRESS_STEP_SKIP) {
				fprintf(out,"#############E_Water_Old %lu prot[%d/%d].ammino[%d/%d].residue= ",icycl,pr,NPROT,i,Plength[pr]);fflush(out);
				fprintf(out,"%d\n",prot[pr].ammino[i].residue);fflush(out);
			}	
			#endif
			if(prot[pr].ammino[i].old_water_contacts<=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]>=0) {
					E+=(HOH_Burried_Threshold-prot[pr].ammino[i].old_water_contacts)*M[prot[pr].ammino[i].residue][0];	
					
				}
				
			}
			if(prot[pr].ammino[i].old_water_contacts>=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]<=0) {
					E+=(HOH_Burried_Threshold-prot[pr].ammino[i].old_water_contacts)*M[prot[pr].ammino[i].residue][0];	
					
				}
				
			}
			#ifdef PROGRESS_WATER			
			if(icycl>PROGRESS_STEP_SKIP) {
				fprintf(out,"#############E_Water_Old OK %lu prot[%d/%d].ammino[%d/%d].residue= ",icycl,pr,NPROT,i,Plength[pr]);fflush(out);
				fprintf(out,"%d\n",prot[pr].ammino[i].residue);fflush(out);
			}	
			#endif
			
		}
		
	}
	return E;
}

double E_Water_New (void){
	
	int indice,i,pr;
	double E=0;
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			#ifdef PROGRESS_WATER			
			if(icycl>PROGRESS_STEP_SKIP) {
				fprintf(out,"#############E_Water_new %lu prot[%d/%d].ammino[%d/%d].residue= ",icycl,pr,NPROT,i,Plength[pr]);fflush(out);
				fprintf(out,"%d\n",prot[pr].ammino[i].residue);fflush(out);
			}	
			#endif
			if(prot[pr].ammino[i].new_water_contacts<=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]>=0) {
					E+=(HOH_Burried_Threshold-prot[pr].ammino[i].new_water_contacts)*M[prot[pr].ammino[i].residue][0];	
				}
			}
			if(prot[pr].ammino[i].new_water_contacts>=HOH_Burried_Threshold){
				if(M[prot[pr].ammino[i].residue][0]<=0) {
					E+=(HOH_Burried_Threshold-prot[pr].ammino[i].new_water_contacts)*M[prot[pr].ammino[i].residue][0];	
				}
			}
			#ifdef PROGRESS_WATER			
			if(icycl>PROGRESS_STEP_SKIP) {
				fprintf(out,"#############E_Water_new OK %lu prot[%d/%d].ammino[%d/%d].residue= ",icycl,pr,NPROT,i,Plength[pr]);fflush(out);
				fprintf(out,"%d\n",prot[pr].ammino[i].residue);fflush(out);
			}	
			#endif
		}
	}
	return E;
}

void Water_Update_Reject (void){
	
	int indice=0,i=0,pr=0;
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].new_water_contacts=prot[pr].ammino[i].old_water_contacts;
		}	
		
		
	}
	return;
}

void Water_Update_Accept (void){
	
	int indice=0,i=0,pr=0;
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].old_water_contacts=prot[pr].ammino[i].new_water_contacts;
		}
	}
	return;
}

void Water_Update_Test (void){
	
	int indice=0,i=0,pr=0;
	FILE *fp=NULL;
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			if(fabs(prot[pr].ammino[i].old_water_contacts-prot[pr].ammino[i].new_water_contacts)>1e-8){
				fprintf(out,"Water_Update_Test Mossaid=%d %lu prot[%d].ammino[%d].new_water_contacts=%20.15lf  prot[%d].ammino[%d].old_water_contacts=%20.15lf\n",mossaid,icycl,pr,i,prot[pr].ammino[i].new_water_contacts,pr,i,prot[pr].ammino[i].old_water_contacts);fflush(out);
				fsamp();
				fp=fopen("final-err.bin","w");
				writeBinary(fp);
				test_celllist_SAW(30);
				test_celllist_CA();
				test_celllist_H();
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
		}
	}
	return;
}

void Pswap (void ){
	
	
	/************************************************
	* Pswap --  randomly swap the type of two       * 
	*    random particles            				*
	*                        						*
	* Parameters:                   				*
	*prot -- is a proteina type structure    		*
	*    where is stored the protein     			*
	*M -- is the interaction matrix          		*
	*                        						*
	* Return:                    					*
	*void                							*
	*************************************************/
	
	long int indice_i=0,indice_j=0;
	int err=0,sim=0,prt=0;
	int pr=0,i=0,j=0,k=0,res[2],indice=0;
	double acc=0;
	double En=0.0,Etest=0.0;
	double testE=0,testO=0;
	double DW=0;
	int testC=0,New_Mean_H_Bonds=0;
	double Enew=0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double deltaerg=0.0,dW=0;
	double espo=0.0,espo1=0.0;
	static double **old_i_X=NULL,**old_i_Y=NULL,**old_i_Z=NULL;
	static double **old_j_X=NULL,**old_j_Y=NULL,**old_j_Z=NULL;
	double Bond=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0,OALN_New, OALN_Old;
	///OTopo_indice_New=0,OTopo_indice_Old=0;
	FILE *fp=NULL;
	
	res[0]=res[1]=0;
	
	
	if(old_i_X==NULL){
		old_i_X=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			old_i_X[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(old_i_Y==NULL){
		old_i_Y=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			old_i_Y[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(old_i_Z==NULL){
		old_i_Z=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			old_i_Z[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(old_j_X==NULL){
		old_j_X=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			old_j_X[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(old_j_Y==NULL){
		old_j_Y=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			old_j_Y[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(old_j_Z==NULL){
		old_j_Z=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			old_j_Z[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	
	
	i=(int)(ran3(&seed)*(Plength[0]/NATOM))*NATOM;
	j=(int)(ran3(&seed)*(Plength[0]/NATOM))*NATOM;
	
	
	if((i!=j)&&(i!=j+1)&&(i!=j-1)) return;
		for(pr=0;pr<NPROT;pr++){
			for(k=0;k<=NSPOTS;k++){ 
				indice_i=i+k;
				indice_j=j+k;
				deltaerg-=Seq_energy_SP_pswap(pr,indice_i,indice_j);
				
				Cold+=sist->ncontact;
				deltaerg-=Seq_energy_SP_pswap(pr,indice_j,indice_i);
				Cold+=sist->ncontact;
			}
			
			deltaerg-=Bond_Energy_SP(pr,i);
			deltaerg-=Bond_Energy_SP(pr,j);
		}
		// It is important to compute the energy sperately from the residue swap otherwise the contribution of the first particles do not include the one changed later
		for(pr=0;pr<NPROT;pr++){		
			
			res[0]=prot[pr].ammino[i].residue;
			res[1]=prot[pr].ammino[j].residue;
			
			
			
			prot[pr].ammino[i].residue=res[1];
			prot[pr].ammino[j].residue=res[0];
			
			if(pr>0){
				if((prot[pr].ammino[i].residue!=prot[pr-1].ammino[i].residue)||(prot[pr].ammino[j].residue!=prot[pr-1].ammino[j].residue)){
					fprintf(out,"Pswap seq non ok icycl=%lu\nPROT[%d].ammino[%d].residue=%d prot[%d].ammino[%d].residue=%d\nPROT[%d].ammino[%d].residue=%d prot[%d].ammino[%d].residue=%d\n",icycl,pr,i,prot[pr].ammino[i].residue,pr-1,i,prot[pr-1].ammino[i].residue,pr,j,prot[pr].ammino[j].residue,pr-1,j,prot[pr-1].ammino[j].residue);
					
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
		for(pr=0;pr<NPROT;pr++){
			if((energy_SAW_SP(pr,i)>0.00)||(energy_SAW_SP(pr,j)>0.00)){
				for(prt=0;prt<NPROT;prt++){
					prot[prt].ammino[i].residue=res[0];
					prot[prt].ammino[j].residue=res[1];
				}
				return;
			}
			
			
			
			
			
			for(k=1;k<=NSPOTS;k++){ 
				indice=i+k;
				
				old_i_X[pr][k]=prot[pr].ammino[indice].x;
				old_i_Y[pr][k]=prot[pr].ammino[indice].y;
				old_i_Z[pr][k]=prot[pr].ammino[indice].z;
				
				indice=j+k;
				
				old_j_X[pr][k]=prot[pr].ammino[indice].x;
				old_j_Y[pr][k]=prot[pr].ammino[indice].y;
				old_j_Z[pr][k]=prot[pr].ammino[indice].z;
				
			}	
			/*---------------Nex we shrink the particle-------*/
			for(k=1;k<=NSPOTS;k++){ 
				indice=i+k;
				
				if(prot[pr].ammino[indice].id==ATOM_H){
					prot[pr].ammino[indice].x-=prot[pr].ammino[i].x;
					prot[pr].ammino[indice].y-=prot[pr].ammino[i].y;
					prot[pr].ammino[indice].z-=prot[pr].ammino[i].z;
					
					prot[pr].ammino[indice].x*=Rmin[res[1]]/Rmin[res[0]];
					prot[pr].ammino[indice].y*=Rmin[res[1]]/Rmin[res[0]];
					prot[pr].ammino[indice].z*=Rmin[res[1]]/Rmin[res[0]];
					
					prot[pr].ammino[indice].x+=prot[pr].ammino[i].x;
					prot[pr].ammino[indice].y+=prot[pr].ammino[i].y;
					prot[pr].ammino[indice].z+=prot[pr].ammino[i].z;	
				}
				
				indice=j+k;
				
				if(prot[pr].ammino[indice].id==ATOM_H){
					prot[pr].ammino[indice].x-=prot[pr].ammino[j].x;
					prot[pr].ammino[indice].y-=prot[pr].ammino[j].y;
					prot[pr].ammino[indice].z-=prot[pr].ammino[j].z;
					
					prot[pr].ammino[indice].x*=Rmin[res[0]]/Rmin[res[1]];
					prot[pr].ammino[indice].y*=Rmin[res[0]]/Rmin[res[1]];
					prot[pr].ammino[indice].z*=Rmin[res[0]]/Rmin[res[1]];
					
					prot[pr].ammino[indice].x+=prot[pr].ammino[j].x;
					prot[pr].ammino[indice].y+=prot[pr].ammino[j].y;
					prot[pr].ammino[indice].z+=prot[pr].ammino[j].z;	
				}
			}
		}
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			sist->EDip_New=Dipole_Self_Cons();
			deltaerg+=sist->EDip_New-sist->EDip;
			Cnew=sist->ndip_contact-sist->dip_contact;
		}
		#endif
		
		for(pr=0;pr<NPROT;pr++){
			for(k=0;k<=NSPOTS;k++){ 
				indice_i=i+k;
				indice_j=j+k;
				deltaerg+=Seq_energy_SP_pswap(pr,indice_i,indice_j);
				Cnew+=sist->ncontact;
				deltaerg+=Seq_energy_SP_pswap(pr,indice_j,indice_i);	
				Cnew+=sist->ncontact;
			}	
			Bond=Bond_Energy_SP(pr,i);
			if((isnan(Bond))||(isnan(1./Bond))){
				fprintf(out,"Pswap bw non ok icycl=%lu, Bond_i=%lf %d, Rmin[res[0]]=%lf Rmin[res[1]]=%lf res[0]=%d res[1]=%d\n",icycl,Bond,i,Rmin[res[0]],Rmin[res[1]],res[0],res[1]);
				for(k=1;k<=NSPOTS;k++){ 
					indice=i+k;
					
					if(prot[pr].ammino[indice].id==ATOM_H){
						fprintf(out,"Old.x=%lf New_x=%lf\n",old_i_X[pr][k],prot[pr].ammino[indice].x);
						fprintf(out,"Old.y=%lf New_y=%lf\n",old_i_Y[pr][k],prot[pr].ammino[indice].y);
						fprintf(out,"Old.z=%lf New_z=%lf\n",old_i_Z[pr][k],prot[pr].ammino[indice].z);
					}
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			deltaerg+=Bond;
			Bond=Bond_Energy_SP(pr,j);		
			if((isnan(Bond))||(isnan(1./Bond))){
				fprintf(out,"Pswap bw non ok icycl=%lu, Bond_j=%lf %d, Rmin[res[0]]=%lf Rmin[res[1]]=%lf res[0]=%d res[0]=%d\n",icycl,Bond,j,Rmin[res[0]],Rmin[res[1]],res[0],res[1]);
				for(k=1;k<=NSPOTS;k++){ 
					indice=j+k;
					
					if(prot[pr].ammino[indice].id==ATOM_H){
						fprintf(out,"Old.x=%lf New_x=%lf\n",old_i_X[pr][k],prot[pr].ammino[indice].x);
						fprintf(out,"Old.y=%lf New_y=%lf\n",old_i_Y[pr][k],prot[pr].ammino[indice].y);
						fprintf(out,"Old.z=%lf New_z=%lf\n",old_i_Z[pr][k],prot[pr].ammino[indice].z);
					}
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			deltaerg+=Bond;
		}
		Cnew=sist->contact+Cnew-Cold;
		if(Cnew>0) Znew=1;
		if((Cnew<0)){
			
			if(icycl > 0) fprintf(out,"Pswap bw non ok icycl=%lu, contact=%d %d %d\n",icycl,Cnew,sist->contact,Cold);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
		if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
		New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
		
		if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
		
		Enew=sist->E+deltaerg;
		
		#ifdef PROGRESS_WATER			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
		#endif
		Ewater=E_Water_New();
		
		
		#ifdef PROGRESS_WATER			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
		#endif
		
		
		#ifdef 	ORDER_TEST_WATER
		
		
		
		order_test_water(&testEWater);
		if(fabs(Ewater-testEWater)>1e-8){
			
			fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			test_celllist_SAW(31);
			test_celllist_CA();
			test_celllist_H();
			
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		#endif
		#ifdef PROGRESS			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
		#endif
		Enew+=Ewater;	
		
		
		#ifdef 	CLIST_TEST
		test_celllist_SAW(32);
		test_celllist_CA();
		test_celllist_H();
		
		#endif
		#ifdef 	ORDER_TEST
		
		
		
		order_test(&testC,&testO,&testE);
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			
			testC+=sist->ndip_contact;
		}
		#endif
		
		if(Cnew!=testC){
			fprintf(out,"CC a local %lu %d %d\n",icycl,Cnew,testC);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/*if(fabs(Onew-testO)>1e-5){
			fprintf(out,"CC b local %lu %15.10lf %15.10lf min=%d max=%d\n",icycl,Onew,testO,kk,kk+kk_end+4);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} */
		
		if(fabs(Enew-sist->EDip_New-testE)>1e-8){
			fprintf(out,"Pswap %lu Enew=%15.10lf testE=%15.10lf i=%d j=%d res[0]=%d res[1]=%d\n",icycl,Enew-sist->EDip_New,testE,i,j,res[0],res[1]);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		
		#endif
		sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT));
		if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
		
		#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,sist->Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);
				fflush(out);
				}	
		
	#endif
		
		DW=Wpot[betaindice][New_Mean_H_Bonds][sist->Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
		
		espo=-(deltaerg)*beta[betaindice]+DW*beta[betaindice];
		if(espo<50.0){
			espo1=-log(1+exp(espo));
		}else{
			espo1=-espo;
		}
		OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
		///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
		OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));	
		if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
		if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
		///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
		///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
		if(OALN_New>=sizeALN) OALN_New=sizeALN-1;
		if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1;
		
		///sampling(sist->density,Enew,sist->Mean_CA_Bonds,sist->touch,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
		sampling(sist->density,Enew,sist->Mean_CA_Bonds,sist->touch,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
		///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
		sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
		
		acc=exp(espo);
		if (ran3(&seed)<acc){
			
			#ifdef DIPOLE_SFC
			if(E_Dip>0){
				Dipole_Accept();
			}
			#endif
			Water_Update_Accept();
			#ifdef WATER_UP_TEST
		mossaid+=2000;
		Water_Update_Test();
		mossaid-=2000;
		#endif
			sist->E=Enew;
			sist->contact=Cnew;
			sist->touch=Znew;
			sist->Mean_H_Bonds=New_Mean_H_Bonds;
			Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
		}else{
			for(pr=0;pr<NPROT;pr++){
				for(k=1;k<=NSPOTS;k++){ 
					indice=i+k;
					prot[pr].ammino[indice].x=old_i_X[pr][k];
					prot[pr].ammino[indice].y=old_i_Y[pr][k];
					prot[pr].ammino[indice].z=old_i_Z[pr][k];
					indice=j+k;
					prot[pr].ammino[indice].x=old_j_X[pr][k];
					prot[pr].ammino[indice].y=old_j_Y[pr][k];
					prot[pr].ammino[indice].z=old_j_Z[pr][k];
				}	
				prot[pr].ammino[i].residue=res[0];
				prot[pr].ammino[j].residue=res[1];
			}
			Water_Update_Reject();
			#ifdef WATER_UP_TEST
			mossaid+=540;
			Water_Update_Test();
			mossaid-=540;
			#endif
			#ifdef DIPOLE_SFC
			if(E_Dip>0){
				Dipole_Reject();
			}
			#endif
		}
	return;
}

void fseq (void ){
	/** SINGLE POINTMUTATION
	 *  only on the first protein
	 * ***************************/
	
	double dx=0,dy=0,dz=0;
	int pr=0,kk=0,kkk=0,res=0,old_res=0;
	static double **oldX=NULL,**oldY=NULL,**oldZ=NULL;
	double r2=0,rs=0;
	double theta=0,phi=0;
	double alpha=0;
	double Etouch=0,EtouchO=0;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc=0,DW=0,espo=0,espo1=0,Eold2=0,deltaP=0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX=0,TY=0,TZ=0,dR=0;
	int i=0,k=0,fseq_test=0,j=0,ii=0,indice=0,jj=0;
	int l=0,lll=0;
	int flag=1;
	int selection=0;
	double testO=0,testOP=0;
	int testC=0;
	double testE=0,Otest=0.0;
	int New_Mean_CA_Bonds=0,New_Mean_H_Bonds=0;
	double Ewater=0,testEWater=0;
	int OEnd_New=0,OEnd_Old=0;
	int OALN_New=0,OALN_Old=0;
	///OTopo_indice_New=0,OTopo_indice_Old=0;
	FILE *fp=NULL;
	fseq_test=0;	
	
	kk=(int)(ran3(&seed)*((Plength[0]/NATOM)));
	res=(int)(ran3(&seed)*(S-1))+1;
	
	if(oldX==NULL){
		oldX=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			oldX[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(oldY==NULL){
		oldY=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			oldY[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	if(oldZ==NULL){
		oldZ=(double **)calloc(NPROT,sizeof(double *));
		for(pr=0;pr<NPROT;pr++){
			oldZ[pr]=(double *)calloc(NATOM,sizeof(double ));
		}
	}
	
	kk*=NATOM;
	if(prot[0].ammino[kk].id!=ATOM_CA){
		fprintf(out,"fseq ref non CA %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(prot[0].ammino[kk].residue<=0){
		fprintf(out,"fseq prot[%d].ammino[%d].residue=%d less equal than 0 !!!\n",pr,kk,prot[pr].ammino[kk].residue);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if(res!=prot[0].ammino[kk].residue){
		for(pr=0;pr<NPROT;pr++){
			for(k=0;k<=NSPOTS;k++){
				indice=kk+k;		
				Eold+=energy_SP_fseq(pr,indice,OLDWATER);
				Oold+=sist->norder;
				Cold+=sist->ncontact;
			}
			Eold+=Bond_Energy_SP(pr,kk);
			#ifdef PROGRESS_WATER			
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
			#endif
			#ifdef PROGRESS_WATER			
			if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossa=%d  E_Water_Old OK %lu\n",mossaid,icycl);fflush(out);}	
			#endif
			#ifdef PROG_FSEQ
			fprintf(out,"fseq A OK %lu\n",icycl);fflush(out);
			#endif
			
			for(k=0;k<=NSPOTS;k++){ 
				indice=kk+k;
				oldX[pr][k]=prot[pr].ammino[indice].x;
				oldY[pr][k]=prot[pr].ammino[indice].y;
				oldZ[pr][k]=prot[pr].ammino[indice].z;
			}	
			
			Dold=0;
			if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
			#ifdef PROG_FSEQ
			fprintf(out,"fseq B OK %lu\n",icycl);fflush(out);
			#endif
		}	
		Eold+=E_Water_Old();
		old_res=prot[0].ammino[kk].residue;
		
		if(sist->np[old_res]<=1){ 
			fprintf(out,"fseq %lu cazzo sist->np[old_res] e' negativo sist->np[%d]=%d prot[%d].ammino[%d].residue=%d NPROT=%d\n",icycl,old_res,sist->np[old_res],pr,kk,prot[pr].ammino[kk].residue,NPROT);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		sist->np[old_res]--;
		for(pr=0;pr<NPROT;pr++){
			prot[pr].ammino[kk].residue=res;
		}
		
		sist->np[res]++;
		if(sist->np[res]<=0){ 
			fprintf(out,"fseq %lu cazzo sist->np[res] e' negativo sist->np[%d]=%d prot[%d].ammino[%d].residue=%d NPROT=%d\n",icycl,res,sist->np[res],pr,kk,prot[pr].ammino[kk].residue,NPROT);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		deltaP=((double)(sist->p[old_res])/(double)(sist->np[res]));	// This is per chain we multiply later by NPROT	
		
		deltaP=log(deltaP);
		if(partint(sist->NP+deltaP)<0){ 
			fprintf(out,"fseq %lu cazzo order e' negativo %f %f %ld beta2=%f\n",icycl,sist->NP,deltaP,lround(sist->NP+deltaP),beta2);
			fsamp();
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		
		if((isnan(Rmin[res]/Rmin[old_res]))||(isnan(Rmin[old_res]/Rmin[res]))){
			fprintf(out,"fseq %lu NAN Rmin[res]=%lf Rmin[old_res]=%lf res=%d old_res=%d\n",icycl,Rmin[res],Rmin[old_res],res,old_res);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/*---------------Nex we shrink the particle-------*/
		for(pr=0;pr<NPROT;pr++){
			for(k=1;k<=NSPOTS;k++){ 
				indice=kk+k;
				
				if(prot[pr].ammino[indice].id==ATOM_H){
					prot[pr].ammino[indice].x-=prot[pr].ammino[kk].x;
					prot[pr].ammino[indice].y-=prot[pr].ammino[kk].y;
					prot[pr].ammino[indice].z-=prot[pr].ammino[kk].z;
					
					if((isnan(prot[pr].ammino[indice].x))||(isnan(1./prot[pr].ammino[indice].x))){
						fprintf(out,"Coord NAN %lu fseq A prot[%d].ammino[%d].x=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].x);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					if((isnan(prot[pr].ammino[indice].y))||(isnan(1./prot[pr].ammino[indice].y))){
						fprintf(out,"Coord NAN %lu fseq A prot[%d].ammino[%d].y=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].y);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					if((isnan(prot[pr].ammino[indice].z))||(isnan(1./prot[pr].ammino[indice].z))){
						fprintf(out,"Coord NAN %lu fseq A prot[%d].ammino[%d].z=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].z);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}	
					
					
					prot[pr].ammino[indice].x*=Rmin[res]/Rmin[old_res];
					prot[pr].ammino[indice].y*=Rmin[res]/Rmin[old_res];
					prot[pr].ammino[indice].z*=Rmin[res]/Rmin[old_res];
					
					
					if((isnan(prot[pr].ammino[indice].x))||(isnan(1./prot[pr].ammino[indice].x))){
						fprintf(out,"Coord NAN %lu fseq B prot[%d].ammino[%d].x=%lf Rmin[%d]=%lf Rmin[%d]=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].x,res,Rmin[res],old_res,Rmin[old_res]);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					if((isnan(prot[pr].ammino[indice].y))||(isnan(1./prot[pr].ammino[indice].y))){
						fprintf(out,"Coord NAN %lu fseq B prot[%d].ammino[%d].y=%lf Rmin[%d]=%lf Rmin[%d]=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].y,res,Rmin[res],old_res,Rmin[old_res]);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					if((isnan(prot[pr].ammino[indice].z))||(isnan(1./prot[pr].ammino[indice].z))){
						fprintf(out,"Coord NAN %lu fseq B prot[%d].ammino[%d].z=%lf Rmin[%d]=%lf Rmin[%d]=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].z,res,Rmin[res],old_res,Rmin[old_res]);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}	
					
					prot[pr].ammino[indice].x+=prot[pr].ammino[kk].x;
					prot[pr].ammino[indice].y+=prot[pr].ammino[kk].y;
					prot[pr].ammino[indice].z+=prot[pr].ammino[kk].z;
					
					
					if((isnan(prot[pr].ammino[indice].x))||(isnan(1./prot[pr].ammino[indice].x))){
						fprintf(out,"Coord NAN %lu fseq C prot[%d].ammino[%d].x=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].x);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					if((isnan(prot[pr].ammino[indice].y))||(isnan(1./prot[pr].ammino[indice].y))){
						fprintf(out,"Coord NAN %lu fseq C prot[%d].ammino[%d].y=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].y);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}
					if((isnan(prot[pr].ammino[indice].z))||(isnan(1./prot[pr].ammino[indice].z))){
						fprintf(out,"Coord NAN %lu fseq C prot[%d].ammino[%d].z=%lf\n",icycl,pr,indice,prot[pr].ammino[indice].z);
						fflush(out);
						MPI_Abort(MPI_COMM_WORLD,err5);
						
					}	
				}
				updt_cell_list (pr,indice);
			}
			
			
		}
		
		
		
		#ifdef PROG_FSEQ
		fprintf(out,"fseq C OK %lu\n",icycl);fflush(out);
		#endif	
		#ifdef 	CLIST_TEST	
		fprintf(out,"fseq KK=%d\n",kk);fflush(out);		
		test_celllist_SAW(33);
		test_celllist_CA();
		test_celllist_H();
		
		#endif			
		/*if ((energy_SAW_test ()>0.0)){
			fprintf(out,"SAW  fw %lu %d %d\n",icycl);
			
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			sist->EDip_New=Dipole_Self_Cons();
			Enew=sist->EDip_New-sist->EDip;
			Cnew=sist->ndip_contact-sist->dip_contact;
		}
		#endif
		
		for(pr=0;pr<NPROT;pr++){
			
			for(k=0;k<=NSPOTS;k++){ 
				indice=kk+k;
				
				Enew+=energy_SP_fseq(pr,indice,NEWWATER);
				Onew+=sist->norder;
				Cnew+=sist->ncontact;
				
				
				
			}
			Enew+=Bond_Energy_SP(pr,kk);
			//	fprintf(out,"MC_brot_f %lu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
			
			#ifdef PROG_FSEQ
			fprintf(out,"fseq D OK %lu\n",icycl);fflush(out);
			#endif
		}
		#ifdef PROGRESS_WATER			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu %lf\n",mossaid,icycl,1./beta[betaindice]);fflush(out);}	
		#endif
		Ewater=E_Water_New();
		
		
		#ifdef PROGRESS_WATER			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  E_Water_New %lu\n",mossaid,icycl);fflush(out);}	
		#endif
		
		
		#ifdef 	ORDER_TEST_WATER
		
		
		
		order_test_water(&testEWater);
		if(fabs(Ewater-testEWater)>1e-8){
			
			fprintf(out,"Mossaid=%d Water %lu %15.10lf %15.10lf\n",mossaid,icycl,Ewater,testEWater);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			test_celllist_SAW(34);
			test_celllist_CA();
			test_celllist_H();
			
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		#endif
		#ifdef PROGRESS			
		if(icycl>PROGRESS_STEP_SKIP) {fprintf(out,"##############Mossaid=%d  order_test_water %lu\n",mossaid,icycl);fflush(out);}	
		#endif
		Enew+=Ewater;	
		
		
		
		
		if(Cnew>0) Znew=1;
		Dnew=Dold;
		if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
		/*if(Enew>0){
			fprintf(out,"Brot energia New positiva %lu %lf\n",icycl,Enew);
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(Eold>0){
			fprintf(out,"Brot energia Old positiva %lu %lf\n",icycl,Eold);
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		Onew=sist->order+Onew-Oold;
		Cnew=sist->contact+Cnew-Cold;
		
		Enew=sist->E+Enew-Eold;
		Eold=sist->E;
		#ifdef PROG_FSEQ
		fprintf(out,"fseq E OK %lu\n",icycl);fflush(out);
		#endif
		#ifdef 	ORDER_TEST	
		order_test(&testC,&testO,&testE);
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			testE+=sist->EDip_New;
			
			testC+=sist->ndip_contact;
		}
		#endif
		if(Cnew!=testC){
			fprintf(out,"testC fseq  %lu %d %d\n",icycl,Cnew,testC);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(fabs(Onew-testO)>1e-8){
			fprintf(out,"testO fseq  %lu %lf %lf\n",icycl,Onew,testO);
			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		if(fabs(Enew-testE)>1e-8){
			fprintf(out,"testE fseq  %lu Enew=%lf testE=%lf sist->EDip_New=%lf kk=%d res=%d\n",icycl,Enew,testE,sist->EDip_New,kk,res);			
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/*strcpy(info_ALN,"initial");
		Linking_Number(prot,pr,ATOM_CA,0,0,Plength[pr],NATOM,info_ALN,&aln_partial);
		if(fabs(aln_new-aln_partial)>1e-8){
		  fprintf(out,"fseq aln test %lu pr=%d %15.10lf %15.10lf min=%d max=%d\n",icycl,pr,aln_new,aln_partial,kk,kk_end);
		
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		test_celllist_SAW(3);
		test_celllist_CA();
		test_celllist_H();
		MPI_Abort(MPI_COMM_WORLD,err5);
		} */
		/*********test for factorial does not work due to the stirling approx*************	
	N=(int)(Plength[0]/NATOM)+1;
		if (N>50){
		  Otest=N*log(N)-N;}
		else{
		  Otest=log(factorial(N));
		}
	
		for(jj=0;jj<S;jj++){

		  if (sist->np[jj]>50) Otest-=(sist->np[jj]*log(sist->np[jj]))-sist->np[jj];
		  else{
		    Otest-=log(factorial(sist->np[jj]));
		  }
		}

		if(fabs(Otest-(sist->NP+deltaP))>1e-5){
			fprintf(out,"NP fseq %lu Otest failed Otest=%lf sist->NP=%lf deltaP=%lf factorial(N)=%g N=%d\n",icycl,Otest,sist->NP,deltaP,factorial(N),N);fflush(out);
			
			for(jj=0;jj<S;jj++){
				fprintf(out,"factorial(sist->np[%d])=%g sist->np[%d]=%d\n",jj,factorial(sist->np[jj]),jj,sist->np[jj]);   
			}
			for(jj=0;jj<S;jj++){
				fprintf(out,"factorial(sist->p[%d])=%g sist->p[%d]=%d\n",jj,factorial(sist->p[jj]),jj,sist->p[jj]);   
			}
			fflush(out);
			fsamp();
			fp=fopen("final-err.bin","w");
			writeBinary(fp);
			MPI_Abort(MPI_COMM_WORLD,err5);
		} 
		/******/

		#endif	


		/*if(Enew>0){
			fprintf(out,"Brot energia New New positiva %lu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		/*for(k=0;k<Plength[pr];k++){  
			
			Enew2+=energy_SP(pr,k);
			
			
		} 
		if(fabs(Enew2-Enew)>0.0001){
			fprintf(out,"Trot energia PDL diversa %lu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		/***********TEST*************/
		//fprintf(out,"fseq D OK %lu\n",icycl);fflush(out);
		/*if((Onew<IminO)){		
			fprintf(out,"MC_brot fw non ok icycl=%lu, order=%d %d %d %d %d\n",icycl,Onew,sist->order,Oold,minO,maxO);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
		if((Cnew<0)){		
			fprintf(out,"fseq non ok icycl=%lu, Cnew=%d sist->contact=%d Cold=%d sist->ndip_contact=%d sist->dip_contact=%d\n",icycl,Cnew,sist->contact,Cold,sist->ndip_contact,sist->dip_contact);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if((Onew<0)){		
		fprintf(out,"fseq non ok icycl=%lu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
		/***********TEST*************/
		New_Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(Onew/DNPROT))-minO;
		if((New_Mean_CA_Bonds>=sizex)) New_Mean_CA_Bonds=sizex-1;
		
		sist->Mean_CA_Bonds=lround(Mean_CA_Bonds_bin*(sist->order/DNPROT))-minO;
		if((sist->Mean_CA_Bonds>=sizex)) sist->Mean_CA_Bonds=sizex-1;
		
		sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT))-minH;
		if((sist->Mean_H_Bonds>=sizeC)) sist->Mean_H_Bonds=sizeC-1;
		
		New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT))-minH;
		
		if((New_Mean_H_Bonds>=sizeC)) New_Mean_H_Bonds=sizeC-1;
		//if((New_Mean_H_Bonds<=minH)) New_Mean_H_Bonds=0;
		
		if((New_Mean_H_Bonds<0)){		
			fprintf(out,"MC_SP_rot New_Mean_H_Bonds negative icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if((New_Mean_H_Bonds>=sizeC)){		
			fprintf(out,"MC_SP_rot New_Mean_H_Bonds too large icycl=%lu, contact=%d %d %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		#ifdef PROG_FSEQ
		fprintf(out,"fseq F OK %lu\n",icycl);fflush(out);
		#endif
		#ifdef PROGRESS			
	if(icycl>PROGRESS_STEP_SKIP) {
		fprintf(out,"##############Mossaid=%d  Bias potential %lu Wpot[%d][%d][%d]-Wpot[%d][%d][%d]\n",mossaid,icycl,betaindice,New_Mean_H_Bonds,New_Mean_CA_Bonds,betaindice,sist->Mean_H_Bonds,sist->Mean_CA_Bonds);
				}	
		
	#endif
		DW=Wpot[betaindice][New_Mean_H_Bonds][New_Mean_CA_Bonds]-Wpot[betaindice][sist->Mean_H_Bonds][sist->Mean_CA_Bonds];
		
		#ifdef PROG_FSEQ
		fprintf(out,"fseq G OK %lu\n",icycl);fflush(out);
		#endif
		
		espo=-beta[betaindice]*(Enew-Eold-beta2*NPROT*deltaP)+DW*beta[betaindice];
		
		
		if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
			fprintf(out,"MC_brot fw non ok icycl=%lu, espo=%lf \n",icycl,espo);
			fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(espo<50.0){
			espo1=-log(1+exp(espo));
		}else{
			espo1=-espo;
		}
		
		OEnd_New=OEnd_Old=lround(OEND_bin*sist->End/DNPROT);
		///OTopo_indice_New=OTopo_indice_Old=lround(sist->Topo_indice/DNPROT);	
		OALN_New=OALN_Old=lround(fabs(OALN_bin*sist->ALN_tot/DNPROT));	
		if(OEnd_New>=sizeEnd) OEnd_New=sizeEnd-1;
	    if(OEnd_Old>=sizeEnd) OEnd_Old=sizeEnd-1;
	    ///if(OTopo_indice_New>=sizeTopo) OTopo_indice_New=sizeTopo-1;
	    ///if(OTopo_indice_Old>=sizeTopo) OTopo_indice_Old=sizeTopo-1;
	    if(OALN_New>=sizeALN) OALN_New=sizeALN-1; 
		if(OALN_Old>=sizeALN) OALN_Old=sizeALN-1; 
	
		///sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OTopo_indice_New,espo+espo1);
		sampling(Dnew,Enew,New_Mean_CA_Bonds,Znew,New_Mean_H_Bonds,OEnd_New,OALN_New,espo+espo1);
		
		#ifdef PROG_FSEQ
		fprintf(out,"fseq H OK %lu\n",icycl);fflush(out);
		#endif
		///sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OTopo_indice_Old,espo1);
		sampling(sist->density,sist->E,sist->Mean_CA_Bonds,sist->touch,sist->Mean_H_Bonds,OEnd_Old,OALN_Old,espo1);
		
		#ifdef PROG_FSEQ
		fprintf(out,"fseq I OK %lu\n",icycl);fflush(out);
		#endif
		
		acc=exp(espo);
		
		if(ran3(&seed)>acc){ // reject
			for(pr=0;pr<NPROT;pr++){
				for(k=0;k<=NSPOTS;k++){ 
					indice=kk+k;							
					prot[pr].ammino[indice].x=oldX[pr][k];
					prot[pr].ammino[indice].y=oldY[pr][k]; 
					prot[pr].ammino[indice].z=oldZ[pr][k]; 
					updt_cell_list_SAW (pr,indice);
					updt_cell_list (pr,indice);
					
				}
				
				prot[pr].ammino[kk].residue=old_res;
			}		
			sist->np[res]=sist->p[res];
			sist->np[old_res]=sist->p[old_res];
			
			#ifdef DIPOLE_SFC
			if(E_Dip>0){
				Dipole_Reject();
			}
			#endif
			#ifdef PROG_FSEQ
			fprintf(out,"fseq L OK %lu\n",icycl);fflush(out);
			#endif
			Water_Update_Reject();
			#ifdef WATER_UP_TEST
			mossaid+=560;
			Water_Update_Test();
			mossaid-=560;
			#endif
			fseq_rejectedener++;
			return;   
		}
		
		
		
		#ifdef DIPOLE_SFC
		if(E_Dip>0){
			Dipole_Accept();
		}
		#endif
		
		Water_Update_Accept();
		#ifdef WATER_UP_TEST
		mossaid+=2100;
		Water_Update_Test();
		mossaid-=2100;
		#endif
		sist->p[res]=sist->np[res];
		sist->p[old_res]=sist->np[old_res];
		
		sist->NP=sist->NP+deltaP;
				
		fseq_accepted++;
		sist->E=Enew;
		
		sist->order=Onew;
		sist->contact=Cnew;
		sist->touch=Znew;
		sist->density=Dnew;
		sist->Mean_CA_Bonds=New_Mean_CA_Bonds;
		sist->Mean_H_Bonds=New_Mean_H_Bonds;
		
		Minimum_Energy(sist->E,sist->Mean_CA_Bonds,sist->Mean_H_Bonds);
		
		#ifdef PROG_FSEQ
		fprintf(out,"fseq M OK %lu\n",icycl);fflush(out);
		#endif
	}
	return;
}

double Seq_energy_SP (int pr,int i){
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0, add_ener_tot=0.,dotprod,dotprod2,value,energh,energhor;
	double op=0;
		
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0, add_ener=0.0,energDip=0.0;
	int numero_partners;
	
	for(indice2=0;indice2<Neigh;indice2++){
				
		icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
		icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
		icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
		
		icel_x=P_Cell(icel_x,N_CA_cells);
		icel_y=P_Cell(icel_y,N_CA_cells);
		icel_z=P_Cell(icel_z,N_CA_cells);
		
		indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
		
		prt=hoc_CA[indice]-1;
		j=hoc_CA[indice+1]-1;
		
		while(j>=0){
			if(prt==pr){
				if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)) {
				}else{
					energCA=Potential_CA(pr,i,prt,j,&add_ener);
					#ifdef DIPOLE_SFC
					energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
					add_ener_tot+=add_ener;
					#else
					energDip=Potential_Dip(pr,i,prt,j,&touch);	
					energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
					#endif
				}				
			}else{
				energCA=Potential_CA(pr,i,prt,j,&add_ener);
				#ifdef DIPOLE_SFC
				energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
				#else
				energDip=Potential_Dip(pr,i,prt,j,&touch);	
				energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
				#endif
			}
			//numero_partners++;
			
			jj=prot[prt].ammino[j].lbw_i;
			prt=prot[prt].ammino[j].lbw_pr;
			j=jj;
		}
	}		
	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->add_ener_tot=add_ener_tot;
	
	return energ;
}

double Seq_energy_SP_pswap (int pr,int i, int ii){
	
	
	/************************************************
	* energy -- calculate the energy interaction    *
	* 	    with the i residue and all the    	    *
	*	    neighbour residues and exclude  	    *
	*	    the backbone interactions	    	    *
	* 	    	    	    	    	    	    *
	* 	    	    	    	    	    	    *
	* Parameters:   	    	    	    	    *
	*	prot -- is a proteina type structure	    *
	*	    	where is stored the protein 	    *
	*	i -- the refernce residue   	    	    *
	*	res -- the type of the i residue    	    *
	*	M -- is the interaction matrix      	    *
	*	    	    	    	    	    	    *
	* Return:	    	    	    	    	    *
	*	energ -- the energy of all the contacts	    *
	*	    	 with particle i    	    	    *
	*************************************************/
	
	
	unsigned long int indice,indice2;
	int icel_x,icel_y,icel_z;
	int j,prt,jj;
	double r;
	double dx,dy,dz;
	double energ=0, add_ener_tot=0.,value;
	
	
	int touch=0,tmp_touch=0;
	double energCA=0.0, add_ener=0.0,energDip=0.0,tmp_energ=0.0;
	
	if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
			case ATOM_CA:
			
			//op+=RMSD_CA_fw(pr,i);
			for(indice2=0;indice2<Neigh;indice2++){
				
				icel_x=floor(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				
				prt=hoc_CA[indice]-1;
				j=hoc_CA[indice+1]-1;
				
				while(j>=0){
					if(prt==pr){
						if(j==ii){
							if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)) {
							}else{
							  energCA=Potential_CA(pr,i,prt,j,&add_ener)/2.0;
								#ifdef DIPOLE_SFC
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
								add_ener_tot+=add_ener;
								#else
								energDip=Potential_Dip(pr,i,prt,j,&tmp_touch)/2.0;	
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
								#endif
								
							}
						}else{
							if((j >=i-NATOM*EXCLUDED_NEIGH)&&(j <=i+NATOM*EXCLUDED_NEIGH)) {
							}else{
								energCA=Potential_CA(pr,i,prt,j,&add_ener);
								#ifdef DIPOLE_SFC
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
								#else
								energDip=Potential_Dip(pr,i,prt,j,&touch);	
								energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale+energDip*DIP_Scale;
								#endif
							}
						}				
					}else{
						energCA=Potential_CA(pr,i,prt,j,&add_ener);
						energ+=Potential_Seq(pr,i,prt,j,energCA)*CA_Scale;
						//op+=energCA;
					}
					//numero_partners++;
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
			}		
			break;
			
			/*ATOMI H*/
			case ATOM_H:
			for(indice2=0;indice2<Neigh;indice2++){
				icel_x=floor(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
				icel_y=floor(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
				icel_z=floor(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
				
				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				
				prt=hoc_H[indice]-1;
				j=hoc_H[indice+1]-1;
				
				while(j>=0){
					if(prt==pr){
						
						if(j==ii){
							
							if((j >=i-NATOM*2)&&(j <=i+NATOM*2)) {
							}else{
								tmp_touch=0;
								tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch);
								energ+=tmp_energ/2.0;
								touch+=tmp_touch/2.0;
							}
						}else{
							if((j >=i-NATOM*2)&&(j <=i+NATOM*2)) {
							}else{
								tmp_touch=0;
								tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch);
								energ+=tmp_energ;
								touch+=tmp_touch;
							}
							
						}
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
						
					}
					
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					
				}
			}		
			break;
		}
	}	
	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->add_ener_tot=add_ener_tot;
	return energ;
	
}

void Perm_Test (int err_pos){
	int j=0;
	
	for(j=0;j<S;j++){
		if(sist->p[j]<1){
			fprintf(out,"permutations Wrong %lu at %d sist->p[%d]=%d\n",icycl,err_pos,j,sist->p[j]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(sist->p[j]>Psize+1){
			fprintf(out,"permutations Wrong %lu at %d sist->p[%d]=%d\n",icycl,err_pos,j,sist->p[j]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(sist->np[j]<1){
			fprintf(out,"permutations Wrong %lu at %d sist->np[%d]=%d\n",icycl,err_pos,j,sist->np[j]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if(sist->np[j]>Psize+1){
			fprintf(out,"permutations Wrong %lu at %d sist->np[%d]=%d\n",icycl,err_pos,j,sist->np[j]);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			
		}
	}
	
	
}

void Overalp_Test (int err_pos, int reject_status){
	int err_flag=0,pr=0,k=0;
	int j=0;
	
	Perm_Test(err_pos);
	
	err_flag=0;
	for(pr=0;pr<NPROT;pr++){
		for(k=0;k<Plength[pr];k++){
			
			if((isnan(prot[pr].ammino[k].x))||(isnan(1./prot[pr].ammino[k].x))){
				fprintf(out,"Coord NAN %lu after move %d reject_status= %d prot[%d].ammino[%d].x=%lf\n",icycl,err_pos,reject_status,pr,k,prot[pr].ammino[k].x);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
			if((isnan(prot[pr].ammino[k].y))||(isnan(1./prot[pr].ammino[k].y))){
				fprintf(out,"Coord NAN %lu after move %d reject_status= %d prot[%d].ammino[%d].y=%lf\n",icycl,err_pos,reject_status,pr,k,prot[pr].ammino[k].y);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
			if((isnan(prot[pr].ammino[k].z))||(isnan(1./prot[pr].ammino[k].z))){
				fprintf(out,"Coord NAN %lu after move %d reject_status= %d prot[%d].ammino[%d].z=%lf\n",icycl,err_pos,reject_status,pr,k,prot[pr].ammino[k].z);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				
			}
			
			if(prot[pr].ammino[k].id==ATOM_CA){
				if( energy_SAW_SP ( pr, k)> 0.0) err_flag=1;
			}
		}
	}
	if(err_flag==1){
		
		fprintf(out,"Energy Overalps %d at icycl= %lu reject_status= %d T=%lf\n",err_pos,icycl,reject_status,1./beta[betaindice]);
		fflush(out);
		for(pr=0;pr<NPROT;pr++){
			for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
				fprintf(out,"%d %d %lf\n",pr,k,energy_SAW_SP ( pr, k));
			}
		}
		fflush(out);
		fsamp();
		MPI_Abort(MPI_COMM_WORLD,err5);
	}	
}


void Linking_Number ( struct proteina *polymer, int pr, int atom_CA, int node1, int node2, int plength, int natom, char *c, double *aln_partial)	{
	double 	dx, dy, dz, dxi, dyi, dzi, dxj, dyj, dzj;
	double 	aln, r2;
	int 	i, j, isucc, jsucc;
	
	if((strcmp(c,"initial")!=0) && (strcmp(c,"pivot")!=0) &&  (strcmp(c,"crankshaft")!=0) && (strcmp(c,"single_move")!=0)){
		fprintf(out,"Error. Wrong content in the string info_ALN in function Linking_Number. icycl= %lu mossaid= %d \n",icycl,mossaid);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	*aln_partial=0.;
	if (strcmp(c,"initial")==0){
	  for(i=atom_CA;i<plength-2*natom;i+=natom){
	    isucc=i+natom;
	    for(j=isucc;j<plength-natom;j+=natom){
	      jsucc=j+natom;
	      dx=polymer[pr].ammino[i].x-polymer[pr].ammino[j].x;
	      dy=polymer[pr].ammino[i].y-polymer[pr].ammino[j].y;
	      dz=polymer[pr].ammino[i].z-polymer[pr].ammino[j].z;
			
	      dxi=polymer[pr].ammino[isucc].x-polymer[pr].ammino[i].x;
	      dyi=polymer[pr].ammino[isucc].y-polymer[pr].ammino[i].y;
	      dzi=polymer[pr].ammino[isucc].z-polymer[pr].ammino[i].z;
			
	      dxj=polymer[pr].ammino[jsucc].x-polymer[pr].ammino[j].x;
	      dyj=polymer[pr].ammino[jsucc].y-polymer[pr].ammino[j].y;
	      dzj=polymer[pr].ammino[jsucc].z-polymer[pr].ammino[j].z;
			
	      aln=dx*(dyi*dzj-dyj*dzi) + dy*(dzi*dxj-dzj*dxi) + dz*(dxi*dyj-dxj*dyi);
	      r2=dx*dx+dy*dy+dz*dz;
	      (*aln_partial)+=aln/pow(r2,1.5);
	    }
	  }
	}

	if (strcmp(c,"pivot")==0){
	  for(i=atom_CA;i<node1;i+=natom){
	    isucc=i+natom;
	    for(j=node1;j<plength-natom;j+=natom){
	      if (i!=j){
		jsucc=j+natom;
		dx=polymer[pr].ammino[i].x-polymer[pr].ammino[j].x;
		dy=polymer[pr].ammino[i].y-polymer[pr].ammino[j].y;
		dz=polymer[pr].ammino[i].z-polymer[pr].ammino[j].z;
			
		dxi=polymer[pr].ammino[isucc].x-polymer[pr].ammino[i].x;
		dyi=polymer[pr].ammino[isucc].y-polymer[pr].ammino[i].y;
		dzi=polymer[pr].ammino[isucc].z-polymer[pr].ammino[i].z;
			
		dxj=polymer[pr].ammino[jsucc].x-polymer[pr].ammino[j].x;
		dyj=polymer[pr].ammino[jsucc].y-polymer[pr].ammino[j].y;
		dzj=polymer[pr].ammino[jsucc].z-polymer[pr].ammino[j].z;
			
		aln=dx*(dyi*dzj-dyj*dzi) + dy*(dzi*dxj-dzj*dxi) + dz*(dxi*dyj-dxj*dyi);
		r2=dx*dx+dy*dy+dz*dz;
		(*aln_partial)+=aln/pow(r2,1.5);
	      }
	    }
	  }
	}
	if ((strcmp(c,"crankshaft")==0) || (strcmp(c,"single_move")==0)){
	  if  ((strcmp(c,"single_move")==0) && (node1<atom_CA))
	    node1+=natom;
	  
	  if  ((strcmp(c,"single_move")==0) && (node2>plength-natom))
	    node2-=natom;
	  
	  for(i=atom_CA;i<node1;i+=natom){  // first contribution
	    isucc=i+natom;
	    for(j=node1;j<node2;j+=natom){
	      jsucc=j+natom;
	      dx=polymer[pr].ammino[i].x-polymer[pr].ammino[j].x;
	      dy=polymer[pr].ammino[i].y-polymer[pr].ammino[j].y;
	      dz=polymer[pr].ammino[i].z-polymer[pr].ammino[j].z;
			
	      dxi=polymer[pr].ammino[isucc].x-polymer[pr].ammino[i].x;
	      dyi=polymer[pr].ammino[isucc].y-polymer[pr].ammino[i].y;
	      dzi=polymer[pr].ammino[isucc].z-polymer[pr].ammino[i].z;
			
	      dxj=polymer[pr].ammino[jsucc].x-polymer[pr].ammino[j].x;
	      dyj=polymer[pr].ammino[jsucc].y-polymer[pr].ammino[j].y;
	      dzj=polymer[pr].ammino[jsucc].z-polymer[pr].ammino[j].z;
			
	      aln=dx*(dyi*dzj-dyj*dzi) + dy*(dzi*dxj-dzj*dxi) + dz*(dxi*dyj-dxj*dyi);
	      r2=dx*dx+dy*dy+dz*dz;
	      (*aln_partial)+=aln/pow(r2,1.5);
	    }
	  }
	  for(i=node1;i<node2;i+=natom){  // second contribution
	    isucc=i+natom;
	    for(j=node2;j<plength-natom;j+=natom){
	      jsucc=j+natom;
	      dx=polymer[pr].ammino[i].x-polymer[pr].ammino[j].x;
	      dy=polymer[pr].ammino[i].y-polymer[pr].ammino[j].y;
	      dz=polymer[pr].ammino[i].z-polymer[pr].ammino[j].z;
			
	      dxi=polymer[pr].ammino[isucc].x-polymer[pr].ammino[i].x;
	      dyi=polymer[pr].ammino[isucc].y-polymer[pr].ammino[i].y;
	      dzi=polymer[pr].ammino[isucc].z-polymer[pr].ammino[i].z;
			
	      dxj=polymer[pr].ammino[jsucc].x-polymer[pr].ammino[j].x;
	      dyj=polymer[pr].ammino[jsucc].y-polymer[pr].ammino[j].y;
	      dzj=polymer[pr].ammino[jsucc].z-polymer[pr].ammino[j].z;
			
	      aln=dx*(dyi*dzj-dyj*dzi) + dy*(dzi*dxj-dzj*dxi) + dz*(dxi*dyj-dxj*dyi);
	      r2=dx*dx+dy*dy+dz*dz;
	      (*aln_partial)+=aln/pow(r2,1.5);
	    }
	  }
	}
    (*aln_partial)*=ALN_PREFACTOR;
}
