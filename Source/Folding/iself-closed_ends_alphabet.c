/*
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    WITH ALPHABET AND STRUCTURE

*/


#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#define _ISOC9X_SOURCE 1
#define _ISOC99_SOURCE 1
#include <math.h>
#include <time.h>
#include <string.h>

#define Nhistogram 10
#define PROGRESS_INIT 0
#define P_Dist(dx) (dx-lround(dx/BoxSize)*BoxSize)
#define N1 (1.0/N)
#define ATOM_CA 0
#define ATOM_H 1
#define BUFFER_CHAR 2000
#define SMAX 99

#define Nbgs_Max2 24
#define Nbgs_Max 12
#define MAXAM 1
#define MAXPROPAG 0
#define Neigh 27
#define Spring_Constant 0.0
#define RMSD_bin 20.0
#define Mean_H_Bonds_bin 1.0
#define NEIGH_MAX 500
#define REJECTSELF 1
#define O_VDWR2 9.2416 /*Van der Waals Radius of Oxygene =1.52 */
struct part{
	double x;
	double y;
	double z;
	double Vx;
	double Vy;
	double Vz;
	double E,En;
	int O,C;
	int On,Cn;
	int residue,Nverl,verlpr[300],verli[300],oorder,ocontact,id;
	int lbw_pr,lbw_i;
	int lfw_pr,lfw_i;
	int lbw_SAW_pr,lbw_SAW_i;
	int lfw_SAW_pr,lfw_SAW_i;
	unsigned long int indice,indice_SAW;
	int spring_anchor;
	
};
struct proteina{
	struct part *ammino;
	
};

struct GlobalSistem{
	double ECA;
	double E;
	double order;
	int touch;
	int contact;
	double density;
	double nE;
	double packing;
	double norder;
	int ntouch;
	int ncontact;
	int RMSD;
	int Mean_H_Bonds;
	double ndensity;
	double Rmin2,Rint2,Rver2,RverH2,Rmax2,Rvgap2,RvgapH2;
	double Rmin_CAsaw,Rmin_CAsaw2;
	double Rmin,Rint,Rver,RverH,Rmax,Rvgap,RvgapH;
	double Rmin6,Rmin12,Bbond[6],RintH2,RintH,DotProd[3];
	//double side,side2,Rbond;
	char Atoms[5][10];
        char Amminoacids[SMAX][4];
	int count;
};
/*********PATCHY PARAMETER*********/
int NSPOTS=4,NATOM=5;
int SPRING_MODEL=1;
int SPG_PATCH2=4;
/********************************/
double MinO=100000.0,MinOH=100000.0;
double MinEO=100000.0,MinEH=100000.0;
double MinE=100000.0,MinOE=100000.0;
char VFlag=0;
double ANGLES;
long seed=-1;
long seed1=-1;
long seed2=-1;
long seed3=-1;
int process=0,Rmin_flag=1;
int N=30,SubN=0,NPROT=1,TOTLENGTH=90,ProtN=30,HalfN=15,flipped=0,LBox=3;
unsigned int *Plength,*Plength_half;
double HalfN2=240.25;
char *SubSeq=NULL;
int Window_Flag;
int Window_Inc=200,Window_Inc2;
int dumm;
int S, *connector=NULL;
double dumm1;

double 	Rmin[SMAX];
double Bond_factor=10;
double Bond_factor_end=1.0;
double **DistMap1=NULL,**DistMap2=NULL;
double MINR=1000.0;
double SHIFT=4.0*5.0/6.0;
double sigmahb12,sigmahb10,sigma6;
double *beta=NULL;
int betaindice;
int Lattice;
int Ustart_Flag=0;
int NLINKS=0;
double umbrella=0.2,Enative;
double Rmin_final,Rmin_final2,Rmin_init=2.0;
int native1,native2;
int minO,maxO;
int sizeZ;
double Numberofsubstrates;
double LJpower=3.0;

unsigned long long int ntest=0,icycl,Niteration;
unsigned long long int ncycl,dur,deltacycle=0;
unsigned long long int Equi1,Equi2,Equi3,nsamp;

double subwaterinter=0,TempBias=0;
int sizex,sizeC,p,my_rank,err5=0,spacing=2;
int sizeEnergy=0, maxE=0, minE=0;
double bin_energy=1.0, Max_energy=0.0, Min_energy=0.0;

double HydrogenBond1,HydrogenBond2,HydrogenBond3,HydrogenBond4,HydrogenBond_r=5.0,HydrogenBond5,HydrogenBond6;
double EHmin,EHmin2;
double LJ2_a,LJ2_b,LJ2_c;
double HB_E_Scale;	
double bin_x;
double 	*rndataen2,*ndataen2,*Density,*rDensity;
double 	*Whisto=NULL,*histo=NULL,*histoC=NULL,***Wpot=NULL,*histofill=NULL,*rhisto=NULL,*rhistoC=NULL,*rhistofill=NULL;
double  *histo_energy=NULL, *rhisto_energy=NULL;

double *ntempswap=NULL,*temthetasto=NULL;
double *rntempswap=NULL,*rtemthetasto=NULL;
double *rootminE=NULL,*rootminO=NULL,*rootminEH=NULL,*rootminEO=NULL,*rootminOH=NULL,*rootminOE=NULL,nmean=0;
struct proteina *prot=NULL;
struct proteina *minOprot=NULL;
struct proteina *maxOprot=NULL;
struct proteina *minprot=NULL;
struct proteina *sub=NULL;
struct GlobalSistem *sist=NULL;
double **M=NULL;

FILE *fSeq=NULL,*fEner=NULL,*fTempControl=NULL,*fHisto_Ord=NULL,*fHisto_2D_Energy=NULL,*out=NULL,*EHout=NULL,*EHout2=NULL,*ECAout=NULL,*fSeqvmd=NULL;
FILE *fMC_brot_bgs=NULL,*fMC_Pivot_fw=NULL,*fMC_All_trasl=NULL,*fMC_brot_local=NULL,*fMC_Pivot_bw=NULL,*fMC_brot_cbmc_fw=NULL;

MPI_Op myExpSum;
double DNPROT;
double PI,PI_2,PI2,PI_180;
double BoxSize,BoxSize2;
double EH_Power=2.0,ECA_Range=7.0,ECA_Range2,RMS_Range=1.0;
double EH_LJPower1=12.0,EH_LJPower1_2=6.0,EH_LJPower2=3.0,EH_LJPower2_2=1.5;

/**************** BGS PARAMETERS*******************/
double abgs=1.0,bbgs=100.0;
/***************************************************/

/*****************CELL LIST******************/
unsigned int *hoc_CA=NULL;
unsigned int *hoc_H=NULL;
unsigned int *hoc_SAW=NULL;
double cell_CA_size,cell_SAW_size,cell_H_size,cell_O_size;
double cell_CA_factor,cell_SAW_factor,cell_H_factor,cell_O_factor;
int N_SAW_cells,N_CA_cells,N_H_cells,N_O_cells;
double *neighcell_CA=NULL,*neighcell_H=NULL,*neighcell_SAW=NULL;
int *neighcells=NULL;
double period_factor_CA,period_factor_H,period_factor_SAW;
double inv_period_factor_CA,inv_period_factor_H,inv_period_factor_SAW;
/********************************************/

/************RMSD****************************/
int *RMSD_neigh=NULL,*RMSD_Neigh=NULL;
double TotRMSDNeigh;
int AmminoN;
/********************************************/

/*************Platonic Solids***************/
void Tetrahedron (double **vertices);
void Cube (double **vertices);
void Dodecahedron (double **vertices);
void Octahedron (double **vertices);
void Icosahedron (double **vertices);
/********************************************/
void Rotation_Random_Sphere (int pr,int k);

int partint 		(double e);
void native 		(void);
void finit		(void);

/*************Bond energy functions***************/
double Bond_Energy_fw (int pr,int input_k);
double Bond_Energy_bw (int pr,int input_k);
double Bond_Energy_SP(int pr,int input_k);

/*************Interaction energies***************/
double energy		(void);
double energy_SP_brot_local (int pr,int i,int min,int max);
double energy_SP(int pr,int i);
double energy_SP_Pivot_fw (int pr,int i);
double energy_SP_Pivot_bw (int pr,int i);
/********************************************/

/*************Self-avoiding Checks***************/
double energy_SAW (int pr,int i);
double energy_SAW_output (int pr,int i);
double energy_SAW_bw (int pr,int i);
double energy_SAW_SP (int pr,int i);
/********************************************/

void fsamp		(void);
void rotor		(void);
void trasl		(void);
void rotor2 		(void);
void flip 		(void);
void hanch		(void);
void Tswap		(void);
void order_All 		(void);
void order_SP (int pr, int i);
void order_test(int *C, double *O, double *E);
double gasdev(long *idum);

void Minimum_Energy (double E,int order,int contact);
void Order_Boudaries (double E,int order,int contact);

void adjustCCAN_bw (int pr,int input_k);
void adjustCCAN_fw (int pr,int input_k);
int MC_SP_trasl(int pr);
int MC_SP_rot(int pr);
int MC_All_trasl(int pr);
int MC_Pivot_fw(int,int);
int MC_brot_local(int);
int MC_Pivot_bw(int,int);
//void MC_trot_fw(int);
//void MC_trot_bw(int);   Obsolete
//void MC_rot(int pr);
///void MC_brot_local_2 (int pr);
///void MC_brot_bgs(int pr);
void Rescaler (void);
void Inflate (void);

/*****************CELL LIST******************/
void new_cell_list (void);
void new_cell_list_SAW (void);
void updt_cell_list (int pr, int i);
void updt_cell_list_SAW (int pr, int i);
int  P_Cell (int x, int N_cels);
//double P_Dist(double dx);
double  P_Cd (double x);
void test_celllist_SAW(void);
void test_celllist_CA(void);
void test_celllist_H(void);
/*******************************************/

/************RMSD****************************/
double RMSD_CA_local(int pr,int i,int min, int max);
double RMSD_CA_SP (int pr,int i);
double RMSD_CA_fw (int pr,int i);
double RMSD_CA_bw (int pr,int i);
/*******************************************/


double Potential_H (int pr,int i,int prt,int j,int *touch );
double Potential (double r,int pr,int i,int prt,int j,int resi,int resj);
double Potential_CA (int pr,int i,int prt,int j);
void W		(void);
double gasdev		(long *idum);
double ran3		(long *idum);
double ran35 		(long *idum);
double ran36 		(long *idum);
double ran37 		(long *idum);
 long lround(double x);
void fastadecoder 	(char **Dec, char **Enc);
void fastadecoder2 (char *Dec, char *Enc);
void sampling 		(double density, double E,int order,int touch,int contact,double espo);
void order_packing (void);
void ExpSum (void *in,void *inout, int *len, MPI_Datatype *dptr);
void Rotation (int pr,int k,double alpha,double u1,double u2,double u3,int flag);
int writeBinary(FILE *);
int readBinary(FILE *);
int  write_psf(void);


unsigned long long int bgs_rejectedself=0,bgs_rejectedener=0,bgs_tried=0,bgs_accepted=0;
unsigned long long int cbmc_fw_rejectedself=0,cbmc_fw_rejectedener=0,cbmc_fw_tried=0,cbmc_fw_accepted=0,efficiency=0,efficiency_tried=1;
unsigned long long int local_rejectedself=0,local_rejectedener=0,local_tried=0,local_accepted=0;
unsigned long long int fw_rejectedself=0,fw_rejectedener=0,fw_tried=0,fw_accepted=0;
unsigned long long int trasl_rejectedself=0,trasl_rejectedener=0,trasl_tried=0,trasl_accepted=0;
unsigned long long int bw_rejectedself=0,bw_rejectedener=0,bw_tried=0,bw_accepted=0;
unsigned long long int MC_SP_trasl_rejectedself=0,MC_SP_trasl_rejectedener=0,MC_SP_trasl_tried=0,MC_SP_trasl_accepted=0;
unsigned long long int MC_SP_rot_rejectedself=0,MC_SP_rot_rejectedener=0,MC_SP_rot_tried=0,MC_SP_rot_accepted=0;;

double ERepul=0.5;
double BGS_Av_Displacement=0.0,BGS_Sigma_Displacement=0.0,BGS_Trial_Displacement=0.0; 
double BGS_Av_Displacement2=0.0,BGS_Sigma_Displacement2=0.0;
double starttime=0,endtime=0;
double HR_Angle1=90.0,HR_Angle2=90.0;
int maxH,IminO,ImaxO,Umbrella_Flag;

int JOBID;
int main(int argc, char** argv){
	
	int mossa=0;
	int self_rejected_flag=0;
	int traslator=3.0;
	int ii,i,j,k,pr,prt,ppr,kk;
	int test,test1;
	double Eold2;
	double pass=0,reduce,Xg,Cg,R;
	double totalvolume=0.0;
	double PTFreq;
	double dx,dy,dz,r2,r;
	double pt_test;
	char* message=NULL;
	char* line=NULL;
	int testC;
	double testE,testO;
	int size=0;
	FILE *fp=NULL;
	FILE *fHisto_3Dnt=NULL,*fHisto_2Dnt=NULL,*fHisto_3Dt=NULL,*fHisto_2Dt=NULL,*fHisto_contact=NULL,*fHisto_Meannt,*fHisto_Meant,*fHisto_contacta=NULL;
	
	message=(char *) calloc (BUFFER_CHAR,sizeof(char));
	line=(char *) calloc (BUFFER_CHAR,sizeof(char));
	MPI_Init(&argc,&argv);
	printf("Init Init\n");fflush(NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	//if(my_rank==0){
		starttime=MPI_Wtime();
	//}
	MPI_Type_size( MPI_DOUBLE, &size );
	printf("double %d MPI_DOUBLE %zu\n",size,sizeof(double));
	MPI_Type_size( MPI_INT, &size );
	printf("int %d MPI_INT %zu\n",size,sizeof(int));
	
#ifdef RUN_ON_SCRATCH	
		fp=fopen("jobid.dat","r");	
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		sscanf (line,"%d\n",&JOBID);
		printf("JobID=%d\n",JOBID);
		fflush(NULL);
		sprintf(message,"/work/tmp/icoluzza/%d/",JOBID);

		if(chdir(message)!=0){
			printf("%d %s does not exists\n",my_rank,message);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}

		fclose(fp);	
	#endif	
	Window_Inc2=Window_Inc/2;
	sprintf(message,"OUT-P-%d.dat",my_rank);
	out=fopen(message,"w");
	
	sprintf(message,"EHOUT-P-%d.dat",my_rank);
	EHout=fopen(message,"w");
	sprintf(message,"EHOUT2-P-%d.dat",my_rank);
	EHout2=fopen(message,"w");
	sprintf(message,"ECAOUT-P-%d.dat",my_rank);
	ECAout=fopen(message,"w");
	
	sprintf(message, "MC_brot_cbmc_fw-P-%d.dat",my_rank);
	fMC_brot_cbmc_fw=fopen(message,"w");
	sprintf(message, "MC_brot_bgs-P-%d.dat",my_rank);
	fMC_brot_bgs=fopen(message,"w");
	sprintf(message, "MC_brot_local-P-%d.dat",my_rank);
	fMC_brot_local=fopen(message,"w");
	sprintf(message, "MC_Pivot_fw-P-%d.dat",my_rank);
	fMC_Pivot_fw=fopen(message,"w");
	sprintf(message, "MC_Pivot_bw-P-%d.dat",my_rank);
	fMC_Pivot_bw=fopen(message,"w");
	sprintf(message, "MC_All_trasl-P-%d.dat",my_rank);
	fMC_All_trasl=fopen(message,"w");
	
	fprintf(fMC_brot_bgs,"#bgs_rejectedself,bgs_rejectedener,bgs_accepted,bgs_tried\n");
	fprintf(fMC_brot_local,"#local_rejectedself,local_rejectedener,local_accepted,local_tried\n");
	fprintf(fMC_Pivot_fw,"#fw_rejectedself,fw_rejectedener,fw_accepted,fw_tried\n");
	fprintf(fMC_All_trasl,"#trasl_rejectedself,trasl_rejectedener,trasl_accepted,trasl_tried\n");
	fprintf(fMC_Pivot_bw,"#bw_rejectedself,bw_rejectedener,bw_accepted,bw_tried\n");
	
	PI=atan(1)*4;
	PI_2=atan(1)*2;
	PI2=atan(1)*8;
	PI_180=atan(1)*4/180;
	MPI_Op_create(&ExpSum,1,&myExpSum);
	sist= (struct GlobalSistem *)calloc(1,sizeof(struct GlobalSistem));
	
	fp=fopen("aapot.dat","r");
	fscanf(fp,"%d",&S);
	S=S+1;// we include into the alphabet also the solvent molecule
	fclose(fp);
	if(S>SMAX){
		fprintf(out,"THE ALPHABET IS TOO BIG!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	fp=fopen("patches_structure.dat","r");
	if ( fp == NULL) {
	  printf ("File patches_structure.dat not found!!!\n");
	  fflush(NULL);
	  MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
	  fscanf(fp,"%d",&NSPOTS);
	  SPG_PATCH2=NSPOTS;
	  fclose(fp);
	}

	M=(double**)calloc(S,sizeof(double*));
	if (M == NULL) {
		fprintf(out,"Allocation M Failed\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	for(i=0;i<S;i++){
		M[i]=(double*)calloc(S,sizeof(double));
		if (M[i] == 0) {
			fprintf(out,"Allocation M[%d] Failed\n",i);
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}
	
	
	
	
	beta=(double *)calloc(p,sizeof(double));
	if(beta==NULL){
		fprintf(out,"allocation of beta failed in main\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	
	/************************************
	* 	Raccogliamo i parametri della	* 
	*	per la simulazione		*
	*************************************/
	fp=fopen("param.dat","r");
	if ( fp == NULL) {
		printf ("File param.dat not found!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		while(feof(fp)==0){
			fgets(line,BUFFER_CHAR*sizeof(char),fp);
			switch (line[0]){
				case 'A':
					switch (line[1]){
					case 'I':
					sscanf (line,"AI%lf\n",&pass);
					sist->Rmin_CAsaw=pass;
					break;
					case 'F':
					sscanf (line,"AF%lf\n",&pass);
					Rmin_final=pass;
					Rmin_final2=Rmin_final*Rmin_final;
					break;
					case '1':
					sscanf (line,"A1%lf\n",&pass);
					HR_Angle1=pass;
					break;
					case '2':
					sscanf (line,"A2%lf\n",&pass);
					HR_Angle2=pass;
					break;
					
					}
					
				
				
				break;
				case 'N':
					switch (line[1]){
						case 'C':
							sscanf (line,"NC%llu\n",&ncycl);
						break;
						case 'I':
							sscanf (line,"NI%llu\n",&Niteration);
						break;
						case 'P':
							sscanf (line,"NP%d\n",&NPROT);
							DNPROT=(double)(NPROT);
						break;
						
					}
			
				break;
				case 'P':
				sscanf (line,"P%lf\n",&PTFreq);
				break;
				case 'C':
				sscanf (line,"C%llu\n",&nsamp);
				break;
				case 'R':
				sscanf (line,"R%ld\n",&seed);
				process=(int)(seed);
				seed=-seed;
				seed1=seed;
				seed2=seed;
				seed3=seed;
				break;
				case 'U':
					switch (line[1]){
					case 'F':
					sscanf (line,"UF%d\n",&Umbrella_Flag);
					break;	
					case 'H':
					sscanf (line,"UH%d\n",&maxH);
					break;
					case 'm':
					sscanf (line,"Um%d\n",&IminO);
					break;
					case 'M':
					sscanf (line,"UM%d\n",&ImaxO);
					break;
					case 'E':
					sscanf (line,"UE%lf\n",&Max_energy);
					break;
					case 'e':
					sscanf (line,"Ue%lf\n",&Min_energy);
					break;
					}
				break;
				case 'S':
				sscanf (line,"S%d\n",&spacing);
				break;
				case 'O':
				sscanf (line,"O%d\n",&Lattice);
				break;
				case 'E':
				switch (line[1]){
					case '1':
					sscanf (line,"E1%llu\n",&Equi1);
					break;
					case '2':
					sscanf (line,"E2%llu\n",&Equi2);
					break;
					case '3':
					sscanf (line,"E3%llu\n",&Equi3);
					break;
					case 'S':
					sscanf (line,"ES%lf\n",&HB_E_Scale);
					break;
					case 'H':
						switch (line[2]){
							case '1':
							sscanf (line,"EH1%lf\n",&EH_Power);
							break;
							case '2':
							sscanf (line,"EH2%lf\n",&EH_LJPower2);
							break;		
							case 'm':
							sscanf (line,"EHm%lf\n",&ERepul);
							break;
						}
					break;
					case 'R':
					sscanf (line,"ER%lf\n",&ECA_Range);
					break;
				}
				case 'B':
				switch (line[1]){
					case 'A':
					sscanf (line,"BA%lf\n",&abgs);
					break;
					case 'B':
					sscanf (line,"BB%lf\n",&bbgs);
					break;
					case 'F':
					sscanf (line,"BF%lf\n",&Bond_factor);
					break;
					case 'E':
					sscanf (line,"BE%lf\n",&Bond_factor_end);
					break;
					case 'e':
					sscanf (line,"BE%lf\n",&bin_energy);
					break;
				        case 'M':
					sscanf (line,"BM%d\n",&SPRING_MODEL);
					break;	
					
				}
				break;
				case 'L':
					sscanf (line,"L%lf\n",&BoxSize);
				break;
				
				
				default:
						
				break;
			}
			
		}
		fclose(fp);
	}
	NATOM=NSPOTS+1;
	i=1;
	do	{
	  sprintf(sist->Amminoacids[i],"%03d",i);
	  i++;
	} while (i<S);


	ECA_Range2=ECA_Range*ECA_Range;
	sist->Rmin=sist->Rmin_CAsaw;
	#ifdef HP
	#else
	for(i=0;i<S;i++){
		Rmin[i]=sist->Rmin/2;
		///fprintf(out,"Rmin[%d]=%lf",i,Rmin[i]);fflush(out); /// TEEEEEEEST
	}
	#endif

	
	
	strcpy(sist->Atoms[ATOM_CA],"CA ");
	strcpy(sist->Atoms[ATOM_H],"H  ");
	
	sist->Bbond[0]=1.4500000;//sist->RbondCaCa= 1.45000;
	//sist->Bbond[1]=1.5200000;//sist->RbondCCa=1.52000;
	//sist->Bbond[2]=1.2300000;//sist->RbondCO= 1.23000;
	//sist->Bbond[3]=1.3300000;//sist->RbondCN= 1.33000;
	sist->Bbond[1]=sist->Rmin_CAsaw/2.0;//sist->RbondCAH= 1.00000;
	
	
	
	
	
	//sist->Rbond=3.8;
	
	
	//if(ECA_Range+5.0>20.0){
		
		/*12=7(H-bond range) + 2(1+1 max distance O-Ca N-CA) + 3(safety region) is to make shure that even for 
		short range interactions the Hydrogen bonds are still taken into account*/
		
		sist->Rint=ECA_Range+5.0;sist->Rver=5.0+sist->Rint;sist->Rmax=sist->Rmin;sist->Rvgap=(sist->Rver-sist->Rint)/2.0-0.1;
	//}else{
		//sist->Rint=17.0;sist->Rver=5.0+sist->Rint;sist->Rmax=sist->Rmin;sist->Rvgap=(sist->Rver-sist->Rint)/2.0-0.1;
	//}
	//sist->Rint=ECA_Range;
	/*is divided by 2.0 because the maximum variation for a move without update is 2*Rvgap (one per particle) and this should 
	not bring particle close enough to interact so 2*Rvgap=Rver-Rint*/
	//fprintf(out,"%lf %lf %lf %lf\n",sist->Rint,sist->Rver,sist->Rmax,sist->Rvgap);
	
	/*HydrogenBond1=cos((90.0/180.0)*PI);
	HydrogenBond2=cos((90.0/180.0)*PI);*/
			
	if(HR_Angle1>140) {
		HydrogenBond3=cos((HR_Angle1/180.0)*PI);
	}else{
		HydrogenBond3=cos((140.0/180.0)*PI);
	}
	if(HR_Angle2>140) {
		HydrogenBond4=cos((HR_Angle2/180.0)*PI);
	}else{
		HydrogenBond4=cos((140.0/180.0)*PI);
	}
	
	HydrogenBond1=cos((HR_Angle1/180.0)*PI);
	HydrogenBond2=cos((HR_Angle2/180.0)*PI);
	HydrogenBond_r=2.5;
	HydrogenBond_r*=HydrogenBond_r;
	sist->RintH=6.0;
	sist->RverH=8.0*sist->RintH;sist->RvgapH=(sist->RverH-sist->RintH)/3.0-0.1;
	
	sist->Rmin2=sist->Rmin*sist->Rmin;
	sist->Rmin_CAsaw2=sist->Rmin_CAsaw*sist->Rmin_CAsaw;
	sist->Rint2=sist->Rint*sist->Rint;
	sist->RintH2=sist->RintH*sist->RintH;
	sist->Rver2=sist->Rver*sist->Rver;
	sist->RverH2=sist->RverH*sist->RverH;
	sist->Rmax2=sist->Rmax*sist->Rmax;
	sist->Rvgap2=sist->Rvgap*sist->Rvgap;
	sist->RvgapH2=sist->RvgapH*sist->RvgapH;
	
	
	
	/*sist->Rmin6=pow(sist->Rmin2,LJpower);
	sist->Rmin12=sist->Rmin6*sist->Rmin6;*/
	
	sigma6=pow(sist->Rmin,6.0);
	EHmin=3.1*HB_E_Scale;
	EHmin2=-EHmin/2.0;
	
	EH_LJPower1_2=EH_LJPower1/2.0;
	EH_LJPower2_2=EH_LJPower2/2.0;
	sigmahb10=pow(2.0,EH_LJPower2)*EH_LJPower1_2*EHmin/(EH_LJPower1_2-EH_LJPower2_2);
	sigmahb12=pow(2.0,EH_LJPower1)*EH_LJPower2_2*EHmin/(EH_LJPower1_2-EH_LJPower2_2);
	
	
	ncycl=ncycl+Equi1+(Equi2+Equi3)*Niteration;
	///dur=partint(ncycl/50);
	///if (dur==0) dur=1;
	
	fprintf(out,"prima di finit ok\n");
	/********************
	* File Creation *
	*********************/
	sprintf(message, "Seq-P-%d-%d.xyz",my_rank,process);
	fSeq=fopen(message,"w");
	if(my_rank==0){
	  sprintf(message,"TempControl-O-%d-P-%d.dat",Lattice,process);
	  fTempControl=fopen(message,"w");
	  if(fTempControl==NULL){
	    fprintf(out,"fTempControl creation failed\n");fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	}
	
	sprintf(message, "Ener-P-%d-%d.dat",my_rank,process);
	fEner=fopen(message,"w");
	
	if(fEner==NULL){
		fprintf(out,"fEner creation failed\n");fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if(fSeq==NULL){
		fprintf(out,"fSeq creation failed\n");fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/************************************************/
	
	/************************************
	* 	Inizializiamo le Variabili	* 
	*************************************/
	finit();
	
	fprintf(out,"finit ok\n");
	fflush(out);
	if(my_rank==0){
		write_psf();
	}
	fprintf(out,"native\n");
	fflush(out);
	native();
	fprintf(out,"native ok\n");
	fflush(out);
	//order_all();
	
	sist->E=energy();
	fprintf(out,"sist->E=%lf without Bond_energy \n",sist->E);fflush(out);
	sist->order=sist->norder;
	sist->touch=sist->ntouch;
	sist->contact=sist->ncontact;
	sist->RMSD=lround(RMSD_bin*sqrt(sist->order/TotRMSDNeigh));
	if(sist->RMSD>=maxO) sist->RMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
	if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	fprintf(out,"sist->order=%lf TotRMSDNeigh=%lf\n",sist->order,TotRMSDNeigh);fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
		  sist->E+=Bond_Energy_fw(pr,i);
		}
	}
	fprintf(out,"sist->E=%lf with Bond_energy \n",sist->E);fflush(out);
	sizex=20*RMSD_bin;
	maxO=sizex;
	minO=0;
	sizeC=maxH;
	printf("sizex=%d sizeC=%d \n",sizex,sizeC);
	printf("minO=%d maxO=%d \n",minO,maxO);
	//if(sub!=NULL) sizeC=2*ProtN;
	maxE=(int)(Max_energy*bin_energy);
	minE=(int)(Min_energy*bin_energy);
	sizeEnergy=maxE-minE;
	printf("minE=%d maxE=%d sizeEnergy=%d\n",minE,maxE,sizeEnergy);
	HalfN2=((ProtN+1)/2)*((ProtN+1)/2);
	
	
	
	
	temthetasto=(double *)calloc(p,sizeof(double));
	if(temthetasto==NULL){
		fprintf(out,"allocation of temthetasto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	ntempswap=(double *)calloc(p,sizeof(double));
	if(ntempswap==NULL){
		fprintf(out,"allocation of ntempswap failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	rtemthetasto=(double *)calloc(p,sizeof(double));
	if(rtemthetasto==NULL){
		fprintf(out,"allocation of rtemthetasto failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	rntempswap=(double *)calloc(p,sizeof(double));
	if(rntempswap==NULL){
		fprintf(out,"allocation of rntempswap failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	rootminE=(double *)calloc(p,sizeof(double));
	rootminEH=(double *)calloc(p,sizeof(double));
	rootminO=(double *)calloc(p,sizeof(double));
	rootminOE=(double *)calloc(p,sizeof(double));
	rootminOH=(double *)calloc(p,sizeof(double));
	rootminEO=(double *)calloc(p,sizeof(double));
	
	rDensity=(double *)calloc(3*p,sizeof(double));	
	Density=(double *)calloc(3*p,sizeof(double));
	
	rndataen2=(double *)calloc(p,sizeof(double));	
	ndataen2=(double *)calloc(p,sizeof(double));
	
	Whisto=(double *)calloc(p*sizeC*sizex,sizeof(double));
	
	
	
	
	Wpot=(double ***)calloc(p,sizeof(double **));
	if( Wpot==NULL){
		fprintf(out,"allocation ofWpot failed in main\n");
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	for(i=0;i<p;i++){
		Wpot[i]=(double **)calloc(sizeC,sizeof(double*));
		for(j=0;j<sizeC;j++){
			Wpot[i][j]=(double *)calloc((sizex),sizeof(double));	
		}	
	}
	
	
	
	
	
	
	
	rhistoC=(double *)calloc(p*sizeC*sizex,sizeof(double ));	
	rhisto=(double *)calloc(p*2*sizex*1000,sizeof(double ));		
	rhistofill=(double *)calloc(p*2*sizex,sizeof(double ));
	
	
	histoC=(double *)calloc(p*sizeC*sizex,sizeof(double ));
	histo=(double *)calloc(p*2*sizex*1000,sizeof(double ));
	histofill=(double *)calloc(p*2*sizex,sizeof(double ));
	
	rhisto_energy=(double *)calloc(p*sizeEnergy,sizeof(double ));
	histo_energy=(double *)calloc(p*sizeEnergy,sizeof(double ));
	
	deltacycle=Equi1;
	
	/*ndataen2=0.0;
	Density0=0.0;
	Density1=0.0;
	Density2=0.0;*/
	fprintf(out,"allocation ok\n");
	for(i=0;i<3*p;i++){
		Density[i]=-1000000.0;
	}
	for(i=0;i<p;i++){
		ndataen2[i]=-1000000.0;
	}
	for(i=0;i<p*sizeC*sizex;i++){
		Whisto[i]=0.0;
		histoC[i]=-1000000.0;
	}
	for(k=0;k<p*2*sizex*1000;k++){
		histo[k]=-1000000.0;		
	}
	for(k=0;k<p*2*sizex;k++){
		histofill[k]=-1000000.0;		
	}
	for(i=0;i<p;i++){
		temthetasto[i]=0.0;
		ntempswap[i]=1.0;
		rtemthetasto[i]=0.0;
		rntempswap[i]=0.0;
	}
	for(k=0;k<p*sizeEnergy;k++){
		histo_energy[k]=-1000000.0;		
	}
	fprintf(out,"Histo Init ok\n");
	fflush(out);
	
	
	for(k=0;k<p;k++){
		sprintf(message,"Wpot-T-%2.3f.dat",1/beta[k]);
		fp=fopen(message,"r");
		if ( fp == NULL) {
			for(i=0;i<sizeC;i++){
				for(j=0;j<sizex;j++){		
					Wpot[k][i][j]=0.0;	
				}	 
			}
		}else{
			
			
			
			fread(&i,sizeof(int),1,fp);
			if(i!=sizeC){
				fprintf(out,"Init Wpot reading on file %s wrong SizeC %d %d\n",message,i,sizeC);

			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
			}
			fread(&i,sizeof(int),1,fp);
			if(i!=sizex){
				fprintf(out,"Init Wpot reading on file %s wrong sizex %d %d\n",message,i,sizex);

			fflush(out);
			
			MPI_Abort(MPI_COMM_WORLD,err5);
			}
			fread(&i,sizeof(int),1,fp);
			if(i!=IminO){
				fprintf(out,"Init Wpot reading on file %s wrong minO %d %d\n",message,i,sizex);

				fflush(out);
			}
			for(i=0;i<sizeC;i++){
				for(j=0;j<sizex;j++){

					
					fread(&bin_x,sizeof(double),1,fp);
					Wpot[k][i][j]=bin_x;

				}

			}
			fclose(fp);
		}
	}
	
	
	
	
	
	Numberofsubstrates=(double)(SubN);
	totalvolume=(LBox*(ProtN)+1)*(LBox*(ProtN)+1)*((LBox-1)*(ProtN)+1);
	
	
	
	
	if(my_rank==0){
		fp=fopen("SimPar","a");
		fprintf(fp,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
		fprintf(fp,"Number of cycles\t\t\t\t=\t %15llu\n",ncycl);
		fprintf(fp,"Number of samplings\t\t\t\t=\t %15llu\n",nsamp);
		fprintf(fp,"Umbrella strenght \t\t\t\t=\t %15.5g\n",umbrella);
		fprintf(fp,"Umbrella forced Minimun (Lattice) \t\t=\t %15d\n",Lattice);
		fprintf(fp,"Equilibrium times \tE1\t\t\t=\t %15llu\n",Equi1);
		fprintf(fp,"Equilibrium times \tE2\t\t\t=\t %15llu\n",Equi2);
		fprintf(fp,"Equilibrium times \tE3\t\t\t=\t %15llu\n",Equi3);
		
		fprintf(fp,"BGS PARAM A \t\t\t=\t %lf\n",abgs);
		fprintf(fp,"BGS PARAM B \t\t\t=\t %lf\n",bbgs);
		fprintf(fp,"Number of multicanonical iterations I\t\t=\t %15llu\n",Niteration);
		fprintf(fp,"Box Size\t\t=\t %lf\n",BoxSize);
		fprintf(fp,"\n Bulk Volume %lf\nNumber of Substrates %lf\n Surface Volume ratio %lf \n",totalvolume,Numberofsubstrates,Numberofsubstrates/totalvolume);
		
		fprintf(fp,"Temperatures\n");
		for(i=0;i<p;i++){
			fprintf(fp,"%lf ",1/beta[i]);
		}
		fprintf(fp,"\n");
		fclose(fp);
	}
	
	fprintf(out,"Before fsamp\n");
	fflush(out);
	
	fsamp();
	
	
	fprintf(out,"Before Barrier\n");
	fflush(out);
	

	
	MPI_Barrier(MPI_COMM_WORLD);
	
	/*fprintf(out,"INFLATING\n");
	fflush(out);
	icycl=1;
	while(Rmin_flag==1){
		
		pr=(int)(ran3(&seed)*NPROT);

		//MC_brot_bgs_init(pr);


		if(icycl%100000==0) Rescaler();
		if(icycl%100==0) Inflate();
		icycl++;
	}
	*/	
	sist->E=energy();
	fprintf(out,"sist->E=%lf \n",sist->E);fflush(out);
	sist->order=sist->norder;
	sist->touch=sist->ntouch;
	sist->contact=sist->ncontact;
	sist->RMSD=lround(RMSD_bin*sqrt(sist->order/TotRMSDNeigh));
	if(sist->RMSD>=maxO) sist->RMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	fprintf(out,"sist->order=%lf TotRMSDNeigh=%lf\n",sist->order,TotRMSDNeigh);fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			sist->E+=Bond_Energy_fw(pr,i);
		}
	}
	
	for(ppr=0;ppr<NPROT;ppr++){
		for(ii=ATOM_CA;ii<Plength[ppr];ii+=NATOM){
				if(energy_SAW(ppr,ii)>1000.0){
						fprintf(out,"OVERLAP ppr=%d ii=%d\n",ppr,ii);
					/*for(pr=0;pr<NPROT;pr++){
						for(i=0;i<Plength[pr];i++){
							fprintf(out,"%lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
							fflush(out);
						}
								
					}*/
					energy_SAW_output(ppr,ii);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
			}
		}
	
	order_test(&testC,&testO,&testE);
	if(sist->contact!=testC){
		fprintf(out,"CC INIT   %d %d\n",sist->contact,testC);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(sist->order-testO)>1e-5){
		fprintf(out,"CC b INIT %10.5lf %10.5lf\n",sist->order,testO);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	
	if(fabs(sist->E-testE)>1e-8){
		fprintf(out,"CC c INIT %15.10lf %15.10lf\n",sist->E,testE);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	
	fsamp();
	fprintf(out,"ENERGIAAAAAAA###########%lf\n",sist->E);
	fflush(out);
	if(my_rank==0){
		
		fp=fopen("SimPar","a");
		fprintf(fp,"Energia iniziale %lf %d\n",sist->E,NPROT);
		fclose(fp);
	}
	
	
	fprintf(out,"SIMULATION\n");fflush(out);
	MPI_Barrier(MPI_COMM_WORLD);
	//MPI_Abort(MPI_COMM_WORLD,err5);


	
	for(icycl=1;icycl<=ncycl;icycl++){
		
		
		/******************/
		/*Esecuzione Mosse*/
		/******************/
		if(NPROT>1) traslator=4.0;
		pt_test=ran36(&seed1);
		if(pt_test>PTFreq){
			
			
			pr=(int)(ran3(&seed)*NPROT);
			
			
			//if(icycl>0) {fprintf(out,"##############MC_brot_local %llu\n",icycl);fflush(out);}	
			//MC_brot_local(pr);
			//cbmc_fw_tried++;
			//if(icycl>0) {fprintf(out,"##############MC_brot_local OK %llu\n",icycl);fflush(out);}
#ifdef NOT_TOPOLOGY_PRES	
			traslator=4.0;
#else
			traslator=2.0;
#endif
			mossa=(int)(ran3(&seed)*traslator);
			//mossa=(int)(ran3(&seed)*2)+3;
			switch(mossa){	
				
				case 0: 		
#ifdef PROGRESS			
					if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_SP_trasl %llu\n",icycl);fflush(out);}	
						 #endif
				
				self_rejected_flag=MC_SP_trasl(pr);
				
				
				
#ifdef PROGRESS
     if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_SP_trasl ok %llu\n",icycl);fflush(out);}	  
 #endif
				MC_SP_trasl_tried++;
				break;
				case 1: 		
#ifdef PROGRESS			
					if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_SP_rot %llu\n",icycl);fflush(out);}	
						 #endif
				
				self_rejected_flag=MC_SP_rot(pr);
				
				
				
#ifdef PROGRESS
     if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_SP_rot ok %llu\n",icycl);fflush(out);}	  
 #endif
				MC_SP_rot_tried++;
				break;
				case 2: 
					
					kk=(int)(ran3(&seed)*((Plength[pr]/NATOM-10)))+5;
					
					if(kk<Plength[pr]/(2.0*NATOM))	{	
						#ifdef PROGRESS
						  if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_Pivot_bw %llu\n",icycl);fflush(out);} 
						 #endif

										self_rejected_flag=MC_Pivot_bw(pr,kk);

										bw_tried++;
						#ifdef PROGRESS
						  if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_Pivot_bw ok %llu\n",icycl);fflush(out);}	  
						 #endif
					}else{

							  #ifdef PROGRESS
						   if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_Pivot_fw %llu\n",icycl);fflush(out);	} 
						 #endif

										self_rejected_flag=MC_Pivot_fw(pr,kk);


										fw_tried++;
						#ifdef PROGRESS
						  if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_Pivot_fw ok %llu\n",icycl);fflush(out);	}  
						 #endif
					}
				break;	
				
				
				
				case 3: 		
#ifdef PROGRESS
    if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_brot_local %llu\n",icycl);fflush(out);}	 
 #endif
				
				self_rejected_flag=MC_brot_local(pr);
				
				
				
#ifdef PROGRESS
   if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_brot_local ok %llu\n",icycl);fflush(out);}	  
 #endif
				break;
				case 4: 		
#ifdef PROGRESS			
					if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_All_trasl %llu\n",icycl);fflush(out);}	
						 #endif
				
				self_rejected_flag=MC_All_trasl(pr);
				
				
				
#ifdef PROGRESS
     if(icycl>PROGRESS_INIT) {fprintf(out,"##############MC_All_trasl ok %llu\n",icycl);fflush(out);}	  
 #endif
				trasl_tried++;
				break;
						 		
			}
			
#ifdef PROGRESS
  if(icycl>PROGRESS_INIT) {fprintf(out,"Tswap notry %llu %.30lf\n\n",icycl,pt_test);fflush(out);} 
 #endif

  if(self_rejected_flag==1){
    sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,0);
  }
			
					
		}else{
#ifdef PROGRESS
  if(icycl>PROGRESS_INIT) {fprintf(out,"Tswap try %llu %.30lf\n",icycl,pt_test);fflush(out);} 
 #endif
			Tswap();
#ifdef PROGRESS
  if(icycl>PROGRESS_INIT) {fprintf(out,"Tswap ok %llu \n",icycl);fflush(out);} 
 #endif
			
		}
		
		if(icycl%100000==0){
			Rescaler();
			sist->E=energy();
					
					sist->order=sist->norder;
					sist->touch=sist->ntouch;
					sist->contact=sist->ncontact;
					sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
		 	for(pr=0;pr<NPROT;pr++){
				for(k=ATOM_CA;k<Plength[pr];k+=NATOM){
					sist->E+=Bond_Energy_fw(pr,k);
			 		
				}
			}
			sist->RMSD=lround(RMSD_bin*sqrt(sist->order/TotRMSDNeigh));	
			if(sist->RMSD>=maxO) sist->RMSD=maxO-1;
			Minimum_Energy(sist->E,sist->RMSD,sist->Mean_H_Bonds);
		}
			 
		/*fflush(NULL);
		if((prot[0].order-minO)>=sizex) if(icycl > 0) fprintf(out,"order troppo grande %llu %d %d\n",icycl,prot[0].order-minO,sizex);
		if(prot[0].contact>=sizeC) if(icycl > 0) fprintf(out,"contact troppo grande %llu %d %d\n",icycl,prot[0].contact,sizeC);	
		if(prot[0].touch>1) if(icycl > 0) fprintf(out,"touch troppo grande %llu %d %d\n",icycl,prot[0].touch,1);
		if(prot[0].E>=500) if(icycl > 0) fprintf(out,"Energia troppo grande %llu %lf %d\n",icycl,prot[0].E,500); */
		
		
		
		
		
		/********************
		* Data sampling *
		*********************/
		
		if (icycl%nsamp==0) {
		  fsamp();
		  MPI_Reduce(temthetasto,rtemthetasto,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
		  MPI_Reduce(ntempswap,rntempswap,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
		  if(my_rank==0){
		    
		    ///sprintf(message,"TempControl-O-%d-P-%d.dat",Lattice,process);
		    ///fTempControl=fopen(message,"w");
		    for(i=0;i<p;i++){
		      fprintf(fTempControl,"%2.4f %2.4f %2.4f\n",1/beta[i],rtemthetasto[i],rntempswap[i]);fflush(fTempControl);
		    }
		    fprintf(fTempControl,"##########################################\n");fflush(fTempControl);
		    
		    ///fclose(fTempControl);
		  }
		}
		/* */
		
		/*****************************
		* Multicanonical iteration*
		******************************/
		
		if((icycl>Equi1)&&(icycl<Niteration*(Equi2+Equi3)+Equi1)){
			if(icycl==deltacycle+Equi2){
				/*if(icycl > 0) fprintf(out,"init Whisto\n");*/
				for(i=0;i<p*sizeC*sizex;i++){		
					Whisto[i]=0.0;	
				}
				/*if(icycl > 0) fprintf(out,"init Whisto ok\n");*/
			}
			
			
			
			
			if(icycl==deltacycle+Equi3+Equi2){
				deltacycle=icycl;
				if(icycl > 0){ fprintf(out,"try W\n");fflush(out);}
				W();
				if(icycl > 0){ fprintf(out,"try W ok\n");fflush(out);}
			}
		}
			
			if(icycl==Equi1){
				
				
				
				for(i=0;i<3*p;i++){
					rDensity[i]=-10000.0;
				}
				for(i=0;i<p*sizeC*sizex;i++){
					rhistoC[i]=-1000000.0;
				}
				
				for(i=0;i<p*2*sizex*1000;i++){
					rhisto[i]=-1000000.0;		
				}
				for(i=0;i<p*2*sizex;i++){
					rhistofill[i]=-1000000.0;		
				}
				for(i=0;i<p;i++){
					rndataen2[i]=-1000000.0;
				}
				for(i=0;i<p*sizeEnergy;i++){
				rhisto_energy[i]=-1000000.0;		
				}
				
				MPI_Reduce(histoC,rhistoC,p*sizeC*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				//MPI_Reduce(histo,rhisto,p*2*sizex*1000,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				MPI_Reduce(histofill,rhistofill,p*2*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				MPI_Reduce(ndataen2,rndataen2,p,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				MPI_Reduce(Density,rDensity,p*3,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				MPI_Reduce(histo_energy,rhisto_energy,p*sizeEnergy,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				//fprintf(out,"MPI_Reduce %llu OK\n",icycl);fflush(out);
				
				for(i=0;i<3*p;i++){
					Density[i]=-1000000.0;
				}
				for(i=0;i<p*sizeC*sizex;i++){
					histoC[i]=-1000000.0;
				}
				
				for(i=0;i<p*2*sizex*1000;i++){
					histo[i]=-1000000.0;		
				}
				for(i=0;i<p*2*sizex;i++){
					histofill[i]=-1000000.0;		
				}
				for(i=0;i<p;i++){
					ndataen2[i]=-1000000.0;
				}
				for(i=0;i<p*sizeEnergy;i++){   
				  histo_energy[i]=-1000000.0;		
				}
				
				if(my_rank==0){
					sprintf(message,"Density-O-%d-P-%d-init.dat",Lattice,process);
					fp=fopen(message,"w");
					
					for(i=0;i<p;i++){
						R=exp(rDensity[i*3]-rDensity[i*3+1]);
						fprintf(fp,"%2.4f %10.4g %10.4g\n",1/beta[i],R*(totalvolume/Numberofsubstrates),
	1/(R*(totalvolume/Numberofsubstrates)));
						
					}
					fprintf(fp,"\n\n");		
					fclose(fp);
					
					for(k=0;k<p;k++){
						sprintf(message,"Histogram_3D-T-%2.3f-O-%d-%d-nt-init.dat",1/beta[k],Lattice,process);
						fHisto_3Dnt=fopen(message,"w");
						
						sprintf(message,"Histogram_2D-T-%2.3f-O-%d-%d-nt-init.dat",1/beta[k],Lattice,process);
						fHisto_2Dnt=fopen(message,"w");
						
						sprintf(message,"Histogram_3D-T-%2.3f-O-%d-%d-t-init.dat",1/beta[k],Lattice,process);
						fHisto_3Dt=fopen(message,"w");
						
						sprintf(message,"Histogram_2D-T-%2.3f-O-%d-%d-t-init.dat",1/beta[k],Lattice,process);
						fHisto_2Dt=fopen(message,"w");
						
						sprintf(message,"Histogram_C-T-%2.3f-O-%d-%d-init.dat",1/beta[k],Lattice,process);
						fHisto_contact=fopen(message,"w");

						sprintf(message,"Histogram_2D-E-%2.3f-%d-init.dat",1/beta[k],process);
						fHisto_2D_Energy=fopen(message,"w");
						

						fwrite(&sizeEnergy,sizeof(int),1,fHisto_2D_Energy);
						bin_x=rndataen2[k];
						fflush(out);
						fwrite(&minE,sizeof(int),1,fHisto_2D_Energy);
						fwrite(&bin_energy,sizeof(double),1,fHisto_2D_Energy);
						fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
						
						for(i=0;i<sizeEnergy;i++){		
						  
						  bin_x=-rhisto_energy[k*sizeEnergy+i]; /// k is the temperature
						  fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
						  
						}
						
						fwrite(&sizex,sizeof(int),1,fHisto_2Dnt);
						fwrite(&minO,sizeof(int),1,fHisto_2Dnt);
						bin_x=rndataen2[k];
						//fprintf(out,"NT %d %d %lf\n",sizex,minO,rndataen2[k]);
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
						
						fwrite(&sizex,sizeof(int),1,fHisto_2Dt);
						fwrite(&minO,sizeof(int),1,fHisto_2Dt);	
						bin_x=rndataen2[k];
						//fprintf(out,"T %d %d %lf\n",sizex,minO,rndataen2[k]);
						fflush(out);
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
						for(i=0;i<sizex;i++){		
							
							bin_x=-rhistofill[k*2*sizex+i];
							//fprintf(out,"		%lf %lf\n",bin_x,(bin_x+rndataen2[k])/beta[k]);
							fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
							
							bin_x=-rhistofill[k*2*sizex+sizex+i];
							fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
							
						}
						
							
							
								 
						fwrite(&sizeC,sizeof(int),1,fHisto_contact);
						fwrite(&sizex,sizeof(int),1,fHisto_contact);
						fwrite(&minO,sizeof(int),1,fHisto_contact);
						bin_x=rndataen2[k];
						fwrite(&bin_x,sizeof(double),1,fHisto_contact);
						for(i=0;i<sizeC;i++){
							for(j=0;j<sizex;j++){
								
								bin_x=-rhistoC[k*sizeC*sizex+i*sizex+j];
								fwrite(&bin_x,sizeof(double),1,fHisto_contact);
								
							}
							
						}
						
						
						
						fwrite(&sizex,sizeof(int),1,fHisto_3Dt);
						bin_x=rndataen2[k];
						fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
						
						
						fwrite(&sizex,sizeof(int),1,fHisto_3Dnt);		
						bin_x=rndataen2[k];
						fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
							
						for(i=0;i<sizex;i++){	
							for(j=0;j<1000;j++){
								
								bin_x=-rhisto[k*2*sizex*1000+sizex*1000+i*1000+j];
								fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
								bin_x=-rhisto[k*2*sizex*1000+i*1000+j];
								fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
								
							}
							
						}
						fclose(fHisto_contact);
						fclose(fHisto_2Dnt);
						fclose(fHisto_3Dnt);
						fclose(fHisto_2Dt);
						fclose(fHisto_3Dt);
						
					}
				}
				/*if(icycl > 0) fprintf(out,"write histo I\n");*/
				
				
				
				
				
				
				
				/*if(icycl > 0) fprintf(out,"write histo I ok\n");*/
				
			}
			if((icycl>Nhistogram*(Equi2+Equi3)+Equi1)&&(icycl%nsamp==0)){
				
#ifdef ORDER_TEST
				for(k=0;k<p;k++){
					sprintf(message,"Histogram_C-P-%d-%d-T-%2.3f.dat",my_rank,process,1/beta[k]);
					fHisto_contacta=fopen(message,"w");
					for(i=0;i<sizeC;i++){
						for(j=0;j<sizex;j++){
							fprintf(fHisto_contacta,"%10.5lf %5d %15.10f\n",j/RMSD_bin,i,(-histoC[k*sizeC*sizex+i*sizex+j]+ndataen2[k])/beta[k]);
						}
						fprintf(fHisto_contacta,"\n");
					}
					fclose(fHisto_contacta);
				}
#endif
				//fprintf(out,"Histogram_C Write %llu OK\n",icycl);fflush(out);
				
				for(i=0;i<3*p;i++){
					rDensity[i]=-10000.0;
				}
				for(i=0;i<p*sizeC*sizex;i++){
					rhistoC[i]=-1000000.0;
				}
				
				for(i=0;i<p*2*sizex*1000;i++){
					rhisto[i]=-1000000.0;		
				}
				for(i=0;i<p*2*sizex;i++){
					rhistofill[i]=-1000000.0;		
				}
				for(i=0;i<p;i++){
					rndataen2[i]=-1000000.0;
				}
				//fprintf(out,"Histo Init %llu OK\n",icycl);fflush(out);
				MPI_Reduce(histoC,rhistoC,p*sizeC*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				//fprintf(out,"MPI_Reduce %llu OK 1\n",icycl);fflush(out);
				//MPI_Reduce(histo,rhisto,p*2*sizex*1000,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				//fprintf(out,"MPI_Reduce %llu OK 2\n",icycl);fflush(out);
				MPI_Reduce(histofill,rhistofill,p*2*sizex,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				//fprintf(out,"MPI_Reduce %llu OK 3\n",icycl);fflush(out);
				MPI_Reduce(ndataen2,rndataen2,p,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				//fprintf(out,"MPI_Reduce %llu OK 4\n",icycl);fflush(out);
				MPI_Reduce(Density,rDensity,p*3,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				MPI_Reduce(histo_energy,rhisto_energy,p*sizeEnergy,MPI_DOUBLE,myExpSum,0,MPI_COMM_WORLD);
				fprintf(out,"MPI_Reduce %lu OK histo_energy\n",icycl);fflush(out);
				//fprintf(out,"MPI_Reduce %llu OK 5\n",icycl);fflush(out);
				
				if(my_rank==0){
					sprintf(message,"Density-O-%d-P-%d.dat",Lattice,process);
					fp=fopen(message,"w");
					
					for(i=0;i<p;i++){
						R=exp(rDensity[i*3]-rDensity[i*3+1]);
						fprintf(fp,"%2.4f %10.4g %10.4g\n",1/beta[i],R*(totalvolume/Numberofsubstrates),
	1/(R*(totalvolume/Numberofsubstrates)));
						
					}
					fprintf(fp,"\n\n");		
					fclose(fp);
					
					for(k=0;k<p;k++){
						sprintf(message,"Histogram_3D-T-%2.3f-O-%d-%d-nt.dat",1/beta[k],Lattice,process);
						fHisto_3Dnt=fopen(message,"w");
						
						sprintf(message,"Histogram_2D-T-%2.3f-O-%d-%d-nt.dat",1/beta[k],Lattice,process);
						fHisto_2Dnt=fopen(message,"w");
						
						sprintf(message,"Histogram_3D-T-%2.3f-O-%d-%d-t.dat",1/beta[k],Lattice,process);
						fHisto_3Dt=fopen(message,"w");
						
						sprintf(message,"Histogram_2D-T-%2.3f-O-%d-%d-t.dat",1/beta[k],Lattice,process);
						fHisto_2Dt=fopen(message,"w");
						
						sprintf(message,"Histogram_C-T-%2.3f-O-%d-%d.dat",1/beta[k],Lattice,process);
						fHisto_contact=fopen(message,"w");
						
						sprintf(message,"Histogram_2D-E-%2.3f-O-%d-%d.dat",1/beta[k],Lattice,process);
						fHisto_2D_Energy=fopen(message,"w");
						
						fwrite(&sizex,sizeof(int),1,fHisto_2Dnt);
						fwrite(&minO,sizeof(int),1,fHisto_2Dnt);
						bin_x=rndataen2[k];
						//fprintf(out,"NT %d %d %lf\n",sizex,minO,rndataen2[k]);
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
						
						fwrite(&sizex,sizeof(int),1,fHisto_2Dt);
						fwrite(&minO,sizeof(int),1,fHisto_2Dt);	
						bin_x=rndataen2[k];
						//fprintf(out,"T %d %d %lf\n",sizex,minO,rndataen2[k]);
						//fflush(out);
						fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
						for(i=0;i<sizex;i++){		
							
							bin_x=-rhistofill[k*2*sizex+i];
							//fprintf(out,"		%lf %lf\n",bin_x,(bin_x+rndataen2[k])/beta[k]);
							fwrite(&bin_x,sizeof(double),1,fHisto_2Dnt);
							
							bin_x=-rhistofill[k*2*sizex+sizex+i];
							fwrite(&bin_x,sizeof(double),1,fHisto_2Dt);
							
						}
						
						fwrite(&sizeEnergy,sizeof(int),1,fHisto_2D_Energy);
						bin_x=rndataen2[k];
						fflush(out);
						fwrite(&minE,sizeof(int),1,fHisto_2D_Energy);
						fwrite(&bin_energy,sizeof(double),1,fHisto_2D_Energy);
						fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
						for(i=0;i<sizeEnergy;i++){		
						  
						  bin_x=-rhisto_energy[k*sizeEnergy+i]; /// k is the temperature, is the indice ok???
						  fwrite(&bin_x,sizeof(double),1,fHisto_2D_Energy);
						  
						}
						
						
							
								 
						fwrite(&sizeC,sizeof(int),1,fHisto_contact);
						fwrite(&sizex,sizeof(int),1,fHisto_contact);
						fwrite(&minO,sizeof(int),1,fHisto_contact);
						bin_x=rndataen2[k];
						fwrite(&bin_x,sizeof(double),1,fHisto_contact);
						for(i=0;i<sizeC;i++){
							for(j=0;j<sizex;j++){
								
								bin_x=-rhistoC[k*sizeC*sizex+i*sizex+j];
								fwrite(&bin_x,sizeof(double),1,fHisto_contact);
								
							}
							
						}
						
						
						
						fwrite(&sizex,sizeof(int),1,fHisto_3Dt);
						bin_x=rndataen2[k];
						fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
						
						
						fwrite(&sizex,sizeof(int),1,fHisto_3Dnt);		
						bin_x=rndataen2[k];
						fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
							
						for(i=0;i<sizex;i++){	
							for(j=0;j<1000;j++){
								
								bin_x=-rhisto[k*2*sizex*1000+sizex*1000+i*1000+j];
								fwrite(&bin_x,sizeof(double),1,fHisto_3Dt);
								bin_x=-rhisto[k*2*sizex*1000+i*1000+j];
								fwrite(&bin_x,sizeof(double),1,fHisto_3Dnt);
								
							}
							
						}
						fclose(fHisto_contact);
						fclose(fHisto_2Dnt);
						fclose(fHisto_3Dnt);
						fclose(fHisto_2Dt);
						fclose(fHisto_3Dt);
						fclose(fHisto_2D_Energy);
						
						
					}
				}
			}
			
			/*if((icycl%500==0)||(icycl==1)){
				
				for(i=0;i<sizex;i++){
					if(icycl > 0) fprintf(out,"Wpot[%d]=%g\n",i,Wpot[i]);
					fflush(NULL);
				}
			}*/
			
			
			
			
			
			/****************************/
			/*Test continuo sulla catena*/
			/****************************/
			/*if (icycl%1 ==0) test();*/
			
		
	}
	/*if(icycl > 0) fprintf(out,"alora va bene \n");
	fflush(NULL);*/
	/******************************/
	/*Saving Iteration potentials */
	/******************************/
	W();
	if(my_rank==0){
	  for(k=0;k<p;k++){
	    sprintf(message,"Wpot-T-%2.3f.dat",1/beta[k]);
	    fp=fopen(message,"w");
	    fwrite(&sizeC,sizeof(int),1,fp);
	    fwrite(&sizex,sizeof(int),1,fp);
	    fwrite(&IminO,sizeof(int),1,fp);
	    for(i=0;i<sizeC;i++){
	      for(j=0;j<sizex;j++){
		
		bin_x=Wpot[k][i][j];
		fwrite(&bin_x,sizeof(double),1,fp);
		
	      }
	      
	    }
	    fclose(fp);
	  }
	}
	/******************************/
	/*Sampling result		 */
	/******************************/
	
	
	/****************************************/
	/*Saving Parallel Tempering Performance */
	/****************************************/
	//MPI_Gather(&temthetasto,1,MPI_DOUBLE,roottemthetasto,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Reduce(temthetasto,rtemthetasto,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	MPI_Reduce(ntempswap,rntempswap,p,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	if(my_rank==0){
		
	  ///sprintf(message,"TempControl-O-%d-P-%d.dat",Lattice,process);
		///fTempControl=fopen(message,"w");
		for(i=0;i<p;i++){
		  fprintf(fTempControl,"%2.4f %2.4f %2.4f\n",1/beta[i],rtemthetasto[i],rntempswap[i]);fflush(fTempControl);
		}
		fprintf(fTempControl,"\n");
		
		fclose(fTempControl);
	}
	
	
	fsamp();
	fclose(fSeq);
	fclose(fEner);
	
	
	
	
	
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gather(&MinE,1,MPI_DOUBLE,rootminE,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinO,1,MPI_DOUBLE,rootminO,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinEH,1,MPI_DOUBLE,rootminEH,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinOE,1,MPI_DOUBLE,rootminOE,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinEO,1,MPI_DOUBLE,rootminEO,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Gather(&MinOH,1,MPI_DOUBLE,rootminOH,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	
	fprintf(out,"%llu %llu %llu %llu\n",bgs_rejectedself,bgs_rejectedener,bgs_accepted,bgs_tried);
	fprintf(out,"%llu %llu %llu %llu %lf\n",cbmc_fw_rejectedself,cbmc_fw_rejectedener,cbmc_fw_accepted,cbmc_fw_tried,(double)(efficiency/efficiency_tried));
	fprintf(out,"%llu %llu %llu %llu\n",local_rejectedself,local_rejectedener,local_accepted,local_tried);
	fprintf(out,"%llu %llu %llu %llu\n",fw_rejectedself,fw_rejectedener,fw_accepted,fw_tried);
	fprintf(out,"%llu %llu %llu bw_tried=%llu\n",bw_rejectedself,bw_rejectedener,bw_accepted,bw_tried);
	fprintf(out,"%llu %llu %llu trasl_tried=%llu\n",trasl_rejectedself,trasl_rejectedener,trasl_accepted,trasl_tried);
	
	
	if(my_rank==0){
		
		
		
		for(i=0;i<p;i++){
			fprintf(out,"%d MinE =%lf MinEO=%lf MinEH=%lf\n",i,rootminE[i],rootminEO[i],rootminEH[i]);
			fprintf(out,"%d MinO =%lf MinOE=%lf MinOH=%lf\n",i,rootminO[i],rootminOE[i],rootminOH[i]);
		}
		
		endtime=MPI_Wtime();
		fprintf(out,"Simulation Time %lf (seconds)\n",endtime-starttime);
	}
	MPI_Finalize();
	
	return(0);
	
}
int writeBinary(FILE *outFile) {
	
	double x,y,z;
	int i,j,k,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=prot[pr].ammino[i].x;
			y=prot[pr].ammino[i].y;
			z=prot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}

int writeBinaryMin(FILE *outFile) {
	
	double x,y,z;
	int i,j,k,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=minprot[pr].ammino[i].x;
			y=minprot[pr].ammino[i].y;
			z=minprot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}
int writeBinaryMinO(FILE *outFile) {
	
	double x,y,z;
	int i,j,k,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=minOprot[pr].ammino[i].x;
			y=minOprot[pr].ammino[i].y;
			z=minOprot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}
int writeBinaryMaxO(FILE *outFile) {
	
	double x,y,z;
	int i,j,k,pr,id;
	/* open the file we are writing to */
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			x=maxOprot[pr].ammino[i].x;
			y=maxOprot[pr].ammino[i].y;
			z=maxOprot[pr].ammino[i].z;
			id=prot[pr].ammino[i].id;
			fwrite(&x,sizeof(double),1,outFile);
			fwrite(&y,sizeof(double),1,outFile);
			fwrite(&z,sizeof(double),1,outFile);
			fwrite(&id,sizeof(int),1,outFile);
		}
	}
	
	
	
	return 0;
}

int  write_psf (void){

    int k,l, j, i, nbonds, nf, pl, tot_atoms=0;
	FILE *fPSFvmd;
	char message[50];
	
	
	sprintf(message, "Topology.psf");
	fPSFvmd=fopen(message,"w");
    fprintf(fPSFvmd,"PSF\n\n");
    fprintf(fPSFvmd,"%8d !TITLE\n\n\n",NPROT);
    for(j=0;j<NPROT;j++){
	tot_atoms += Plength[j];
    }
    fprintf(fPSFvmd,"%8d !NATOM\n",tot_atoms);
    l=1;

    for(j=0;j<NPROT;j++){
		for(i=0;i< Plength[j];i=i+NATOM){
	    	fprintf(fPSFvmd,"%8d PROT %-3d  %03d  CA   CA     %7.6f        %6.4f           0\n", l,j+1,prot[j].ammino[i].residue,0.0,12.011);
			for(k=1;k<NATOM;k++){
	    		fprintf(fPSFvmd,"%8d PROT %-3d  %03d  H    H      %7.6f        %6.4f           0\n", l+k,j+1,prot[j].ammino[i].residue,0.0,1.008);
			}
	    	l+=NATOM;
		}
    }
    fprintf(fPSFvmd,"\n");
    nbonds = 0;
    for (j=0;j<NPROT;j++){
	nbonds += Plength[j]-1;
    }
    fprintf(fPSFvmd,"%9d !NBOND\n",nbonds);
    l=1;
    nf=0;
    pl=0;
    for (j=0;j<NPROT;j++){
	pl += Plength[j];
		for(i=0;i<Plength[j];i=i+NATOM){
			for(k=1;k<NATOM;k++){
	    		fprintf(fPSFvmd," %7d %7d",l,l+k); nf = nf+1; // HCA
	    		if (nf == 4) {
					fprintf(fPSFvmd,"\n"); nf=0;
	    		}
			}	   
	    	if (l<pl-NATOM){
				fprintf(fPSFvmd," %7d %7d",l,l+NATOM); nf = nf+1; // CACA
				if (nf == 4) {
		    		fprintf(fPSFvmd,"\n"); nf=0;
				}
	    	} 
	    	l+=NATOM;
		}
    }
	fclose(fPSFvmd);
	return(0);
}

int readBinary(FILE *inFile) {
	
	double x,y,z;
	int i,j,k,pr,id,indice;
	
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			fread(&x,sizeof(double),1,inFile);
			fread(&y,sizeof(double),1,inFile);
			fread(&z,sizeof(double),1,inFile);
			fread(&id,sizeof(int),1,inFile);
			prot[pr].ammino[i].x=x;
			prot[pr].ammino[i].y=y;
			prot[pr].ammino[i].z=z;
			prot[pr].ammino[i].id=id;
			
			
		}
	}
	/**for(j=0;j<NPROT;j++){
		
		for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			for(k=0;k<NSPOTS;k++){

				indice=i+k+1;
				if((k==0)||(k==SPG_PATCH2-1)){
					if(SPRING_MODEL==1) {
						prot[j].ammino[indice].spring_anchor=1;
					}else{
						prot[j].ammino[indice].spring_anchor=0;
					}
				}else{
					prot[j].ammino[indice].spring_anchor=0;
				}
			}
		}
	}**/
	
	
	return 0;
}
void Rotation_Random_Sphere (int pr,int k){
	
	
	
	double  u1, u2, u3;
	 double a,b,a2,b2,c;
	double v1,v2,v3;
	 double u12,u22,u32;
	 double a11,a12,a13,a21,a22,a23,a31,a32,a33;
	double nx,ny,nz;
	double dx,dy,dz,r2;
	double mod;
	int i,indice;
	
	
	/*****************RANDOM ROTATIONS**************/
	//Random Quaternion 
	a =gasdev(&seed);
	u1=gasdev(&seed);
	u2=gasdev(&seed);
	u3=gasdev(&seed);
	mod=sqrt(a*a+u1*u1+u2*u2+u3*u3);
	//Quaternion  Normalization
	a/=mod;
	u1/=mod;
	u2/=mod;
	u3/=mod; 
		
		
	/*if(fabs(a*a+u1*u1+u2*u2+u3*u3-1.0)>1e-10) {
		fprintf(out,"PDL1 %llu %30.25lf\n",icycl,a*a+u1*u1+u2*u2+u3*u3-1.0);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	a2=a*a;
	u12=u1*u1;
	u22=u2*u2;
	u32=u3*u3;
	
	
	
	a11=(a2+u12-u22-u32);
	a12=2*(u1*u2-a*u3);
	a13=2*(u1*u3+a*u2);

	a21=2*(u2*u1+a*u3);
	a22=(a2-u12+u22-u32);
	a23=2*(u2*u3-a*u1);

	a31=2*(u3*u1-a*u2);
	a32=2*(u3*u2+a*u1);
	a33=(a2-u12-u22+u32);
	
	


	//if(icycl>20) fprintf(out,"U %lf %lf %lf\n",u1,u2,u3);
	//if(icycl>20) fprintf(out,"V %lf %lf %lf %lf %lf\n",v1,v2,v3,a2,b2);
	
	
	
	for(i=0;i<NSPOTS;i++){
		indice=k+i+1;
		
		v1=prot[pr].ammino[indice].x-prot[pr].ammino[k].x;
		v2=prot[pr].ammino[indice].y-prot[pr].ammino[k].y;
		v3=prot[pr].ammino[indice].z-prot[pr].ammino[k].z;


		nx=a11*v1+a12*v2+a13*v3;
		ny=a21*v1+a22*v2+a23*v3;
		nz=a31*v1+a32*v2+a33*v3;

		prot[pr].ammino[indice].x=nx+prot[pr].ammino[k].x;
		prot[pr].ammino[indice].y=ny+prot[pr].ammino[k].y;
		prot[pr].ammino[indice].z=nz+prot[pr].ammino[k].z;
		
	}
	return;
	//if(icycl>0) fprintf(out,"N %lf %lf %lf %lf\n",nx,ny,nz,alpha);
	//if(icycl>20) fprintf(out,"N2 %lf %lf %lf\n",nx2,ny2,nz2);
}
void Rotation (int pr,int k,double alpha,double u1,double u2,double u3,int flag){
	
	
	
	
	static double a,b,a2,b2,c;
	double v1,v2,v3;
	static double u12,u22,u32;
	static double a11,a12,a13,a21,a22,a23,a31,a32,a33;
	double nx,ny,nz;
	double dx,dy,dz,r2;
	double modulo;
	
	v1=prot[pr].ammino[k].x;
	v2=prot[pr].ammino[k].y;
	v3=prot[pr].ammino[k].z;
	if(flag==1){
	a=cos(alpha);
	a2=a*a;
	
	c=sin(alpha);
	
	modulo=sqrt(u1*u1+u2*u2+u3*u3);
	u1/=modulo;
	u2/=modulo;
	u3/=modulo;
	if(fabs(u1*u1+u2*u2+u3*u3-1.0)>1e-10) {
		fprintf(out,"PDL1 %llu %30.25lf\n",icycl,u1*u1+u2*u2+u3*u3-1.0);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	u1*=c;
	u2*=c;
	u3*=c;
	u12=u1*u1;
	u22=u2*u2;
	u32=u3*u3;
	/*modulo=sqrt(u12+u22+u32+a2);
	//if(icycl>0) printf("Modulo=%lf c=%lf\nu1=%lf u2=%lf u3=%lf\nalpha=%lf\n",modulo,c,u1,u2,u3,alpha);
	u1/=modulo;
	u2/=modulo;
	u3/=modulo;
	a/=modulo;
	a2=a*a;
	u12=u1*u1;
	u22=u2*u2;
	u32=u3*u3;*/
	if(fabs(c*c+a2-1.0)>1e-10) {
		fprintf(out,"PDL 2 %llu %30.25lf\n",icycl,c*c+a2-1.0);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	a11=(a2+u12-u22-u32);
	a12=2*(u1*u2-a*u3);
	a13=2*(u1*u3+a*u2);

	a21=2*(u2*u1+a*u3);
	a22=(a2-u12+u22-u32);
	a23=2*(u2*u3-a*u1);

	a31=2*(u3*u1-a*u2);
	a32=2*(u3*u2+a*u1);
	a33=(a2-u12-u22+u32);
	
	
}

	//if(icycl>20) fprintf(out,"U %lf %lf %lf\n",u1,u2,u3);
	//if(icycl>20) fprintf(out,"V %lf %lf %lf %lf %lf\n",v1,v2,v3,a2,b2);

	
	nx=a11*v1+a12*v2+a13*v3;
	ny=a21*v1+a22*v2+a23*v3;
	nz=a31*v1+a32*v2+a33*v3;

	prot[pr].ammino[k].x=nx;
	prot[pr].ammino[k].y=ny;
	prot[pr].ammino[k].z=nz;
	
	
	//if(icycl>0) fprintf(out,"N %lf %lf %lf %lf\n",nx,ny,nz,alpha);
	//if(icycl>20) fprintf(out,"N2 %lf %lf %lf\n",nx2,ny2,nz2);
}









void finit (void){
	
	/************************************************
	* finit -- initialize the protein:	 		*
	*	take the interaction matrix M form the*
	*	file aapot.dat			*
	*	take the protein struct from the file *
	*	prot					*
	*	give a random sequence to the protein *	
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	void						*
	*************************************************/
	
	
	FILE *fp=NULL;
	int i=0,j=0,k=0,l=0,pr,prt,id,kk;
	double n,n2;
	double dx1,dy1,dz1;
	int n1,p2=0,sign;
	char *SubRes=NULL,*pch=NULL;
	double x,y,z,R;
	int a;
	int subminx=10000,subminy=10000,subMAXx=-100000,subMAXy=-100000,NRx=0,NRy=0;
	double dx,dy,dz,theta,r2,alpha;
	unsigned int indice;
	double testO,testOP;
	int testC;
	double testE;
	long int indicen;
	int **Res=NULL;
	double **vertices=NULL;
	double module=0.;
	char* message=NULL;
	char* line=NULL;
	
	message=(char *) calloc (BUFFER_CHAR,sizeof(char));
	line=(char *) calloc (BUFFER_CHAR,sizeof(char));
	
	fp=fopen("tempera.dat","r");
	if ( fp == NULL) {
		printf ("File tempera.dat not found!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		for(j=0;j<p;j++){
			//fgets(line,BUFFER_CHAR*sizeof(char),fp);
			fscanf (fp,"%lf\n",&x);
			beta[j]=1/x;
		}
		betaindice=my_rank;
		fclose(fp);
	}
	
	
	fp=fopen("aapot.dat","r");
	if ( fp == NULL) {
		printf ("File aapot.dat not found!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
	  fscanf (fp,"%d",&dumm); 
	  fscanf (fp,"%lf",&dumm1);
	  fscanf (fp,"%lf",&dumm1); /// These are to mantain the same aapot file from folding
		for(j=0;j<S;j++){
			for(i=j;i<S;i++){
				fscanf (fp,"%lf",&M[i][j]);
				//M[i][j]*=HB_E_Scale;
				M[j][i]=M[i][j];
			}
		}
	}
	fclose(fp);
	
	/* Test Aquisizione Matrice delle interazioni*/
	/*for(i=0;i<S;i++){
		for(j=0;j<S;j++){
			fprintf(out,"%2.2f ",fabs(M[i][j]));
		}
		fprintf(out,"\n");
	}*/
	
	fp=fopen("prot","r");
	if ( fp == NULL) {
		printf ("File prot not found!!!!\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}else{
		
		fprintf(out,"NPROT=%d\n",NPROT);
		prot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
		minprot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
		minOprot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
		maxOprot= (struct proteina *)calloc(NPROT,sizeof(struct proteina));
		
		
		Plength=(unsigned int *) calloc((NPROT+1),sizeof(unsigned int));
		Plength_half=(unsigned int *) calloc((NPROT+1),sizeof(unsigned int));
		ProtN=0;
		pr=0;
		
		for(pr=0;pr<NPROT;pr++){
			fgets(line,BUFFER_CHAR*sizeof(char),fp);
			p2=sscanf (line,"%*s %lf",&n); /// Number of beads
			fprintf(out,"Nresidues=%lf\n",n);
			Plength[pr]=(unsigned int)(n)*(NATOM);
			Plength_half[pr]=(unsigned int)(n/2.0)*NATOM;
			if(ProtN<=Plength[pr]) ProtN=Plength[pr];
			
		}
		AmminoN=ProtN/NATOM;
		fprintf(out,"ProtN=%d AmminoN=%d\n",ProtN,AmminoN);
		fprintf(out,"CHAINS\n");fflush(out);	
		HalfN=partint(ProtN/2);
		sizeZ=partint(LBox*(ProtN)/2);
		//sist->side=LBox*(ProtN)*sist->Rbond;
		//sist->side2=sist->side/2.0;
		rewind(fp);
		Res=(int **)calloc(NPROT,sizeof(int *));
		for(j=0;j<NPROT;j++){
		  fprintf(out,"pr=%d Nbeads=%d\n",j,Plength[j]/NATOM+1);fflush(out);
		  Res[j]=(int *)calloc((Plength[j]/NATOM+1),sizeof(int));
		  if(Res[j]==NULL){
		    fprintf(out,"allocation of Res failed in finit\n");
		    fflush(out);
		    MPI_Abort(MPI_COMM_WORLD,err5);
		  }
		  prot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		  minprot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		  minOprot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		  maxOprot[j].ammino= (struct part *)calloc((Plength[j]),sizeof(struct part));
		  if(prot[j].ammino==NULL){
		    fprintf(out,"allocation of prot->ammino failed in finit\n");
		    fflush(out);
		    MPI_Abort(MPI_COMM_WORLD,err5);
		  }
			
		  fgets(line,BUFFER_CHAR*sizeof(char),fp);
		  i=0;
		  
		  pch = strtok (line,"-");
		  while (pch != NULL){
		    
		    if(i<(Plength[j]/NATOM)){
		      ///for (i=0;i<(Plength[j]/NATOM+1);i++){
		      Res[j][i]=atoi(pch);
		      if(Res[j][i]>=S){
			printf("Seqeunce requires a larger alphabet of interaction\n");fflush(NULL);
			printf("Res[%d][%d]=%d S=%d\n",j,i,Res[j][i],S);fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		      }
		      pch = strtok (NULL, "-");
		      ///printf("Res[%d][%d]=%d\n",j,i,Res[j][i]);fflush(NULL);
		      i++;
		      
		    }
		  }
		  
		  for(i=0;i<(Plength[j]/NATOM);i++){
		    for(k=0;k<NATOM;k++){
		      prot[j].ammino[i*NATOM+k].residue=Res[j][i];
		    }
		  }
		  
		  fprintf (out,"initial seq pr=%d  ",pr);fflush(out);
		  for(i=0;i<(Plength[j]/NATOM -1);i++){
		    fprintf (out,"%d-",prot[j].ammino[i*NATOM].residue);fflush(out);
		  }
		  fprintf (out,"%d\n",prot[j].ammino[Plength[j]-1].residue);fflush(out);
		  free(Res); 
		  
		}
	
				
		fprintf(out,"allocation of Seq ok\n");fflush(out);
		
		/*Pos[N][N][N]=1;*/
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				prot[i].ammino[k].x=0;
				prot[i].ammino[k].y=0;
				prot[i].ammino[k].z=0;
				prot[i].ammino[k].Vx=0;
				prot[i].ammino[k].Vy=0;
				prot[i].ammino[k].Vz=0;
				
			}
		
		}
				
		fclose(fp);
	}
	///fastadecoder(Res,Seq);
	fprintf(out,"fastadecoder of Seq ok\n");fflush(out);
	//BoxSize=1.5*(double)(ProtN/5.0+1)*(2.447976); //the number 2.447976 is the bond length
	BoxSize*=(ProtN/NATOM+1.0)*(sist->Rmin_CAsaw*1.5); //the number 2.447976 is the bond length
	BoxSize2=BoxSize/2.0;
	
	
	
	
	/*for(indice=0;indice<Neigh;indice++){
		fprintf(out,"Finit indice=%d neighx=%d neighy=%d neighz=%d\n",indice,neighcells[indice*3],neighcells[indice*3+1],neighcells[indice*3+2]);fflush(out);
	}*/
	//MPI_Abort(MPI_COMM_WORLD,err5);
	fp=fopen("sub","r");
	if ( fp == NULL) {
		fprintf (out,"No sub found\n");
		fflush(out);
		
	}else{
		
		sub= (struct proteina *)calloc(1,sizeof(struct proteina));
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		sscanf (line,"%*s %lf",&n);
		
		SubN=(int)(n);
		Plength[NPROT]=SubN;
		SubSeq=(char *)calloc((SubN+1),sizeof(char));
		SubRes=(char *)calloc((SubN+1),sizeof(char));
		if(SubSeq==NULL){
			fprintf(out,"allocation of SubSeq failed in finit\n");
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		sub->ammino= (struct part *)calloc((Plength[NPROT]),sizeof(struct part));
		rewind(fp);
		
		
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		sscanf (line,"%s %*f",SubSeq);
		
		
		fclose(fp); 
		fp=fopen("sub1","r");
		if ( fp == NULL) {
			printf ("Substrate sequence present but no substrate coordinates\n");
			
			fflush(NULL);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}else{
			
			fgets(line,BUFFER_CHAR*sizeof(char),fp);
			sscanf (line,"%d\n",&n1);
			fgets(line,BUFFER_CHAR*sizeof(char),fp);
			
			for(i=0,j=0;i<n1;i++){
				fgets(line,BUFFER_CHAR*sizeof(char),fp);
				p2=sscanf (line,"%*s%lf%lf%lf\n",&x,&y,&z);
				if(p2<3){
					fprintf(out,"Bad file Format prot3!!!\n");
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}else{
					if((i-1)%2){
						sub->ammino[j%SubN].x=x;
						sub->ammino[j%SubN].y=y;
						sub->ammino[j%SubN].z=z;
						j++;
					}
				}
			}
			
			fclose(fp);
		} 
		for(i=0;i<SubN;i++){
			
			
			if(subminx>=sub->ammino[i].x) subminx=sub->ammino[i].x;
			if(subminy>=sub->ammino[i].y) subminy=sub->ammino[i].y;
			if(subMAXx<=sub->ammino[i].x) subMAXx=sub->ammino[i].x;
			if(subMAXy<=sub->ammino[i].y) subMAXy=sub->ammino[i].y;
		}
		NRx=partint(LBox*ProtN/(2*(subMAXx-subminx+1)));
		NRy=partint(LBox*ProtN/(2*(subMAXy-subminy+1)));
	}
	
	
	
	/*fprintf(out,"Seq=%s\n",Seq);
	fprintf(out,"Seq[0]=%c\n",Seq[0]);*/

	vertices=(double **)calloc((NATOM),sizeof(double*));
	for(j=0;j<NATOM;j++){
		vertices[j]=(double *)calloc(3,sizeof(double));
	}
	
	connector=(int *)calloc((NATOM),sizeof(int)); /// here I memorize at which patch is attached the spring
	for (i=0;i<NATOM;i++){
		connector[i]=0;
	}
	
	fp=fopen("patches_structure.dat","r");
	fscanf(fp,"%d",&k); /// this value is the number NSPOTS ... I've already red it
	vertices[0][0]=vertices[0][1]=vertices[0][2]=0.;	///coordinates of the C_alpha
	///connector[0]=0;
	for (k=1;k<NATOM;k++)	{
	  fscanf(fp,"%lf",&vertices[k][0]);
	  fscanf(fp,"%lf",&vertices[k][1]);
	  fscanf(fp,"%lf",&vertices[k][2]);
	  fscanf(fp,"%d",&connector[k]);
	  module=sqrt(vertices[k][0]*vertices[k][0] + vertices[k][1]*vertices[k][1] + vertices[k][2]*vertices[k][2]);
	  vertices[k][0]=(sist->Bbond[1])*vertices[k][0]/module;
	  vertices[k][1]=(sist->Bbond[1])*vertices[k][1]/module;
	  vertices[k][2]=(sist->Bbond[1])*vertices[k][2]/module;
	}
	fclose(fp);

	if(SPRING_MODEL==1)	{
	  if ( (connector[1]!=1) || (connector[SPG_PATCH2]!=1) || (vertices[1][2] > vertices[SPG_PATCH2][2]) ){
	    fprintf(out,"The first and last patches in patches_total.dat must be the binding onces if SPRING_MODEL=1, and in the right z order => the last patch must have a z > first patch!!! \n");
	    fflush(out);
	    MPI_Abort(MPI_COMM_WORLD,err5);
	  }
	}



	
	
	fprintf(out,"prot3\n");
	fflush(out);
	fp=fopen("prot3","r");
	if ( fp == NULL) {
		
		
					
		
	  for(j=0;j<NPROT;j++){
		
	    for(i=ATOM_CA;i<Plength[j];i+=NATOM){
			
	      for(k=0;k<NATOM;k++)	{
		prot[j].ammino[i+k].x=vertices[k][0] + j*spacing + 1.0;
		prot[j].ammino[i+k].y=vertices[k][1];
		prot[j].ammino[i+k].z=vertices[k][2] + sist->Rmin_CAsaw*1.1*i/NATOM + 1.0;
		if(SPRING_MODEL==1)	{
		  prot[j].ammino[i+k].spring_anchor=connector[k];
		}
		else{
			prot[j].ammino[i+k].spring_anchor=0;	
		}
		
		  	
		if (k==0)
		  prot[j].ammino[i].id=ATOM_CA;
		else
		  prot[j].ammino[i+k].id=ATOM_H;
	      }
			
	    }
				
	  }
	
	/***********************************************/
	
	}else{
	  readBinary(fp);
	  Rescaler();
	  for(j=0;j<NPROT;j++){
	    for(i=ATOM_CA;i<Plength[j];i+=NATOM){
	      for(k=0;k<NATOM;k++)	{
		if(SPRING_MODEL==1)
		  prot[j].ammino[i+k].spring_anchor=connector[k];
		else
		  prot[j].ammino[i+k].spring_anchor=0;
	      }
	    }
	  }	
	}
	
	fprintf(out,"prot3 ok\n");
	fflush(out);
	sprintf(message, "VMD/Seqvmd-T-%lf-INIT.pdb",1/beta[betaindice]);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		kk=1;
		for(i=0;i<Plength[j];i++){
			x=prot[j].ammino[i].x-prot[0].ammino[0].x+BoxSize/2.0;
			y=prot[j].ammino[i].y-prot[0].ammino[0].y+BoxSize/2.0;
			z=prot[j].ammino[i].z-prot[0].ammino[0].z+BoxSize/2.0;
			if(NPROT>1) {x=P_Cd(x);y=P_Cd(y);z=P_Cd(z);}
		
			fprintf(fSeqvmd,"ATOM  %5d  %s %s  %4d    %8.3lf%8.3lf%8.3lf\n",j*NPROT+i+1,sist->Atoms[prot[j].ammino[i].id],"ALA",kk,x,y,z);
			if((i>0)&&((i+1)%NATOM==0)) kk++;
		}
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	
	
	
	
	
	N=TOTLENGTH+SubN;
	fprintf(out,"contact \n");fflush(out);
	DistMap1=(double**)calloc(ProtN,sizeof(double*));
	for(i=0;i<ProtN;i++){
		DistMap1[i]=(double*)calloc(ProtN,sizeof(double));
		
	}
	DistMap2=(double**)calloc(ProtN,sizeof(double*));
	for(i=0;i<ProtN;i++){
		DistMap2[i]=(double*)calloc(ProtN,sizeof(double));
		
	}
	for(i=0;i<(NPROT);i++){
		
			fprintf(out,"Plengths[%d]=%d\n",i,Plength[i]);
	
	}
	fflush(out);
	for(i=0;i<ProtN;i++){
		for(j=0;j<ProtN;j++){
			
					//fprintf(out,"Contact %d %d %d %d\n",i,j,k,l);
					DistMap1[i][j]=-1; 
					DistMap2[i][j]=-1; 
					//contact[i][j][k][l]=1;
			
		}
	}
	
	fprintf(out,"contact ok\n");fflush(out);
	
	fprintf(out,"seq set \n");fflush(out);
		
	N_SAW_cells=partint(BoxSize/(sist->Rmin*1.1));
	cell_SAW_size=BoxSize/N_SAW_cells;
	
	N_CA_cells=partint(BoxSize/(sist->Rint*1.1));
	cell_CA_size=BoxSize/N_CA_cells;
	
	
	N_H_cells=N_O_cells=partint(BoxSize/(sist->RintH*1.1));
	cell_H_size=cell_O_size=BoxSize/N_O_cells;

	fprintf(out,"new_cell_list N_CA_cells=%d cell_CA_size=%lf BoxSize=%lf\n",N_CA_cells,cell_CA_size,BoxSize);fflush(out);
	fprintf(out,"new_cell_list N_H_cells=%d cell_H_size=%lf\n",N_H_cells,cell_H_size);fflush(out);
	fprintf(out,"new_cell_list N_SAW_cells=%d cell_SAW_size=%lf\n",N_SAW_cells,cell_SAW_size);fflush(out);
	
	neighcells=(int *)calloc(3*27,sizeof(int));
	
	indice=0;
	for(i=-1;i<2;i++){
		for(j=-1;j<2;j++){
			for(k=-1;k<2;k++){
				neighcells[indice*3]=i;
				neighcells[indice*3+1]=j;
				neighcells[indice*3+2]=k;
				fprintf(out,"Finit indice=%d neighx=%d neighy=%d neighz=%d\n",indice,neighcells[indice*3],neighcells[indice*3+1],neighcells[indice*3+2]);fflush(out);
				indice++;
				
			}
		}
	}
	
	
	new_cell_list_SAW();
	test_celllist_SAW();
	
	new_cell_list();
	test_celllist_CA();
	test_celllist_H();
	
	/**********RMSD INIT**************/
	
	TotRMSDNeigh=0.0;
	RMSD_Neigh=(int*)calloc(AmminoN,sizeof(int));
	RMSD_neigh=(int*)calloc(AmminoN*NEIGH_MAX,sizeof(int));
	for(indicen=0;indicen<AmminoN;indicen++){
		RMSD_Neigh[indicen]=0;
	}
	
	/**********************************/
	fprintf(out,"set energy \n");fflush(out);
	/*sist->E=0.0;
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			//fprintf(out,"prot[%d].ammino[%d].id=%d\n",pr,i,prot[pr].ammino[i].id);
			prot[pr].ammino[i].E=energy(pr,i);
			prot[pr].ammino[i].O=sist->norder;
			prot[pr].ammino[i].C=sist->ncontact;
			sist->E=sist->E+prot[pr].ammino[i].E;
			prot[pr].ammino[i].On=prot[pr].ammino[i].Cn=0;
			prot[pr].ammino[i].En=0.0;
			
		}
	}*/
			
			
	sist->E=energy();
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			sist->E+=Bond_Energy_fw(pr,i);
		}
	}
	/*fprintf(out,"Cell list summing test E=%lf\n",sist->E);fflush(out);
	order_test(&testC,&testO,&testE);
	
	fprintf(out,"Total summing test testO=%lf testC=%d testE=%lf\n",testO,testC,testE);fflush(out);
	//MPI_Abort(MPI_COMM_WORLD,err5);
	if(my_rank==0){
		
		fp=fopen("SimPar","a");
		fprintf(fp,"Energia iniziale %lf %d\n",sist->E,NPROT);
		fclose(fp);
	}
	fprintf(out,"ENERGIAAAAAAA###########%lf\n",sist->E);
	fflush(out);
	if(sist->E>99990.0){
		for(pr=0;pr<NPROT;pr++){
			for(i=0;i<Plength[pr];i++){
				fprintf(out,"%lf %lf %lf\n",
				prot[pr].ammino[i].x,
				prot[pr].ammino[i].y,
				prot[pr].ammino[i].z);
				fflush(out);
				
			}
		}
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	
	fprintf(out,"set energy ok\n");fflush(out);
	
}	





double Potential (double r,int pr,int i,int prt,int j,int resi,int resj){
	
	double potential,energ=0.0,energCA=0.0;
	double LJ_repulsive,LJ_attractive;
	
		potential=(double)(M[resi][resj]);
		
		
			
				
				LJ_repulsive=-1.0/(1.0 + exp(2.5*(ECA_Range - r))) + 1.0;


				energ=potential*LJ_repulsive;

			
		if(icycl%nsamp==0)fprintf(ECAout,"%llu %lf %lf %lf %lf\n",icycl,1/beta[betaindice],r,LJ_repulsive,potential);	
		
		return energ;
	
}

double Potential_CA (int pr,int i,int prt,int j){
	
	double potential,energ=0.0,energCA=0.0;
	double LJ_repulsive,LJ_attractive;
	//double oop=0;
	double dx,dy,dz;
	double value,r,rCA2;
	
		dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
		dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
		dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

		dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

		rCA2=(dx*dx+ dy*dy + dz*dz);
		//fprintf(out,"rCA2=%lf\t\tdx=%lf dy=%lf dz=%lf\n",rCA2,dx,dy,dz);fflush(out);
		if((j < i-NATOM)&&(MINR>rCA2)) MINR=rCA2;
		if (rCA2<sist->Rint2){
			r=sqrt(rCA2);
			
			sist->count++;
			//fprintf(out,"\t\t%lf %lf %lf ------ %lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z,prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);fflush(out);
			potential=(double)(M[prot[pr].ammino[i].residue][prot[prt].ammino[j].residue]);
			energCA=(-1.0/(1.0 + exp(2.5*(ECA_Range - r))) + 1.0)*potential;
			energ+=energCA;
			//fprintf(out,"Energ FW %lf %lf %lf %lf\n",r,energCA,potential,energ);fflush(out);
		}
		
		return energ;
	
}
double RMSD_CA_local (int pr,int i,int min,int max){
	
	int j,prt,k;
	double dx,dy,dz;
	double value,r,rCA2;
	long int indice,indicen,indicet;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_local icycl=%llu indice Big indice=%ld indicen=%ld indicet=%ld i=%d MAX=%d\n",icycl,indice,indicen,indicet,i,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
		
		if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
		}else{
			if((j>min)&&(j<max)){
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;

				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

				rCA2=(dx*dx+ dy*dy + dz*dz);


				r=sqrt(rCA2);
				
				value=DistMap1[i][j]-r;
				value*=value;
				op+=value/2.0;
			}else{
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;

				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

				rCA2=(dx*dx+ dy*dy + dz*dz);


				r=sqrt(rCA2);
				
				value=DistMap1[i][j]-r;
				value*=value;
				op+=value;
			}
		}
		


	}
		
	return op;
	
}
double RMSD_CA_SP (int pr,int i){
	
	int j,prt,k;
	double dx,dy,dz;
	double value,r,rCA2;
	long int indice,indicen,indicet;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_fw icycl=%llu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
	
			if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
			}else{
				//fprintf(out,"RMSD_CA_SP yes  i=%d j=%d \n",i,j);fflush(out);
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;

				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

				rCA2=(dx*dx+ dy*dy + dz*dz);


				r=sqrt(rCA2);
				
					value=DistMap1[i][j]-r;
					value*=value;
					op+=value;


					//op++;				
			}/*else{
				fprintf(out,"RMSD_CA_fw no  i=%d j=%d \n",i,j);fflush(out);
			}*/
		


	}
		
	return op;
	
}
double RMSD_CA_fw (int pr,int i){
	
	int j,prt,k;
	double dx,dy,dz;
	double value,r,rCA2;
	long int indice,indicen,indicet;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_fw icycl=%llu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		
		j=RMSD_neigh[indice];
		
	
			if(j<i-NATOM*2){
				//fprintf(out,"RMSD_CA_fw yes  i=%d j=%d \n",i,j);fflush(out);
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;

				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

				rCA2=(dx*dx+ dy*dy + dz*dz);


				r=sqrt(rCA2);
				
					value=DistMap1[i][j]-r;
					value*=value;
					op+=value;


					//op++;				
			}/*else{
				fprintf(out,"RMSD_CA_fw no  i=%d j=%d \n",i,j);fflush(out);
			}*/
		


	}
		
	return op;
	
}

double RMSD_CA_bw (int pr,int i){
	
	int j,prt,k;
	double dx,dy,dz;
	double value,r,rCA2;
	long int indice,indicen,indicet;
	double op=0.0;
	
	indicen=(i-ATOM_CA)/NATOM;
	indicet=indicen*NEIGH_MAX;
	//indice=indicen*2*NEIGH_MAX;
	//fprintf(out,"pr=%d i=%d indicen=%ld\n",pr,i,indicen);fflush(out);
	
	for(k=0;k<RMSD_Neigh[indicen];k++){
		indice=indicet+k;
		//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
		if(indice>=AmminoN*NEIGH_MAX){
			fprintf(out,"RMSD_CA_bw icycl=%llu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	
		j=RMSD_neigh[indice];
		
		
			if(j>i+NATOM*2){
				//fprintf(out,"RMSD_CA_bw yes  i=%d j=%d \n",i,j);fflush(out);
				dx=prot[pr].ammino[i].x-prot[pr].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[pr].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[pr].ammino[j].z;

				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

				rCA2=(dx*dx+ dy*dy + dz*dz);


				r=sqrt(rCA2);
				
					value=DistMap1[i][j]-r;
					value*=value;
					op+=value;


					//op++;				
			}/*else{
				fprintf(out,"RMSD_CA_bw no  i=%d j=%d \n",i,j);fflush(out);
			}*/
		


	}
		
	return op;
	
}

double Potential_H (int pr,int i,int prt,int j,int *touch ){
	
	double r2,r6,r12,rh2,r;
	double LJ_repulsive,LJ_attractive;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,energh,energhor;
	int ttouch=0;
	double energCA=0.0,energCA2=0.0,potential=0.0;
	double rCA;
	int indice_iCA,indice_jCA;
	
	
	if((prot[pr].ammino[i].spring_anchor!=1)&&(prot[prt].ammino[j].spring_anchor!=1)){
		/****i-H---j-H***/
		dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
		dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
		dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

		dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

		r2=(dx*dx+ dy*dy + dz*dz);
		if(r2<sist->RintH2){
			indice_iCA=rint(i/NATOM)*NATOM;
			indice_jCA=rint(j/NATOM)*NATOM;
			/****i-CA---i-H***/
			dx1=(prot[pr].ammino[indice_iCA].x-prot[pr].ammino[i].x);
			dy1=(prot[pr].ammino[indice_iCA].y-prot[pr].ammino[i].y);
			dz1=(prot[pr].ammino[indice_iCA].z-prot[pr].ammino[i].z);
			dx1=P_Dist(dx1);dy1=P_Dist(dy1);dz1=P_Dist(dz1);

			dotprod=-(dx*dx1+dy*dy1+dz*dz1)/(sqrt(r2)*sist->Bbond[1]);
			/****j-CA---j-H***/
			dx1=(prot[prt].ammino[indice_jCA].x-prot[prt].ammino[j].x);
			dy1=(prot[prt].ammino[indice_jCA].y-prot[prt].ammino[j].y);
			dz1=(prot[prt].ammino[indice_jCA].z-prot[prt].ammino[j].z);
			dx1=P_Dist(dx1);dy1=P_Dist(dy1);dz1=P_Dist(dz1);


			dotprod2=(dx*dx1+dy*dy1+dz*dz1)/(sqrt(r2)*sist->Bbond[1]);								



			if((dotprod<=HydrogenBond1)&&(dotprod2<=HydrogenBond2)) {
				//fprintf(out,"%lf %lf\n",dotprod,acos(dotprod)*180.0/PI);

				r6=pow(r2,EH_LJPower2_2);
				r12=pow(r2,EH_LJPower1_2);
				energhor=pow(dotprod*dotprod2,EH_Power);
				energh=(sigmahb12/r12-sigmahb10/r6);
				energ=(energh)*energhor;
				if(energ>100.0) energ=100.0;
				if((HydrogenBond3>=dotprod)&&(HydrogenBond4>=dotprod2)&&(r2<HydrogenBond_r)) ttouch++;

				if(icycl%nsamp==0)
				{
					dx=prot[pr].ammino[indice_iCA].x-prot[prt].ammino[indice_jCA].x;
					dy=prot[pr].ammino[indice_iCA].y-prot[prt].ammino[indice_jCA].y;
					dz=prot[pr].ammino[indice_iCA].z-prot[prt].ammino[indice_jCA].z;
					rCA=sqrt(dx*dx+ dy*dy + dz*dz);
					fprintf(EHout2,"%llu %lf %lf %lf %lf %lf %lf %lf %lf %lf\n %d %d %d %d ->> RCA=%lf\n",icycl,1/beta[betaindice],
					r2,sigmahb12,sigmahb10,dotprod,dotprod2,energh,energhor,energ,pr,i,prt,j,rCA);
					fflush(EHout2);
				}
				//fprintf(out,"%llu FW O %d H %d %lf\n",icycl,i,j,energ);fflush(out);

				//if(icycl > 180) fprintf(out,"TOUCH \n");

			}
			if(icycl%nsamp==0)fprintf(EHout,"%lf %lf %lf %lf %lf %lf %lf\n",1/beta[betaindice],r2,sigmahb12,sigmahb10,dotprod,dotprod2,energ);
			if((dotprod>1+1e-05)||(dotprod<-1-1e-05)){
				fprintf(out,"1a fw CAZOOOOOO %llu %30.20lf %lf \n",icycl,dotprod,HydrogenBond1);
				fprintf(out,"%lf ",r2);

				dx=(prot[pr].ammino[indice_iCA].x-prot[pr].ammino[i].x);
				dy=(prot[pr].ammino[indice_iCA].y-prot[pr].ammino[i].y);
				dz=(prot[pr].ammino[indice_iCA].z-prot[pr].ammino[i].z);
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				r2=(dx*dx+ dy*dy + dz*dz);
				fprintf(out,"indice_iCA=%d i=%d %lf %lf\n",indice_iCA,i,r2,sist->Bbond[1]);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			if((dotprod2>1+1e-05)||(dotprod2<-1-1e-05)){
				fprintf(out,"1b fw CAZOOOOOO %llu %30.20lf %lf \n",icycl,dotprod2,HydrogenBond2);
				fprintf(out,"%lf ",r2);


				dx=(prot[prt].ammino[indice_jCA].x-prot[prt].ammino[j].x);
				dy=(prot[prt].ammino[indice_jCA].y-prot[prt].ammino[j].y);
				dz=(prot[prt].ammino[indice_jCA].z-prot[prt].ammino[j].z);
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				r2=(dx*dx+ dy*dy + dz*dz);
				fprintf(out,"%lf %lf\n",r2,sist->Bbond[1]);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			//if(icycl > 180) fprintf(out,"FW %d %d %lf %lf %lf",i,j,r2,dotprod,dotprod2);


		}				
	}
		*touch+=ttouch;
		//if(ttouch>0) fprintf(out,"H-2 ttouch %d H %d %d O %d %d\n",ttouch,prt,j+2,pr,i+3);fflush(out);
		//if((j+2==83)&&(i+3==99)) fprintf(out,"H-2 ttouch %d H %d %d O %d %d %lf\n",ttouch,prt,j+2,pr,i+3,r2);fflush(out);
		return energ;
	
}
double energy (void){

	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/

	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	int i,pr;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	double op=0;
	
	
	int count=0;
	int touch=0,tmp_touch=0;
	double rCA2,energCA=0.0;
	int numero_partners;
	
	
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
	if(prot[pr].ammino[i].spring_anchor!=1){		
	switch(prot[pr].ammino[i].id){

		/*ATOMI CA*/
			case ATOM_CA:
				op+=RMSD_CA_fw(pr,i);
	for(indice2=0;indice2<Neigh;indice2++){
		//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
					
					
			icel_x=rint(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
			indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;

			prt=hoc_CA[indice]-1;
			j=hoc_CA[indice+1]-1;
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);

		//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){
				//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
				if(prt==pr){
					if(j <i-NATOM*2){
						 energCA=Potential_CA(pr,i,prt,j);	
						 energ+=energCA;
						//fprintf(out,"Energ E[%d][%d][%d][%d]=%lf\n",pr,i,prt,j,energCA);fflush(out);
					 }				
				}else{
					energCA=Potential_CA(pr,i,prt,j)/2.0;
					energ+=energCA;
					//fprintf(out,"Energ E[%d][%d][%d][%d]=%lf\n",pr,i,prt,j,energCA);fflush(out);
				}
				
				//numero_partners++;
				
				jj=prot[prt].ammino[j].lbw_i;
				prt=prot[prt].ammino[j].lbw_pr;
				j=jj;
			}
			/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
}		
break;

/*ATOMI H*/
			case ATOM_H:
	for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);	
					
			indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;

			prt=hoc_H[indice]-1;
			j=hoc_H[indice+1]-1;
			//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){
			  ///fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
					if(prt==pr){
						if(j <i-NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&tmp_touch)/2.0;
						
					}
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
			//if((icycl>=0)&&(icycl<=3100)){
			//		fprintf(out,"H pr=%d i=%d partners=%d\n",pr,i,numero_partners);
			//		fflush(out);
					//	}
			

		}		
		break;

/*ATOMI O*/
	

}
}  
}
}
 if(tmp_touch%2 !=0){
	   	fprintf(out,"tmp_touch=%d not even\n",tmp_touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);

	}
	
	touch+=tmp_touch/2;
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;
	
	//if(icycl > 0) fprintf(out,"energy_fw  %llu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
} 
 

double energy_SAW_test (){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int pr,i,k,j,jj,prt;
	unsigned int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	int op=0;
	int count=0;
	int touch=0;
	int Nver=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	for(pr=0;pr<NPROT;pr++){
		for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
			for(prt=0;prt<NPROT;prt++){
					for (j=ATOM_CA;j<Plength[prt];j+=NATOM){

						if(prt==pr){
							if(j <i){
								dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
								dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
								dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

								dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

								rCA2=(dx*dx+ dy*dy + dz*dz);
								if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);

									return energ;
									}
							}
						}else{

							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

							rCA2=(dx*dx+ dy*dy + dz*dz);
							if (rCA2<sist->Rmin2){
							 energ=99999.0;
							//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);

							return energ;
							}
						}
					}
				}
			
		}
	}
		
				
				
		

		
	
	
	return energ;
}

double energy_SAW_local (int pr,int i,int min , int max){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int k,j,jj,prt;
	unsigned int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	int op=0;
	int count=0;
	int touch=0;
	int Nver=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
		for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			
			indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
			
			prt=hoc_SAW[indice]-1;
			j=hoc_SAW[indice+1]-1;
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(j>=0){
				
				if(prt==pr){
					 if((j >=i)&&(j <=i)){
						 }else{
							 if((j>min)&&(j<max)){
						 	}else{
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);

									return energ;
									}
								}
					}
				}else{
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);

									return energ;
									}
					}
				
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
				
		

		
	
	
	return energ;
}

double energy_SAW_output (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int k,j,jj,prt;
	unsigned int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	int op=0;
	int count=0;
	int touch=0;
	int Nver=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
		for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			
			indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
			
			prt=hoc_SAW[indice]-1;
			j=hoc_SAW[indice+1]-1;
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(j>=0){
				
				if(prt==pr){
					if(j <i){
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
							energ=99999.0;
							fprintf(out,"%d %d %d %d\n",pr,i,prt,j);
							fprintf(out,"%lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
							fprintf(out,"%lf %lf %lf\n",prot[prt].ammino[j].x,prot[prt].ammino[j].y,prot[prt].ammino[j].z);
							fprintf(out,"r=%lf rmin=%lf\n",sqrt(rCA2),sist->Rmin);
							return energ;
						}
					}
				}else{
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									fprintf(out,"%d %d %d %d\n",pr,i,prt,j);

									return energ;
									}
					}
				
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
				
		

		
	
	
	return energ;
}
double energy_SAW_SP (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int k,j,jj,prt;
	unsigned int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	int op=0;
	int count=0;
	int touch=0;
	int Nver=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
		for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			
			indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
			
			prt=hoc_SAW[indice]-1;
			j=hoc_SAW[indice+1]-1;
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(j>=0){
				
				if(prt==pr){
					if(j !=i){
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									 //fprintf(out,"%d %d %d %d\n",pr,i,prt,j);

									return energ;
									}
					}
				}else{
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);

									return energ;
									}
					}
				
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
				
		

		
	
	
	return energ;
}
double energy_SAW (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int k,j,jj,prt;
	unsigned int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	int op=0;
	int count=0;
	int touch=0;
	int Nver=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
	
		for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			
			indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
			
			prt=hoc_SAW[indice]-1;
			j=hoc_SAW[indice+1]-1;
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(j>=0){
				
				if(prt==pr){
					if(j <i){
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									 //fprintf(out,"%d %d %d %d\n",pr,i,prt,j);

									return energ;
									}
					}
				}else{
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//fprintf(out,"%d %d %d %d\n",pr,i,prt,j);

									return energ;
									}
					}
				
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
		
		
	}		
				
		

		
	
	
	return energ;
}

double energy_SAW_bw (int pr,int i){
	
	
	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/
	
	
	int k,j,jj,prt;
	unsigned int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	int op=0;
	int count=0;
	int touch=0;
	int Nver=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	
		
		for(indice2=0;indice2<Neigh;indice2++){
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size)+neighcells[indice2*3+2];
	//		fprintf(out,"energy_SAW_bw  1 indice2=%lu\nicel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
			if(indice>=2*N_SAW_cells*N_SAW_cells*N_SAW_cells){
		//		fprintf(out,"energy_SAW_bw indice=%lu N_SAW_cells=%lu too big\nicel_x=%d icel_y=%d icel_z=%d\n",indice,N_SAW_cells,icel_x,icel_y,icel_z);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		//	fprintf(out,"energy_SAW_bw  2 indice2=%lu\n",indice2);fflush(out);
			prt=hoc_SAW[indice]-1;
			j=hoc_SAW[indice+1]-1;
			
		//	fprintf(out,"energy_SAW_bw 3 indice2=%lu\n",indice2);fflush(out);

			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(j>=0){
				
				if(prt==pr){
					if(j >i){
						
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);

									return energ;
									}
					}
				}else{
						dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
						dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
						dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
		
						dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
						rCA2=(dx*dx+ dy*dy + dz*dz);
						if (rCA2<sist->Rmin2){
									 energ=99999.0;
									//if(icycl > 1500) fprintf(out,"%d %d %d %d %d\n",pr,i,prt,j,k);

									return energ;
									}
					}
				
			
			jj=prot[prt].ammino[j].lbw_SAW_i;
			prt=prot[prt].ammino[j].lbw_SAW_pr;
			j=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	//	fprintf(out,"energy_SAW_bw ok indice2=%lu\n",indice2);fflush(out);
		
		
	}		
				
		

		
	
	
	return energ;
}

double energy_SP_brot_local (int pr,int i,int min,int max){

	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/

	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	double op=0,opCA=0.0;
	
	
	int count=0,temp_count=0;
	double touch=0;
	int temp_touch=0;
	double rCA2,energCA=0.0,energ_int=0.0,energ_ext=0.0;
	int numero_partners;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
	//fprintf(out,"Energ Local %llu i=%d id=%d\n",icycl,i,prot[pr].ammino[i].id);fflush(out);
	if(prot[pr].ammino[i].spring_anchor!=1){
	switch(prot[pr].ammino[i].id){

		/*ATOMI CA*/
			case ATOM_CA:
				opCA=RMSD_CA_local(pr,i,min,max);
				op+=opCA;
				//fprintf(out,"Energ_local %llu op[%d][%d]=%lf\n",icycl,pr,i,opCA);fflush(out);
				
	for(indice2=0;indice2<Neigh;indice2++){
		//fprintf(out,"Energ Local CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			//fprintf(out,"Energ Local CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
					
					
			icel_x=rint(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			//fprintf(out,"Energ Local CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
			indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;

			prt=hoc_CA[indice]-1;
			j=hoc_CA[indice+1]-1;

		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);

		//fprintf(out,"Energ Local CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){
				//fprintf(out,"Energ Local CA loop j=%d prt=%d\n",j,prt);fflush(out);
				if(prt==pr){
					
						 if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
						 }else{
							 if((j>min)&&(j<max)){
						 	}else{
							 	//fprintf(out,"-------------> j=%d id=%d\n",j,prot[prt].ammino[j].id);fflush(out);
								energCA=Potential_CA(pr,i,prt,j);
								energ_ext+=energCA;
								energ+=energCA;
							}
						 }
					 			
				}else{
					energCA=Potential_CA(pr,i,prt,j);
					energ+=energCA;
				}
				
				//numero_partners++;
				
				jj=prot[prt].ammino[j].lbw_i;
				prt=prot[prt].ammino[j].lbw_pr;
				j=jj;
			}
			/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
}		
break;

/*ATOMI H*/
			case ATOM_H:
	for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
					
					
			indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;

			prt=hoc_H[indice]-1;
			j=hoc_H[indice+1]-1;
			
			//fprintf(out,"Energ Local H %d %d %d %d\n",pr,i,prt,j);fflush(out);
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){

					if(prt==pr){
						 if((j >=i-NATOM*2)&&(j <=i+NATOM*2)){
						 }else{
							 if((j>min)&&(j<max)){
						 	}else{
							 	//fprintf(out,"-------------> j=%d id=%d\n",j,prot[prt].ammino[j].id);fflush(out);
								temp_touch=0;
								energCA=Potential_H(pr,i,prt,j,&temp_touch);
								touch+=temp_touch;
								energ_ext+=energCA;
								energ+=energCA;
							}
						}
						
					}else{
						temp_touch=0;
						energCA=Potential_H(pr,i,prt,j,&temp_touch);
						touch+=temp_touch;
						energ+=energCA;
					}
					
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"H pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
			

		}		
		break;

/*ATOMI O*/


}
}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=rint(touch);
	sist->norder=op;

	//if(icycl > 0) fprintf(out,"energy_local  %llu i=%d energ_int=%lf energ_ext=%lf\n",icycl,i,energ_int,energ_ext); fflush(out);
	return energ;
}
double energy_SP (int pr,int i){

	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/

	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	double op=0;
	double tmp_energ;
	int tmp_touch;
	
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0;
	int numero_partners;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
if(prot[pr].ammino[i].spring_anchor!=1){
	switch(prot[pr].ammino[i].id){

		/*ATOMI CA*/
			case ATOM_CA:
				op+=RMSD_CA_SP(pr,i);
				//energ+=Bond_Energy_SP(pr,i);
	for(indice2=0;indice2<Neigh;indice2++){
		//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
					
					
			icel_x=rint(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
			indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;

			prt=hoc_CA[indice]-1;
			j=hoc_CA[indice+1]-1;

		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);

		//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){
				//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
				if(prt==pr){
					if((j >=i-NATOM*2)&&(j <=i+NATOM*2)) {
					}else{
						energ+=Potential_CA(pr,i,prt,j);
					}					
				}else{
					energ+=Potential_CA(pr,i,prt,j);
				}
				//numero_partners++;
				
				jj=prot[prt].ammino[j].lbw_i;
				prt=prot[prt].ammino[j].lbw_pr;
				j=jj;
			}
			/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
}		
break;

/*ATOMI H*/
			case ATOM_H:
	for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
					
					
			indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;

			prt=hoc_H[indice]-1;
			j=hoc_H[indice+1]-1;
			
			//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){
					
					if(prt==pr){
						if((j >=i-NATOM*2)&&(j <=i+NATOM*2)) {
						}else{
							/*if((icycl>=1411)&&(icycl<=1411)){
					fprintf(out,"H pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);
					fflush(out);
				}*/
						tmp_touch=0;
						 	tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch);
							energ+=tmp_energ;
							touch+=tmp_touch;
#ifdef PROGRESS
							if((icycl>=1411)&&(icycl<=1411)){
								fprintf(out,"energy_SP %llu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,tmp_energ,tmp_touch);fflush(out);
							}
#endif
					 	}
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
						#ifdef PROGRESS
						if((icycl>=1411)&&(icycl<=1411)){
							fprintf(out,"energy_SP %llu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,tmp_energ,tmp_touch);fflush(out);
						}
#endif
					}
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					
				}
				
			

		}		
		break;

	

}
}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;
	
	//if(icycl > 0) fprintf(out,"energy_fw  %llu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
}
double energy_SP_Pivot_fw (int pr,int i){

	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/

	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor;
	double op=0;
	
	
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0;
	int numero_partners;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
if(prot[pr].ammino[i].spring_anchor!=1){
	switch(prot[pr].ammino[i].id){

		/*ATOMI CA*/
			case ATOM_CA:
				op+=RMSD_CA_fw(pr,i);
	for(indice2=0;indice2<Neigh;indice2++){
		//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
					
					
			icel_x=rint(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
			indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;

			prt=hoc_CA[indice]-1;
			j=hoc_CA[indice+1]-1;

		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);

		//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){
				//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
				if(prt==pr){
					if(j <i-NATOM*2) energ+=Potential_CA(pr,i,prt,j);					
				}else{
					energ+=Potential_CA(pr,i,prt,j);
				}
				//numero_partners++;
				
				jj=prot[prt].ammino[j].lbw_i;
				prt=prot[prt].ammino[j].lbw_pr;
				j=jj;
			}
			/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"CA pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
}		
break;

/*ATOMI H*/
			case ATOM_H:
	for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			  icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			  icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
					
					
			indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;

			prt=hoc_H[indice]-1;
			j=hoc_H[indice+1]-1;
			
			//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			//numero_partners=0;
			while(j>=0){

					if(prt==pr){
						if(j <i-NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
					}
					//numero_partners++;
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
				/*if((icycl>=3000)&&(icycl<=3100)){
					fprintf(out,"H pr=%d i=%d partners=%d\n",pr,i,numero_partners);
					fflush(out);
				}*/
			

		}		
		break;

/*ATOMI O*/
	

}
}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;

	//if(icycl > 0) fprintf(out,"energy_fw  %llu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
} 





double energy_SP_Pivot_bw (int pr,int i){

	/************************************************
	* energy -- calculate the energy interaction*
	* 	with the i residue and all the	*
	*	neighbour residues 			*
	* 						*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	i -- the refernce residue 		*
	*	res -- the type of the i residue	*
	*	M -- is the interaction matrix	*
	*						*
	* Return:					*
	*	energ -- the energy of the protein	*
	*						*
	*************************************************/

	unsigned long int indice,indice2,indice3;
	int icel_x,icel_y,icel_z;
	int k,j,prt,jj;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0.0,dotprod,dotprod2,value,energh,energhor;
	double op=0.0;
	
	int count=0;
	int touch=0;
	double rCA2,energCA=0.0;
	//if(icycl%10000)printf("%d\n",prot[pr].ammino[i].Nverl);
if(prot[pr].ammino[i].spring_anchor!=1){
	switch(prot[pr].ammino[i].id){

		/*ATOMI CA*/
			case ATOM_CA:
				op+=RMSD_CA_bw(pr,i);
	for(indice2=0;indice2<Neigh;indice2++){
		//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
			
			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			
			//fprintf(out,"Energ FW CA N_CA_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_CA_cells,icel_x,icel_y,icel_z);fflush(out);
			indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;

			prt=hoc_CA[indice]-1;
			j=hoc_CA[indice+1]-1;

		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);

		//fprintf(out,"Energ FW CA %lu %d %d %d %d\n",indice,pr,i,prt,j);fflush(out);
			while(j>=0){
					//fprintf(out,"Energ FW CA loop j=%d prt=%d\n",j,prt);fflush(out);
					if(prt==pr){
						if(j>i+NATOM*2)  energ+=Potential_CA(pr,i,prt,j);

						
					}else{

						 energ+=Potential_CA(pr,i,prt,j);
					}
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}
}		
break;

/*ATOMI H*/
			case ATOM_H:
	for(indice2=0;indice2<Neigh;indice2++){
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
			icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
			icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
			
			indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;

			prt=hoc_H[indice]-1;
			j=hoc_H[indice+1]-1;
			
			//fprintf(out,"Energ FW H %d %d %d %d\n",pr,i,prt,j);fflush(out);
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(j>=0){

					if(prt==pr){
						if(j>i+NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);
						
					}else{
						energ+=Potential_H(pr,i,prt,j,&touch);
					}
					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
				}

}		
break;


	

}
}	
	sist->ntouch=0;
	if(touch>0) sist->ntouch=1;
	
	sist->ncontact=touch;
	sist->norder=op;

	//if(icycl > 0) fprintf(out,"energy_fw  %llu count=%d energ=%lf\n",icycl,count,energ); fflush(out);
	return energ;
}
 
void order_All (void){
	
	int pr,i;
	int kk,k,j,prt;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0.0,dotprod,dotprod2,value,energh,energhor;
	double op=0.0;

	int touch=0;
	double rCA2,energCA=0.0;
	sist->density=0;
	
	
	//if(icycl > 0) fprintf(out,"order 1626\n");
	for(pr=0;pr<NPROT;pr++){
		for (i=0;i<Plength[pr];i++){
			
			if(prot[pr].ammino[i].spring_anchor!=1){
			
						switch(prot[pr].ammino[i].id){

					/*ATOMI CA*/
						case ATOM_CA:
							op+=RMSD_CA_fw(pr,i);
							for(prt=0;prt<NPROT;prt++){
								for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
								if(prot[prt].ammino[j].spring_anchor!=1){
								if(prt==pr){
									if(j >i+NATOM*2) energ+=Potential_CA(pr,i,prt,j);
								}else{

									energ+=Potential_CA(pr,i,prt,j);
								}
							}
								
							}
			}		
			break;

			/*ATOMI H*/
						case ATOM_H:
						for(prt=0;prt<NPROT;prt++){
							for(k=ATOM_CA;k<Plength[prt];k+=NATOM){
			
								
								for(kk=0;kk<NSPOTS;kk++){
				
										j=k+kk+1;

									if(prot[prt].ammino[j].spring_anchor!=1){
									if(prt==pr){
										if(j >i+NATOM*2) energ+=Potential_H(pr,i,prt,j,&touch);

									}else{
										energ+=Potential_H(pr,i,prt,j,&touch);
									}
								}
								}

								}

						}		
			break;

			

			}
			
			
			
			
		if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) sist->density+=1.0;
	}
		}
	}
	
	
	sist->touch=0;
	if(touch>0) sist->touch=1;
	
	sist->contact=touch;
	sist->order=op;
	
	sist->RMSD=lround(RMSD_bin*sqrt(sist->order/TotRMSDNeigh));
	if(sist->RMSD>=maxO) sist->RMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	
	
	return;
	
}

void order_test (int *C, double *O, double *E){
	
	int pr,i;
	int kk,k,j,prt;
	double r2,r6,r12,rh2;
	double r,LJ_repulsive,LJ_attractive,potential;
	double dx,dy,dz;
	double dx1,dy1,dz1;
	double energ=0,dotprod,dotprod2,value,energh,energhor,tmp_energ;
	double op=0.0,opCA=0.0;
	int touch=0,tmp_touch=0;
	int tmp_touch2=0; 
	double rCA2,energCA=0.0;
	sist->density=0;
	
	
	//if(icycl > 0) fprintf(out,"order 1626\n");
	for(pr=0;pr<NPROT;pr++){
		for (i=0;i<Plength[pr];i++){
				
					
				
				switch(prot[pr].ammino[i].id){

					/*ATOMI CA*/
						case ATOM_CA:
							if(prot[pr].ammino[i].spring_anchor!=1){
							opCA=RMSD_CA_fw(pr,i);
							op+=opCA;
							//fprintf(out,"order_test op[%d][%d]=%lf\n",pr,i,opCA);fflush(out);
							
							for(prt=0;prt<NPROT;prt++){
								for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
									if(prot[prt].ammino[j].spring_anchor!=1){	
									if(prt==pr){
										if(j <i-NATOM*2){
											 tmp_energ=Potential_CA(pr,i,prt,j);
											 //fprintf(out,"order_test E[%d][%d][%d][%d]=%lf\n",pr,i,prt,j,tmp_energ);fflush(out);
											 energ+=tmp_energ;
										 }
									}else{

										tmp_energ=Potential_CA(pr,i,prt,j)/2.0;
										//fprintf(out,"order_test E[%d][%d][%d][%d]=%lf\n",pr,i,prt,j,tmp_energ);fflush(out);
										energ+=tmp_energ;
									}
								}

								}
							}
						}
							tmp_energ=Bond_Energy_fw(pr,i);
							energ+=tmp_energ;
							break;

			/*ATOMI H*/
						case ATOM_H:
							if(prot[pr].ammino[i].spring_anchor!=1){
						for(prt=0;prt<NPROT;prt++){
							for(k=ATOM_CA;k<Plength[prt];k+=NATOM){
			
								for(kk=0;kk<NSPOTS;kk++){
				
										j=k+kk+1;

									if(prot[prt].ammino[j].spring_anchor!=1){	
									if(prt==pr){
										if(j <i-NATOM*2) {
											tmp_touch2=0;
											tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch2);
											touch+=tmp_touch2;
											energ+=tmp_energ;
											//if((icycl>=0)&&(icycl<=1411)){
											//  fprintf(out,"order_test %llu E_H[%d][%d][%d][%d]=%lf touch=%d\n",icycl,pr,i,prt,j,tmp_energ,tmp_touch2);fflush(out);
											//}										
										}

									}else{
										tmp_energ=Potential_H(pr,i,prt,j,&tmp_touch)/2.0;
										energ+=tmp_energ;
										//fprintf(out,"order_test %llu E_HO[%d][%d][%d][%d]=%lf \n",icycl,pr,i,prt,j,tmp_energ);fflush(out);
										//fprintf(out,"Order Test H %lf\n",energ);fflush(out);
									}
								}

								}
							}

						}
					}		
			break;

			

			}
			
			
			
			
		if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) sist->density+=1.0;
		
		//fprintf(out,"order_test %llu E[%d][%d]=%lf \n",icycl,pr,i,tmp_energ);fflush(out);
	
		}
	}
	//fprintf(out,"Order Test final %lf\n",energ);fflush(out);	
	 if(tmp_touch%2 !=0){
	   	fprintf(out,"order_test %llu tmp_touch=%d not even\n",icycl,tmp_touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);

	}
	touch+=tmp_touch/2;
	*C=touch;
	*O=op;
	*E=energ;
	
	if(touch<0){
		
		fprintf(out,"touch < 0  %llu %d\n",icycl,touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	return;
	
}





void fsamp (void){
	
	/************************************************
	* fsamp -- write the file with the result of*
	* 	the simulation every nsamp steps*
	*	and convert the squence from number *
	* 	to letters 				*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*	fp -- the file where to write 	*
	*						*
	* Return:					*
	*	void				*
	*						*
	*************************************************/
	
	char sequenza[]="OACDEFGHIKLMNPQRSTVWY";
	
	int i,Z=0,j,k,pr,prt,locallinks,kk;
	double lunbias=0,r2,dx,dy,dz,r6;
	double averagex,averagey,averagez;
	static int **indice=NULL,NTOT;
	static unsigned long long int iter=0;
	double x,y,z;
	char message[200];
	FILE *fp;
	
	if(indice==NULL) {
		
		indice=(int **)calloc(NPROT,sizeof(int*));
		for(pr=0;pr<NPROT;pr++){
			indice[pr]=(int *)calloc(Plength[pr],sizeof(int));
		}
		NTOT=0;
		for(pr=0;pr<NPROT;pr++){
			for(i=1;i<Plength[pr];i+=NATOM){
				NTOT++;
				indice[pr][i]=NTOT;
				
			}
		}
	}
	/*for(j=0;j<NPROT;j++){
		fprintf(fSeq,"%d\n",(Plength[j]*2-1));
		fprintf(fSeq,"\n");
		for(i=0;i<Plength[j];i++){
			fprintf(fSeq,"%c %10.5lf %10.5lf %10.5lf\nX 0.00 0.00 0.00\n",sequenza[prot[j].ammino[i].residue],
			(prot[j].ammino[i].x-prot[0].ammino[0].x),
			(prot[j].ammino[i].y-prot[0].ammino[0].y),
			(prot[j].ammino[i].z-prot[0].ammino[0].z));
			
		}
	}*/
	//fprintf(out,"VMD Samp %llu\n",icycl);
	sprintf(message, "VMD/Seqvmd-T-%lf-I-%08llu.pdb",1/beta[betaindice],iter);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		kk=1;
		for(i=0;i<Plength[j];i++){
			x=prot[j].ammino[i].x-prot[0].ammino[0].x+BoxSize/2.0;
			y=prot[j].ammino[i].y-prot[0].ammino[0].y+BoxSize/2.0;
			z=prot[j].ammino[i].z-prot[0].ammino[0].z+BoxSize/2.0;
			if(NPROT>1) {x=P_Cd(x);y=P_Cd(y);z=P_Cd(z);}
		
			fprintf(fSeqvmd,"ATOM  %5d  %s %03d  %4d    %8.3lf%8.3lf%8.3lf\n",j*NPROT+i+1,sist->Atoms[prot[j].ammino[i].id],prot[j].ammino[i].residue,kk,x,y,z);
			if(i%NATOM==0) kk++;
		}
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	
	
	sprintf(message, "MinE-P-%d.pdb",my_rank);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		kk=1;
		for(i=0;i<Plength[j];i++){
			x=minprot[j].ammino[i].x-minprot[0].ammino[0].x+BoxSize/2.0;
			y=minprot[j].ammino[i].y-minprot[0].ammino[0].y+BoxSize/2.0;
			z=minprot[j].ammino[i].z-minprot[0].ammino[0].z+BoxSize/2.0;
			if(NPROT>1) {x=P_Cd(x);y=P_Cd(y);z=P_Cd(z);}
		
			fprintf(fSeqvmd,"ATOM  %5d  %s %03d  %4d    %8.3lf%8.3lf%8.3lf\n",j*NPROT+i+1,sist->Atoms[prot[j].ammino[i].id],prot[j].ammino[i].residue,kk,x,y,z);
			if(i%NATOM==0) kk++;
		}
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	
	sprintf(message, "MinO-P-%d.pdb",my_rank);
	fSeqvmd=fopen(message,"w");
	for(j=0;j<NPROT;j++){
		kk=1;
		for(i=0;i<Plength[j];i++){
			x=minOprot[j].ammino[i].x-minOprot[0].ammino[0].x+BoxSize/2.0;
			y=minOprot[j].ammino[i].y-minOprot[0].ammino[0].y+BoxSize/2.0;
			z=minOprot[j].ammino[i].z-minOprot[0].ammino[0].z+BoxSize/2.0;
			if(NPROT>1) {x=P_Cd(x);y=P_Cd(y);z=P_Cd(z);}
		
			fprintf(fSeqvmd,"ATOM  %5d  %s %03d  %4d    %8.3lf%8.3lf%8.3lf\n",j*NPROT+i+1,sist->Atoms[prot[j].ammino[i].id],prot[j].ammino[i].residue,kk,x,y,z);
			if(i%NATOM==0) kk++;
		}
	}
	fprintf(fSeqvmd,"CRYST1%9.3lf%9.3lf%9.3lf  90.00  90.00  90.00 P 1           1\n",BoxSize,BoxSize,BoxSize);
	fclose(fSeqvmd);
	
	
	/*//fprintf(out,"VMD Samp OK %llu\n",icycl);fflush(out);
	sprintf(message, "CMAPS/CMAP-T-%lf-O-%d-I-%08llu.dat",1/beta[betaindice],Lattice,iter);
	fp=fopen(message,"w");
	
	NLINKS=0;
	//fprintf(fp,"%d\n",NPROT);
	
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			locallinks=0;
			
			for(prt=0;prt<NPROT;prt++){
				for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
				
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;
				
				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
				
				r2=(dx*dx+ dy*dy + dz*dz);
				
				//if (r2<sist->Rint2){
					locallinks++;
				//}
				}
			}
			if(NLINKS<=locallinks) NLINKS=locallinks;
		}
	}
	fprintf(fp,"%d\n",NTOT+1);
	fprintf(fp,"%d\n",NLINKS);
	fprintf(fp,"0\n");
	for(j=0;j<NLINKS;j++) fprintf(fp,"0 0 -1 -1 -1 -1\n");
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			locallinks=0;
			for(prt=0;prt<NPROT;prt++){
				for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
				
				
					dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
					dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
					dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

					r2=(dx*dx+ dy*dy + dz*dz);

					if (r2<sist->Rint2){
						//r6=sigma6/pow(r2,LJpower);
						fprintf(fp,"%d %d %d %d %d %d %15.10lf\n",indice[pr][i],indice[prt][j],pr,i,prt,j,r2);
						locallinks++;
					}

				}
			}
			for(j=0;j<NLINKS-locallinks;j++) fprintf(fp,"%d 0 -1 -1 -1 -1\n",indice[pr][i]);
		}
	}
	fclose(fp);*/
	//order_packing ();
	
	//fprintf(out,"CMAPS Samp OK %llu\n",icycl);fflush(out);
	fprintf(fEner,"%llu ",iter);
	fflush(fEner);
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
	if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	sist->RMSD=lround(RMSD_bin*sqrt(sist->order/TotRMSDNeigh));
 	if(sist->RMSD>=maxO) sist->RMSD=maxO-1;
  
		fprintf(fEner,"%10.5lf %10.5f %10.5f %10.5f %10.5f %10.5f %5d %10.5f %10.5lf %10.5lf %lf\n",sist->E,MinE,MinO/RMSD_bin,MinEO/RMSD_bin,MinOE,sqrt(MINR),sist->Mean_H_Bonds,sist->RMSD/RMSD_bin,sqrt(sist->order/TotRMSDNeigh),Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD],1/beta[betaindice]); 
	
	fflush(fEner);
	
	//fprintf(out,"ENER Samp OK %llu\n",icycl);fflush(out);
	fprintf(fMC_brot_cbmc_fw,	"%lf %llu %llu %llu %llu %llu %llu %llu\n",1/beta[betaindice],icycl,cbmc_fw_rejectedself,
			cbmc_fw_rejectedener,cbmc_fw_accepted,cbmc_fw_tried,efficiency,efficiency_tried);
	fprintf(fMC_brot_bgs,	"%lf %llu %llu %llu %llu %llu\n",1/beta[betaindice],icycl,bgs_rejectedself,bgs_rejectedener,bgs_accepted,bgs_tried);
	fprintf(fMC_brot_local,	"%lf %llu %llu %llu %llu %llu\n",1/beta[betaindice],icycl,local_rejectedself,local_rejectedener,local_accepted,local_tried);
	fprintf(fMC_Pivot_fw,	"%lf %llu %llu %llu %llu %llu\n",1/beta[betaindice],icycl,fw_rejectedself,fw_rejectedener,fw_accepted,fw_tried);
	fprintf(fMC_All_trasl,	"%lf %llu %llu %llu %llu %llu\n",1/beta[betaindice],icycl,trasl_rejectedself,trasl_rejectedener,trasl_accepted,trasl_tried);
	fprintf(fMC_Pivot_bw,	"%lf %llu %llu %llu %llu %llu\n",1/beta[betaindice],icycl,bw_rejectedself,bw_rejectedener,bw_accepted,bw_tried);
	fflush(fMC_All_trasl);
	fflush(fMC_brot_cbmc_fw);
	fflush(fMC_brot_bgs);
	fflush(fMC_brot_local);
	fflush(fMC_Pivot_fw);
	fflush(fMC_Pivot_bw);
	iter++;
	//fprintf(out,"ACCEPT Samp OK %llu\n",icycl);fflush(out);
	sprintf(message, "VMD/Seqvmd-T-%lf-O-%d-I-%08llu.bin",1/beta[betaindice],Lattice,iter);
	fp=fopen(message,"w");
	writeBinary(fp);
	fclose(fp);
	sprintf(message,"final-P-%d.out",my_rank);
	fp=fopen(message,"w");
	writeBinary(fp);
	fclose(fp);
	sprintf(message,"MinE-P-%d.bin",my_rank);
	fp=fopen(message,"w");
	writeBinaryMin(fp);
	fclose(fp);
	sprintf(message,"MinO-P-%d.bin",my_rank);
	fp=fopen(message,"w");
	writeBinaryMinO(fp);
	fclose(fp);
	//fprintf(out,"BINARY Samp OK %llu\n",icycl);fflush(out);
	
}





void native (void){
	
	FILE *fp=NULL;
	
	int i=0,j=0,k=0,kk=0,n1=0,p2=0,pr=0,prt=0,a=0,linea=0,test=0;
	int inew=0, jnew=0;
	double x=0,y=0,z=0;
	double r2=0,r6=0,r12=0;
	double dx=0,dy=0,dz=0;
	double E1=0.0,E2=0.0;
	char* line=NULL;
	double r=0,LJ=0,LJ_repulsive=0,potential=0;
	long int indice=0,indicen=0,indicet=0;
	long int indicenn=0,indicett=0,indicex=0;	
	
	line=(char *) calloc (BUFFER_CHAR,sizeof(char));
	
	E1=0.0;
	E2=0.0;
	fprintf(out,"native 2230\n");
	fflush(out);
	fp=fopen("native1","r");
	if ( fp == NULL) {
		TotRMSDNeigh=1.0;
		/*printf ("File native1 not found\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);*/
	}else{
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		while(!feof(fp)){
			fgets(line,BUFFER_CHAR*sizeof(char),fp);

			p2=sscanf (line,"%*d %*d %d %d %d %d %lf\n",&pr,&i,&prt,&j,&x);
			if(p2!=5){
				fprintf(out,"Wrong native format %s\n",line);fflush(out);
				exit(0);
			}
			///fprintf(out,"pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);fflush(out);	
			if(pr>-1){
				
				
				
			  if(pr==prt){///ignores the first lines relative to the solvent with j=!i
			    if(( j!= i+5)&&(j != i)&&(j != i-5)&&(j != i-2*5)&&(j != i+2*5)){/// exlude the neighbours, the number 5 is because in the contact map the beads total indexes in the chains are written in intervals of 5, if here I read them in intervals of 5 but I rename them with new indexes is ok!
						
			      r=sqrt(x);/// x is the distance between the two beads			
			      inew=i*NATOM/5;
			      jnew=j*NATOM/5;
			      E1+=Potential(r,pr,inew,prt,jnew,prot[pr].ammino[inew].residue,prot[prt].ammino[jnew].residue)/2.0;		
							if(x<sist->Rint2) {
							  ///fprintf(out,"pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);fflush(out);
							  indicen=(inew-ATOM_CA)/NATOM;///This is because in the contact map the indexes of CA are in intervals of 5
													
								indice=NEIGH_MAX*indicen+RMSD_Neigh[indicen];
								/// This is the index of the total vector containing the list of the indexes of the neighbours, every j neighbour RMSD_Neigh[indicen]++ and the indice scrolls of 1. When i changes the indice scrolls of NEIGH_MAX*indicen, so every i has a slot of NEIGHT_MAX elements in the vector.
								if(indice>=AmminoN*NEIGH_MAX){
									fprintf(out,"indice Big indice=%ld MAX=%d\n",indice,AmminoN*NEIGH_MAX);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
								///fprintf(out,"indice=%ld indicen=%ld\n",indice,indicen);fflush(out);
								
								RMSD_neigh[indice]=jnew;
								RMSD_Neigh[indicen]++;///if the beads are close update the RMSD of this bead indicen
								///fprintf(out,"Native 1 Neigh[%d][%ld]=%d dist=%lf limit=%lf\n",i,indice,j,x,sist->Rint2);fflush(out);
								TotRMSDNeigh++;
								///fprintf(out,"RMSD_neigh[%ld]=%d RMSD_neigh[%ld]=%d RMSD_Neigh[%ld]=%d\n",indice,RMSD_neigh[indice],indice+1,RMSD_neigh[indice+1],indicen,RMSD_Neigh[indicen]);fflush(out);
								if(RMSD_Neigh[indicen]>=NEIGH_MAX){
									fprintf(out,"Too many neigh in the native1 structure Neigh=%d MAX=%d\n",RMSD_Neigh[indicen],NEIGH_MAX);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
							}
							 DistMap1[jnew][inew]=DistMap1[inew][jnew]=r;
					}
			  }else{ /// between different proteins

					r=sqrt(x);						
					E1+=Potential(r,pr,inew,prt,jnew,prot[pr].ammino[inew].residue,prot[prt].ammino[jnew].residue)/2.0;	
								
						/*	if(x<sist->Rint2) {
								indicen=(i-ATOM_CA)/NATOM;
								
								indice=2*NEIGH_MAX*(indicen)+2*RMSD_Neigh[indicen];
								RMSD_neigh[indice]=prt;
								RMSD_neigh[indice+1]=j;
								RMSD_Neigh[indicen]++;
								if(RMSD_Neigh[indicen]>=NEIGH_MAX){
									fprintf(out,"Too many neigh in the native1 structure Neigh=%d MAX=%d\n",RMSD_Neigh[indicen],NEIGH_MAX);
									fflush(out);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
							}
						 DistMap1[pr][i][prt][j]=r;*/
				}
			}
		}		
		fclose(fp);	
	}
	TotRMSDNeigh*=NPROT;
	/******** Test Neighbour native list ****/
	for(i=ATOM_CA;i<Plength[0];i+=NATOM){
		indicen=(i-ATOM_CA)/NATOM;
		indicet=indicen*NEIGH_MAX;
		for(k=0;k<RMSD_Neigh[indicen];k++){
			indice=indicet+k;
			//fprintf(out,"\t\t indice=%ld k=%d\n",indice,k);fflush(out);
			if(indice>=AmminoN*NEIGH_MAX){
			  ///fprintf(out,"RMSD_CA_fw icycl=%llu indice Big indice=%ld MAX=%d\n",icycl,indice,AmminoN*NEIGH_MAX);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}

			j=RMSD_neigh[indice];
			
			indicenn=(j-ATOM_CA)/NATOM;
			indicett=indicenn*NEIGH_MAX;
			test=0;
			for(kk=0;kk<RMSD_Neigh[indicenn];kk++){
				indicex=indicett+kk;
				if(RMSD_neigh[indicex]==i) test++;
			}
			//fprintf(out,"Native neigh test=%d Neigh[%d][%ld]=%d\n",test,i,indice,j);fflush(out);
			if(test !=1){
				
				
				for(kk=0;kk<RMSD_Neigh[indicen];kk++){
					indicex=indicet+kk;
					fprintf(out,"----------> Neigh[%d][%ld]=%d\n",j,indicex,RMSD_neigh[indicex]);fflush(out);
				}
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
	}
	
	

	 fprintf(out,"native 2329\n");
	fflush(out);
	fp=fopen("native2","r");
	linea=0;
	if ( fp == NULL) {
		/*printf ("File native2 not found\n");
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);*/
	}else{
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		fgets(line,BUFFER_CHAR*sizeof(char),fp);
		while(!feof(fp)){
			fgets(line,BUFFER_CHAR*sizeof(char),fp);
			p2=sscanf (line,"%*d%*d%d%d%d%d%lf\n",&pr,&i,&prt,&j,&x);
			
			/*fprintf(out,"native %d\n",linea);
			fflush(out);
			fprintf(out,"native %d %d %d %d %lf\n",pr,i,prt,j,x);
			fflush(out);
			if(pr>-1) fprintf(out,"native resA %d resB %d\n",prot[pr].ammino[i].residue,prot[prt].ammino[j].residue);			
			fflush(out);
			if(pr>-1) fprintf(out,"native contact %d \n",contact[pr][i][prt][j]);
			fflush(out);
			
			fprintf(out,"native %d %d\n",NPROT,ProtN);
			fflush(out);*/
			linea++;
			if(pr>-1){
				
				if(pr==prt){
					if(( j!= i+5)&&(j != i)&&(j != i-5)&&(j != i-2*5)&&(j != i+2*5)){
						r=sqrt(x);						
						E2+=Potential(r,pr,i,prt,j,prot[pr].ammino[i].residue,prot[prt].ammino[j].residue)/2.0;					
						
					}
				}else{

					r=sqrt(x);						
						E2+=Potential(r,pr,i,prt,j,prot[pr].ammino[i].residue,prot[prt].ammino[j].residue)/2.0;					
						
				}
			}
			/*fprintf(out,"OK3\n");
			fflush(out);*/
		}		
		fclose(fp);	
	}
	native2=0;
	
	 fprintf(out,"native 2422\n");
	fflush(out);
	native1=0;
	
	for(i=0;i<ProtN;i++){
			
		for(j=0;j<ProtN;j++){
					
			if(( j!= i+NATOM)&&(j != i)&&(j != i-NATOM)&&(j != i-2*NATOM)&&(j != i+2*NATOM)){
				if(DistMap1[i][j]>0) native1++;

			}
					
			
		}
	}
	
	/*for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			for(prt=0;prt<NPROT;prt++){
				for(j=0;j<Plength[prt];j++){
					if(icycl > 0) fprintf(out,"%d ",contact[pr][i][prt][j]);
				}
			}
			if(icycl > 0) fprintf(out,"\n");
		}
	}*/
	
	fprintf(out,"native 2433\n");
	fflush(out);
	native1/=2;	
	native2/=2;	
	if(my_rank==0){
		fp=fopen("SimPar","a");
		fprintf(fp,"Native structure1\n");
		fprintf(fp,"Enative=%lf\n",E1);
		fprintf(fp,"Order=%d\n",native1);
		fprintf(fp,"Native structure 2\n");
		fprintf(fp,"Enative=%lf\n",E2);
		fprintf(fp,"Order=%d\n",native2);
		fclose(fp);
	}
	
	if(E1>=E2){
		Enative=E2;
	}else{
		Enative=E1;
	}
	
}

void fastadecoder (char **Dec, char **Enc){
	int pr,i;
	
	for(pr=0;pr<NPROT;pr++){
		
		for(i=0;i<Plength[pr]/NATOM;i++){
			switch (Enc[pr][i]) {
				case 'A':
				Dec[pr][i]=1;
				break;
				case 'C':
				Dec[pr][i]=2;
				break;
				case 'D':
				Dec[pr][i]=3;
				break;
				case 'E':
				Dec[pr][i]=4;
				break;
				case 'F':
				Dec[pr][i]=5;
				break;
				case 'G':
				Dec[pr][i]=6;
				break;
				case 'H':
				Dec[pr][i]=7;
				break;
				case 'I':
				Dec[pr][i]=8;
				break;
				case 'K':
				Dec[pr][i]=9;
				break;
				case 'L':
				Dec[pr][i]=10;
				break;
				case 'M':
				Dec[pr][i]=11;
				break;
				case 'N':
				Dec[pr][i]=12;
				break;
				case 'P':
				Dec[pr][i]=13;
				break;
				case 'Q':
				Dec[pr][i]=14;
				break;
				case 'R':
				Dec[pr][i]=15;
				break;
				case 'S':
				Dec[pr][i]=16;
				break;
				case 'T':
				Dec[pr][i]=17;
				break;
				case 'V':
				Dec[pr][i]=18;
				break;
				case 'W':
				Dec[pr][i]=19;
				break;
				case 'Y':
				Dec[pr][i]=20;
				break;
				case 'X':
				Dec[pr][i]=1;
				break;
				default:
				fprintf(out,"errore nel codice della Senquenza Residuo[%d]=%c unkwon\n",i,Enc[pr][i]);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			} 
		}
	}
}

void fastadecoder2 (char *Dec, char *Enc){
	int i;
	
	for(i=0;i<N;i++){
		switch (Enc[i]) {
	case 'A':
	Dec[i]=1;
	break;
	case 'C':
	Dec[i]=2;
	break;
	case 'D':
	Dec[i]=3;
	break;
	case 'E':
	Dec[i]=4;
	break;
	case 'F':
	Dec[i]=5;
	break;
	case 'G':
	Dec[i]=6;
	break;
	case 'H':
	Dec[i]=7;
	break;
	case 'I':
	Dec[i]=8;
	break;
	case 'K':
	Dec[i]=9;
	break;
	case 'L':
	Dec[i]=10;
	break;
	case 'M':
	Dec[i]=11;
	break;
	case 'N':
	Dec[i]=12;
	break;
	case 'P':
	Dec[i]=13;
	break;
	case 'Q':
	Dec[i]=14;
	break;
	case 'R':
	Dec[i]=15;
	break;
	case 'S':
	Dec[i]=16;
	break;
	case 'T':
	Dec[i]=17;
	break;
	case 'V':
	Dec[i]=18;
	break;
	case 'W':
	Dec[i]=19;
	break;
	case 'Y':
	Dec[i]=20;
	break;
	case 'X':
	Dec[i]=1;
	break;
	default:
	if(icycl > 0) fprintf(out,"errore nel codice della Senquenza Residuo[%d]=%c unkwon\n",i,Enc[i]);
	fflush(out);
	MPI_Abort(MPI_COMM_WORLD,err5);
		} 
	}
}
void Tswap (void){
	
	/************************************************
	* Tswap --Swap the states at different 	*
	*		temperatures		*
	* 						*
	* Parameters: 				*
	*	prot -- is a proteina type structure	*
	*		where is stored the protein 	*
	*						*
	* Return:					*
	*	void				*
	*						*
	*************************************************/
	
	int i,j,kk;
	static int kkt=0;
	int tag = 0 ;
	int position=0;
	int rr,a,b,my_indice,j_indice;
	
	
	double acc=0.0,DW=0.0,rrrr,espo,espo1;
	
	double DE=0.0;
	
	
	
	
	static double *sendord=NULL;
	static double *recvord=NULL;
	
	MPI_Status status;
	
	
	
	if(sendord==NULL) sendord=(double *)calloc((p*7),sizeof(double));
	if(recvord==NULL) recvord=(double *)calloc((p*7),sizeof(double));
	
	
	
	//fprintf(out,"Tswap settingup try\n");fflush(out);
	for (i=0;i<p*(7);i++){		
		sendord[i]=0.0;
		recvord[i]=0.0;
	}
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
	if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	sist->RMSD=lround(RMSD_bin*sqrt(sist->order/TotRMSDNeigh));
	if(sist->RMSD>=maxO) sist->RMSD=maxO-1;
	sendord[my_rank*7]=(double)(sist->RMSD);
	sendord[my_rank*7+1]=(double)(sist->Mean_H_Bonds);
	
	if(sist->Mean_H_Bonds>0) sendord[my_rank*7+2]=1;
	sendord[my_rank*7+3]=sist->E;
	sendord[my_rank*7+4]=sist->density;	
	sendord[my_rank*7+5]=(double)(betaindice);
	sendord[betaindice*7+6]=(double)(my_rank);
	//fprintf(out,"ALL Reduce try\n");fflush(out);
	MPI_Allreduce(sendord,recvord,p*7,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	
	//fprintf(out,"Tswap Sampling try\n");fflush(out);
	my_indice=my_rank*7;
	
	for (j=0;j<p;j++){
		j_indice=j*7;
		if(j!=my_rank){
			
			DW=		Wpot[betaindice][(int)(recvord[j_indice+1])][(int)(recvord[j_indice])]-
			Wpot[betaindice][(int)(recvord[my_indice+1])][(int)(recvord[my_indice])]+
			Wpot[(int)(recvord[j_indice+5])][(int)(recvord[my_indice+1])][(int)(recvord[my_indice])]-
			Wpot[(int)(recvord[j_indice+5])][(int)(recvord[j_indice+1])][(int)(recvord[j_indice])];
			
			espo=(beta[betaindice]-beta[(int)(recvord[j_indice+5])])*(recvord[my_indice+3]-recvord[j_indice+3])+DW;
					
					
			if(espo<50.0){
				espo1=-log(1+exp(espo));
			}else{
				espo1=-espo;
			}
			
			
			if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
			if(icycl > 0) fprintf(out,"Cavolo TSWAP 1  %llu %lf %d %d espo=%lf\n",icycl,1/beta[betaindice],betaindice,(int)(recvord[j_indice+5]),espo);
			fprintf(out,"%d %d %d\n",j_indice,(int)(recvord[j_indice]),(int)(recvord[j_indice+1]));
			fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",recvord[my_indice+3],recvord[j_indice+3],DW);
			fprintf(out,"(int)(recvord[j_indice+1])=%d\n",(int)(recvord[j_indice+1]));
			fprintf(out,"(int)(recvord[j_indice])=%d\n",(int)(recvord[j_indice]));
			fprintf(out,"(int)(recvord[my_indice+1])=%d\n",(int)(recvord[my_indice+1]));
			fprintf(out,"(int)(recvord[my_indice+1])=%d\n",(int)(recvord[my_indice+1]));
			
			fprintf(out,"Simulation Time %lf (seconds)\n",MPI_Wtime()-starttime);
	
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				
			//fprintf(out,"TSWAP non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,espo1);fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",recvord[my_indice+3],recvord[j_indice+3],DW);fflush(out);
			sampling(recvord[j_indice+4],recvord[j_indice+3],(int)(recvord[j_indice]),(int)(recvord[j_indice+2]),(int)(recvord[j_indice+1]),espo+espo1);
			
			sampling(recvord[my_indice+4],recvord[my_indice+3],(int)(recvord[my_indice]),(int)(recvord[my_indice+2]),(int)(recvord[my_indice+1]),espo1);	
		}
	}

	
	
	
	
	//fprintf(out,"Tswap Sampling ok %llu\n",icycl);fflush(out);
	
	
	rr=(int)(ran35(&seed2)*2);
	rrrr=ran37(&seed3);
	
	
	for (i=rr;i<p-1;i+=2){
		a=(int)(recvord[(i)*7+6]);
		b=(int)(recvord[(i+1)*7+6]);
		
		if (my_rank==a){
			
			
			
			DE=recvord[(b)*7+3]-recvord[(a)*7+3];
			
			DW=		Wpot[i][(int)(recvord[b*7+1])][(int)(recvord[b*7])]-
			Wpot[i][(int)(recvord[a*7+1])][(int)(recvord[a*7])]+
			Wpot[i+1][(int)(recvord[a*7+1])][(int)(recvord[a*7])]-
			Wpot[i+1][(int)(recvord[b*7+1])][(int)(recvord[b*7])];
			if(betaindice!=i){
				if(icycl > 0) fprintf(out,"cavolo\n");
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			acc=exp((beta[i+1]-beta[i])*(DE)+DW);
			//acc=acc/(1+acc);
			//if(icycl > 0) fprintf(out,"Tswap  %llu %lf %lf %lf %lf %d %d %d\n",icycl,DE,acc,rrrr,DW,a,b,my_rank);
			ntempswap[betaindice]++;
			if (rrrr<acc){
				temthetasto[betaindice]++;
				
				
				/*sist->order=	(int)(recvord[b*7]);
				sist->contact=	(int)(recvord[b*7+1]);
				sist->touch=	(int)(recvord[b*7+2]);
				sist->E=		recvord[b*7+3];
				sist->density=	recvord[b*7+4];*/
				betaindice=		(int)(recvord[b*7+5]);
				
				
				
			}
		}
		
		
		if (my_rank==b){
			
			
			
			if(betaindice!=i+1){
				if(icycl > 0) fprintf(out,"cavolo\n");
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			DE=recvord[(b)*7+3]-recvord[(a)*7+3];
			
			
			
			DW=		Wpot[i][(int)(recvord[b*7+1])][(int)(recvord[b*7])]-
			Wpot[i][(int)(recvord[a*7+1])][(int)(recvord[a*7])]+
			Wpot[i+1][(int)(recvord[a*7+1])][(int)(recvord[a*7])]-
			Wpot[i+1][(int)(recvord[b*7+1])][(int)(recvord[b*7])];
			
			
			
			
			acc=exp((beta[i+1]-beta[i])*(DE)+DW);
			//acc=acc/(1+acc);
			//if(icycl > 0) fprintf(out,"Tswap  %llu %lf %lf %lf %lf %d %d %d\n",icycl,DE,acc,rrrr,DW,a,b,my_rank);
			ntempswap[betaindice]++;
			if (rrrr<acc){
				temthetasto[betaindice]++;
				
				
				
				/*sist->order=	(int)(recvord[a*7]);
				sist->contact=	(int)(recvord[a*7+1]);
				sist->touch=	(int)(recvord[a*7+2]);
				sist->E=		recvord[a*7+3];
				sist->density=	recvord[a*7+4];*/
				betaindice=		(int)(recvord[a*7+5]);
				
				
			}	
		}	 
	}
	
	/***********TEST***********/
	/*if((sist->touch==0)||(sist->touch==1)){
	}else{
		if(icycl > 0) fprintf(out,"Tswap non ok icycl=%llu, touch=%d\n",icycl,sist->touch);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((sist->order<minO)){
		
		if(icycl > 0) fprintf(out,"Tswap non ok icycl=%llu, order=%d %d %d\n",icycl,sist->order,minO,maxO);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST***********/
}


void W (void){
	int i,j,k;
	char message[300];
	/*int minWimean=0,minWjmean=0;*/
	FILE *fp;
	
	static unsigned long long int iter=0;
	
	static double *rWhisto=NULL,*minW=NULL,*minWmean=NULL;
	
	
	if(minW==NULL) minW=(double *)calloc(p,sizeof(double));
	if(minWmean==NULL) minWmean=(double *)calloc(p,sizeof(double));
	if(rWhisto==NULL) rWhisto=(double *)calloc((p*sizeC*sizex),sizeof(double));
	
	
	
	iter++;
	for(k=0;k<p;k++){
		minW[k]=1e10;
		minWmean[k]=1e10;
	}
	for(i=0;i<p*sizeC*sizex;i++){
		
		rWhisto[i]=-1000.0;
		
	}
	fprintf(out,"W MPI_Allreduce try\n");fflush(out);
	MPI_Allreduce(Whisto,rWhisto,p*sizeC*sizex,MPI_DOUBLE,myExpSum,MPI_COMM_WORLD);
	fprintf(out,"W MPI_Allreduce try OK\n");fflush(out);
	for(i=0;i<p*sizeC*sizex;i++){
		
		Whisto[i]=rWhisto[i];
		if(isnan(Whisto[i])!=0){
			if(icycl > 0) fprintf(out,"Cavolo %llu %lf Whisto[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto[i]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
	}
	
	for(k=0;k<p;k++){
		for(i=0;i<sizeC;i++){
			for(j=0;j<sizex;j++){
				Wpot[k][i][j]-=umbrella*Whisto[k*sizeC*sizex+i*sizex+j];
				if(Wpot[k][i][j]<minW[k]) minW[k]=Wpot[k][i][j];
				
			}
		}
		
		for(i=0;i<sizeC;i++){
			for(j=0;j<sizex;j++){
				Wpot[k][i][j]=Wpot[k][i][j]-minW[k];
				if(Wpot[k][i][j]>1e9){
					if(icycl > 0) fprintf(out,"Cavolo Wpot[%d][%d][%d]=%lf\n",k,i,j,Wpot[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				if(isnan(Wpot[k][i][j])!=0){
					if(icycl > 0) fprintf(out,"Cavolo %llu %lf Whisto[%d]=%lf\n",icycl,1/beta[betaindice],i,Whisto[i]);
					if(icycl > 0) fprintf(out,"Cavolo Wpot[%d][%d][%d]=%lf\n",k,i,j,Wpot[k][i][j]);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				
			}
		}
	}
	
	if(my_rank==0){
		for(k=0;k<p;k++){
		sprintf(message,"Histogram_Ord-T-%2.3f-O-%d-%d.dat",1/beta[k],Lattice,process);
		fHisto_Ord=fopen(message,"w");
		for(i=0;i<sizeC;i++){
			for(j=0;j<sizex;j++){

				fprintf(fHisto_Ord,"%5llu %5d %10.5lf %15.10f\n",iter,i,j/RMSD_bin,Wpot[k][i][j]);
			} 
			fprintf(fHisto_Ord,"\n");
		}

		fprintf(fHisto_Ord,"# icycl=%10llu %lf (seconds)\n",icycl,MPI_Wtime()-starttime);
		fprintf(fHisto_Ord,"\n\n\n");
		fclose(fHisto_Ord);
	
	sprintf(message,"Wpot-T-%2.3f.dat",1/beta[k]);
	fp=fopen(message,"w");
	fwrite(&sizeC,sizeof(int),1,fp);
	fwrite(&sizex,sizeof(int),1,fp);
	fwrite(&IminO,sizeof(int),1,fp);
	for(i=0;i<sizeC;i++){
		for(j=0;j<sizex;j++){

			bin_x=Wpot[k][i][j];
			fwrite(&bin_x,sizeof(double),1,fp);

		}

	}
	fclose(fp);
	}
	}
}
void Minimum_Energy (double E,int order,int contact){
	int i,k;
	
	if(MinE>=E) {
		MinE=E;
	 	MinEO=order;
		MinEH=contact;
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				minprot[i].ammino[k].x=prot[i].ammino[k].x;
				minprot[i].ammino[k].y=prot[i].ammino[k].y;
				minprot[i].ammino[k].z=prot[i].ammino[k].z;
			}
		}
	}
	
	if(MinO>=order) {
		MinOE=E;
	 	MinO=order;
		MinOH=contact;
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				minOprot[i].ammino[k].x=prot[i].ammino[k].x;
				minOprot[i].ammino[k].y=prot[i].ammino[k].y;
				minOprot[i].ammino[k].z=prot[i].ammino[k].z;
			}
		}
	}
	
}

void Order_Boudaries (double E,int order,int contact){
	int i,k;
	
	if(order==ImaxO) {
		
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				maxOprot[i].ammino[k].x=prot[i].ammino[k].x;
				maxOprot[i].ammino[k].y=prot[i].ammino[k].y;
				maxOprot[i].ammino[k].z=prot[i].ammino[k].z;
			}
		}
	}
	if(order==IminO) {
		
		for(i=0;i<NPROT;i++){
			for (k=0;k<Plength[i];k++){
				minOprot[i].ammino[k].x=prot[i].ammino[k].x;
				minOprot[i].ammino[k].y=prot[i].ammino[k].y;
				minOprot[i].ammino[k].z=prot[i].ammino[k].z;
			}
		}
	}
	
}


void sampling (double density, double E,int order,int touch,int contact,double espo){
	
	unsigned long int indice=0;
	long int indice2=0;
if((contact<0)){
			
			fprintf(out,"sampling non ok contact<0 icycl=%llu, contact=%d \n",icycl,contact);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}		
	
if(contact<maxH){
	if((order<maxO)&&(order>minO)){
		/***********TEST***********/
		if((touch==0)||(touch==1)){
		}else{
			fprintf(out,"sampling non ok 5 icycl=%llu, touch=%d sist->touch=%d\n",icycl,touch,sist->touch);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		if((order<minO)){
			
			fprintf(out,"sampling non ok 6 icycl=%llu, order=%d %d %d\n",icycl,order,minO,maxO);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/***********TEST***********/
		
		indice=betaindice*sizeC*sizex+contact*sizex+(order);
		//fprintf(out,"Whisto[%d]=%lf\n",indice,Whisto[indice]);
		/***********TEST***********/
		if(indice>=p*sizeC*sizex){
			fprintf(out,"sampling non ok 1 indice=%lu p*sizeC*sizex=%d betaindice=%d sizex=%d sizeC=%d order=%d IminO=%d\n",indice,
			p*sizeC*sizex,betaindice,sizex,sizeC,order,IminO);
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/***********TEST***********/
		
		if(espo>=Whisto[indice]){
			
			Whisto[indice]=espo+log(1+exp(Whisto[indice]-espo));
			
		}else{
			Whisto[indice]=Whisto[indice]+log(1+exp(espo-Whisto[indice]));
		}
		if(isnan(Whisto[indice])!=0){
			if(icycl > 0) fprintf(out,"Cavolo sampling %llu %lf Whisto[%lu]=%lf\n",icycl,1/beta[betaindice],indice,Whisto[indice]);
			fprintf(out,"%lf %d %d %d %d\n",espo,order,contact,ImaxO,IminO);			
			fprintf(out,"Simulation Time %lf (seconds)\n",MPI_Wtime()-starttime);	
			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		/*if(icycl > 0) fprintf(out,"filling Whisto ok\n");*/
		//fprintf(out,"Whisto[%d]=%lf\n",indice,Whisto[indice]);
		
		
		
		
		
		
		if((icycl<Equi1)||(icycl>Nhistogram*(Equi2+Equi3)+Equi1)){	
			espo=espo-Wpot[betaindice][contact][(order)];
			
			if(espo>=ndataen2[betaindice]){
				ndataen2[betaindice]=espo+log(1+exp(ndataen2[betaindice]-espo));
			}else{
				ndataen2[betaindice]=ndataen2[betaindice]+log(1+exp(espo-ndataen2[betaindice]));
			}
			
			if(density>0){
				
				if(espo>=Density[betaindice*3+1]){
					Density[betaindice*3+1]=espo+log(1+exp(Density[betaindice*3+1]-espo));
				}else{
					Density[betaindice*3+1]=Density[betaindice*3+1]+log(1+exp(espo-Density[betaindice*3+1]));
				} 
				
			}else{
				if(touch>0){	
					if(espo>=Density[betaindice*3]){
						Density[betaindice*3]=espo+log(1+exp(Density[betaindice*3]-espo));
					}else{
						Density[betaindice*3]=Density[betaindice*3]+log(1+exp(espo-Density[betaindice*3]));
					}
					
				}else{
					if(espo>=Density[betaindice*3+2]){
						Density[betaindice*3+2]=espo+log(1+exp(Density[betaindice*3+2]-espo));
					}else{
						Density[betaindice*3+2]=Density[betaindice*3+2]+log(1+exp(espo-Density[betaindice*3+2]));
					}
					
				}
			}
			
			indice=betaindice*2*sizex+touch*sizex+(order);
			
			/***********TEST***********/
			if(indice>=p*2*sizex){
				fprintf(out,"sampling non ok 2 indice=%lu p*2*sizex=%d betaindice=%d sizex=%d touch=%d order=%d minO=%d\n",indice,
				p*2*sizex,betaindice,sizex,touch,order,IminO);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			/***********TEST***********/
			
			if(espo>=histofill[indice]){
				histofill[indice]=espo+log(1+exp(histofill[indice]-espo));
			}else{
				histofill[indice]=histofill[indice]+log(1+exp(espo-histofill[indice]));
			} 
			/************histo energy ***********************/
			indice2=( long int)( (E-Min_energy)*bin_energy );
				
			if (indice2<0) indice2=0;
		
			if (indice2>=sizeEnergy) indice2=sizeEnergy-1;
		
			indice2=betaindice*sizeEnergy + indice2;
		
			///printf("\nbetaindice = %d\t\t SizeEnergy = %d\t\tindice = %ld\n\n",betaindice,sizeEnergy,indice); fflush(NULL);
		
			if(indice2>=(unsigned int)(p*sizeEnergy)){
			  fprintf(out,"sampling ENERGY non ok 3 indice2=%lu p*sizeEnergy=%d betaindice=%d sizeEnergy=%d partint((E*bin_energy)-minE))=%d\n",
				  indice2,p*sizeEnergy,betaindice,sizeEnergy,partint( E*bin_energy-minE) );
			  fflush(out);
			  MPI_Abort(MPI_COMM_WORLD,err5);
			}
			if(espo>=histo_energy[indice2]){
			  histo_energy[indice2]=espo+log(1+exp(histo_energy[indice2]-espo));
			}else{
			  histo_energy[indice2]=histo_energy[indice2]+log(1+exp(espo-histo_energy[indice2]));
			}
			


			/*******************************/
			
			indice=partint(E+500.0);
			if((indice>0)&&(indice<1000)){
				indice=betaindice*2*sizex*1000+touch*sizex*1000+(order)*1000+indice;
				
				/***********TEST***********/
				if(indice>=p*2*sizex*1000){
					fprintf(out,"sampling non ok 3 indice=%lu p*2*sizex*1000=%d betaindice=%d sizex=%d touch=%d order=%d minO=%d partint((E+500))=%d\n",indice,
					p*2*sizex*1000,betaindice,sizex,touch,order,IminO,partint((E+500)));
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				/***********TEST***********/
				
				if(espo>=histo[indice]){
					histo[indice]=espo+log(1+exp(histo[indice]-espo));
				}else{
					histo[indice]=histo[indice]+log(1+exp(espo-histo[indice]));
				}
			}
			
			indice=betaindice*sizeC*sizex+contact*sizex+(order);
			
			/***********TEST***********/
			if(indice>=p*sizeC*sizex){
				fprintf(out,"sampling non ok 4 indice=%lu p*sizeC*sizex=%d betaindice=%d sizex=%d sizeC=%d order=%d IminO=%d\n",indice,
				p*sizeC*sizex,betaindice,sizex,sizeC,order,IminO);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			/***********TEST***********/			
			if(espo>=histoC[indice]){
				histoC[indice]=espo+log(1+exp(histoC[indice]-espo));
			}else{
				histoC[indice]=histoC[indice]+log(1+exp(espo-histoC[indice]));
			}
			
			
		}
	}
}
	/*if(icycl > 0) fprintf(out,"filling histo ok\n");*/
}


/*---------------------------*/ 


int MC_brot_local (int pr){
	double dx,dy,dz;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	int self_flag=0;
	//static int **vicini=NULL,*nvicini=NULL;
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX,RotY,RotZ,modulo;
	int i,k=0,test=0,test1,kk,j,ii,jj,jjj,pprt;
	unsigned int indice,indice2,indice3;
	int l,lll;
	int kkk,flag=1,kk_end;
	test=test1=0;
	double angle1,angle2,random1,random2;
	double ux,uy,uz;
	double vx,vy,vz;
	double rr;
	int icel_x,icel_y,icel_z;
	double testO,testOP;
	int testC;
	double testE;
	int NewRMSD,New_Mean_H_Bonds;
	FILE *fp;
	
	kk=(int)(ran3(&seed)*(Plength[pr]/NATOM));
	if(kk>Plength[pr]/NATOM-4){
		MC_Pivot_fw(pr,kk);
		fw_tried++;
		return(0);
	}
	if(kk<3){
		MC_Pivot_bw(pr,kk);
		bw_tried++;
		return(0);
	}
	local_tried++;
	if(ran3(&seed)<0.5){
		rr=ran3(&seed);
		kk_end=(int)(rr*(Plength[pr]/NATOM-kk-4))+3+kk;
		
		if(kk_end*NATOM>=(Plength[pr]-1)){
			fprintf(out,"Local kk_end kk_end=%u kk=%d rr=%lf Plength[pr]/NATOM-kk=%d\n",kk_end,kk,rr,Plength[pr]/NATOM-kk-4);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
	}else{
	  	kk_end=3+kk;
  	}	
	
	kk=kk*NATOM;
	kk_end=kk_end*NATOM;
	
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"CAZOOOOOOO kk=%d %d %d\n",kk,prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(prot[pr].ammino[kk_end].id!=ATOM_CA){
		fprintf(out,"CAZOOOOOOO kk_end=%d %d %d\n",kk_end,prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((kk>=Plength[pr]-1)||(kk<0)){
		fprintf(out,"KK out of range MC_Pivot_fw icycl %llu kk %d\n",icycl,kk);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));

	//Rotation is from last patch of Particle kk to the first patch of particle kk_end if spring_model=1
	
	if(SPRING_MODEL==1){
	  dx=prot[pr].ammino[kk_end+1].x-prot[pr].ammino[kk+SPG_PATCH2].x;  /// SPG_PATCH2 =NSPOTS
	  dy=prot[pr].ammino[kk_end+1].y-prot[pr].ammino[kk+SPG_PATCH2].y;
	  dz=prot[pr].ammino[kk_end+1].z-prot[pr].ammino[kk+SPG_PATCH2].z;
	}else{
	  dx=prot[pr].ammino[kk_end].x-prot[pr].ammino[kk].x;  /// SPG_PATCH2 =NSPOTS
	  dy=prot[pr].ammino[kk_end].y-prot[pr].ammino[kk].y;
	  dz=prot[pr].ammino[kk_end].z-prot[pr].ammino[kk].z;
	}
	
	//dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	RotX=dx;
	RotY=dy;
	RotZ=dz;
	/*RotX=dx/sist->Bbond[1];
	RotY=dy/sist->Bbond[1];
	RotZ=dz/sist->Bbond[1];*/
	/*modulo=sqrt(dx*dx+dy*dy+dz*dz);
	RotX=dx/modulo;
	RotY=dy/modulo;
	RotZ=dz/modulo;
	modulo=sqrt(RotX*RotX+RotY*RotY+RotZ*RotZ);
	if(fabs(modulo-1.000000)>1e-17){
		fprintf(out,"FW Module %llu %30.20lf %d\n",icycl,modulo,kk);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/**/
	//if(ran3(&seed)<0.8){
		alpha=(PI*ran3(&seed)/5.0)-PI/10.0;
	//}else{
		//alpha=(PI*ran3(&seed))-PI/2.0;
	//}
	//alpha=PI/4.0;
	//alpha=(2*ran3(&seed))-1.0;
	
	
	/*for(k=0;k<Plength[pr];k++){
		oldX1[k]=prot[pr].ammino[k].x;
		oldY1[k]=prot[pr].ammino[k].y;
		oldZ1[k]=prot[pr].ammino[k].z;
	}*/
	/*for(k=0;k<Plength[pr];k++){
		nvicini[k]=prot[pr].ammino[k].Nverl;
	for(j=0;j<prot[pr].ammino[k].Nverl;j++){
		vicini[k][j]=prot[pr].ammino[k].verli[j];
	}
}*/
		/*************OLD STATE ENERGY****************/
	for(kkk=kk+NATOM;kkk<kk_end;kkk++){

		indice=kkk;	
		if(indice>=(Plength[pr]-1)){
			fprintf(out,"Local Eold indice=%u kk=%d kkk=%d\n",indice,kk,kkk);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
		oldX[indice]=prot[pr].ammino[indice].x;
		oldY[indice]=prot[pr].ammino[indice].y;
		oldZ[indice]=prot[pr].ammino[indice].z;

		Eold+=energy_SP_brot_local(pr,indice,kk+SPG_PATCH2,kk_end);
		//order_SP(pr,indice);
		//if(icycl > 0) fprintf(out,"Eold %lf\n",Eold);


		Oold+=sist->norder;
		Cold+=sist->ncontact;
		
			
			
		
	}	
	
	
	
	
	
	/********************************************************/
	//fprintf(out,"MC_brot_local A OK %llu\n",icycl);fflush(out);
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"kk %d angle %lf\n",kk,alpha*360.0/PI);
	//fflush(out);
	
	Eold+=Bond_Energy_bw(pr,kk+NATOM);
	Eold+=Bond_Energy_fw(pr,kk_end-NATOM);
	/*for(k=0;k<NPROT;k++){
		for(kkk=ATOM_CA;kkk<Plength[k];kkk+=NATOM){
			fprintf(out,"MC_brot_local %llu Bending-Old[%d][%d]=%15.10lf\n",icycl,k,kkk,Bond_Energy_fw(k,kkk));fflush(out);
		}	
	}*/
		
	//fprintf(out,"MC_brot_local B OK %llu\n",icycl);fflush(out);		
	//sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
// if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->Mean_H_Bonds);fflush(out);}
	/*for(k=kk-1;k<Plength[pr];k++){ 
		if(prot[pr].ammino[k].id==ATOM_H){
			dx=(prot[pr].ammino[k-4].x-prot[pr].ammino[k].x);
			dy=(prot[pr].ammino[k-4].y-prot[pr].ammino[k].y);
			dz=(prot[pr].ammino[k-4].z-prot[pr].ammino[k].z);
			r2=(dx*dx+ dy*dy + dz*dz);
			if(fabs(sqrt(r2)-sist->Bbond[4])>2.0e-5){
				fprintf(out,"P H-N wrong %llu %d %30.20lf \n%d %lf\n",icycl,kk,sqrt(r2),k,alpha);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
		
		
	}*/
	

	/****** Torsion Start***********/	
	for(kkk=kk+NATOM;kkk<kk_end;kkk++){

		indice=kkk;	
		
			//if(icycl>3576610){fprintf(out,"Rotation %llu %d %d\n",icycl,kk,indice);fflush(out);}
			
			
			prot[pr].ammino[indice].x-=prot[pr].ammino[kk].x;
			prot[pr].ammino[indice].y-=prot[pr].ammino[kk].y;
			prot[pr].ammino[indice].z-=prot[pr].ammino[kk].z;




			Rotation(pr,indice,alpha,RotX,RotY,RotZ,flag);
			 flag=0;


			prot[pr].ammino[indice].x+=prot[pr].ammino[kk].x; 
			prot[pr].ammino[indice].y+=prot[pr].ammino[kk].y;
			prot[pr].ammino[indice].z+=prot[pr].ammino[kk].z;
			
			if(prot[pr].ammino[indice].id==ATOM_CA){
				updt_cell_list_SAW (pr,indice);
				if ((energy_SAW_local(pr,indice,kk+SPG_PATCH2,kk_end)>000.0)){
						
						for(lll=kk+NATOM;lll<kk_end;lll++){
							indice=lll;
							

								prot[pr].ammino[indice].x=oldX[indice];
								prot[pr].ammino[indice].y=oldY[indice]; 
								prot[pr].ammino[indice].z=oldZ[indice]; 

								updt_cell_list_SAW (pr,indice);
								updt_cell_list (pr,indice);


							
						}
						
						local_rejectedself++;
						return(1);
					}	
						
			}
			
			
			updt_cell_list (pr,indice);

			

		

	}
	
	
	
	
	
	
	/****** Torsion Over***********/
	
	#ifdef 	ORDER_TEST
		test_celllist_SAW();
		test_celllist_CA();
		test_celllist_H();
		
	#endif
	Enew=0.0;		 
	for(kkk=kk+NATOM;kkk<kk_end;kkk++){

		indice=kkk;	
		if(indice>=(Plength[pr]-1)){
			fprintf(out,"Local Enew indice=%u kk=%d kkk=%d\n",indice,kk,kkk);fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}
			Enew+=energy_SP_brot_local(pr,indice,kk+SPG_PATCH2,kk_end);
			Onew+=sist->norder;
			Cnew+=sist->ncontact;	
			
		
		
	}	
	
	
	Enew+=Bond_Energy_bw(pr,kk+NATOM);
	Enew+=Bond_Energy_fw(pr,kk_end-NATOM);
	
	
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
			
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	
	
#ifdef 	ORDER_TEST
	
	
	
	order_test(&testC,&testO,&testE);

	if(Cnew!=testC){
		fprintf(out,"CC a local %llu %d %d\n",icycl,Cnew,testC);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b local %llu %15.10lf %15.10lf min=%d max=%d\n",icycl,Onew,testO,kk,kk_end);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	
	if(fabs(Enew-testE)>1e-8){
		fprintf(out,"CC d local %llu %15.10lf %15.10lf min=%d max=%d\n",icycl,Enew,testE,kk,kk_end);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	
#endif			
	/***********TEST*************/
	
	if((Cnew<0)){		
		fprintf(out,"MC_brot local non ok icycl=%llu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"MC_brot local non ok icycl=%llu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST*************/
	
	NewRMSD=lround(RMSD_bin*sqrt(Onew/TotRMSDNeigh));
	if((NewRMSD>=maxO)) NewRMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot local New_Mean_H_Bonds negative icycl=%llu, Cnew= %d sist->contact= %d Cold= %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=maxH)) New_Mean_H_Bonds=maxH-1;
	DW=Wpot[betaindice][New_Mean_H_Bonds][NewRMSD]-Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD];
	
	//fprintf(out,"MC_Pivot_fw C 2 OK %llu\n",icycl);fflush(out);
	
	espo=-(Enew-Eold)*beta[betaindice]+DW;
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot local non ok icycl=%llu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fprintf(out,"New_Mean_H_Bonds=%d\n",New_Mean_H_Bonds);
		fprintf(out,"NewRMSD=%d\n",NewRMSD);
		fprintf(out,"sist->Mean_H_Bonds=%d\n",sist->Mean_H_Bonds);
		fprintf(out,"sist->RMSD=%d\n",sist->RMSD);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	
	
	//fprintf(out,"MC_brot fw non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,espo1);fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);fflush(out);
		sampling(Dnew,Enew,NewRMSD,Znew,New_Mean_H_Bonds,espo+espo1);
	
		sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,espo1);
	
	
	
	//fprintf(out,"MC_Pivot_fw E OK %llu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
	
	
	
    	if(ran3(&seed)>acc){ // reject
		//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
		for(lll=kk+NATOM;lll<kk_end;lll++){
			indice=lll;
			

				prot[pr].ammino[indice].x=oldX[indice];
				prot[pr].ammino[indice].y=oldY[indice]; 
				prot[pr].ammino[indice].z=oldZ[indice]; 

				updt_cell_list_SAW (pr,indice);
				updt_cell_list (pr,indice);


			
		}
		
	
			
		/*for(ii=0;ii<Plength[pr];ii++){
			
			if(fabs(oldX1[ii]-prot[pr].ammino[ii].x)>1e-10){ fprintf(out,"Update fw X %llu %d %d %lf %lf\n",icycl,kk,ii,oldX1[ii],prot[pr].ammino[ii].x);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldY1[ii]-prot[pr].ammino[ii].y)>1e-10){ fprintf(out,"Update fw Y %llu %d %d %lf %lf\n",icycl,kk,ii,oldY1[ii],prot[pr].ammino[ii].y);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldZ1[ii]-prot[pr].ammino[ii].z)>1e-10){ fprintf(out,"Update fw Z %llu %d %d %lf %lf\n",icycl,kk,ii,oldZ1[ii],prot[pr].ammino[ii].z);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
		}*/
		
		/*for(k=0;k<Plength[pr];k++){
			test=0;
			if(prot[pr].ammino[k].Nverl!=nvicini[k]){fprintf(out,"Wrong number of neigh %llu %d %d %d\n",icycl,k,prot[pr].ammino[k].Nverl,nvicini[k]);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			for(j=0;j<prot[pr].ammino[k].Nverl;j++){
				
				for(jj=0;jj<nvicini[k];jj++){
					if(vicini[k][jj]==prot[pr].ammino[k].verli[j]) test++;
				}
					
			}		
					
			if(test!=prot[pr].ammino[k].Nverl){
				fprintf(out,"Update fw Verl %llu %d %d\n",icycl,test,prot[pr].ammino[k].Nverl);
				for(j=0;j<prot[pr].ammino[k].Nverl;j++){
					fprintf(out,"vicini[%d][%d]=%d %d\n",k,j,vicini[k][j],prot[pr].ammino[k].verli[j]);
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				}
			
		}*/
		
		
		
		local_rejectedener++;
		return(0);   
	}
	
	
	
	
	
	
	
	

	
	
	
	
	//fprintf(out,"MC_Pivot_fw G OK %llu\n",icycl);fflush(out);
	local_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %llu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->RMSD=NewRMSD;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	
	Minimum_Energy(sist->E,sist->RMSD,sist->contact);
	//Order_Boudaries(sist->E,sist->order,sist->contact);
	
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}
int MC_All_trasl (int pr){
	double dx,dy,dz;
	int self_flag=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	static double *oldX1=NULL,*oldY1=NULL,*oldZ1=NULL;
	//static int **vicini=NULL,*nvicini=NULL;
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX,TY,TZ,dR;
	int i,k,test=0,test1,j,ii,indice,jj;
	int l,lll;
	int kkk,flag=1;
	int selection;
	double testO,testOP;
	int testC;
	double testE;
	int NewRMSD,New_Mean_H_Bonds;
	FILE *fp;
	test=test1=0;
	
	
	
	//kk=(int)(ran3(&seed)*(Plength[pr]/NATOM));
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	if(oldX1==NULL) oldX1=(double *)calloc(ProtN,sizeof(double));
	if(oldY1==NULL) oldY1=(double *)calloc(ProtN,sizeof(double));
	if(oldZ1==NULL) oldZ1=(double *)calloc(ProtN,sizeof(double));
	
	//fprintf(out,"MC_All_trasl A OK %llu\n",icycl);fflush(out);
	
	
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	dR=ran3(&seed)*2.0;
	
	TX=sin(phi)*cos(theta)*dR;
	TY=sin(phi)*sin(theta)*dR;
	TZ=cos(phi)*dR;
	
	
	
	for(k=0;k<Plength[pr];k++){ 
				
					Eold+=energy_SP_Pivot_fw(pr,k);
					Oold+=sist->norder;
					Cold+=sist->ncontact;
				
	}
		//fprintf(out,"MC_All_trasl %llu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	//fprintf(out,"MC_All_trasl A OK %llu\n",icycl);fflush(out);
	for(k=0;k<Plength[pr];k++){ 
		
				oldX[k]=prot[pr].ammino[k].x;
				oldY[k]=prot[pr].ammino[k].y;
				oldZ[k]=prot[pr].ammino[k].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"MC_All_trasl B OK %llu\n",icycl);fflush(out);
	for(k=0;k<Plength[pr];k++){ 
		
			
	
				//if(icycl>0){fprintf(out,"Rotation %llu %d %d\n",icycl,kk,indice);fflush(out);}
				prot[pr].ammino[k].x+=TX;
				prot[pr].ammino[k].y+=TY;
				prot[pr].ammino[k].z+=TZ;




				
				
				if(prot[pr].ammino[k].id==ATOM_CA){
					//if(icycl>0){fprintf(out,"Update cell SAW %llu %d %d\n",icycl,kk,indice);fflush(out);}
					updt_cell_list_SAW (pr,k);
					if ((energy_SAW(pr,k)>000.0)){  // reject
						for(l=0;l<=k;l++){ 
							

							prot[pr].ammino[l].x=oldX[l];
							prot[pr].ammino[l].y=oldY[l]; 
							prot[pr].ammino[l].z=oldZ[l]; 
							updt_cell_list_SAW (pr,l);

							updt_cell_list (pr,l);


								
							
						}
						//if(icycl>0){fprintf(out,"Test cell before rejection SAW\n");fflush(out);}
						//test_celllist_SAW();
						//if(icycl>0){fprintf(out,"Test cell before rejection CA\n");fflush(out);}
						//test_celllist_CA();
						//if(icycl>0){fprintf(out,"Test cell before rejection H\n");fflush(out);}
						//test_celllist_H();
						//if(icycl>0){fprintf(out,"Test cell before rejection O\n");fflush(out);}
						//
						//if(icycl>0){fprintf(out,"Test cell before rejection OK\n");fflush(out);}
						trasl_rejectedself++;
						return(REJECTSELF);
					}
				}
				//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
				updt_cell_list (pr,k);
				//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
			//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
				
			
			
		
	
	}
	//fprintf(out,"MC_All_trasl C OK %llu\n",icycl);fflush(out);
		
#ifdef 	ORDER_TEST			
   test_celllist_SAW();
   test_celllist_CA();
   test_celllist_H();
   
#endif			
	/*if ((energy_SAW_test ()>0.0)){
			 fprintf(out,"SAW  fw %llu %d %d\n",icycl);

			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
	
	for(k=0;k<Plength[pr];k++){ 
				
					Enew+=energy_SP_Pivot_fw(pr,k);
					Onew+=sist->norder;
					Cnew+=sist->ncontact;
			
					
						
	}
	
	//	fprintf(out,"MC_brot_f %llu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
	//fprintf(out,"MC_All_trasl C OK %llu\n",icycl);fflush(out);
		
	
	
		
	
	//fprintf(out,"ok1\n");
	  
	
	
	//fprintf(out,"ok2\n");
	
	
	
	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %llu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %llu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
			
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//fprintf(out,"MC_All_trasl D OK %llu\n",icycl);fflush(out);
#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	if(Cnew!=testC){
		fprintf(out,"CC a trasl  %llu %d %d\n",icycl,Cnew,testC);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b trasl  %llu %lf %lf\n",icycl,Onew,testO);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"CC d trasl  %llu %lf %lf\n",icycl,Enew,testE);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
#endif	
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %llu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP_Pivot_fw(pr,k);
	
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %llu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"MC_brot trasl non ok icycl=%llu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"MC_brot trasl non ok icycl=%llu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/***********TEST*************/
	NewRMSD=lround(RMSD_bin*sqrt(Onew/TotRMSDNeigh));
	if((NewRMSD>=maxO)) NewRMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot trasl New_Mean_H_Bonds negative icycl=%llu, Cnew= %d sist->contact= %d Cold= %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=maxH)) New_Mean_H_Bonds=maxH-1;
	
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][NewRMSD]-Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD];
	
	//fprintf(out,"MC_All_trasl E OK %llu\n",icycl);fflush(out);
	
	espo=-(Enew-Eold)*beta[betaindice]+DW;
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot trasl non ok icycl=%llu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fprintf(out,"New_Mean_H_Bonds=%d\n",New_Mean_H_Bonds);
		fprintf(out,"NewRMSD=%d\n",NewRMSD);
		fprintf(out,"sist->Mean_H_Bonds=%d\n",sist->Mean_H_Bonds);
		fprintf(out,"sist->RMSD=%d\n",sist->RMSD);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	
	
	//fprintf(out,"MC_brot fw non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,espo1);fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);fflush(out);
		sampling(Dnew,Enew,NewRMSD,Znew,New_Mean_H_Bonds,espo+espo1);
	
		sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,espo1);
	
	
	
	//fprintf(out,"MC_All_trasl F OK %llu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
    	if(ran3(&seed)>acc){ // reject
		//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
			//fprintf(out,"MC_All_trasl Fbis OK %llu\n",icycl);fflush(out);
		for(l=0;l<Plength[pr];l++){ 							
			prot[pr].ammino[l].x=oldX[l];
			prot[pr].ammino[l].y=oldY[l]; 
			prot[pr].ammino[l].z=oldZ[l]; 
			//fprintf(out,"MC_All_trasl reject %llu l=%d\n",icycl,l);fflush(out);
			updt_cell_list_SAW (pr,l);
			updt_cell_list (pr,l);
		}
	
		
			
		/*for(ii=0;ii<Plength[pr];ii++){
			
			if(fabs(oldX1[ii]-prot[pr].ammino[ii].x)>1e-10){ fprintf(out,"Update fw X %llu %d %d %lf %lf\n",icycl,kk,ii,oldX1[ii],prot[pr].ammino[ii].x);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldY1[ii]-prot[pr].ammino[ii].y)>1e-10){ fprintf(out,"Update fw Y %llu %d %d %lf %lf\n",icycl,kk,ii,oldY1[ii],prot[pr].ammino[ii].y);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldZ1[ii]-prot[pr].ammino[ii].z)>1e-10){ fprintf(out,"Update fw Z %llu %d %d %lf %lf\n",icycl,kk,ii,oldZ1[ii],prot[pr].ammino[ii].z);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
		}*/
		
		/*for(k=0;k<Plength[pr];k++){
			test=0;
			if(prot[pr].ammino[k].Nverl!=nvicini[k]){fprintf(out,"Wrong number of neigh %llu %d %d %d\n",icycl,k,prot[pr].ammino[k].Nverl,nvicini[k]);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			for(j=0;j<prot[pr].ammino[k].Nverl;j++){
				
				for(jj=0;jj<nvicini[k];jj++){
					if(vicini[k][jj]==prot[pr].ammino[k].verli[j]) test++;
				}
					
			}		
					
			if(test!=prot[pr].ammino[k].Nverl){
				fprintf(out,"Update fw Verl %llu %d %d\n",icycl,test,prot[pr].ammino[k].Nverl);
				for(j=0;j<prot[pr].ammino[k].Nverl;j++){
					fprintf(out,"vicini[%d][%d]=%d %d\n",k,j,vicini[k][j],prot[pr].ammino[k].verli[j]);
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				}
			
		}*/
		
		
		
		trasl_rejectedener++;
		return(0);   
	}
	
	
	
	
	
	
	

	
	//fprintf(out,"MC_All_trasl G OK %llu\n",icycl);fflush(out);
	trasl_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %llu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->RMSD=NewRMSD;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
//fprintf(out,"MC_All_trasl H OK %llu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->RMSD,sist->contact);
	//Order_Boudaries(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}
int MC_SP_rot (int pr){
	int kk;
	int self_flag=0;
	double dx,dy,dz;
	double RotX,RotY,RotZ;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	static double *oldX1=NULL,*oldY1=NULL,*oldZ1=NULL;
	//static int **vicini=NULL,*nvicini=NULL;
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX,TY,TZ,dR;
	int i,k,test=0,test1,j,ii,indice,jj;
	int l,lll;
	int kkk,flag=1;
	int selection;
	double testO,testOP;
	int testC;
	double testE;
	int NewRMSD,New_Mean_H_Bonds;
	FILE *fp;
	test=test1=0;
	
	
	
	//kk=(int)(ran3(&seed)*(Plength[pr]/NATOM));
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	if(oldX1==NULL) oldX1=(double *)calloc(ProtN,sizeof(double));
	if(oldY1==NULL) oldY1=(double *)calloc(ProtN,sizeof(double));
	if(oldZ1==NULL) oldZ1=(double *)calloc(ProtN,sizeof(double));
	
	//fprintf(out,"MC_SP_rot A OK %llu\n",icycl);fflush(out);
	
	kk=(int)(ran3(&seed)*((Plength[pr]/NATOM)));
	kk*=NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"MC_SP_rot ref non CA %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/*Random Axis*/
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	alpha=(PI*ran3(&seed)/10.0)-PI/20.0;
	
	
	
	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
				
					Eold+=energy_SP(pr,indice);
					Oold+=sist->norder;
					Cold+=sist->ncontact;
				
	}
	Eold+=Bond_Energy_SP(pr,kk);
		//fprintf(out,"MC_SP_rot %llu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	//fprintf(out,"MC_SP_rot A OK %llu\n",icycl);fflush(out);
	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
		
				oldX[indice]=prot[pr].ammino[indice].x;
				oldY[indice]=prot[pr].ammino[indice].y;
				oldZ[indice]=prot[pr].ammino[indice].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"MC_SP_rot B OK %llu\n",icycl);fflush(out);
	for(k=0;k<NSPOTS;k++){ 
		
		indice=kk+k+1;
				prot[pr].ammino[indice].x-=prot[pr].ammino[kk].x;
				prot[pr].ammino[indice].y-=prot[pr].ammino[kk].y;
				prot[pr].ammino[indice].z-=prot[pr].ammino[kk].z;




				Rotation(pr,indice,alpha,RotX,RotY,RotZ,flag);
				 flag=0;


				prot[pr].ammino[indice].x+=prot[pr].ammino[kk].x;
				prot[pr].ammino[indice].y+=prot[pr].ammino[kk].y;
				prot[pr].ammino[indice].z+=prot[pr].ammino[kk].z;
			
	
				//if(icycl>0){fprintf(out,"Rotation %llu %d %d\n",icycl,kk,indice);fflush(out);}




				
				
				
				//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
				updt_cell_list (pr,indice);
				//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
			//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
				
			
			
		
	
	}
	//fprintf(out,"MC_SP_rot C OK %llu\n",icycl);fflush(out);
		
#ifdef 	ORDER_TEST			
   test_celllist_SAW();
   test_celllist_CA();
   test_celllist_H();
   
#endif			
	/*if ((energy_SAW_test ()>0.0)){
			 fprintf(out,"SAW  fw %llu %d %d\n",icycl);

			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
	
	for(k=0;k<NSPOTS;k++){ 
		indice=kk+k+1;
				
					Enew+=energy_SP(pr,indice);
					Onew+=sist->norder;
					Cnew+=sist->ncontact;
			
					
						
	}
	Enew+=Bond_Energy_SP(pr,kk);
	//fprintf(out,"MC_brot_f %llu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
	//fprintf(out,"MC_SP_rot C OK %llu\n",icycl);fflush(out);
		
	
	
		
	
	//fprintf(out,"ok1\n");
	  
	
	
	//fprintf(out,"ok2\n");
	
	
	
	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %llu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %llu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
			
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//fprintf(out,"MC_SP_rot D OK %llu\n",icycl);fflush(out);
#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	if(Cnew!=testC){
		fprintf(out,"CC a SP_rotl  %llu Cnew=%d testC=%d Cold=%d sist->contact=%d\n",icycl,Cnew,testC,Cold,sist->contact);

		fflush(out);
		/*testE=0.;
		testO=0;
		testC=0;
		for(k=0;k<Plength[pr];k++){ 
				
					testE+=energy_SP_Pivot_fw(pr,k);
					testO+=sist->norder;
					testC+=sist->ncontact;
				
		}
		fprintf(out,"from energy_SP_Pivot_fw %lf %lf %d\n",testE,testO,testC);*/
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b SP_rotl  %llu %lf %lf\n",icycl,Onew,testO);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"CC d SP_rotl  %llu %lf %lf pr=%d kk=%d\n",icycl,Enew,testE,pr,kk);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
#endif	
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %llu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP(pr,k);
	
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %llu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/
	//fprintf(out,"MC_SP_rot D OK %llu\n",icycl);fflush(out);
	if((Onew<0)){		
		fprintf(out,"MC_SP_rot non ok icycl=%llu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Cnew<0)){		
		fprintf(out,"MC_SP_rot non ok icycl=%llu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST*************/
	NewRMSD=lround(RMSD_bin*sqrt(Onew/TotRMSDNeigh));
	if((NewRMSD>=maxO)) NewRMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds negative icycl=%llu, Cnew= %d sist->contact= %d Cold= %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=maxH)) New_Mean_H_Bonds=maxH-1;
	
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][NewRMSD]-Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD];
	
	//fprintf(out,"MC_SP_rot E OK %llu\n",icycl);fflush(out);
	
	espo=-(Enew-Eold)*beta[betaindice]+DW;
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_SP_rot non ok icycl=%llu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fprintf(out,"New_Mean_H_Bonds=%d\n",New_Mean_H_Bonds);
		fprintf(out,"NewRMSD=%d\n",NewRMSD);
		fprintf(out,"sist->Mean_H_Bonds=%d\n",sist->Mean_H_Bonds);
		fprintf(out,"sist->RMSD=%d\n",sist->RMSD);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	
	
	//fprintf(out,"MC_brot fw non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,espo1);fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);fflush(out);
		sampling(Dnew,Enew,NewRMSD,Znew,New_Mean_H_Bonds,espo+espo1);
	
		sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,espo1);
	
	
	
	//fprintf(out,"MC_SP_rot F OK %llu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
    	if(ran3(&seed)>acc){ // reject
		//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
			//fprintf(out,"MC_SP_rot Fbis OK %llu\n",icycl);fflush(out);
		for(k=0;k<NSPOTS;k++){ 
			indice=kk+k+1;							
			prot[pr].ammino[indice].x=oldX[indice];
			prot[pr].ammino[indice].y=oldY[indice]; 
			prot[pr].ammino[indice].z=oldZ[indice]; 
			//fprintf(out,"MC_SP_rot reject %llu l=%d\n",icycl,l);fflush(out);
			updt_cell_list_SAW (pr,indice);
			updt_cell_list (pr,indice);
		}
	
		
			
		/*for(ii=0;ii<Plength[pr];ii++){
			
			if(fabs(oldX1[ii]-prot[pr].ammino[ii].x)>1e-10){ fprintf(out,"Update fw X %llu %d %d %lf %lf\n",icycl,kk,ii,oldX1[ii],prot[pr].ammino[ii].x);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldY1[ii]-prot[pr].ammino[ii].y)>1e-10){ fprintf(out,"Update fw Y %llu %d %d %lf %lf\n",icycl,kk,ii,oldY1[ii],prot[pr].ammino[ii].y);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldZ1[ii]-prot[pr].ammino[ii].z)>1e-10){ fprintf(out,"Update fw Z %llu %d %d %lf %lf\n",icycl,kk,ii,oldZ1[ii],prot[pr].ammino[ii].z);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
		}*/
		
		/*for(k=0;k<Plength[pr];k++){
			test=0;
			if(prot[pr].ammino[k].Nverl!=nvicini[k]){fprintf(out,"Wrong number of neigh %llu %d %d %d\n",icycl,k,prot[pr].ammino[k].Nverl,nvicini[k]);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			for(j=0;j<prot[pr].ammino[k].Nverl;j++){
				
				for(jj=0;jj<nvicini[k];jj++){
					if(vicini[k][jj]==prot[pr].ammino[k].verli[j]) test++;
				}
					
			}		
					
			if(test!=prot[pr].ammino[k].Nverl){
				fprintf(out,"Update fw Verl %llu %d %d\n",icycl,test,prot[pr].ammino[k].Nverl);
				for(j=0;j<prot[pr].ammino[k].Nverl;j++){
					fprintf(out,"vicini[%d][%d]=%d %d\n",k,j,vicini[k][j],prot[pr].ammino[k].verli[j]);
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				}
			
		}*/
		
		
		
		MC_SP_rot_rejectedener++;
		return(REJECTSELF);   
	}
	
	
	
	
	
	
	

	
	//fprintf(out,"MC_SP_rot G OK %llu\n",icycl);fflush(out);
	MC_SP_rot_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %llu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->RMSD=NewRMSD;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
//fprintf(out,"MC_SP_rot H OK %llu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->RMSD,sist->contact);
	//Order_Boudaries(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}
int MC_SP_trasl (int pr){
	double dx,dy,dz;
	int kk;
	int self_flag=0;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	static double *oldX1=NULL,*oldY1=NULL,*oldZ1=NULL;
	//static int **vicini=NULL,*nvicini=NULL;
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double TX,TY,TZ,dR;
	int i,k,test=0,test1,j,ii,indice,jj;
	int l,lll;
	int kkk,flag=1;
	int selection;
	double testO,testOP;
	int testC;
	double testE;
	int NewRMSD,New_Mean_H_Bonds;
	FILE *fp;
	test=test1=0;
	
	
	
	//kk=(int)(ran3(&seed)*(Plength[pr]/NATOM));
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	if(oldX1==NULL) oldX1=(double *)calloc(ProtN,sizeof(double));
	if(oldY1==NULL) oldY1=(double *)calloc(ProtN,sizeof(double));
	if(oldZ1==NULL) oldZ1=(double *)calloc(ProtN,sizeof(double));
	
	//fprintf(out,"MC_SP_trasl A OK %llu\n",icycl);fflush(out);
	
	
	kk=(int)(ran3(&seed)*((Plength[pr]/NATOM)));
	kk*=NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"MC_SP_trasl ref non CA %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	dR=ran3(&seed)*2.0;
	
	TX=sin(phi)*cos(theta)*dR;
	TY=sin(phi)*sin(theta)*dR;
	TZ=cos(phi)*dR;
	
	
	
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
				
					Eold+=energy_SP(pr,indice);
					Oold+=sist->norder;
					Cold+=sist->ncontact;
				
	}
	Eold+=Bond_Energy_SP(pr,kk);
		//fprintf(out,"MC_SP_trasl %llu  sist->order=%lf Oold=%lf\n",icycl,sist->order,Oold);fflush(out);
	//fprintf(out,"MC_SP_trasl A OK %llu\n",icycl);fflush(out);
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
		
				oldX[indice]=prot[pr].ammino[indice].x;
				oldY[indice]=prot[pr].ammino[indice].y;
				oldZ[indice]=prot[pr].ammino[indice].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"MC_SP_trasl B OK %llu\n",icycl);fflush(out);
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
		
			
	
				//if(icycl>0){fprintf(out,"Rotation %llu %d %d\n",icycl,kk,indice);fflush(out);}
				prot[pr].ammino[indice].x+=TX;
				prot[pr].ammino[indice].y+=TY;
				prot[pr].ammino[indice].z+=TZ;




				
				
				if(prot[pr].ammino[indice].id==ATOM_CA){
					//if(icycl>0){fprintf(out,"Update cell SAW %llu %d %d\n",icycl,kk,indice);fflush(out);}
					updt_cell_list_SAW (pr,kk);
					if ((energy_SAW_SP(pr,kk)>000.0)){  // reject
						

							prot[pr].ammino[kk].x=oldX[kk];
							prot[pr].ammino[kk].y=oldY[kk]; 
							prot[pr].ammino[kk].z=oldZ[kk]; 
							updt_cell_list_SAW (pr,kk);

							updt_cell_list (pr,kk);


								
							
						
						//if(icycl>0){fprintf(out,"Test cell before rejection SAW\n");fflush(out);}
						//test_celllist_SAW();
						//if(icycl>0){fprintf(out,"Test cell before rejection CA\n");fflush(out);}
						//test_celllist_CA();
						//if(icycl>0){fprintf(out,"Test cell before rejection H\n");fflush(out);}
						//test_celllist_H();
						//if(icycl>0){fprintf(out,"Test cell before rejection O\n");fflush(out);}
						//
						//if(icycl>0){fprintf(out,"Test cell before rejection OK\n");fflush(out);}
						trasl_rejectedself++;
						return(REJECTSELF); 
					}
				}
				//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
				updt_cell_list (pr,indice);
				//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
			//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
				
			
			
		
	
	}
	//fprintf(out,"MC_SP_trasl C OK %llu\n",icycl);fflush(out);
		
#ifdef 	ORDER_TEST			
   test_celllist_SAW();
   test_celllist_CA();
   test_celllist_H();
   
#endif			
	/*if ((energy_SAW_test ()>0.0)){
			 fprintf(out,"SAW  fw %llu %d %d\n",icycl);

			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
	
	for(k=0;k<=NSPOTS;k++){ 
		indice=kk+k;
				
					Enew+=energy_SP(pr,indice);
					Onew+=sist->norder;
					Cnew+=sist->ncontact;
			
					
						
	}
	Enew+=Bond_Energy_SP(pr,kk);
	//	fprintf(out,"MC_brot_f %llu  sist->order=%lf Onew=%lf\n",icycl,sist->order,Onew);fflush(out);
	//fprintf(out,"MC_SP_trasl C OK %llu\n",icycl);fflush(out);
		
	
	
		
	
	//fprintf(out,"ok1\n");
	  
	
	
	//fprintf(out,"ok2\n");
	
	
	
	
	
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %llu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %llu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
			
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//fprintf(out,"MC_SP_trasl D OK %llu\n",icycl);fflush(out);
#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	if(Cnew!=testC){
		fprintf(out,"CC a SP_trasl  %llu %d %d\n",icycl,Cnew,testC);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b SP_trasl  %llu %lf %lf\n",icycl,Onew,testO);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-5){
		fprintf(out,"CC d SP_trasl  %llu %lf %lf\n",icycl,Enew,testE);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
#endif	
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %llu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP(pr,k);
	
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %llu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"SP_trasl non ok icycl=%llu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"SP_trasl non ok icycl=%llu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	/***********TEST*************/
	NewRMSD=lround(RMSD_bin*sqrt(Onew/TotRMSDNeigh));
	if((NewRMSD>=maxO)) NewRMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_SP_rot New_Mean_H_Bonds negative icycl=%llu, Cnew= %d sist->contact= %d Cold= %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=maxH)) New_Mean_H_Bonds=maxH-1;
	
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][NewRMSD]-Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD];
	
	//fprintf(out,"MC_SP_trasl E OK %llu\n",icycl);fflush(out);
	
	espo=-(Enew-Eold)*beta[betaindice]+DW;
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"SP_trasl non ok icycl=%llu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fprintf(out,"New_Mean_H_Bonds=%d\n",New_Mean_H_Bonds);
		fprintf(out,"NewRMSD=%d\n",NewRMSD);
		fprintf(out,"sist->Mean_H_Bonds=%d\n",sist->Mean_H_Bonds);
		fprintf(out,"sist->RMSD=%d\n",sist->RMSD);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	
	
	//fprintf(out,"MC_brot fw non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,espo1);fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);fflush(out);
		sampling(Dnew,Enew,NewRMSD,Znew,New_Mean_H_Bonds,espo+espo1);
	
		sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,espo1);
	
	
	
	//fprintf(out,"MC_SP_trasl F OK %llu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
    	if(ran3(&seed)>acc){ // reject
		//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
			//fprintf(out,"MC_SP_trasl Fbis OK %llu\n",icycl);fflush(out);
		for(k=0;k<=NSPOTS;k++){ 
			indice=kk+k;							
			prot[pr].ammino[indice].x=oldX[indice];
			prot[pr].ammino[indice].y=oldY[indice]; 
			prot[pr].ammino[indice].z=oldZ[indice]; 
			//fprintf(out,"MC_SP_trasl reject %llu l=%d\n",icycl,l);fflush(out);
			updt_cell_list_SAW (pr,indice);
			updt_cell_list (pr,indice);
		}
	
		
			
		/*for(ii=0;ii<Plength[pr];ii++){
			
			if(fabs(oldX1[ii]-prot[pr].ammino[ii].x)>1e-10){ fprintf(out,"Update fw X %llu %d %d %lf %lf\n",icycl,kk,ii,oldX1[ii],prot[pr].ammino[ii].x);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldY1[ii]-prot[pr].ammino[ii].y)>1e-10){ fprintf(out,"Update fw Y %llu %d %d %lf %lf\n",icycl,kk,ii,oldY1[ii],prot[pr].ammino[ii].y);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldZ1[ii]-prot[pr].ammino[ii].z)>1e-10){ fprintf(out,"Update fw Z %llu %d %d %lf %lf\n",icycl,kk,ii,oldZ1[ii],prot[pr].ammino[ii].z);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
		}*/
		
		/*for(k=0;k<Plength[pr];k++){
			test=0;
			if(prot[pr].ammino[k].Nverl!=nvicini[k]){fprintf(out,"Wrong number of neigh %llu %d %d %d\n",icycl,k,prot[pr].ammino[k].Nverl,nvicini[k]);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			for(j=0;j<prot[pr].ammino[k].Nverl;j++){
				
				for(jj=0;jj<nvicini[k];jj++){
					if(vicini[k][jj]==prot[pr].ammino[k].verli[j]) test++;
				}
					
			}		
					
			if(test!=prot[pr].ammino[k].Nverl){
				fprintf(out,"Update fw Verl %llu %d %d\n",icycl,test,prot[pr].ammino[k].Nverl);
				for(j=0;j<prot[pr].ammino[k].Nverl;j++){
					fprintf(out,"vicini[%d][%d]=%d %d\n",k,j,vicini[k][j],prot[pr].ammino[k].verli[j]);
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				}
			
		}*/
		
		
		
	      
		MC_SP_trasl_rejectedener++;
		return(0);   
	}
	
	
	
	
	
	
	

	
	//fprintf(out,"MC_SP_trasl G OK %llu\n",icycl);fflush(out);
	MC_SP_trasl_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %llu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->RMSD=NewRMSD;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
//fprintf(out,"MC_SP_trasl H OK %llu\n",icycl);fflush(out);
	Minimum_Energy(sist->E,sist->RMSD,sist->contact);
	//Order_Boudaries(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
}



int MC_Pivot_fw (int pr,int kk){
	double dx,dy,dz;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	
	//static int **vicini=NULL,*nvicini=NULL;
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX,RotY,RotZ,modulo;
	int i,k,test=0,test1,j,ii,jj;
	int l,lll;
	int kkk,flag=1;
	double testO,testOP;
	int testC;
	double testE;
	int NewRMSD,New_Mean_H_Bonds;
	FILE *fp;
	test=test1=0;
	
	
	
	//kk=(int)(ran3(&seed)*(Plength[pr]/NATOM));
	
	kk=kk*NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"CAZOOOOOOO %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((kk>=Plength[pr]-1)||(kk<0)){
		fprintf(out,"KK out of range MC_Pivot_fw icycl %llu kk %d\n",icycl,kk);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	//fprintf(out,"MC_Pivot_fw A OK %llu\n",icycl);fflush(out);
	
	
	/*Random Axis*/
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	
	
	/*Previsou Part Axis*/
	/*dx=prot[pr].ammino[kk+1].x-prot[pr].ammino[kk].x;
	dy=prot[pr].ammino[kk+1].y-prot[pr].ammino[kk].y;
	dz=prot[pr].ammino[kk+1].z-prot[pr].ammino[kk].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	RotX=dx;
	RotY=dy;
	RotZ=dz;*/
			
			
	/*RotX=dx/sist->Bbond[1];
	RotY=dy/sist->Bbond[1];
	RotZ=dz/sist->Bbond[1];*/
	/*modulo=sqrt(dx*dx+dy*dy+dz*dz);
	RotX=dx/modulo;
	RotY=dy/modulo;
	RotZ=dz/modulo;
	modulo=sqrt(RotX*RotX+RotY*RotY+RotZ*RotZ);
	if(fabs(modulo-1.000000)>1e-17){
		fprintf(out,"FW Module %llu %30.20lf %d\n",icycl,modulo,kk);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/**/
	alpha=(PI*ran3(&seed)/10.0)-PI/20.0;
	//alpha=(2*ran3(&seed))-1.0;

	
	
	/*for(k=0;k<Plength[pr];k++){
		oldX1[k]=prot[pr].ammino[k].x;
		oldY1[k]=prot[pr].ammino[k].y;
		oldZ1[k]=prot[pr].ammino[k].z;
	}*/
	/*for(k=0;k<Plength[pr];k++){
		nvicini[k]=prot[pr].ammino[k].Nverl;
	for(j=0;j<prot[pr].ammino[k].Nverl;j++){
		vicini[k][j]=prot[pr].ammino[k].verli[j];
	}
}*/
	//fprintf(out,"MC_Pivot_fw B OK %llu\n",icycl);fflush(out);
	for(k=kk;k<Plength[pr];k++){ 						   
			
				
			

				
				
				Eold+=energy_SP_Pivot_fw(pr,k);
				//order_SP(pr,k);
				//if(icycl > 0) fprintf(out,"Eold %lf\n",Eold);
		
		
				Oold+=sist->norder;
				Cold+=sist->ncontact;
				/*if(Cold>sist->contact){
					fprintf(out,"AA a fw %llu %d %d %d %d %d\n",icycl,k,Cold,Oold-IminO,sist->contact,sist->order-IminO);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}
				if(Oold>sist->order){
					fprintf(out,"AA b fw %llu %d %d %d %d %d\n",icycl,k,Cold,Oold-IminO,sist->contact,sist->order-IminO);
					fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
				}*/
			
		
		
	}	
	Eold+=Bond_Energy_bw(pr,kk);
	Eold+=Bond_Energy_fw(pr,Plength[pr]-SPG_PATCH2);
	//fprintf(out,"MC_Pivot_fw C OK %llu\n",icycl);fflush(out);
	/*for(k=0;k<NPROT;k++){ 
		for(kkk=0;kkk<Plength[k];kkk++){ 
			fprintf(out,"Eold[%d][%d]=%lf\n",k,kkk,energy_SP_Pivot_fw(k,kkk));
		}
	}*/
	for(k=kk;k<Plength[pr];k++){ 
		
				//if(icycl>3576610){fprintf(out,"Eold %llu %d %d\n",icycl,kk,k);fflush(out);}
				oldX[k]=prot[pr].ammino[k].x;
				oldY[k]=prot[pr].ammino[k].y;
				oldZ[k]=prot[pr].ammino[k].z;
		
	}	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"MC_Pivot_fw D OK %llu\n",icycl);fflush(out);
	//fprintf(out,"kk %d angle %lf\n",kk,alpha*360.0/PI);
	//fflush(out);
	
	
		
		
	
	//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
	/*for(k=kk-1;k<Plength[pr];k++){ 
		if(prot[pr].ammino[k].id==ATOM_H){
			dx=(prot[pr].ammino[k-4].x-prot[pr].ammino[k].x);
			dy=(prot[pr].ammino[k-4].y-prot[pr].ammino[k].y);
			dz=(prot[pr].ammino[k-4].z-prot[pr].ammino[k].z);
			r2=(dx*dx+ dy*dy + dz*dz);
			if(fabs(sqrt(r2)-sist->Bbond[4])>2.0e-5){
				fprintf(out,"P H-N wrong %llu %d %30.20lf \n%d %lf\n",icycl,kk,sqrt(r2),k,alpha);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
		}
		
		
	}*/
	
	for(k=kk;k<Plength[pr];k++){ 
		
			
		
			
				//if(icycl>0){fprintf(out,"Rotation %llu %d %d\n",icycl,kk,k);fflush(out);}
				prot[pr].ammino[k].x-=prot[pr].ammino[kk-NATOM].x;
				prot[pr].ammino[k].y-=prot[pr].ammino[kk-NATOM].y;
				prot[pr].ammino[k].z-=prot[pr].ammino[kk-NATOM].z;




				Rotation(pr,k,alpha,RotX,RotY,RotZ,flag);
				 flag=0;


				prot[pr].ammino[k].x+=prot[pr].ammino[kk-NATOM].x;
				prot[pr].ammino[k].y+=prot[pr].ammino[kk-NATOM].y;
				prot[pr].ammino[k].z+=prot[pr].ammino[kk-NATOM].z;
				
				
				
				
				
				if(prot[pr].ammino[k].id==ATOM_CA){
					//if(icycl>0){fprintf(out,"Update cell SAW %llu %d %d\n",icycl,kk,k);fflush(out);}
					updt_cell_list_SAW (pr,k);
					if ((energy_SAW(pr,k)>000.0)){  // reject
						for(l=kk;l<=k;l++){ 
							
								

									prot[pr].ammino[l].x=oldX[l];
									prot[pr].ammino[l].y=oldY[l]; 
									prot[pr].ammino[l].z=oldZ[l]; 

									updt_cell_list_SAW (pr,l);
									updt_cell_list (pr,l);


								
							
						}
						//if(icycl>0){fprintf(out,"Test cell before rejection SAW\n");fflush(out);}
						//test_celllist_SAW();
						//if(icycl>0){fprintf(out,"Test cell before rejection CA\n");fflush(out);}
						//test_celllist_CA();
						//if(icycl>0){fprintf(out,"Test cell before rejection H\n");fflush(out);}
						//test_celllist_H();
						//if(icycl>0){fprintf(out,"Test cell before rejection O\n");fflush(out);}
						//
						//if(icycl>0){fprintf(out,"Test cell before rejection OK\n");fflush(out);}
						fw_rejectedself++;
						return(REJECTSELF) ;
					}
				}
				
				//if(icycl>0){fprintf(out,"Update cell\n");fflush(out);}
				updt_cell_list (pr,k);
				//if(icycl>0){fprintf(out,"Test overlaps\n");fflush(out);}
			//if(icycl>0){fprintf(out,"Test overlaps OK \n");fflush(out);}
				
			
			
		
	
	}
	//fprintf(out,"MC_Pivot_fw E OK %llu\n",icycl);fflush(out);
		
		#ifdef 	ORDER_TEST
   test_celllist_SAW();
   test_celllist_CA();
   test_celllist_H();
   
	#endif	
	/*if ((energy_SAW_test ()>0.0)){
			 fprintf(out,"SAW  fw %llu %d %d\n",icycl);

			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
	
	for(k=kk;k<Plength[pr];k++){ 						   
			
				
			

				
					Enew+=energy_SP_Pivot_fw(pr,k);
					Onew+=sist->norder;
					Cnew+=sist->ncontact;
				
			
		
	}
	Enew+=Bond_Energy_bw(pr,kk);
	Enew+=Bond_Energy_fw(pr,Plength[pr]-SPG_PATCH2);
	//fprintf(out,"MC_Pivot_fw F OK %llu\n",icycl);fflush(out);
		
	
	
		
	
	//fprintf(out,"ok1\n");
	  
	
	
	//fprintf(out,"ok2\n");
	
	
	
	
	
	
	if(Cnew>0) Znew=1;
	
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	Dnew=sist->density+Dnew-Dold;
	
	//fprintf(out,"MC_Pivot_fw G OK %llu\n",icycl);fflush(out);
	
	
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	
	 
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %llu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %llu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	//Enew=0.0;
	/*for(k=0;k<NPROT;k++){ 
		for(kkk=0;kkk<Plength[k];kkk++){ 
			if((k==pr)&&(kkk>=kk)){
				fprintf(out,"Enew[%d][%d]=%lf *\n",k,kkk,energy_SP_Pivot_fw(k,kkk));fflush(out);
			}else{
				fprintf(out,"Enew[%d][%d]=%lf\n",k,kkk,energy_SP_Pivot_fw(k,kkk));fflush(out);
			}
		}
	}*/
#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	if(Cnew!=testC){
		fprintf(out,"CC a fw  %llu %d %d\n",icycl,Cnew,testC);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b fw  %llu %lf %lf\n",icycl,Onew,testO);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-8){
		fprintf(out,"CC d fw  %llu %15.10lf %15.10lf\n",icycl,Enew,testE);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
#endif	
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %llu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/*for(k=0;k<Plength[pr];k++){  
		
		Enew2+=energy_SP_Pivot_fw(pr,k);
	
		
	} 
	if(fabs(Enew2-Enew)>0.0001){
		fprintf(out,"Trot energia PDL diversa %llu Enew=%lf Enew2=%lf sist->E=%lf Eold=%lf\n",icycl,Enew,Enew2,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"MC_brot fw non ok icycl=%llu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"MC_brot fw non ok icycl=%llu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/***********TEST*************/
	NewRMSD=lround(RMSD_bin*sqrt(Onew/TotRMSDNeigh));
	if((NewRMSD>=maxO)) NewRMSD=maxO-1;
	
	if((NewRMSD<0)){		
		fprintf(out,"MC_brot fw non ok icycl=%llu, NewRMSD=%d %lf %lf \n",icycl,NewRMSD,Onew,TotRMSDNeigh);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	//fprintf(out,"MC_Pivot_fw Gbis OK %llu, betaindice=%d Cnew=%d sist->contact=%d NewRMSD=%d sist->RMSD=%d\n",icycl,betaindice,Cnew,sist->contact,NewRMSD,sist->RMSD);fflush(out);
	
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot fw New_Mean_H_Bonds negative icycl=%llu, Cnew= %d sist->contact= %d Cold= %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=maxH)) New_Mean_H_Bonds=maxH-1;
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][NewRMSD]-Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD];
	
	//fprintf(out,"MC_Pivot_fw H OK %llu\n",icycl);fflush(out);
	
	espo=-(Enew-Eold)*beta[betaindice]+DW;
	
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot fw non ok icycl=%llu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fprintf(out,"New_Mean_H_Bonds=%d\n",New_Mean_H_Bonds);
		fprintf(out,"NewRMSD=%d\n",NewRMSD);
		fprintf(out,"sist->Mean_H_Bonds=%d\n",sist->Mean_H_Bonds);
		fprintf(out,"sist->RMSD=%d\n",sist->RMSD);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	//fprintf(out,"MC_Pivot_fw I OK %llu\n",icycl);fflush(out);
	
	//fprintf(out,"MC_brot fw non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,espo1);fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);fflush(out);
		sampling(Dnew,Enew,NewRMSD,Znew,New_Mean_H_Bonds,espo+espo1);
	
		sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,espo1);
	
	
	
	//fprintf(out,"MC_Pivot_fw J OK %llu\n",icycl);fflush(out);
	
	acc=exp(espo);
	
    	if(ran3(&seed)>acc){ // reject
		//if(icycl>3576610){fprintf(out,"Orders %llu %d %d\n",icycl,sist->order,sist->contact);fflush(out);}
		for(k=kk;k<Plength[pr];k++){ 
			
					//if(icycl>3576610){fprintf(out,"reject 1 %llu %d %d\n",icycl,kk,k);fflush(out);}
					prot[pr].ammino[k].x=oldX[k];
					prot[pr].ammino[k].y=oldY[k]; 
					prot[pr].ammino[k].z=oldZ[k]; 
					updt_cell_list_SAW (pr,k);
					updt_cell_list (pr,k);
					
				
			
		}
	
		
			
		/*for(ii=0;ii<Plength[pr];ii++){
			
			if(fabs(oldX1[ii]-prot[pr].ammino[ii].x)>1e-10){ fprintf(out,"Update fw X %llu %d %d %lf %lf\n",icycl,kk,ii,oldX1[ii],prot[pr].ammino[ii].x);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldY1[ii]-prot[pr].ammino[ii].y)>1e-10){ fprintf(out,"Update fw Y %llu %d %d %lf %lf\n",icycl,kk,ii,oldY1[ii],prot[pr].ammino[ii].y);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			if(fabs(oldZ1[ii]-prot[pr].ammino[ii].z)>1e-10){ fprintf(out,"Update fw Z %llu %d %d %lf %lf\n",icycl,kk,ii,oldZ1[ii],prot[pr].ammino[ii].z);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
		}*/
		
		/*for(k=0;k<Plength[pr];k++){
			test=0;
			if(prot[pr].ammino[k].Nverl!=nvicini[k]){fprintf(out,"Wrong number of neigh %llu %d %d %d\n",icycl,k,prot[pr].ammino[k].Nverl,nvicini[k]);fflush(out);MPI_Abort(MPI_COMM_WORLD,err5);}
			for(j=0;j<prot[pr].ammino[k].Nverl;j++){
				
				for(jj=0;jj<nvicini[k];jj++){
					if(vicini[k][jj]==prot[pr].ammino[k].verli[j]) test++;
				}
					
			}		
					
			if(test!=prot[pr].ammino[k].Nverl){
				fprintf(out,"Update fw Verl %llu %d %d\n",icycl,test,prot[pr].ammino[k].Nverl);
				for(j=0;j<prot[pr].ammino[k].Nverl;j++){
					fprintf(out,"vicini[%d][%d]=%d %d\n",k,j,vicini[k][j],prot[pr].ammino[k].verli[j]);
				}
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
				}
			
		}*/
		
		
		
		fw_rejectedener++;
		return(0);   
	}
	
	
	
	
	
	
	

	
	//fprintf(out,"MC_Pivot_fw L OK %llu\n",icycl);fflush(out);
	fw_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %llu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->RMSD=NewRMSD;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;

	Minimum_Energy(sist->E,sist->RMSD,sist->contact);
	//Order_Boudaries(sist->E,sist->order,sist->contact);
	return(0);
	/*Ener=Ener-EtouchO+Etouch;*/
	
} 




int MC_Pivot_bw (int pr,int kk){
	double dx,dy,dz;
	static double *oldX=NULL,*oldY=NULL,*oldZ=NULL;
	
	double r2,rs;
	double theta,phi;
	double alpha;
	double Etouch,EtouchO;
	double Enew2=0.0,Eold=0.0,Enew=0.0,acc,DW,espo,espo1,Eold2=0.0;
	double Oold=0.0,Onew=0.0;
	int Zold=0,Znew=0;
	int Cold=0,Cnew=0;
	double Dold=0.0,Dnew=0.0;
	double RotX,RotY,RotZ,modulo;
	int i,k,test=0,j,ii,test1=0;
	int kkk,flag=1;
	int l,lll;
	int prt;
	double testO,testOP;
	int testC;
	double testE;
	int NewRMSD,New_Mean_H_Bonds;
	FILE *fp;
	
	
	
	
	//kk=(int)(ran3(&seed)*(Plength[pr]/5));
	
	
	kk=kk*NATOM;
	if(prot[pr].ammino[kk].id!=ATOM_CA){
		fprintf(out,"CAZOOOOOOO %d %d\n",prot[pr].ammino[kk].id,ATOM_CA);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	if((kk>=Plength[pr]-1)||(kk<0)){
		fprintf(out,"KK out of range MC_Pivot_bw icycl %llu kk %d\n",icycl,kk);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	
	
	if(oldX==NULL) oldX=(double *)calloc(ProtN,sizeof(double));
	if(oldY==NULL) oldY=(double *)calloc(ProtN,sizeof(double));
	if(oldZ==NULL) oldZ=(double *)calloc(ProtN,sizeof(double));
	
	
	theta=2*PI*ran3(&seed);
	phi=acos(2*ran3(&seed)-1);
	
	RotX=sin(phi)*cos(theta);
	RotY=sin(phi)*sin(theta);
	RotZ=cos(phi);
	
	/*dx=prot[pr].ammino[kk-1].x-prot[pr].ammino[kk].x;
	dy=prot[pr].ammino[kk-1].y-prot[pr].ammino[kk].y;
	dz=prot[pr].ammino[kk-1].z-prot[pr].ammino[kk].z;
	
	dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
	RotX=dx;
	RotY=dy;
	RotZ=dz;*/
	
	
	alpha=(PI*ran3(&seed)/5.0)-PI/10.0;
//	if(icycl>0) {fprintf(out,"ok 1 %llu\n",icycl);fflush(out);}	
	for(k=kk-1;k>=0;k--){	
		
			Eold+=energy_SP_Pivot_bw(pr,k);
			
		
			Oold+=sist->norder;
			Cold+=sist->ncontact;
			
		
	
		
	}
	Eold+=Bond_Energy_bw(pr,kk);
	Eold+=Bond_Energy_bw(pr,0);
	//fprintf(out,"MC_Pivot_bw A OK %llu\n",icycl);fflush(out);
	/*testE=sist->E;
	for(k=Plength[pr]-1;k>=0;k--){ 
		testE-=energy_SP_Pivot_bw(pr,k);
	}*/
	
	
	
	/*for(k=0;k<NPROT;k++){ 
		for(kkk=Plength[k]-1;kkk>=0;kkk--){ 
			fprintf(out,"Eold[%d][%d]=%lf\n",k,kkk,energy_SP_Pivot_bw(k,kkk));
		}
	}*/
	
	
	
	for(k=kk-1;k>=0;k--){	
		
			oldX[k]=prot[pr].ammino[k].x;
			oldY[k]=prot[pr].ammino[k].y;
			oldZ[k]=prot[pr].ammino[k].z;
		
			
			
		
		
	}
	
	sist->count=0;
	
	Dold=0;
	if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dold=1.0;
	//fprintf(out,"kk %d angle %lf\n",kk,alpha*360.0/PI);
	//fflush(out);
	test=0;
	
	
	
		
//if(icycl>0) {fprintf(out,"ok 2 %llu\n",icycl);fflush(out);}	
for(k=kk-1;k>=0;k--){	
		
			//fprintf(out,"%d %d %d %d\n",kk,prot[pr].ammino[kk].id,k,prot[pr].ammino[k].id);
			prot[pr].ammino[k].x-=prot[pr].ammino[kk].x;
			prot[pr].ammino[k].y-=prot[pr].ammino[kk].y;
			prot[pr].ammino[k].z-=prot[pr].ammino[kk].z;




			Rotation(pr,k,alpha,RotX,RotY,RotZ,flag);
			 flag=0;


			prot[pr].ammino[k].x+=prot[pr].ammino[kk].x;
			prot[pr].ammino[k].y+=prot[pr].ammino[kk].y;
			prot[pr].ammino[k].z+=prot[pr].ammino[kk].z;
			
		//	if(icycl>0) {fprintf(out,"ok Rotation %d\n",k);fflush(out);}	
			if(prot[pr].ammino[k].id==ATOM_CA){
				updt_cell_list_SAW (pr,k);
				if ((energy_SAW_bw(pr,k)>0.0)){  // reject
					//if(icycl>560) fprintf(out,"#####################Reject %d\n",k);
					for(l=kk-1;l>=k;l--){	
						

							prot[pr].ammino[l].x=oldX[l];
							prot[pr].ammino[l].y=oldY[l]; 
							prot[pr].ammino[l].z=oldZ[l];
							
						//	if(icycl>0) {fprintf(out,"ok Reject Coord %d\n",k);fflush(out);}	
							updt_cell_list_SAW (pr,l);
						//	if(icycl>0) {fprintf(out,"ok Reject SAW %d\n",k);fflush(out);}	
							updt_cell_list (pr,l);
						//	if(icycl>0) {fprintf(out,"ok Reject CELL %d\n",k);fflush(out);}	


						

					}
				//	if(icycl>0) {fprintf(out,"ok Reject %d\n",k);fflush(out);}	
					//test_celllist_SAW();
					//test_celllist_CA();
					//test_celllist_H();
					//



					bw_rejectedself++;
					return(REJECTSELF);
				}
			}
			
		//	if(icycl>0) {fprintf(out,"ok SAW %d\n",k);fflush(out);}	
			updt_cell_list (pr,k);
		//	if(icycl>0) {fprintf(out,"ok Update %d\n",k);fflush(out);}	
			
			
			
		
		
	
	
	
	
}

	/*if ((energy_SAW_test ()>0.0)){
			 fprintf(out,"SAW  bw %llu %d %d\n",icycl);

			fflush(out);
			MPI_Abort(MPI_COMM_WORLD,err5);
		}*/
				#ifdef 	ORDER_TEST
	test_celllist_SAW();
	test_celllist_CA();
	test_celllist_H();
	  		  
#endif			
//	if(icycl>0) {fprintf(out,"ok 3 %llu\n",icycl);fflush(out);}	
		//fprintf(out,"MC_Pivot_bw B OK %llu\n",icycl);fflush(out);		
	for(k=kk-1;k>=0;k--){	
			
		
			//if(icycl>560) fprintf(out,"#####################\n");
				Enew+=energy_SP_Pivot_bw(pr,k);
			//if(icycl%nsamp==0) fprintf(out,"Enew %lf\n",Enew);
				
			Onew+=sist->norder;
			Cnew+=sist->ncontact;	
		

		 
		
	
		
	}
	Enew+=Bond_Energy_bw(pr,kk);
	Enew+=Bond_Energy_bw(pr,0);
	//fprintf(out,"MC_Pivot_bw C OK %llu\n",icycl);fflush(out);
	
	
	//fprintf(out,"ok1\n");
	//if(icycl>0) {fprintf(out,"ok 3 %llu\n",icycl);fflush(out);}	
	  
//	fprintf(out,"Onew=%lf  Oold=%lf \n",Onew,Oold);fflush(out);
	
	//fprintf(out,"ok2\n");
	Onew=sist->order+Onew-Oold;
	Cnew=sist->contact+Cnew-Cold;
	/*Onew=0;
	Cnew=0;
	for(prt=0;prt<NPROT;prt++){
		for (i=1;i<Plength[prt];i+=5){
			energy_SP_Pivot_bw(prt,i);
			Onew+=sist->norder;
			Cnew+=sist->ncontact;
		}
	}*/
	//if(icycl > 0) fprintf(out,"energ_bw  %llu count=%d\n",icycl,sist->count);
	sist->count=0;
	
	if(Cnew>0) Znew=1;
	Dnew=Dold;
	if(k==HalfN) if(((prot[pr].ammino[HalfN].z-sizeZ)*(prot[pr].ammino[HalfN].z-sizeZ))>=HalfN2) Dnew=1.0;
	Dnew=sist->density+Dnew-Dold;
	
	
	/***********TEST*************/
	if((Cnew<0)){		
		fprintf(out,"MC_brot bw non ok icycl=%llu, Cnew= %d sist->contact= %d Cold= %d \n",icycl,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((Onew<0)){		
		fprintf(out,"MC_brot bw non ok icycl=%llu, Onew= %lf sist->order= %lf Oold= %lf \n",icycl,Onew,sist->order,Oold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	/***********TEST*************/
	
	NewRMSD=lround(RMSD_bin*sqrt(Onew/TotRMSDNeigh));
	if((NewRMSD>=maxO)) NewRMSD=maxO-1;
	sist->Mean_H_Bonds=lround(Mean_H_Bonds_bin*(sist->contact/DNPROT));
 if(sist->Mean_H_Bonds>=maxH) sist->Mean_H_Bonds=maxH-1;
	New_Mean_H_Bonds=lround(Mean_H_Bonds_bin*(Cnew/DNPROT));
	
	if((New_Mean_H_Bonds<0)){		
		fprintf(out,"MC_brot bw New_Mean_H_Bonds negative icycl=%llu, Cnew= %d sist->contact= %d Cold= %d %d \n",icycl,New_Mean_H_Bonds,Cnew,sist->contact,Cold);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if((New_Mean_H_Bonds>=maxH)) New_Mean_H_Bonds=maxH-1;
	
	DW=Wpot[betaindice][New_Mean_H_Bonds][NewRMSD]-Wpot[betaindice][sist->Mean_H_Bonds][sist->RMSD];
	espo=-(Enew-Eold)*beta[betaindice]+DW;
	if(espo<50.0){
		espo1=-log(1+exp(espo));
	}else{
		espo1=-espo;
	}
	
	if((isnan(espo)!=0)||(isnan(1.0/espo)!=0)){
		fprintf(out,"MC_brot bw non ok icycl=%llu, espo=%lf \n",icycl,espo);
		fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);
		fprintf(out,"New_Mean_H_Bonds=%d\n",New_Mean_H_Bonds);
		fprintf(out,"NewRMSD=%d\n",NewRMSD);
		fprintf(out,"sist->Mean_H_Bonds=%d\n",sist->Mean_H_Bonds);
		fprintf(out,"sist->RMSD=%d\n",sist->RMSD);
		fflush(out);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	
	
	/*if(Enew>0){
		fprintf(out,"Brot energia New positiva %llu %lf\n",icycl,Enew);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(Eold>0){
		fprintf(out,"Brot energia Old positiva %llu %lf\n",icycl,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	Enew=sist->E+Enew-Eold;
	Eold=sist->E;
	
	
	/*for(k=0;k<NPROT;k++){ 
		for(kkk=Plength[k]-1;kkk>=0;kkk--){ 
			if((k==pr)&&(kkk<=kk)){
				fprintf(out,"Enew[%d][%d]=%lf *\n",k,kkk,energy_SP_Pivot_bw(k,kkk));
			}else{
				fprintf(out,"Enew[%d][%d]=%lf\n",k,kkk,energy_SP_Pivot_bw(k,kkk));
			}
			
		}
	}
	
	for(k=Plength[pr]-1;k>=0;k--){ 
		testE+=energy_SP_Pivot_bw(pr,k);
	}
	fprintf(out,"Enew-tot=%lf\n",testE);
	testE=0.0;*/
#ifdef 	ORDER_TEST	
	order_test(&testC,&testO,&testE);
	if(Cnew!=testC){
		fprintf(out,"CC a bw %llu %d %d\n",icycl,Cnew,testC);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
	if(fabs(Onew-testO)>1e-5){
		fprintf(out,"CC b bw %llu %.20lf %.20lf\n",icycl,Onew,testO);

		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	} 
	if(fabs(Enew-testE)>1e-8){
		fprintf(out,"CC d bw %llu %15.10lf %15.10lf pr=%d kk=%d\n",icycl,Enew,testE,pr,kk);

		
		for(k=ATOM_CA;k<Plength[pr]-NATOM;k+=NATOM){	
			fprintf(out,"pr=%d kk=%d Bond_fw=%lf Bond_bw=%lf\n",pr,k,Bond_Energy_fw(pr,k),Bond_Energy_bw(pr,k+NATOM));
			
		}
		fprintf(out,"Ends pr=%d Bond_fw=%lf Bond_bw=%lf\n",pr,Bond_Energy_fw(pr,Plength[pr]-SPG_PATCH2),Bond_Energy_bw(pr,0));
		fflush(out);
		fsamp();
		fp=fopen("final-err.bin","w");
		writeBinary(fp);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}
#endif
	/*if(Enew>0){
		fprintf(out,"Brot energia New New positiva %llu %lf %lf %lf\n",icycl,Enew,sist->E,Eold);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	
	//fprintf(out,"MC_brot bw non ok icycl=%llu, espo=%lf %lf\n",icycl,espo,log(1+exp(espo)));fprintf(out,"Enew=%lf Eold=%lf DW=%lf\n",Enew,Eold,DW);fflush(out);

	/***********TEST*************/
	
		sampling(Dnew,Enew,NewRMSD,Znew,New_Mean_H_Bonds,espo+espo1);
	
		sampling(sist->density,sist->E,sist->RMSD,sist->touch,sist->Mean_H_Bonds,espo1);
	
	
	
	
	acc=exp(espo);
	if (ran3(&seed)>acc){ // reject
		for(k=kk-1;k>=0;k--){	
			
				prot[pr].ammino[k].x=oldX[k];
				prot[pr].ammino[k].y=oldY[k]; 
				prot[pr].ammino[k].z=oldZ[k]; 
				
				updt_cell_list_SAW (pr,k);
				updt_cell_list (pr,k);
			

		}
			
		
		
		
		
		
		
				bw_rejectedener++;
				return(0);   
	}
	
	
	
	
	
	
	
	
	
	//if(icycl>6417534) {fprintf(out,"ok 5 %llu\n",icycl);fflush(out);}	
	bw_accepted++;
	sist->E=Enew;
	/*if(sist->E>0){
		fprintf(out,"Brot energia finale positiva %llu %lf\n",icycl,sist->E);
		fflush(NULL);
		MPI_Abort(MPI_COMM_WORLD,err5);
	}*/
	sist->order=Onew;
	sist->contact=Cnew;
	sist->touch=Znew;
	sist->density=Dnew;
	sist->RMSD=NewRMSD;
	sist->Mean_H_Bonds=New_Mean_H_Bonds;
	/*Ener=Ener-EtouchO+Etouch;*/
	Minimum_Energy(sist->E,sist->RMSD,sist->contact);
	//Order_Boudaries(sist->E,sist->order,sist->contact);
	return(0);
}


void ExpSum (void *ina,void *output, int *len, MPI_Datatype *dptr){
	
	
	int i=0;
	double c=0;
	double *in,*inout;
	in=(double *) ina;
	inout=(double *) output;
	for(i=0;i<*len;++i){
		if(in[i]>inout[i]){
			
			inout[i]=in[i]+log(1+exp(inout[i]-in[i]));
		}else{
			inout[i]+=log(1+exp(in[i]-inout[i]));
		}
	}
	return;
}


double gasdev(long *idum)
	{
	
	static int iset=0;
	static double gset;
	double fac,rsq,v1,v2;
	
	if  (iset == 0) {
		do {
			v1=2.0*ran3(idum)-1.0;
			v2=2.0*ran3(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} else {
		iset=0;
		return gset;
	}
}

	










void Rescaler (void){
	
	int pr,i,prt,j,k;
	double dx,dy,dz,r2,r;
	double Etouch,EtouchO;
	int indice_kCA;
	
	
				fprintf(out,"################## Rescaling %llu\n",icycl);
						
						
				for(pr=0;pr<NPROT;pr++){
					for(k=0;k<Plength[pr];k++){
						if(prot[pr].ammino[k].id==ATOM_H){
							indice_kCA=rint(k/NATOM)*NATOM;
							dx=(prot[pr].ammino[indice_kCA].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[indice_kCA].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[indice_kCA].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[1])>1e-10){
								fprintf(out,"H-CA rescale %llu %d %30.20lf \n",icycl,k,r-sist->Bbond[1]);fflush(out);
								prot[pr].ammino[k].x-=prot[pr].ammino[indice_kCA].x;
								prot[pr].ammino[k].y-=prot[pr].ammino[indice_kCA].y;
								prot[pr].ammino[k].z-=prot[pr].ammino[indice_kCA].z;
								
								prot[pr].ammino[k].x*=sist->Bbond[ATOM_H]/r;
								prot[pr].ammino[k].y*=sist->Bbond[ATOM_H]/r;
								prot[pr].ammino[k].z*=sist->Bbond[ATOM_H]/r;
										
								prot[pr].ammino[k].x+=prot[pr].ammino[indice_kCA].x;
								prot[pr].ammino[k].y+=prot[pr].ammino[indice_kCA].y;
								prot[pr].ammino[k].z+=prot[pr].ammino[indice_kCA].z;
								
								
							
							dx=(prot[pr].ammino[indice_kCA].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[indice_kCA].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[indice_kCA].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[1])>1e-10){
								fprintf(out,"H-CA wrong %llu %d %30.20lf \n",icycl,k,r-sist->Bbond[1]);fflush(out);
								MPI_Abort(MPI_COMM_WORLD,err5);
							}
						}
							
						}
						
						
						
						
						
					
					
					}
				}
				fprintf(out,"################## Rescaling ok %llu\n",icycl);
						
						return;
						
				
}
/*void Bond_test (int flag){
	
	int pr,i,prt,j,k;
	double dx,dy,dz,r2,r;
	
	double dx1,dy1,dz1;
	
			
	
				//fprintf(out,"################## Bond_test %llu %d\n",icycl,flag);fflush(out);
				for(pr=0;pr<NPROT;pr++){
					for(k=0;k<Plength[pr];k++){
						if(prot[pr].ammino[k].id==ATOM_H){
							
							dx=(prot[pr].ammino[k-4].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[k-4].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[k-4].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[4])>1e-10){
								
								fprintf(out,"H-N %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->Bbond[4]);fflush(out);
								fsamp();
								MPI_Abort(MPI_COMM_WORLD,err5);
							
						}
							
						}
						
						if(prot[pr].ammino[k].id==ATOM_O){
							dx=(prot[pr].ammino[k-1].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[k-1].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[k-1].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[2])>1e-10){
								
								fprintf(out,"O-C %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->Bbond[2]);fflush(out);
								fsamp();
								MPI_Abort(MPI_COMM_WORLD,err5);
							
						}
						}
						if(prot[pr].ammino[k].id==ATOM_N){
							dx=(prot[pr].ammino[k+1].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[k+1].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[k+1].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[0])>1e-10){
								
									fprintf(out,"CA-N %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->Bbond[0]);fflush(out);
									fsamp();
									MPI_Abort(MPI_COMM_WORLD,err5);
								
							}
						}
						if(prot[pr].ammino[k].id==ATOM_CA){

							dx=(prot[pr].ammino[k+1].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[k+1].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[k+1].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[1])>1e-10){
								
									fprintf(out,"C-CA %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->Bbond[1]);fflush(out);
									fsamp();
									MPI_Abort(MPI_COMM_WORLD,err5);
								
							}
						}
						if(k>0){//otherwise has problems with the last N where k+3 >Plength
						if(prot[pr].ammino[k].id==ATOM_N){

							dx=(prot[pr].ammino[k-3].x-prot[pr].ammino[k].x);
							dy=(prot[pr].ammino[k-3].y-prot[pr].ammino[k].y);
							dz=(prot[pr].ammino[k-3].z-prot[pr].ammino[k].z);
							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
							r2=(dx*dx+ dy*dy + dz*dz);
							r=sqrt(r2);
							if(fabs(r-sist->Bbond[3])>1e-10){
								
									fprintf(out,"C-N %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->Bbond[3]);fflush(out);
									fsamp();
									MPI_Abort(MPI_COMM_WORLD,err5);
								
							}
						}
					}
					}
				}
				//fprintf(out,"################## Bond_test ok %llu\n",icycl);fflush(out);
				for(pr=0;pr<NPROT;pr++){
					for(k=0;k<Plength[pr];k+=NATOM){
						
						
						
						dx=(prot[pr].ammino[k+ATOM_CA].x-prot[pr].ammino[k+ATOM_N].x);
						dy=(prot[pr].ammino[k+ATOM_CA].y-prot[pr].ammino[k+ATOM_N].y);
						dz=(prot[pr].ammino[k+ATOM_CA].z-prot[pr].ammino[k+ATOM_N].z);

						dx1=(prot[pr].ammino[k+ATOM_C].x-prot[pr].ammino[k+ATOM_N].x);
						dy1=(prot[pr].ammino[k+ATOM_C].y-prot[pr].ammino[k+ATOM_N].y);
						dz1=(prot[pr].ammino[k+ATOM_C].z-prot[pr].ammino[k+ATOM_N].z);
						
						r=dx*dx1+dy*dy1+dz*dz1;
						if(fabs(sist->DotProd[2]-r)>1e-07){
							
							fprintf(out,"CNCa %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->DotProd[2]);fflush(out);
								fsamp();
								MPI_Abort(MPI_COMM_WORLD,err5);
						}
						
						dx=(prot[pr].ammino[k+ATOM_N].x-prot[pr].ammino[k+ATOM_C].x);
						dy=(prot[pr].ammino[k+ATOM_N].y-prot[pr].ammino[k+ATOM_C].y);
						dz=(prot[pr].ammino[k+ATOM_N].z-prot[pr].ammino[k+ATOM_C].z);

						dx1=(prot[pr].ammino[k+ATOM_CA].x-prot[pr].ammino[k+ATOM_C].x);
						dy1=(prot[pr].ammino[k+ATOM_CA].y-prot[pr].ammino[k+ATOM_C].y);
						dz1=(prot[pr].ammino[k+ATOM_CA].z-prot[pr].ammino[k+ATOM_C].z);
						
						
						
						
						r=dx*dx1+dy*dy1+dz*dz1;
						if(fabs(sist->DotProd[1]-r)>1e-07){
							
							fprintf(out,"CaCN %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->DotProd[1]);fflush(out);
								fsamp();
								MPI_Abort(MPI_COMM_WORLD,err5);
						}
						
						dx=(prot[pr].ammino[k+ATOM_N].x-prot[pr].ammino[k+ATOM_CA].x);
						dy=(prot[pr].ammino[k+ATOM_N].y-prot[pr].ammino[k+ATOM_CA].y);
						dz=(prot[pr].ammino[k+ATOM_N].z-prot[pr].ammino[k+ATOM_CA].z);

						dx1=(prot[pr].ammino[k+ATOM_C].x-prot[pr].ammino[k+ATOM_CA].x);
						dy1=(prot[pr].ammino[k+ATOM_C].y-prot[pr].ammino[k+ATOM_CA].y);
						dz1=(prot[pr].ammino[k+ATOM_C].z-prot[pr].ammino[k+ATOM_CA].z);
						
						r=dx*dx1+dy*dy1+dz*dz1;
						if(fabs(sist->DotProd[0]-r)>1e-07){
							
							fprintf(out,"NCaC %d wrong %llu %d %30.20lf \n",flag,icycl,k,r-sist->DotProd[0]);fflush(out);
								fsamp();
								MPI_Abort(MPI_COMM_WORLD,err5);
						}
						
						
					}
				}
}*/

void Inflate (void){
	
	int pr,i,prt,j,k;
	double dx,dy,dz,r2,r;
	if(Rmin_flag==1){
		MINR=1000.0;
		fprintf(out,"INFLATE %d %lf %lf\n",Rmin_flag,MINR,sist->Rmin);
		fflush(out);
		for(pr=0;pr<NPROT;pr++){
			for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
				for(prt=0;prt<NPROT;prt++){
					for(j=ATOM_CA;j<Plength[prt];j+=NATOM){
			//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
			
			//if(icycl > 0) fprintf(out,"##%d###%d %d %d %d\n",k,pr,i,prt,j);
				dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
				dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
				dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

				dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

				r2=(dx*dx+ dy*dy + dz*dz);
				if((prot[pr].ammino[i].id==ATOM_CA)&&(prot[prt].ammino[j].id==ATOM_CA)){
				    if(pr==prt){
					if((j != i+NATOM)&&(j != i)&&(j != i-NATOM)&&(MINR>r2)) MINR=r2; 
					}else{
						if(MINR>r2) MINR=r2; 
					}

				}
			}
		}
	}

		}
		sist->Rmin=sqrt(MINR);
		fprintf(out,"INFLATE %d %lf %lf ",Rmin_flag,MINR,sist->Rmin);
		fflush(out);
		//printf("INFLATE %d %lf %lf\n",Rmin_flag,MINR,sist->Rmin);
		//fflush(NULL);
		if(sist->Rmin>=sist->Rmin_CAsaw){
			 sist->Rmin=sist->Rmin_CAsaw;
			 Rmin_flag=0;
		 }
		fprintf(out," final choice %lf\n",sist->Rmin);
		fflush(out);
		//if(ECA_Range+5.0>12.0){
		
		/*12=7(H-bond range) + 2(1+1 max distance O-Ca N-CA) + 3(safety region) is to make shure that even for 
		short range interactions the Hydrogen bonds are still taken into account*/
		
		sist->Rint=ECA_Range+5.0;sist->Rver=5.0+sist->Rint;sist->Rmax=sist->Rmin;sist->Rvgap=(sist->Rver-sist->Rint)/2.0-0.1;
	//}else{
		//sist->Rint=12.0;sist->Rver=5.0+sist->Rint;sist->Rmax=sist->Rmin;sist->Rvgap=(sist->Rver-sist->Rint)/2.0-0.1;
	//}
	
	//sist->Rint=ECA_Range;
		/*is divided by 2.0 because the maximum variation for a move without update is 2*Rvgap (one per particle) and this should 
		not bring particle close enough to interact so 2*Rvgap=Rver-Rint*/
		//fprintf(out,"%lf %lf %lf %lf\n",sist->Rint,sist->Rver,sist->Rmax,sist->Rvgap);

		

		sist->Rmin2=sist->Rmin*sist->Rmin;
		sist->Rmin_CAsaw2=sist->Rmin_CAsaw*sist->Rmin_CAsaw;
		sist->Rint2=sist->Rint*sist->Rint;
		
		sist->Rver2=sist->Rver*sist->Rver;
		
		sist->Rmax2=sist->Rmax*sist->Rmax;
		sist->Rvgap2=sist->Rvgap*sist->Rvgap;
		
		
		LJ2_a=sist->Rmin;
		LJ2_c=ECA_Range;
		
		//LJ2_b=(exp((LJ2_a-LJ2_c)/2.0)+1.0);
		//LJ2_b*=LJ2_b;
		//printf("exp(%lf-x) + %lf/(1.0 + exp((%lf - x))) - %lf\n",LJ2_a,LJ2_b,LJ2_c,LJ2_b);
		
		
		LJ2_b=(-1.0/(-1.0+1.0/(1.0+exp(LJ2_c-LJ2_a))));
		printf("%lf/(1.0 + exp((%lf - x))) - %lf\n",LJ2_b,LJ2_c,LJ2_b);
		
		fflush(NULL);
		/*sist->Rmin6=pow(sist->Rmin2,LJpower);
		sist->Rmin12=sist->Rmin6*sist->Rmin6;*/

		sigma6=pow(sist->Rmin,6.0);
		
		
		MINR=1000.0;
	}
}



double Bond_Energy_bw (int pr,int input_k){
	
	
	/************
	Uses The CA as the reference for and always computes the Bending energy with respec to the
	previous residue.
	
	**********/
	
	double dx,dy,dz;

	double r;
	int indice_a,indice_b;
	
	double ener=0.0;
	#define Bond_extension 1.0
	
	
	
	
	if(SPRING_MODEL==1) {
		
		if(input_k==0){
		  indice_a=input_k+1; /// With the last one if it is the first one: closed ends
			indice_b=Plength[pr]-1;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;

			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

			r=sqrt(dx*dx+dy*dy+dz*dz);
			//fprintf(out,"BW indice_a=%d indice_b=%d r=%lf\n",indice_a,indice_b,r);fflush(out);
			ener=Bond_factor_end*r*r;
		}
		if(input_k-NATOM>=0){  /// With the previous one if it is not the first one
			indice_a=input_k+1;
			indice_b=input_k-NATOM+SPG_PATCH2;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;



			r=sqrt(dx*dx+dy*dy+dz*dz);
			if(r>Bond_extension) ener+=Bond_factor*(r-Bond_extension)*(r-Bond_extension);
			

		}
			
			
		
		
	}else{
	  if(input_k-NATOM>=0){
		indice_b=input_k-NATOM;
		dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
		dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
		dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
		r=sqrt(dx*dx+dy*dy+dz*dz);

		if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
		  fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
		  fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
		  fflush(out);
		  MPI_Abort(MPI_COMM_WORLD,err5);
		}
		sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
		ener=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	  }
	}
	
	return(ener);
}

double Bond_Energy_fw (int pr,int input_k){
	
	
	/************
	Uses The CA as the reference for and always computes the Bending energy with respec to the
	next residue.
	
	**********/
	
	double dx,dy,dz;

	double r;
	int indice_a,indice_b;
	
	double ener=0.0;
#define  Bond_extension 1.0
	
	
	
	
	
	if(SPRING_MODEL==1) {
		
	  if(input_k==Plength[pr]-SPG_PATCH2-1){ /// This is the spring to close the ends, between the last and the first patches of the whole chain
					indice_a=input_k+SPG_PATCH2;
					indice_b=1;
					dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
					dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
					dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;


					dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
					r=sqrt(dx*dx+dy*dy+dz*dz);
					//fprintf(out,"FW indice_a=%d indice_b=%d r=%lf\n",indice_a,indice_b,r);fflush(out);
					ener=Bond_factor_end*r*r;
		}
	  if(input_k+NATOM<Plength[pr]){ /// Exclude the last bead not bonded to the next
				indice_a=input_k+SPG_PATCH2;
				indice_b=input_k+NATOM+1;
				dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
				dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
				dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;



				r=sqrt(dx*dx+dy*dy+dz*dz);

				if(r>Bond_extension) ener+=Bond_factor*(r-Bond_extension)*(r-Bond_extension); /// The equilibrium distance is 1 now, as in the seek

		}
				

	}else{
	  if(input_k+NATOM<Plength[pr]){
	    indice_b=input_k+NATOM;
	    dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
	    dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
	    dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;



	    r=sqrt(dx*dx+dy*dy+dz*dz);

	    if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
	      fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
	      fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
				  
	      fflush(out);
	      MPI_Abort(MPI_COMM_WORLD,err5);
	    }
	    sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
	    ener=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	    ///fprintf(out,"prot[%d].ammino[%d].residue=%d Rmin_input_k=%lf Rmin_indiceb=%lf sist->Bbond[5]=%lf ener=%lf\n",pr,input_k,prot[pr].ammino[input_k].residue,Rmin[prot[pr].ammino[input_k].residue],Rmin[prot[pr].ammino[indice_b].residue],sist->Bbond[5],ener); fflush(out);
	  }
	}
	
	return(ener);
}

double Bond_Energy_SP (int pr,int input_k){
	
	
	/************
	Uses The CA as the reference for and always computes the Bending energy with respec to the
	next residue.
	
	**********/
	
	double dx,dy,dz;

	double r;
	int indice_a,indice_b;
	
	double ener=0.0;
#define Bond_extension 1.0
	
	
	
	
	if(SPRING_MODEL==1) {
		
	  if(input_k+NATOM<Plength[pr]){ /// with the next bead patch, only if it is not the last one
			indice_a=input_k+SPG_PATCH2;
			indice_b=input_k+NATOM+1;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;
			r=sqrt(dx*dx+dy*dy+dz*dz);
			if(r>Bond_extension) ener+=Bond_factor*(r-Bond_extension)*(r-Bond_extension); /// The equilibrium distance is 1 now, as in the seek
			
		}		
			
	  if(input_k-NATOM>=0){ /// with the previous bead patch, only if it si not the first one
			indice_a=input_k+1;
			indice_b=input_k-NATOM+SPG_PATCH2;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;
			r=sqrt(dx*dx+dy*dy+dz*dz);
			
			
			if(r>Bond_extension) ener+=Bond_factor*(r-Bond_extension)*(r-Bond_extension); /// The equilibrium distance is 1 now, as in the seek
				
		}
		
	  if(input_k==Plength[pr]-SPG_PATCH2-1){ /// with the first one if it is the last one: closed ends
			indice_a=input_k+SPG_PATCH2;
			indice_b=1;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;

			
			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
			r=sqrt(dx*dx+dy*dy+dz*dz);

			ener+=Bond_factor_end*r*r;
			
	  }
	  if(input_k==0){ /// with the last one if it is the first one: closed ends
			indice_a=input_k+1;
			indice_b=Plength[pr]-1;
			dx=prot[pr].ammino[indice_a].x-prot[pr].ammino[indice_b].x;
			dy=prot[pr].ammino[indice_a].y-prot[pr].ammino[indice_b].y;
			dz=prot[pr].ammino[indice_a].z-prot[pr].ammino[indice_b].z;

			dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);
		
			r=sqrt(dx*dx+dy*dy+dz*dz);
			//fprintf(out,"SP indice_a=%d indice_b=%d r=%lf\n",indice_a,indice_b,r);fflush(out);
			ener+=Bond_factor_end*r*r;
			
	  }
		
		
		
		
		
	}else{
	  if(input_k+NATOM<Plength[pr]){
		indice_b=input_k+NATOM;
		dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
		dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
		dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;

		r=sqrt(dx*dx+dy*dy+dz*dz);
		
		if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
		  fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
		  fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
	
		  fflush(out);
		  MPI_Abort(MPI_COMM_WORLD,err5);
		}
		sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
		ener=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	  }


	  if(input_k-NATOM>=0){   /// I added this, there was not
	    indice_b=input_k-NATOM;
	    dx=prot[pr].ammino[input_k].x-prot[pr].ammino[indice_b].x;
	    dy=prot[pr].ammino[input_k].y-prot[pr].ammino[indice_b].y;
	    dz=prot[pr].ammino[input_k].z-prot[pr].ammino[indice_b].z;
	    
	    r=sqrt(dx*dx+dy*dy+dz*dz);
	
	    if((prot[pr].ammino[input_k].residue>=S)||(prot[pr].ammino[indice_b].residue>=S)){
	      fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,input_k,prot[pr].ammino[input_k].residue);
	      fprintf(out,"prot[%d].ammino[%d].residue=%d\n",pr,indice_b,prot[pr].ammino[indice_b].residue);
	
	      fflush(out);
	      MPI_Abort(MPI_COMM_WORLD,err5);
	    }
	    sist->Bbond[5]=(Rmin[prot[pr].ammino[input_k].residue]+Rmin[prot[pr].ammino[indice_b].residue]);
	    ener+=Bond_factor*(r-sist->Bbond[5])*(r-sist->Bbond[5]);
	  }
	  
	}
	
	return(ener);
}






void new_cell_list (void){
	

		
	unsigned long int indice;
	int i,j,k;
	int icel_x,icel_y,icel_z,pr;
	int ppr,ii;
	
	
	
	
	fprintf(out,"new_cell_list N_CA_cells==%d cell_CA_size=%lf BoxSize=%lf\nperiod_factor_CA=%lf`n",N_CA_cells,cell_CA_size,BoxSize,period_factor_CA);fflush(out);
	fprintf(out,"new_cell_list N_H_cells==%d cell_H_size=%lf\n",N_H_cells,cell_H_size);fflush(out);
		if(hoc_CA==NULL) hoc_CA=(unsigned int*) calloc(2*N_CA_cells*N_CA_cells*N_CA_cells,sizeof(unsigned int));
		if(hoc_H==NULL) hoc_H=(unsigned int*) calloc(2*N_H_cells*N_H_cells*N_H_cells,sizeof(unsigned int));
		
	
	fprintf(out,"new_cell_list ok 1\n");fflush(out);
	for(indice=0;indice<2*N_CA_cells*N_CA_cells*N_CA_cells;indice++){
		hoc_CA[indice]=0;
	}
	fprintf(out,"new_cell_list ok 2\n");fflush(out);
	for(indice=0;indice<2*N_H_cells*N_H_cells*N_H_cells;indice++){
		hoc_H[indice]=0;
	}
	fprintf(out,"new_cell_list ok 3\n");fflush(out);
	
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			prot[pr].ammino[i].lfw_pr=prot[pr].ammino[i].lfw_i=-1;
		}
	}
	for(pr=0;pr<NPROT;pr++){
		for(i=0;i<Plength[pr];i++){
			if(prot[pr].ammino[i].spring_anchor!=1){
			
			switch(prot[pr].ammino[i].id){
			
			/*ATOMI CA*/
				case ATOM_CA:
				
				/*icel_x=(prot[pr].ammino[i].x/cell_CA_size);
				icel_y=(prot[pr].ammino[i].y/cell_CA_size);
				icel_z=(prot[pr].ammino[i].z/cell_CA_size);

				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);*/
				
				icel_x=rint(prot[pr].ammino[i].x/cell_CA_size);
				icel_y=rint(prot[pr].ammino[i].y/cell_CA_size);
				icel_z=rint(prot[pr].ammino[i].z/cell_CA_size);

				icel_x=P_Cell(icel_x,N_CA_cells);
				icel_y=P_Cell(icel_y,N_CA_cells);
				icel_z=P_Cell(icel_z,N_CA_cells);
				
				indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
				ppr=hoc_CA[indice]-1;ii=hoc_CA[indice+1]-1;
				fprintf(out,"New list CA pr=%d i=%d\nppr=%d ii=%d\nicel_x=%d icel_y=%d icel_z=%d\n",pr,i,ppr,ii,icel_x,icel_y,icel_z);fflush(out);
				
				prot[pr].ammino[i].lbw_pr=ppr;
				prot[pr].ammino[i].lbw_i=ii;
				if((ppr!=-1)&&(ii!=-1)){
					prot[ppr].ammino[ii].lfw_pr=pr;
					prot[ppr].ammino[ii].lfw_i=i;
				}
				hoc_CA[indice]=pr+1;
				hoc_CA[indice+1]=i+1;
				prot[pr].ammino[i].indice=indice;
				break;
				/*ATOMI H*/
				case ATOM_H:
				/*icel_x=(prot[pr].ammino[i].x/cell_H_size);
				icel_y=(prot[pr].ammino[i].y/cell_H_size);
				icel_z=(prot[pr].ammino[i].z/cell_H_size);

				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);*/
				
				icel_x=rint(prot[pr].ammino[i].x/cell_H_size);
				icel_y=rint(prot[pr].ammino[i].y/cell_H_size);
				icel_z=rint(prot[pr].ammino[i].z/cell_H_size);

				icel_x=P_Cell(icel_x,N_H_cells);
				icel_y=P_Cell(icel_y,N_H_cells);
				icel_z=P_Cell(icel_z,N_H_cells);
				
				indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
				ppr=hoc_H[indice]-1;ii=hoc_H[indice+1]-1;
				fprintf(out,"New list H pr=%d i=%d\nppr=%d ii=%d\nicel_x=%d icel_y=%d icel_z=%d\n",pr,i,ppr,ii,icel_x,icel_y,icel_z);fflush(out);
				prot[pr].ammino[i].lbw_pr=ppr;
				prot[pr].ammino[i].lbw_i=ii;
				if((ppr!=-1)&&(ii!=-1)){
					prot[ppr].ammino[ii].lfw_pr=pr;
					prot[ppr].ammino[ii].lfw_i=i;
				}
				hoc_H[indice]=pr+1;
				hoc_H[indice+1]=i+1;
				prot[pr].ammino[i].indice=indice;
				break;
				
				
			}
		}	
		}
	}
	fprintf(out,"new_cell_list ok 5\n");fflush(out);
//	MPI_Abort(MPI_COMM_WORLD,err5);
	return;
}


void new_cell_list_SAW (void){
	

		
	unsigned long int indice;
	int icel_x,icel_y,icel_z;
	int ppr,ii,i,j,k,pr;
	
	
	
	if(hoc_SAW==NULL){
		hoc_SAW=(unsigned int*) calloc(2*N_SAW_cells*N_SAW_cells*N_SAW_cells,sizeof(unsigned int));
	}
	fprintf(out,"new_cell_list_SAW ok 1\n");fflush(out);
	for(indice=0;indice<2*N_SAW_cells*N_SAW_cells*N_SAW_cells;indice++){
		hoc_SAW[indice]=0;
	}
	fprintf(out,"new_cell_list_SAW ok 2\n");fflush(out);
	
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			prot[pr].ammino[i].lfw_SAW_pr=prot[pr].ammino[i].lfw_SAW_i=-1;
		}
	}
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			
			
			
			
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size);
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size);
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size);
			
			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
			ppr=hoc_SAW[indice]-1;ii=hoc_SAW[indice+1]-1;
			prot[pr].ammino[i].lbw_SAW_pr=ppr;
			prot[pr].ammino[i].lbw_SAW_i=ii;
			if((ppr!=-1)&&(ii!=-1)){
				prot[ppr].ammino[ii].lfw_SAW_pr=pr;
				prot[ppr].ammino[ii].lfw_SAW_i=i;
			}
			hoc_SAW[indice]=pr+1;
			hoc_SAW[indice+1]=i+1;
			prot[pr].ammino[i].indice_SAW=indice;	
		}
	}
	
	return;
	
}

void updt_cell_list (int pr, int i){
	

		
	unsigned long int indice,indice_old;
	int j,k;
	int icel_x,icel_y,icel_z;
	int ppr,ii;
	
		if(prot[pr].ammino[i].spring_anchor!=1){
		switch(prot[pr].ammino[i].id){
			
		/*ATOMI CA*/
			case ATOM_CA:
				
				
		//if(icycl>0){fprintf(out,"Update cell particella CA precedente\n");fflush(out);}
			
		/*Aggiorna la catena della particella precedente*/
		indice_old=prot[pr].ammino[i].indice;	
		ppr=prot[pr].ammino[i].lbw_pr; //pallina precedente
		ii=prot[pr].ammino[i].lbw_i;
		//if(icycl>0){fprintf(out,"Update cell particella CA precedente ppr=%d ii=%d\n",ppr,ii);fflush(out);}
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_pr=prot[pr].ammino[i].lfw_pr;
			prot[ppr].ammino[ii].lfw_i= prot[pr].ammino[i].lfw_i; // se la pallina non e' la prima
		}else{
			if((prot[pr].ammino[i].lfw_pr==-1)&&(prot[pr].ammino[i].lfw_i==-1)){
				hoc_CA[indice_old]=hoc_CA[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				ppr=prot[pr].ammino[i].lfw_pr; // se la pallina e' la prima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lfw_i;   // inizializza il link posteriore (lbw) della pallina successiva a -1
												// in modo che l'inzio della lista si aggiornato
				prot[ppr].ammino[ii].lbw_pr=-1;
				prot[ppr].ammino[ii].lbw_i=-1;
			}
		}
		//if(icycl>0){fprintf(out,"Update cell particella CA successiva\n");fflush(out);}
		ppr=prot[pr].ammino[i].lfw_pr; //pallina successiva
		ii=prot[pr].ammino[i].lfw_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lbw_pr=prot[pr].ammino[i].lbw_pr;
			prot[ppr].ammino[ii].lbw_i= prot[pr].ammino[i].lbw_i; // se la pallina  non e' l'ultima
		}else{
			if((prot[pr].ammino[i].lbw_pr==-1)&&(prot[pr].ammino[i].lbw_i==-1)){
				hoc_CA[indice_old]=hoc_CA[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				hoc_CA[indice_old]=prot[pr].ammino[i].lbw_pr+1;
				hoc_CA[indice_old+1]=prot[pr].ammino[i].lbw_i+1;

				ppr=prot[pr].ammino[i].lbw_pr; // se la pallina e' l'ultima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lbw_i;   // inizializza il link successivo (lfw) della pallina precedente a -1
											    // in modo che la lista sia aggiornata

				prot[ppr].ammino[ii].lfw_pr=-1;
				prot[ppr].ammino[ii].lfw_i=-1;
			}
		}
		
		// aggiungiamo la pallina lla nuova lista
		
		//if(icycl>0){fprintf(out,"Update cell particella CA NEW cel\n");fflush(out);}
		/*icel_x=(prot[pr].ammino[i].x/cell_CA_size);
		icel_y=(prot[pr].ammino[i].y/cell_CA_size);
		icel_z=(prot[pr].ammino[i].z/cell_CA_size);

		icel_x=P_Cell(icel_x,N_CA_cells);
		icel_y=P_Cell(icel_y,N_CA_cells);
		icel_z=P_Cell(icel_z,N_CA_cells);*/
		
		icel_x=rint(prot[pr].ammino[i].x/cell_CA_size);
		icel_y=rint(prot[pr].ammino[i].y/cell_CA_size);
		icel_z=rint(prot[pr].ammino[i].z/cell_CA_size);

		icel_x=P_Cell(icel_x,N_CA_cells);
		icel_y=P_Cell(icel_y,N_CA_cells);
		icel_z=P_Cell(icel_z,N_CA_cells);
		
		indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
		ppr=hoc_CA[indice]-1;ii=hoc_CA[indice+1]-1;
		prot[pr].ammino[i].lbw_pr=ppr;
		prot[pr].ammino[i].lbw_i=ii;
		prot[pr].ammino[i].lfw_pr=-1;
		prot[pr].ammino[i].lfw_i=-1;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_pr=pr;
			prot[ppr].ammino[ii].lfw_i=i;
		}
		hoc_CA[indice]=pr+1;
		hoc_CA[indice+1]=i+1;
		prot[pr].ammino[i].indice=indice;	
		//if(icycl>0){fprintf(out,"Update cell particella CA OK\n");fflush(out);}
		break;
		
		/*ATOMI H*/
		
		
		
		case ATOM_H:
		/*Aggiorna la catena della particella precedente*/
		//	if(icycl>0){fprintf(out,"Update cell particella H precedente\n");fflush(out);}
		indice_old=prot[pr].ammino[i].indice;	
		ppr=prot[pr].ammino[i].lbw_pr; //pallina precedente
		ii=prot[pr].ammino[i].lbw_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_pr=prot[pr].ammino[i].lfw_pr;
			prot[ppr].ammino[ii].lfw_i= prot[pr].ammino[i].lfw_i; // se la pallina non e' la prima
		}else{
			if((prot[pr].ammino[i].lfw_pr==-1)&&(prot[pr].ammino[i].lfw_i==-1)){
				hoc_H[indice_old]=hoc_H[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				ppr=prot[pr].ammino[i].lfw_pr; // se la pallina e' la prima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lfw_i;   // inizializza il link posteriore (lbw) della pallina successiva a -1
												// in modo che l'inzio della lista si aggiornato
				prot[ppr].ammino[ii].lbw_pr=-1;
				prot[ppr].ammino[ii].lbw_i=-1;
			}
		}
		//if(icycl>0){fprintf(out,"Update cell particella H successiva\n");fflush(out);}
		ppr=prot[pr].ammino[i].lfw_pr; //pallina successiva
		ii=prot[pr].ammino[i].lfw_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lbw_pr=prot[pr].ammino[i].lbw_pr;
			prot[ppr].ammino[ii].lbw_i= prot[pr].ammino[i].lbw_i; // se la pallina  non e' l'ultima
		}else{
			if((prot[pr].ammino[i].lbw_pr==-1)&&(prot[pr].ammino[i].lbw_i==-1)){
				hoc_H[indice_old]=hoc_H[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				hoc_H[indice_old]=prot[pr].ammino[i].lbw_pr+1;
				hoc_H[indice_old+1]=prot[pr].ammino[i].lbw_i+1;

				ppr=prot[pr].ammino[i].lbw_pr; // se la pallina e' l'ultima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lbw_i;   // inizializza il link successivo (lfw) della pallina precedente a -1
											    // in modo che la lista sia aggiornata

				prot[ppr].ammino[ii].lfw_pr=-1;
				prot[ppr].ammino[ii].lfw_i=-1;
			}
		}
		
		// aggiungiamo la pallina lla nuova lista
		
		//if(icycl>0){fprintf(out,"Update cell particella H new\n");fflush(out);}
		/*icel_x=(prot[pr].ammino[i].x/cell_H_size);
		icel_y=(prot[pr].ammino[i].y/cell_H_size);
		icel_z=(prot[pr].ammino[i].z/cell_H_size);

		icel_x=P_Cell(icel_x,N_H_cells);
		icel_y=P_Cell(icel_y,N_H_cells);
		icel_z=P_Cell(icel_z,N_H_cells);*/
		
		icel_x=rint(prot[pr].ammino[i].x/cell_H_size);
		icel_y=rint(prot[pr].ammino[i].y/cell_H_size);
		icel_z=rint(prot[pr].ammino[i].z/cell_H_size);

		icel_x=P_Cell(icel_x,N_H_cells);
		icel_y=P_Cell(icel_y,N_H_cells);
		icel_z=P_Cell(icel_z,N_H_cells);
		
		indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
		ppr=hoc_H[indice]-1;ii=hoc_H[indice+1]-1;
		prot[pr].ammino[i].lbw_pr=ppr;
		prot[pr].ammino[i].lbw_i=ii;
		prot[pr].ammino[i].lfw_pr=-1;
		prot[pr].ammino[i].lfw_i=-1;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_pr=pr;
			prot[ppr].ammino[ii].lfw_i=i;
		}
		hoc_H[indice]=pr+1;
		hoc_H[indice+1]=i+1;
		prot[pr].ammino[i].indice=indice;	
		//if(icycl>0){fprintf(out,"Update cell particella H Ok\n");fflush(out);}
	}
		
		
}
		return;
	
}



void updt_cell_list_SAW (int pr, int i){
	

		
	unsigned long int indice,indice_old;
	int j,k;
	int icel_x,icel_y,icel_z;
	int ppr,ii;
	
		
	if(prot[pr].ammino[i].id==ATOM_CA){
	
		//if(icycl>0){fprintf(out,"Update cell particella precedente\n");fflush(out);}
	
		/*Aggiorna la catena della particella precedente*/
		indice_old=prot[pr].ammino[i].indice_SAW;
		ppr=prot[pr].ammino[i].lbw_SAW_pr; //pallina precedente
		ii=prot[pr].ammino[i].lbw_SAW_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_SAW_pr=prot[pr].ammino[i].lfw_SAW_pr;
			prot[ppr].ammino[ii].lfw_SAW_i= prot[pr].ammino[i].lfw_SAW_i; // se la pallina non e' la prima
		}else{
			if((prot[pr].ammino[i].lfw_SAW_pr==-1)&&(prot[pr].ammino[i].lfw_SAW_i==-1)){
				hoc_SAW[indice_old]=hoc_SAW[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				ppr=prot[pr].ammino[i].lfw_SAW_pr; // se la pallina e' la prima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lfw_SAW_i;   // inizializza il link posteriore (lbw) della pallina successiva a -1
												// in modo che l'inzio della lista si aggiornato
				prot[ppr].ammino[ii].lbw_SAW_pr=-1;
				prot[ppr].ammino[ii].lbw_SAW_i=-1;
			}
		}
		//if(icycl>0){fprintf(out,"Update cell particella precedente OK \n");fflush(out);}
		//if(icycl>0){fprintf(out,"Update cell particella successiva\n");fflush(out);}
		ppr=prot[pr].ammino[i].lfw_SAW_pr; //pallina successiva
		ii=prot[pr].ammino[i].lfw_SAW_i;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lbw_SAW_pr=prot[pr].ammino[i].lbw_SAW_pr;
			prot[ppr].ammino[ii].lbw_SAW_i= prot[pr].ammino[i].lbw_SAW_i; // se la pallina  non e' l'ultima
		}else{
			if((prot[pr].ammino[i].lbw_SAW_pr==-1)&&(prot[pr].ammino[i].lbw_SAW_i==-1)){
				hoc_SAW[indice_old]=hoc_SAW[indice_old+1]=0; // se la pallina e' l'unica nella cella
			}else{
				hoc_SAW[indice_old]=prot[pr].ammino[i].lbw_SAW_pr+1;
				hoc_SAW[indice_old+1]=prot[pr].ammino[i].lbw_SAW_i+1;

				ppr=prot[pr].ammino[i].lbw_SAW_pr; // se la pallina e' l'ultima nella lista nella cella, ma non l'unica, allora 
				ii=prot[pr].ammino[i].lbw_SAW_i;   // inizializza il link successivo (lfw) della pallina precedente a -1
											    // in modo che la lista sia aggiornata

				prot[ppr].ammino[ii].lfw_SAW_pr=-1;
				prot[ppr].ammino[ii].lfw_SAW_i=-1;
			}
		}
		//if(icycl>0){fprintf(out,"Update cell particella successiva OK\n");fflush(out);}
		// aggiungiamo la pallina lla nuova lista
		//if(icycl>0){fprintf(out,"Update cell New cell\n");fflush(out);}
		icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size);
		icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size);
		icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size);

		icel_x=P_Cell(icel_x,N_SAW_cells);
		icel_y=P_Cell(icel_y,N_SAW_cells);
		icel_z=P_Cell(icel_z,N_SAW_cells);
		indice=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
		ppr=hoc_SAW[indice]-1;ii=hoc_SAW[indice+1]-1;
		//if(icycl>0){fprintf(out,"Update cell New cell OK1\n");fflush(out);}
		prot[pr].ammino[i].lbw_SAW_pr=ppr;
		prot[pr].ammino[i].lbw_SAW_i=ii;
		prot[pr].ammino[i].lfw_SAW_pr=-1;
		prot[pr].ammino[i].lfw_SAW_i=-1;
		if((ppr!=-1)&&(ii!=-1)){
			prot[ppr].ammino[ii].lfw_SAW_pr=pr;
			prot[ppr].ammino[ii].lfw_SAW_i=i;
		}
		hoc_SAW[indice]=pr+1;
		hoc_SAW[indice+1]=i+1;
		prot[pr].ammino[i].indice_SAW=indice;	
		//if(icycl>0){fprintf(out,"Update cell New cell OK2\n");fflush(out);}
	}
		return;
	
}





void test_celllist_H (void){
	
	
	unsigned long int indice,indice_old,indice2;
	  int j,k,pprt,jjj;
	 int icel_x,icel_y,icel_z;
	 int ppr,ii;
	int test,pr,prt,i,jj,test5;
	double dx,dy,dz,rCA2;
	
	// TEST_H 1 connected chains
	//fprintf(out,"TEST_H 1\n");fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_H;i<Plength[pr];i++){
			if(prot[pr].ammino[i].spring_anchor!=1){
			if(prot[pr].ammino[i].id==ATOM_H){
			//fprintf(out,"TEST_H 1  pr=%d i=%d\n",pr,i);fflush(out);
			ppr=prot[pr].ammino[i].lbw_pr;
			ii=prot[pr].ammino[i].lbw_i;
			//fprintf(out,"TEST_H 1  ppr=%d ii=%d\n",ppr,ii);fflush(out);
			if((ppr!=-1)&&(ii!=-1)){
				if((prot[ppr].ammino[ii].lfw_pr!=pr)||(prot[ppr].ammino[ii].lfw_i!=i)){
					fprintf(out,"TEST_H 1 celllist SAW failed pr=%d i=%d ppr=%d ii=%d\n prot[ppr].ammino[ii].lfw_pr=%d prot[ppr].ammino[ii].lfw_i=%d\n",pr,i,ppr,ii,prot[ppr].ammino[ii].lfw_pr,prot[ppr].ammino[ii].lfw_i);fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			
		}
	}
}
	}
		
}
//fprintf(out,"TEST_H 2 and 3\n");fflush(out);
	//TEST_H 2 all particles are in a cell
	//TEST_H 3 all particles are in the cocrrect cell
	for(indice=0;indice<N_H_cells*N_H_cells*N_H_cells;indice++){
		
			pr=hoc_H[2*indice]-1;
			i=hoc_H[2*indice+1]-1;
			
			//fprintf(out,"TEST_H 2  pr=%d i=%d\n",pr,i);fflush(out);
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(i>=0){
				
			if(prot[pr].ammino[i].indice!=2*indice){
				fprintf(out,"TEST_H 3a failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",pr,i,prot[pr].ammino[i].indice,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
						
			/*icel_x=(prot[pr].ammino[i].x/cell_H_size);
			icel_y=(prot[pr].ammino[i].y/cell_H_size);
			icel_z=(prot[pr].ammino[i].z/cell_H_size);

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_H_size);
			icel_y=rint(prot[pr].ammino[i].y/cell_H_size);
			icel_z=rint(prot[pr].ammino[i].z/cell_H_size);

			icel_x=P_Cell(icel_x,N_H_cells);
			icel_y=P_Cell(icel_y,N_H_cells);
			icel_z=P_Cell(icel_z,N_H_cells);
			
			indice2=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
						
			if(indice2!=2*indice){
				fprintf(out,"TEST_H 3b failed misplaced particle pr=%d i=%d\n indice2=%lu indice=%lu\n",pr,i,2*indice2,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}		
				
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	//fprintf(out,"TEST_H 4\n");fflush(out);
	// TEST_H4 no duplicated particles
	for(indice=0;indice<N_H_cells*N_H_cells*N_H_cells;indice++){
		
			pr=hoc_H[2*indice]-1;
			i=hoc_H[2*indice+1]-1;
			
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(i>=0){
				test=0;
				for(indice2=0;indice2<N_H_cells*N_H_cells*N_H_cells;indice2++){
					prt=hoc_H[2*indice2]-1;
					j=hoc_H[2*indice2+1]-1;
				while(j>=0){

					if((prt==pr)&&(i==j)) {
						test++;
						if(test>1) fprintf(out,"TEST_H 4 failed particle pr=%d i=%d duplicated %d times prt=%d j=%d\n",pr,i,test,prt,j);
					}



					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					/*if(rCA2<sist->Rint2){
						prot[pr].ammino[i].verlpr[Nver]=prt;
						prot[pr].ammino[i].verli[Nver]=j;
						prot[pr].ammino[i].Nverl=Nver;
						Nver++;
					}*/
				}
			}	
			if(test>1) {
				fprintf(out,"TEST_H 4 failed particle pr=%d i=%d duplicated %d times\n",pr,i,test);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}

	// TEST_H5 no duplicated particles
	for(pr=0;pr<NPROT;pr++){
		for (i=0;i<Plength[pr];i++){
			if((prot[pr].ammino[i].id==ATOM_H)&&(prot[pr].ammino[i].spring_anchor!=1)){
			for(prt=0;prt<NPROT;prt++){
				for (j=0;j<Plength[prt];j++){
					if((prot[prt].ammino[j].id==ATOM_H)&&(prot[prt].ammino[j].spring_anchor!=1)){
						
							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

							rCA2=(dx*dx+ dy*dy + dz*dz);

							if (rCA2<sist->RintH2){
								
								test5=0;		
								for(indice2=0;indice2<Neigh;indice2++){
							//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
									/*icel_x=(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
									icel_y=(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
									icel_z=(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

									//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);

									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);*/
									
									icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
									icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
									icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];

									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);
									
									if((icel_x>=N_H_cells)||(icel_y>=N_H_cells)||(icel_z>=N_H_cells)){
										fprintf(out,"TEST_H 5 icel too big\n");
										fprintf(out,"TEST_H 5 N_H_cells=%d x=%lf y=%lf z=%lf\n",N_H_cells,(prot[pr].ammino[i].x),(prot[pr].ammino[i].y),(prot[pr].ammino[i].z));fflush(out);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										icel_x=rint(prot[pr].ammino[i].x/cell_H_size);
										icel_y=rint(prot[pr].ammino[i].y/cell_H_size);
										icel_z=rint(prot[pr].ammino[i].z/cell_H_size);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										
										icel_x=P_Cell(icel_x,N_H_cells);
										icel_y=P_Cell(icel_y,N_H_cells);
										icel_z=P_Cell(icel_z,N_H_cells);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										icel_x=rint(prot[pr].ammino[i].x/cell_H_size)+neighcells[indice2*3];
										icel_y=rint(prot[pr].ammino[i].y/cell_H_size)+neighcells[indice2*3+1];
										icel_z=rint(prot[pr].ammino[i].z/cell_H_size)+neighcells[indice2*3+2];
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										icel_x=P_Cell(icel_x,N_H_cells);
										icel_y=P_Cell(icel_y,N_H_cells);
										icel_z=P_Cell(icel_z,N_H_cells);
										fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
										MPI_Abort(MPI_COMM_WORLD,err5);
										
									}
									//fprintf(out,"TEST_H 5 N_H_cells=%d icel_x=%d icel_y=%d icel_z=%d\n",N_H_cells,icel_x,icel_y,icel_z);fflush(out);
									indice=icel_x*2*N_H_cells*N_H_cells+icel_y*2*N_H_cells+icel_z*2;
									//if(indice==prot[prt].ammino[j].indice) test5++;
									pprt=hoc_H[indice]-1;
									jj=hoc_H[indice+1]-1;
									//if(indice==prot[prt].ammino[j].indice) test5++;
									while(jj>=0){
											if((jj==j)&&(pprt==prt))    test5++;
											//fprintf(out,"--->pprt=%d jj=%d\n",pprt,jj);fflush(out);
											jjj=prot[pprt].ammino[jj].lbw_i;
											pprt=prot[pprt].ammino[jj].lbw_pr;
											jj=jjj;
									}
								}
								if(test5!=1){
									fprintf(out,"TEST_H 5 failed particle prt=%d j=%d wrong neigh_cell of pr=%d i=%d\n  test5=%d Rinit=%lf N_H_cells=%d cell_H_size=%lf cell_H_factor=%lf\n",prt,j,pr,i,test5,sist->RintH,N_H_cells,cell_H_size,cell_H_factor);
									icel_x=rint(prot[pr].ammino[i].x/cell_H_size);
									icel_y=rint(prot[pr].ammino[i].y/cell_H_size);
									icel_z=rint(prot[pr].ammino[i].z/cell_H_size);
									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);
									fprintf(out,"I icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
									fprintf(out,"I %lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									icel_x=rint(prot[prt].ammino[j].x/cell_H_size);
									icel_y=rint(prot[prt].ammino[j].y/cell_H_size);
									icel_z=rint(prot[prt].ammino[j].z/cell_H_size);
									icel_x=P_Cell(icel_x,N_H_cells);
									icel_y=P_Cell(icel_y,N_H_cells);
									icel_z=P_Cell(icel_z,N_H_cells);
									fprintf(out,"J icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
									fprintf(out,"J %lf %lf %lf\n",prot[pr].ammino[j].x,prot[pr].ammino[j].y,prot[pr].ammino[j].z);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
							}
					
				}
			}
			}
		}
		}
	}
		
		return;
			
}
void test_celllist_CA(void){
	
	
	unsigned long int indice,indice_old,indice2;
	 int j,k,pprt,jjj;
	 int icel_x,icel_y,icel_z;
	 int ppr,ii;
	int test,pr,prt,i,jj;
	int test5;
	double dx,dy,dz,rCA2;
	
	// TEST_CA 1 connected chains
	//fprintf(out,"TEST_CA 1\n");fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			//fprintf(out,"TEST_CA 1  pr=%d i=%d\n",pr,i);fflush(out);
			ppr=prot[pr].ammino[i].lbw_pr;
			ii=prot[pr].ammino[i].lbw_i;
			//fprintf(out,"TEST_CA 1  ppr=%d ii=%d\n",ppr,ii);fflush(out);
			if((ppr!=-1)&&(ii!=-1)){
				if((prot[ppr].ammino[ii].lfw_pr!=pr)||(prot[ppr].ammino[ii].lfw_i!=i)){
					fprintf(out,"TEST_CA 1 celllist SAW failed pr=%d i=%d ppr=%d ii=%d\n prot[ppr].ammino[ii].lfw_pr=%d prot[ppr].ammino[ii].lfw_i=%d\n",pr,i,ppr,ii,prot[ppr].ammino[ii].lfw_pr,prot[ppr].ammino[ii].lfw_i);fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			
		}
	}
		
}
//fprintf(out,"TEST_CA 2 and 3\n");fflush(out);
	//TEST_CA 2 all particles are in a cell
	//TEST_CA 3 all particles are in the cocrrect cell
	for(indice=0;indice<N_CA_cells*N_CA_cells*N_CA_cells;indice++){
		
			pr=hoc_CA[2*indice]-1;
			i=hoc_CA[2*indice+1]-1;
			
			//fprintf(out,"TEST_CA 2  pr=%d i=%d\n",pr,i);fflush(out);
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(i>=0){
				
			if(prot[pr].ammino[i].indice!=2*indice){
				fprintf(out,"TEST_CA 3a failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",pr,i,prot[pr].ammino[i].indice,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
						
			/*icel_x=(prot[pr].ammino[i].x/cell_CA_size);
			icel_y=(prot[pr].ammino[i].y/cell_CA_size);
			icel_z=(prot[pr].ammino[i].z/cell_CA_size);

			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);*/
			
			icel_x=rint(prot[pr].ammino[i].x/cell_CA_size);
			icel_y=rint(prot[pr].ammino[i].y/cell_CA_size);
			icel_z=rint(prot[pr].ammino[i].z/cell_CA_size);

			icel_x=P_Cell(icel_x,N_CA_cells);
			icel_y=P_Cell(icel_y,N_CA_cells);
			icel_z=P_Cell(icel_z,N_CA_cells);
			
			indice2=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
						
			if(indice2!=2*indice){
				fprintf(out,"TEST_CA 3b failed misplaced particle pr=%d i=%d\n indice2=%lu indice=%lu\n",pr,i,2*indice2,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}		
				
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	//fprintf(out,"TEST_CA 4\n");fflush(out);
	// TEST_CA4 no duplicated particles
	for(indice=0;indice<N_CA_cells*N_CA_cells*N_CA_cells;indice++){
		
			pr=hoc_CA[2*indice]-1;
			i=hoc_CA[2*indice+1]-1;
			
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(i>=0){
				test=0;
				for(indice2=0;indice2<N_CA_cells*N_CA_cells*N_CA_cells;indice2++){
					prt=hoc_CA[2*indice2]-1;
					j=hoc_CA[2*indice2+1]-1;
				while(j>=0){

					if((prt==pr)&&(i==j)) {
						test++;
						if(test>1) fprintf(out,"TEST_CA 4 failed particle pr=%d i=%d duplicated %d times prt=%d j=%d\n",pr,i,test,prt,j);
					}



					jj=prot[prt].ammino[j].lbw_i;
					prt=prot[prt].ammino[j].lbw_pr;
					j=jj;
					/*if(rCA2<sist->Rint2){
						prot[pr].ammino[i].verlpr[Nver]=prt;
						prot[pr].ammino[i].verli[Nver]=j;
						prot[pr].ammino[i].Nverl=Nver;
						Nver++;
					}*/
				}
			}	
			if(test>1) {
				fprintf(out,"TEST_CA 4 failed particle pr=%d i=%d duplicated %d times\n",pr,i,test);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			jj=prot[pr].ammino[i].lbw_i;
			pr=prot[pr].ammino[i].lbw_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	// TEST_CA5 correct neigh
	for(pr=0;pr<NPROT;pr++){
		for (i=ATOM_CA;i<Plength[pr];i+=NATOM){
			for(prt=0;prt<NPROT;prt++){
				for (j=ATOM_CA;j<Plength[prt];j+=NATOM){
					
							//fprintf(out,"TEST_CA 5 CA pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);fflush(out);
							
							dx=prot[pr].ammino[i].x-prot[prt].ammino[j].x;
							dy=prot[pr].ammino[i].y-prot[prt].ammino[j].y;
							dz=prot[pr].ammino[i].z-prot[prt].ammino[j].z;

							dx=P_Dist(dx);dy=P_Dist(dy);dz=P_Dist(dz);

							rCA2=(dx*dx+ dy*dy + dz*dz);

							if (rCA2<sist->Rint2){
								
								test5=0;	
								//fprintf(out,"TEST_CA 5 CA pr=%d i=%d prt=%d j=%d\n",pr,i,prt,j);fflush(out);	
								for(indice2=0;indice2<Neigh;indice2++){
							//fprintf(out,"Energ FW CA indice2=%d neighx=%d neighy=%d neighz=%d\n",indice2,neighcells[indice2*3],neighcells[indice2*3+1],neighcells[indice2*3+2]);fflush(out);
									/*icel_x=(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
									icel_y=(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
									icel_z=(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

									//fprintf(out,"Energ FW CA indice2=%d icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);

									icel_x=P_Cell(icel_x,N_CA_cells);
									icel_y=P_Cell(icel_y,N_CA_cells);
									icel_z=P_Cell(icel_z,N_CA_cells);*/
									
									icel_x=rint(prot[pr].ammino[i].x/cell_CA_size)+neighcells[indice2*3];
									icel_y=rint(prot[pr].ammino[i].y/cell_CA_size)+neighcells[indice2*3+1];
									icel_z=rint(prot[pr].ammino[i].z/cell_CA_size)+neighcells[indice2*3+2];

									icel_x=P_Cell(icel_x,N_CA_cells);
									icel_y=P_Cell(icel_y,N_CA_cells);
									icel_z=P_Cell(icel_z,N_CA_cells);

									//fprintf(out,"TEST_CA 5 CA indice2=%lu icel_x=%d icel_y=%d icel_z=%d\n",indice2,icel_x,icel_y,icel_z);fflush(out);
									indice=icel_x*2*N_CA_cells*N_CA_cells+icel_y*2*N_CA_cells+icel_z*2;
									//if(indice==prot[prt].ammino[j].indice) test5++;
									pprt=hoc_CA[indice]-1;
									jj=hoc_CA[indice+1]-1;
									
									
									
										
								
									while(jj>=0){
											if((jj==j)&&(pprt==prt))  {
												test5++;
												//fprintf(out,"--->pprt=%d jj=%d j=%d\n",pprt,jj,j);fflush(out);
											}
										//	fprintf(out,"TEST_CA 5 CA \t\t\t pprt=%d jj=%d\n",pprt,jj);fflush(out);
											jjj=prot[pprt].ammino[jj].lbw_i;
											pprt=prot[pprt].ammino[jj].lbw_pr;
											jj=jjj;
									}
								}
								if(test5!=1){
									fprintf(out,"TEST_CA 5 test5=%d failed particle prt=%d j=%d wrong neigh_cell of pr=%d i=%d\n  test5=%d Rinit=%lf %lf\n",test5,prt,j,pr,i,test5,sist->Rint,cell_CA_size);
									icel_x=rint(prot[pr].ammino[i].x/cell_CA_size);
									icel_y=rint(prot[pr].ammino[i].y/cell_CA_size);
									icel_z=rint(prot[pr].ammino[i].z/cell_CA_size);
									icel_x=P_Cell(icel_x,N_CA_cells);
									icel_y=P_Cell(icel_y,N_CA_cells);
									icel_z=P_Cell(icel_z,N_CA_cells);
									fprintf(out,"I icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
									fprintf(out,"I %lf %lf %lf\n",prot[pr].ammino[i].x,prot[pr].ammino[i].y,prot[pr].ammino[i].z);
									icel_x=rint(prot[prt].ammino[j].x/cell_CA_size);
									icel_y=rint(prot[prt].ammino[j].y/cell_CA_size);
									icel_z=rint(prot[prt].ammino[j].z/cell_CA_size);
									icel_x=P_Cell(icel_x,N_CA_cells);
									icel_y=P_Cell(icel_y,N_CA_cells);
									icel_z=P_Cell(icel_z,N_CA_cells);
									fprintf(out,"J icel_x=%u icel_y=%u icel_z=%u\n",icel_x,icel_y,icel_z);
									fprintf(out,"J %lf %lf %lf\n",prot[pr].ammino[j].x,prot[pr].ammino[j].y,prot[pr].ammino[j].z);
									MPI_Abort(MPI_COMM_WORLD,err5);
								}
							}
						
					
				}
			}
		}
	}
											
											
											
	
	
		
		return;
			
}
void test_celllist_SAW(void){
	
	
	unsigned long int indice,indice_old,indice2;
	 int j,k;
	 int icel_x,icel_y,icel_z;
	 int ppr,ii;
	int test,pr,prt,i,jj;
	
	// TEST 1 connected chains
	//fprintf(out,"TEST 1\n");fflush(out);
	for(pr=0;pr<NPROT;pr++){
		for(i=ATOM_CA;i<Plength[pr];i+=NATOM){
			//fprintf(out,"TEST 1  pr=%d i=%d\n",pr,i);fflush(out);
			ppr=prot[pr].ammino[i].lbw_SAW_pr;
			ii=prot[pr].ammino[i].lbw_SAW_i;
			//fprintf(out,"TEST 1  ppr=%d ii=%d\n",ppr,ii);fflush(out);
			if((ppr!=-1)&&(ii!=-1)){
				if((prot[ppr].ammino[ii].lfw_SAW_pr!=pr)||(prot[ppr].ammino[ii].lfw_SAW_i!=i)){
					fprintf(out,"TEST 1 celllist SAW failed pr=%d i=%d ppr=%d ii=%d\n prot[ppr].ammino[ii].lfw_SAW_pr=%d prot[ppr].ammino[ii].lfw_SAW_i=%d\n",pr,i,ppr,ii,prot[ppr].ammino[ii].lfw_SAW_pr,prot[ppr].ammino[ii].lfw_SAW_i);fflush(out);
					MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			
		}
	}
		
}
//fprintf(out,"TEST 2 and 3\n");fflush(out);
	//TEST 2 all particles are in a cell
	//TEST 3 all particles are in the cocrrect cell
	for(indice=0;indice<N_SAW_cells*N_SAW_cells*N_SAW_cells;indice++){
		
			pr=hoc_SAW[2*indice]-1;
			i=hoc_SAW[2*indice+1]-1;
			
			//fprintf(out,"TEST 2  pr=%d i=%d\n",pr,i);fflush(out);
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(i>=0){
				
			if(prot[pr].ammino[i].indice_SAW!=2*indice){
				fprintf(out,"TEST 3a failed misplaced particle pr=%d i=%d\n prot[pr].ammino[i].indice=%lu indice=%lu\n",pr,i,prot[pr].ammino[i].indice_SAW,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
						
			icel_x=rint(prot[pr].ammino[i].x/cell_SAW_size);
			icel_y=rint(prot[pr].ammino[i].y/cell_SAW_size);
			icel_z=rint(prot[pr].ammino[i].z/cell_SAW_size);

			icel_x=P_Cell(icel_x,N_SAW_cells);
			icel_y=P_Cell(icel_y,N_SAW_cells);
			icel_z=P_Cell(icel_z,N_SAW_cells);
			
			indice2=icel_x*2*N_SAW_cells*N_SAW_cells+icel_y*2*N_SAW_cells+icel_z*2;
						
			if(indice2!=2*indice){
				fprintf(out,"TEST 3b failed misplaced particle pr=%d i=%d\n indice2=%lu indice=%lu\n",pr,i,2*indice2,2*indice);fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}		
				
			
			jj=prot[pr].ammino[i].lbw_SAW_i;
			pr=prot[pr].ammino[i].lbw_SAW_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	//fprintf(out,"TEST 4\n");fflush(out);
	// TEST4 no duplicated particles
	for(indice=0;indice<N_SAW_cells*N_SAW_cells*N_SAW_cells;indice++){
		
			pr=hoc_SAW[2*indice]-1;
			i=hoc_SAW[2*indice+1]-1;
			
			
			
		//if(icycl > 0) fprintf(out,"##%d###%d %d\n",k,prot[pr].ammino[i].verlpr[k],prot[pr].ammino[i].verli[k]);
		
		//if(icycl > 0) fprintf(out,"#%d %d %d %d\n",pr,i,prt,j);fflush(out);
			while(i>=0){
				test=0;
				for(indice2=0;indice2<N_SAW_cells*N_SAW_cells*N_SAW_cells;indice2++){
					prt=hoc_SAW[2*indice2]-1;
					j=hoc_SAW[2*indice2+1]-1;
				while(j>=0){

					if((prt==pr)&&(i==j)) {
						test++;
						if(test>1) fprintf(out,"TEST 4 failed particle pr=%d i=%d duplicated %d times prt=%d j=%d\n",pr,i,test,prt,j);
					}



					jj=prot[prt].ammino[j].lbw_SAW_i;
					prt=prot[prt].ammino[j].lbw_SAW_pr;
					j=jj;
					/*if(rCA2<sist->Rint2){
						prot[pr].ammino[i].verlpr[Nver]=prt;
						prot[pr].ammino[i].verli[Nver]=j;
						prot[pr].ammino[i].Nverl=Nver;
						Nver++;
					}*/
				}
			}	
			if(test>1) {
				fprintf(out,"TEST 4 failed particle pr=%d i=%d duplicated %d times\n",pr,i,test);
				fflush(out);
				MPI_Abort(MPI_COMM_WORLD,err5);
			}
			
			jj=prot[pr].ammino[i].lbw_SAW_i;
			pr=prot[pr].ammino[i].lbw_SAW_pr;
			i=jj;
			/*if(rCA2<sist->Rint2){
				prot[pr].ammino[i].verlpr[Nver]=prt;
				prot[pr].ammino[i].verli[Nver]=j;
				prot[pr].ammino[i].Nverl=Nver;
				Nver++;
			}*/
		}
	}
	
	
		
		return;
			
}
int  P_Cell (int x, int N_cels){
	 int x1;
	x1=x;
	if (x>=N_cels) x1=x-rint(x/N_cels)*N_cels;

	if (x<0)	x1=x+(1-rint((x+1)/N_cels))*N_cels;  
		
	return (x1);
}




double  P_Cd (double x){
	 double x1;
	x1=x;
	if (x>=BoxSize) x1=x-(int)(x/BoxSize)*BoxSize;

	if (x<0)	x1=x+(1-(int)(x/BoxSize))*BoxSize;  

	return (x1);
}
