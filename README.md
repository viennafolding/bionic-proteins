# README #


### What is this repository for? ###

This repository contains the programs used for the SEEK DESIGN and FOLDING simualtions in

1. Coluzza, I., Van Oostrum, P. D. J., Capone, B., Reimhult, E. & Dellago, C. Sequence controlled self-knotting colloidal patchy polymers. Phys. Rev. Lett. 110, 75501 (2013).
2. Coluzza, I. & Dellago, C. The configurational space of colloidal patchy polymers with heterogeneous sequences. J. Phys. Condens. Matter 24, 284111 (2012).
3. Coluzza, I., van Oostrum, P. D. J., Capone, B., Reimhult, E. & Dellago, C. Design and folding of colloidal patchy polymers. Soft Matter DOI, 10.1039/c2sm26967h (2012).

### How do I get set up? ###
SOURCE:

All the source codes and makefiles are in the Source folder

EXECUTION:

SEEK searches for designable targets
DESIGN optimizes the sequence of the structure created by SEEK
FOLDING measure the configurationa free energy of the designed sequences

Simulations can be executed in the folders:

Seek/Simul_patches_3_Alphabet20/L-50-E-0.0/P-1/

Design/O-0650.0-C-67.0_08-07-2015_153300/P-1_T-20.0/

Folding/Simul_patches_3_Alphabet20/P-1/



### Who do I talk to? ###

Contact: ivan.coluzza@univie.ac.at
